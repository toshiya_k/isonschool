[key]Greek
[lang:el]Greek
[lang:en]Greek
[lang:ja]ギリシャ語
[lang:af]Griekse
[lang:az]Yunan
[lang:id]Yunani
[lang:ms]Yunani
[lang:bs]Grčki
[lang:ca]Grec
[lang:cs]řecký
[lang:da]græsk
[lang:de]griechisch
[lang:et]Kreeka keel
[lang:es]griego
[lang:eu]Greziako
[lang:fil]Greek
[lang:fr]grec
[lang:gl]Grego
[lang:hr]grčki
[lang:zu]IsiGrikhi
[lang:is]Gríska
[lang:it]greco
[lang:sw]Mgiriki
[lang:lv]Grieķu
[lang:lt]Graikų kalba
[lang:hu]görög
[lang:nl]Grieks
[lang:no]gresk
[lang:uz]Yunon
[lang:pl]grecki
[lang:pt]grego
[lang:ro]greacă
[lang:sq]grek
[lang:sk]grécky
[lang:sl]Grško
[lang:sr]Грчки
[lang:fi]kreikkalainen
[lang:sv]grekisk
[lang:vi]người Hy Lạp
[lang:tr]Yunan
[lang:be]Грэчаская
[lang:bg]Гръцки
[lang:ky]Грекче
[lang:kk]Грек
[lang:mk]Грчки
[lang:mn]Грек хэл
[lang:ru]греческий
[lang:sr]Грчки
[lang:uk]Грецька
[lang:hy]Հունական
[lang:iw]יווני
[lang:ur]یونانی
[lang:ar]اليونانية
[lang:fa]یونانی
[lang:ne]ग्रीक
[lang:mr]ग्रीक
[lang:hi]यूनानी
[lang:bn]গ্রিক
[lang:pa]ਯੂਨਾਨੀ
[lang:gu]ગ્રીક
[lang:ta]கிரேக்கம்
[lang:te]గ్రీకు
[lang:kn]ಗ್ರೀಕ್
[lang:ml]ഗ്രീക്ക്
[lang:si]ග්‍රීක
[lang:th]กรีก
[lang:lo]ກເຣັກ
[lang:my]ဂရိ
[lang:ka]ბერძნული
[lang:am]ግሪክኛ
[lang:km]ក្រិក
[lang:zh-Hans]希腊语
[lang:zh-Hant]希臘語
[lang:ko]그리스 어
[lang:co]Grecu
[lang:fy]Gryksk
[lang:ha]Girkanci
[lang:ig]Grik
[lang:jv]Yunani
[lang:ku]Yewnanî
[lang:lb]Griichesch
[lang:mg]grika
[lang:mt]Grieg
[lang:mi]Kariki
[lang:ps]یوناني
[lang:sm]Greek
[lang:gd]Grèigeach
[lang:st]Ka Segerike
[lang:sn]ChiGiriki
[lang:sd]يوناني
[lang:so]Giriig
[lang:su]Yunani
[lang:tg]Юнонӣ
[lang:cy]Groeg
[lang:xh]IsiGrike
[lang:yi]גריכיש
[lang:yo]Greek
---

[key]Speak Greek!
[lang:el]Speak Greek!
[lang:en]Speak Greek!
[lang:ja]ギリシャ語を話そう！
[lang:af]Praat Grieks!
[lang:az]Yunan dilində danışın!
[lang:id]Berbahasa Yunani!
[lang:ms]Bercakap Bahasa Yunani!
[lang:bs]Govorite grčki!
[lang:ca]Parla grec!
[lang:cs]Mluvte řecky!
[lang:da]Tal græsk!
[lang:de]Sprich Griechisch!
[lang:et]Räägi kreeka keelt!
[lang:es]Habla griego
[lang:eu]Hitz grekoa!
[lang:fil]Magsalita ng Greek!
[lang:fr]Parlez grec!
[lang:gl]Fala grego!
[lang:hr]Govori grčki!
[lang:zu]Khuluma isiGrikhi!
[lang:is]Tala grísku!
[lang:it]Parla greco!
[lang:sw]Ongea Kiyunani!
[lang:lv]Runā grieķu valodā!
[lang:lt]Kalbėk graikiškai!
[lang:hu]Beszélj görögül!
[lang:nl]Spreek Grieks!
[lang:no]Snakk gresk!
[lang:uz]Yunoncha gapiring!
[lang:pl]Mów po grecku!
[lang:pt]Fala grego!
[lang:ro]Vorbeste greaca!
[lang:sq]Flisni greqisht!
[lang:sk]Hovorte po grécky!
[lang:sl]Govori grško!
[lang:sr]Говорите грчки!
[lang:fi]Puhu kreikkaa!
[lang:sv]Prata grekiska!
[lang:vi]Nói tiếng Hy Lạp!
[lang:tr]Yunanca konuş!
[lang:be]Размаўляйце па-грэцку!
[lang:bg]Говори гръцки!
[lang:ky]Сүйлөгүлө грек!
[lang:kk]Грекше сөйлеңіз!
[lang:mk]Зборувај грчки!
[lang:mn]Грекээр ярь!
[lang:ru]Говори по-гречески!
[lang:sr]Говорите грчки!
[lang:uk]Розмовляйте по-грецьки!
[lang:hy]Խոսեք հունարեն:
[lang:iw]דבר יוונית!
[lang:ur]یونانی بولیں!
[lang:ar]تكلم اليونانية!
[lang:fa]صحبت یونانی!
[lang:ne]ग्रीक बोल्नुहोस्!
[lang:mr]ग्रीक बोला!
[lang:hi]बोलो ग्रीक!
[lang:bn]গ্রীক কথা বলুন!
[lang:pa]ਯੂਨਾਨੀ ਬੋਲੋ!
[lang:gu]ગ્રીક બોલો!
[lang:ta]கிரேக்கம் பேசுங்கள்!
[lang:te]గ్రీకు మాట్లాడండి!
[lang:kn]ಗ್ರೀಕ್ ಮಾತನಾಡಿ!
[lang:ml]ഗ്രീക്ക് സംസാരിക്കുക!
[lang:si]ග්‍රීක කතා කරන්න!
[lang:th]พูดภาษากรีก!
[lang:lo]ເວົ້າພາສາກະເຣັກ!
[lang:my]ဂရိစကားပြောပါ
[lang:ka]ილაპარაკე ბერძნულად!
[lang:am]ግሪክኛ ይናገሩ!
[lang:km]និយាយភាសាក្រិក!
[lang:zh-Hans]说希腊语！
[lang:zh-Hant]說希臘語！
[lang:ko]그리스어로 이야기하십시오!
[lang:co]Parlate u Grecu!
[lang:fy]Sprek Gryksk!
[lang:ha]Yi magana Girkanci!
[lang:ig]Kwu Grik!
[lang:jv]Ngomong Yunani!
[lang:ku]Yewnanî bipeyivin!
[lang:lb]Schwätzt Griichesch!
[lang:mg]Mitenena grika!
[lang:mt]Kellem Grieg!
[lang:mi]Korero Kariki!
[lang:ps]یوناني وایې!
[lang:sm]Tautala i le gagana Eleni!
[lang:gd]Bruidhinn Grèigeach!
[lang:st]Bua Greek!
[lang:sn]Taura chiGiriki!
[lang:sd]يوناني ڳالهايو!
[lang:so]Ku hadal Griig!
[lang:su]Nyarios Yunani!
[lang:tg]Ба забони юнонӣ гап занед!
[lang:cy]Siaradwch Roeg!
[lang:xh]Thetha isiGrike!
[lang:yi]רעדן גריכיש!
[lang:yo]Sọ Giriki!
---

