[key]easy origami
[lang:en]easy origami
[lang:ja]簡単な折り紙
[lang:af]maklik origami
[lang:az]asan origami
[lang:id]origami mudah
[lang:ms]origami mudah
[lang:bs]lako origami
[lang:ca]fàcil d'Origami
[lang:cs]snadná origami
[lang:da]let origami
[lang:de]leicht Origami
[lang:et]lihtne origami
[lang:es]fácil de Origami
[lang:eu]papiroflexia erraza
[lang:fil]madaling Origami
[lang:fr]facile origamis
[lang:gl]fácil origami
[lang:hr]lako origami
[lang:zu]origami lula
[lang:is]auðvelt Origami
[lang:it]facile origami
[lang:sw]rahisi origami
[lang:lv]viegli origami
[lang:lt]lengva origami
[lang:hu]egyszerű origami
[lang:nl]makkelijk origami
[lang:no]enkel origami
[lang:uz]oson origami
[lang:pl]proste origami
[lang:pt]fácil origami
[lang:ro]origami ușor
[lang:sq]origami lehtë
[lang:sk]ľahká origami
[lang:sl]preprosta origami
[lang:sr]лако origami
[lang:fi]helppo origami
[lang:sv]lätt origami
[lang:vi]dễ dàng origami
[lang:tr]kolay origami
[lang:be]лёгка арыгамі
[lang:bg]лесно оригами
[lang:ky]жеңил оригами
[lang:kk]оңай оригами
[lang:mk]лесно оригами
[lang:mn]хялбар тогоруу
[lang:ru]легко оригами
[lang:sr]лако origami
[lang:uk]легко орігамі
[lang:el]εύκολο origami
[lang:hy]հեշտ Origami
[lang:iw]אוריגמי קל
[lang:ur]آسان اوریگامی
[lang:ar]من السهل اوريغامي
[lang:fa]اریگامی آسان
[lang:ne]सजिलो ओरिगेमी
[lang:mr]सोपे ओरिगामी
[lang:hi]आसान ओरिगेमी
[lang:bn]সহজ অরিগ্যামি
[lang:pa]ਆਸਾਨ Origami
[lang:gu]સરળ ઓરિગામિ
[lang:ta]எளிதாக ஓரிகமி
[lang:te]సులభంగా origami
[lang:kn]ಸುಲಭ ಒರಿಗಮಿ
[lang:ml]എളുപ്പത്തിൽ ഒറിഗമി
[lang:si]පහසු ඔරිගාමි
[lang:th]พับง่าย
[lang:lo]origami ງ່າຍ
[lang:my]လွယ်ကူသောစက္ကူခေါက်ခြင်း
[lang:ka]მარტივი origami
[lang:am]ቀላል የወረቀት
[lang:km]origami ជាការងាយស្រួល
[lang:zh-Hans]易折纸
[lang:zh-Hant]易摺紙
[lang:ko]쉬운 종이 접기
[lang:co]facili fari l 'origami
[lang:fy]easy Origami
[lang:ha]sauki Origami
[lang:ig]mfe Origami
[lang:jv]origami gampang
[lang:ku]origami bi hêsanî
[lang:lb]einfach ORIGAMI
[lang:mg]mora origami
[lang:mt]origami faċli
[lang:mi]orikami ngāwari
[lang:ps]آسانه origami
[lang:sm]origami faigofie
[lang:gd]furasta origami
[lang:st]bonolo origami
[lang:sn]nyore Kurumi
[lang:sd]آسان origami
[lang:so]origami fudud
[lang:su]origami gampang
[lang:tg]origami осон
[lang:cy]origami hawdd
[lang:xh]lula origami
[lang:yi]גרינג אָריגאַמי
[lang:yo]rorun origami
---

[key]origami
[lang:en]origami
[lang:ja]折り紙
[lang:af]origami
[lang:az]origami
[lang:id]origami
[lang:ms]origami
[lang:bs]origami
[lang:ca]origami
[lang:cs]origami
[lang:da]origami
[lang:de]Origami
[lang:et]origami
[lang:es]origami
[lang:eu]papiroflexia
[lang:fil]Origami
[lang:fr]origami
[lang:gl]origami
[lang:hr]Origami
[lang:zu]origami
[lang:is]Origami
[lang:it]origami
[lang:sw]origami
[lang:lv]origami
[lang:lt]origami
[lang:hu]origami
[lang:nl]origami
[lang:no]origami
[lang:uz]origami
[lang:pl]origami
[lang:pt]origami
[lang:ro]origami
[lang:sq]origami
[lang:sk]origami
[lang:sl]origami
[lang:sr]оригами
[lang:fi]origami
[lang:sv]origami
[lang:vi]Nghệ thuật sắp xếp giấy Nhật Bản
[lang:tr]Japon kağıt katlama sanatı
[lang:be]арыгамі
[lang:bg]оригами
[lang:ky]оригами
[lang:kk]оригами
[lang:mk]оригами
[lang:mn]тогоруу
[lang:ru]оригами
[lang:sr]оригами
[lang:uk]орігамі
[lang:el]origami
[lang:hy]Origami
[lang:iw]אוריגמי
[lang:ur]اوریگامی
[lang:ar]فن قص الورق
[lang:fa]اریگامی
[lang:ne]ओरिगेमी
[lang:mr]ओरिगामी
[lang:hi]ओरिगेमी
[lang:bn]অরিগ্যামি
[lang:pa]Origami
[lang:gu]ઓરિગામિ
[lang:ta]ஓரிகமி
[lang:te]Origami
[lang:kn]ಒರಿಗಮಿ
[lang:ml]ഒറിഗമി
[lang:si]ඔරිගාමි
[lang:th]พับ
[lang:lo]origami
[lang:my]စက္ကူခေါက်ခြင်း
[lang:ka]origami
[lang:am]የወረቀት
[lang:km]origami
[lang:zh-Hans]折纸
[lang:zh-Hant]摺紙
[lang:ko]종이 접기
[lang:co]'origami
[lang:fy]Origami
[lang:ha]Origami
[lang:ig]Origami
[lang:jv]origami
[lang:ku]origami
[lang:lb]ORIGAMI
[lang:mg]origami
[lang:mt]origami
[lang:mi]orikami
[lang:ps]origami
[lang:sm]origami
[lang:gd]origami
[lang:st]origami
[lang:sn]Kurumi
[lang:sd]origami
[lang:so]origami
[lang:su]origami
[lang:tg]origami
[lang:cy]origami
[lang:xh]origami
[lang:yi]אָריגאַמי
[lang:yo]origami
---

[key]how to make origami
[lang:en]how to make origami
[lang:ja]折り紙の作り方
[lang:af]hoe om origami maak
[lang:az]origami etmək üçün necə
[lang:id]bagaimana membuat origami
[lang:ms]bagaimana untuk membuat origami
[lang:bs]kako napraviti origami
[lang:ca]com fer origami
[lang:cs]jak dělat origami
[lang:da]hvordan man laver origami
[lang:de]wie man Origami
[lang:et]kuidas teha origami
[lang:es]cómo hacer origami
[lang:eu]papiroflexia nola egin
[lang:fil]kung paano gumawa ng Origami
[lang:fr]comment faire l'origami
[lang:gl]como facer origami
[lang:hr]kako napraviti origami
[lang:zu]indlela yokwenza origami
[lang:is]verðuru að gera Origami
[lang:it]come fare origami
[lang:sw]jinsi ya kufanya origami
[lang:lv]kā padarīt origami
[lang:lt]kaip padaryti origami
[lang:hu]hogyan origami
[lang:nl]hoe origami maken
[lang:no]hvordan å lage origami
[lang:uz]origami qilish qanday
[lang:pl]jak zrobić origami
[lang:pt]como fazer origami
[lang:ro]modul de a face origami
[lang:sq]si për të bërë origami
[lang:sk]ako robiť origami
[lang:sl]kako narediti origami
[lang:sr]како направити оригами
[lang:fi]miten origami
[lang:sv]hur man gör origami
[lang:vi]làm thế nào để làm cho origami
[lang:tr]origami nasıl
[lang:be]як зрабіць арыгамі
[lang:bg]как да се направи оригами
[lang:ky]оригами жасоого кантип
[lang:kk]оригами қалай жасауға
[lang:mk]како да се направи оригами
[lang:mn]тогоруу хийж хэрхэн
[lang:ru]как сделать оригами
[lang:sr]како направити оригами
[lang:uk]як зробити орігамі
[lang:el]πώς να κάνει origami
[lang:hy]թե ինչպես կարելի է անել, origami
[lang:iw]איך לעשות אוריגמי
[lang:ur]اوریگامی بنانے کا طریقہ
[lang:ar]كيفية جعل اوريغامي
[lang:fa]چگونه به اریگامی
[lang:ne]ओरिगेमी कसरी बनाउने
[lang:mr]ओरिगामी कसे
[lang:hi]कैसे ओरिगेमी बनाने के लिए
[lang:bn]কিভাবে অরিগ্যামি করতে
[lang:pa]ਨੂੰ Origami ਕਰਨ ਲਈ
[lang:gu]કેવી રીતે ઓરિગામિ બનાવવા માટે
[lang:ta]ஓரிகமி எப்படி
[lang:te]origami చేయడానికి ఎలా
[lang:kn]ಒರಿಗಮಿ ಮಾಡಲು ಹೇಗೆ
[lang:ml]എങ്ങനെ ഒറിഗമി നടത്താൻ
[lang:si]ඔරිගාමි බවට පත් කර ගන්නේ කෙසේද
[lang:th]วิธีที่จะทำให้พับ
[lang:lo]ວິທີການເຮັດໃຫ້ origami
[lang:my]စက္ကူခေါက်ခြင်းအောင်ဘယ်လို
[lang:ka]როგორ, რათა origami
[lang:am]የወረቀት ማድረግ እንደሚችሉ
[lang:km]របៀបដើម្បីធ្វើឱ្យ origami
[lang:zh-Hans]如何使折纸
[lang:zh-Hant]如何使摺紙
[lang:ko]종이 접기를 만드는 방법
[lang:co]pasta fatta 'origami
[lang:fy]hoe te meitsjen Origami
[lang:ha]yadda za a yi Origami
[lang:ig]esi mee ka Origami
[lang:jv]carane nggawe origami
[lang:ku]çawa ji bo ku origami
[lang:lb]wéi ze maachen ORIGAMI
[lang:mg]ny fomba hanaovana origami
[lang:mt]kif jagħmlu origami
[lang:mi]me pehea ki te hanga orikami
[lang:ps]څنګه origami کړي
[lang:sm]le auala e faia origami
[lang:gd]ciamar a dhèanamh origami
[lang:st]kamoo ho ka etsa origami
[lang:sn]sei kuti Kurumi
[lang:sd]ڪيئن origami ڪرڻ
[lang:so]sida loo sameeyo origami
[lang:su]kumaha carana sangkan origami
[lang:tg]чӣ тавр ба origami
[lang:cy]sut i wneud origami
[lang:xh]indlela yokwenza origami
[lang:yi]ווי צו מאַכן אָריגאַמי
[lang:yo]bi o lati ṣe origami
---

[key]airplane
[lang:en]airplane
[lang:ja]飛行機
[lang:af]vliegtuig
[lang:az]təyyarə
[lang:id]pesawat terbang
[lang:ms]kapal terbang
[lang:bs]avion
[lang:ca]avió
[lang:cs]letoun
[lang:da]fly
[lang:de]Flugzeug
[lang:et]lennuk
[lang:es]avión
[lang:eu]hegazkin
[lang:fil]eruplano
[lang:fr]avion
[lang:gl]avión
[lang:hr]zrakoplov
[lang:zu]yendiza
[lang:is]flugvél
[lang:it]aereo
[lang:sw]ndege
[lang:lv]lidmašīna
[lang:lt]lėktuvas
[lang:hu]repülőgép
[lang:nl]vliegtuig
[lang:no]fly
[lang:uz]samolyot
[lang:pl]samolot
[lang:pt]avião
[lang:ro]avion
[lang:sq]aeroplan
[lang:sk]lietadlo
[lang:sl]letalo
[lang:sr]авион
[lang:fi]lentokone
[lang:sv]flygplan
[lang:vi]Máy bay
[lang:tr]uçak
[lang:be]самалёт
[lang:bg]самолет
[lang:ky]учак
[lang:kk]ұшақ
[lang:mk]авион
[lang:mn]онгоц
[lang:ru]самолет
[lang:sr]авион
[lang:uk]літак
[lang:el]αεροπλάνο
[lang:hy]ինքնաթիռ
[lang:iw]מטוס
[lang:ur]ہوائی جہاز
[lang:ar]طائرة
[lang:fa]هواپیما
[lang:ne]हवाइजहाज
[lang:mr]विमान
[lang:hi]विमान
[lang:bn]বিমান
[lang:pa]ਜਹਾਜ਼
[lang:gu]વિમાન
[lang:ta]விமானம்
[lang:te]విమానం
[lang:kn]ಏರೋಪ್ಲೇನ್
[lang:ml]ആകാശവിമാനം
[lang:si]ගුවන් යානා
[lang:th]เครื่องบิน
[lang:lo]ເຮືອບິນ
[lang:my]လေယာဉ်ပျံ
[lang:ka]თვითმფრინავი
[lang:am]አውሮፕላን
[lang:km]យន្តហោះ
[lang:zh-Hans]飞机
[lang:zh-Hant]飛機
[lang:ko]비행기
[lang:co]paghjella
[lang:fy]fleantúch
[lang:ha]jirgin sama
[lang:ig]ụgbọelu
[lang:jv]pesawat
[lang:ku]balafir
[lang:lb]Airplane
[lang:mg]fiaramanidina
[lang:mt]ajruplan
[lang:mi]manureva
[lang:ps]الوتکې
[lang:sm]vaʻalele
[lang:gd]itealan
[lang:st]lifofane
[lang:sn]ndege
[lang:sd]هوائي اڏي
[lang:so]diyaarad
[lang:su]kapal udara
[lang:tg]тайёра
[lang:cy]awyren
[lang:xh]inqwelo-moya
[lang:yi]אַעראָפּלאַן
[lang:yo]Okoofurufu
---

[key]balloon
[lang:en]balloon
[lang:ja]風船
[lang:af]ballon
[lang:az]şar
[lang:id]balon
[lang:ms]belon
[lang:bs]balon
[lang:ca]globus
[lang:cs]balón
[lang:da]ballon
[lang:de]Ballon
[lang:et]balloon
[lang:es]globo
[lang:eu]puxika
[lang:fil]lobo
[lang:fr]ballon
[lang:gl]globo
[lang:hr]balon
[lang:zu]ibhaluni
[lang:is]blaðra
[lang:it]Palloncino
[lang:sw]puto
[lang:lv]balons
[lang:lt]balionas
[lang:hu]ballon
[lang:nl]ballon
[lang:no]ballong
[lang:uz]shar
[lang:pl]balon
[lang:pt]balão
[lang:ro]balon
[lang:sq]tullumbace
[lang:sk]balónik
[lang:sl]balon
[lang:sr]балон
[lang:fi]ilmapallo
[lang:sv]ballong
[lang:vi]quả bóng bay
[lang:tr]balon
[lang:be]паветраны шар
[lang:bg]балон
[lang:ky]учуучу аба шары
[lang:kk]әуе шары
[lang:mk]балон
[lang:mn]бөмбөлөг
[lang:ru]баллон
[lang:sr]балон
[lang:uk]повітряна куля
[lang:el]μπαλόνι
[lang:hy]փուչիկ
[lang:iw]בלון
[lang:ur]بیلون
[lang:ar]بالون
[lang:fa]بالون
[lang:ne]बेलुन
[lang:mr]फुगा
[lang:hi]गुब्बारा
[lang:bn]বেলুন
[lang:pa]ਗੁਬਾਰਾ
[lang:gu]બલૂન
[lang:ta]பலூன்
[lang:te]బెలూన్
[lang:kn]ಬಲೂನ್
[lang:ml]ബലൂണ്
[lang:si]බැලුන
[lang:th]บอลลูน
[lang:lo]ປູມເປົ້າ
[lang:my]မီးပုံးပျံ
[lang:ka]balloon
[lang:am]ላስቲክ
[lang:km]ប៉េងប៉ោង
[lang:zh-Hans]气球
[lang:zh-Hant]氣球
[lang:ko]풍선
[lang:co]scatuletta
[lang:fy]ballon
[lang:ha]balan-balan
[lang:ig]balloon
[lang:jv]balon
[lang:ku]balon
[lang:lb]Ballon
[lang:mg]balloon
[lang:mt]bużżieqa
[lang:mi]poihau
[lang:ps]بالون
[lang:sm]paluni
[lang:gd]bailiùn
[lang:st]balune
[lang:sn]chibharuma
[lang:sd]لغڙ
[lang:so]buufin
[lang:su]balon
[lang:tg]баллон
[lang:cy]balŵn
[lang:xh]ibhaloni
[lang:yi]באַלאָן
[lang:yo]alafẹfẹ
---

[key]camera
[lang:en]camera
[lang:ja]カメラ
[lang:af]kamera
[lang:az]kamera
[lang:id]kamera
[lang:ms]kamera
[lang:bs]kamera
[lang:ca]càmera
[lang:cs]Fotoaparát
[lang:da]kamera
[lang:de]Kamera
[lang:et]kaamera
[lang:es]cámara
[lang:eu]kamera
[lang:fil]camera
[lang:fr]caméra
[lang:gl]cámara
[lang:hr]fotoaparat
[lang:zu]ikhamera
[lang:is]myndavél
[lang:it]telecamera
[lang:sw]kamera
[lang:lv]kamera
[lang:lt]fotoaparatas
[lang:hu]kamera
[lang:nl]camera
[lang:no]kamera
[lang:uz]kamera
[lang:pl]aparat fotograficzny
[lang:pt]Câmera
[lang:ro]aparat foto
[lang:sq]aparat fotografik
[lang:sk]fotoaparát
[lang:sl]kamera
[lang:sr]Камера
[lang:fi]kamera
[lang:sv]kamera
[lang:vi]Máy ảnh
[lang:tr]kamera
[lang:be]камера
[lang:bg]камера
[lang:ky]фотоаппарат
[lang:kk]фотоаппарат
[lang:mk]камера
[lang:mn]зургийн аппарат
[lang:ru]камера
[lang:sr]Камера
[lang:uk]камера
[lang:el]ΦΩΤΟΓΡΑΦΙΚΗ ΜΗΧΑΝΗ
[lang:hy]ֆոտոխցիկ
[lang:iw]מצלמה
[lang:ur]کیمرہ
[lang:ar]الة تصوير
[lang:fa]دوربین
[lang:ne]क्यामेरा
[lang:mr]कॅमेरा
[lang:hi]कैमरा
[lang:bn]ক্যামেরা
[lang:pa]ਕੈਮਰਾ
[lang:gu]કૅમેરા
[lang:ta]புகைப்பட கருவி
[lang:te]కెమెరా
[lang:kn]ಕ್ಯಾಮೆರಾ
[lang:ml]കാമറ
[lang:si]කැමරා
[lang:th]กล้อง
[lang:lo]ກ້ອງ​ຖ່າຍ​ຮູບ
[lang:my]ကင်မရာ
[lang:ka]კამერა
[lang:am]ካሜራ
[lang:km]កាមេរ៉ា
[lang:zh-Hans]相机
[lang:zh-Hant]相機
[lang:ko]카메라
[lang:co]Camera
[lang:fy]kamera
[lang:ha]kyamara
[lang:ig]igwefoto
[lang:jv]kamera
[lang:ku]kamîra
[lang:lb]Kamera
[lang:mg]fakan-tsary
[lang:mt]kamera
[lang:mi]kāmera
[lang:ps]کامرې
[lang:sm]mea puʻeata
[lang:gd]'chamara
[lang:st]khamera
[lang:sn]kamera
[lang:sd]ڪئميرا
[lang:so]camera
[lang:su]kamera
[lang:tg]камера
[lang:cy]camera
[lang:xh]camera
[lang:yi]אַפּאַראַט
[lang:yo]kamẹra
---

[key]flying bird
[lang:en]flying bird
[lang:ja]パタパタ鶴
[lang:af]vlieënde voël
[lang:az]uçan quş
[lang:id]terbang burung
[lang:ms]burung terbang
[lang:bs]leteća ptica
[lang:ca]ocell que vola
[lang:cs]létající ptačí
[lang:da]flyvende fugl
[lang:de]fliegender Vogel
[lang:et]lendavad lind
[lang:es]Ave volando
[lang:eu]hegan hegazti
[lang:fil]lumilipad ibon
[lang:fr]oiseau volant
[lang:gl]paxaro voando
[lang:hr]leti ptica
[lang:zu]inyoni flying
[lang:is]fljúga fugl
[lang:it]Uccello volante
[lang:sw]flying ndege
[lang:lv]lidošana putns
[lang:lt]plaukioja paukščių
[lang:hu]repülő madár
[lang:nl]vliegende vogel
[lang:no]flygende fugl
[lang:uz]uchib qush
[lang:pl]latający ptak
[lang:pt]pássaro voando
[lang:ro]pasare zburătoare
[lang:sq]zog fluturues
[lang:sk]lietajúci vtáčie
[lang:sl]plujejo ptic
[lang:sr]летећа птица
[lang:fi]lentävä lintu
[lang:sv]flygande fågel
[lang:vi]chim bay
[lang:tr]uçan kuş
[lang:be]якая ляціць птушка
[lang:bg]летяща птица
[lang:ky]учкан куш
[lang:kk]ұшатын құс
[lang:mk]летање птица
[lang:mn]нисдэг шувуу
[lang:ru]летящая птица
[lang:sr]летећа птица
[lang:uk]летить птах
[lang:el]που φέρουν πουλί
[lang:hy]թռչող թռչուն
[lang:iw]ציפור עפה
[lang:ur]پرواز پرندوں
[lang:ar]الطيور تحلق
[lang:fa]پرنده ی در حال پرواز
[lang:ne]उडान चरा
[lang:mr]उडणार्या पक्ष्याला
[lang:hi]उड़ता पंछी
[lang:bn]উড়ন্ত পাখি
[lang:pa]ਉਡਾਣ ਪੰਛੀ
[lang:gu]ઉડતી પક્ષી
[lang:ta]பறக்கும் பறவையின்
[lang:te]ఎగిరే పక్షి
[lang:kn]ಹಾರುವ ಹಕ್ಕಿ
[lang:ml]പറക്കുന്ന പക്ഷി
[lang:si]පියාසර පක්ෂි
[lang:th]นกบิน
[lang:lo]ນົກຊະນິດເກມບິນ
[lang:my]ပျံငှက်
[lang:ka]საფრენი ფრინველის
[lang:am]የሚበር ወፍ
[lang:km]បក្សីហោះ
[lang:zh-Hans]飞鸟
[lang:zh-Hant]飛鳥
[lang:ko]비행 조류
[lang:co]aceddu aceddi
[lang:fy]fleanende fûgel
[lang:ha]tashi tsuntsu
[lang:ig]na-efe efe nnụnụ
[lang:jv]mabur manuk
[lang:ku]bird que
[lang:lb]Teppech Vugel
[lang:mg]vorona manidina
[lang:mt]li jtajru l-għasafar
[lang:mi]manu rere
[lang:ps]د الوتلو مرغۍ
[lang:sm]lele manulele
[lang:gd]eun a 'sgiathalaich
[lang:st]fofang nonyana
[lang:sn]kubhururuka shiri
[lang:sd]پرواز پکي
[lang:so]shimbir duulaya
[lang:su]ngalayang manuk
[lang:tg]паррандае
[lang:cy]adar yn hedfan
[lang:xh]intaka flying
[lang:yi]פליענדיק פויגל
[lang:yo]flying eye

