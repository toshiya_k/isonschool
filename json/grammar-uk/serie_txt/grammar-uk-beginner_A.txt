[seriename]grammar-uk-beginner
[exercisename]A
[publishhours]6
[problemnum]10
[audiolang]uk
[videolang]en
[voice:x]F1,M1,F1
[serietitle]Ukrainian grammar exercises
[title]{Ukrainian grammar exercises} ⭐️ #[1] [:VideoTitle:]
[description]{Learn Ukrainian grammar!} {Change a word in a sentence.}\n\n[:GrammarDescriptionTranslationWithRuby:]\n\n{To teachers and schools:}\n{This video is published under Creative Commons License.}\n  CC BY 3.0 https://creativecommons.org/licenses/by/3.0/ \n{Feel free to use this video in your classes!}\n\n{We use Google Translate service for localization. If you find some mistakes, please let us know.}\n
[tags]{Ukrainian grammar|15|,}
[hidesruby]true
[caption:translation]{Translation}
[voicekeyname:F1]uk-UA-Wavenet-A
[font:title]NotoSansDisplay-SemiBold.ttf
[font:subtitle]NotoSansDisplay-SemiBold.ttf
[font:problem]NotoSansDisplay-SemiBold.ttf
[font:ruby]NotoSansDisplay-Condensed.ttf

[langs]all
[palette]151418,828993,ACC7D8,8C5C66,F3F4F5
[palette]EFEEEB,A6835B,8A542A,96A9AA,3D3E2D
[palette]F5F4F5,AC7236,65C31B,BFB268,2E2930
[palette]F9F8F7,60B96F,1CCE40,6A828C,222026
[palette]F1F1EA,798889,454444,949394,212225
[palette]212127,F1C71B,4CBEEF,738B44,F5F7F8
[palette]EBECE5,A19845,644D3F,717A7F,222323
[palette]241D2A,D9B946,85C0EB,A73A37,F7F6F4
[palette]19191F,64A0C1,3199E7,6D5B71,FAFAF9
[palette]EBF2EE,90B388,5B8F2E,4994AF,272E39
[palette]EAE9E8,7D767C,5C423A,7B847F,1B171C
[palette]1E2635,F057E8,48ABF3,9C409D,F0F3EE
[palette]27242B,7F888A,81E3EF,816D6F,F8F9F7
[palette]EEF0EF,7F7C7C,5F3739,8A787B,201A20
[palette]EEE9E7,827173,5F3F3C,7C7C7C,181518
[palette]FAFAFA,77999F,6E493E,AC9592,24161A
[palette]EBECEA,A97E50,6C4E40,76939A,29312E
[palette]1E1F2D,7BADD3,1A78EB,938185,F0F1F3
[palette]F4F3F0,AE8457,7D5237,8C8C8D,251C21
[palette]FBFBFA,8B9C7A,6E6D3F,8A9293,1E2134
[palette]EFF0EC,707369,32B13D,7FA2A0,202020
[palette]271D29,7633C6,C44BE0,5D3988,EEEFF0
[palette]F0ECEB,696C67,18CC38,80A39E,1E2520
[palette]F4F2EF,D07F54,7B3B38,A67181,281B22
[palette]EEE9E7,836F70,5F3F3C,7C7C7C,181519
[palette]223039,787D7A,83E4EF,7C8278,F4F6F4
[palette]DFDCD6,946B57,6E271E,867D69,1C181C
[palette]EBEAE9,7E8479,665D3A,7C7D7C,1A181D
[palette]1E1E29,8094B3,5996F1,6D5C86,F2F3F1
[palette]F9F8F7,918359,3B9542,D38F37,352E2B
[palette]F5F5F1,B85E21,369842,D68E39,211C1B
[palette]FAF6F7,696B67,19D039,6EB9B5,2D2C32
[palette]F8F7F5,B08462,9E2724,AC9D8E,2D2524
[palette]1A1623,7D9DBF,58C9E7,666F8F,F4F6F5
[palette]3C2B26,816082,92B1E9,B67D57,F3F3F3
[palette]F2F0F2,746560,43982F,899EA3,291B36
[palette]F4EDEF,6F726E,35C42B,6CB3A6,2E2E32
[palette]141918,68B2CD,1854D4,46C839,EAEEF0
[palette]1D1F27,53AEC3,3496E9,86516C,F1EDDC
[palette]F4F1F0,AD7B5E,914A29,9E8D83,4C2D28
[palette]F2F1EE,746E6C,684C41,7F8086,2B2429
[palette]F8F5F5,767575,5F313B,7C7B7C,201A1F
[palette]F3EEEA,DB6465,534643,9D9394,262529
[palette]EEEBE8,978C6F,5D463E,7B7B84,272325
[palette]292E35,7D99A6,5BDDEF,797273,F4F4F0
[palette]FBFAFA,8B9C7A,717041,79889D,1D222F
[palette]1C2420,608CA2,27AFEE,5A686D,F3F2F0
[palette]F9F7F9,886F29,419950,F3C038,333B2C
[palette]F2F1F0,C0774A,8F4D2B,A79D87,49312C
[palette]D6CBCA,7F716B,604D19,718179,131318
[palette]FBF9FA,8C9B7A,6F7243,8B9194,2F3931
[palette]0D1216,83ADC2,5ACBE4,917878,F6F7F5
[palette]0F191C,F2C81B,4BC0EF,718B44,F4F6F4
[palette]F3F2F1,8C826E,705F42,8D9586,2C3D39
[palette]ECECEB,688582,2F694F,54C99D,221921
[palette]1F1A24,70A4C0,298FE8,818383,F2F2EC
[palette]1A1E25,867C79,80CBD3,82546A,E6E7DB
[palette]F5EFF2,696B67,16CB3A,81B4AD,2C2729
[palette]1C1821,5D6576,22C1EF,616363,F1F3F1
[palette]EBE6E8,524533,5FA22F,A77D4B,243126
[palette]FBFAF9,898E8D,77583A,868190,1A1923
[palette]F6F3F1,B35F4D,61403F,A87D95,1B1B23
[palette]111317,8AA5BD,4ABFF5,607C9F,F7F6F4
[palette]1A1827,89D7E4,46D1E6,A84260,F6F9F9
[palette]EDEAE5,988F70,873D23,898480,1C1D1E
[palette]111519,818484,9DAFC6,6A7483,F4F4F2
[palette]0C1011,6088AF,4CBFF0,646466,F1F1ED
[palette]201C30,737286,2795EE,7C7C75,F4F5F3
[palette]1B1623,6E89B2,22B7DB,AC3F5B,F5F7F4
[palette]F0EEE8,8C937F,754323,8B8C84,1D2321
[palette]F1F2F4,6F7B4D,16CA39,63BA6D,211D23
[palette]1C262C,929385,82D0F2,7B757F,F4F8F7
[palette]1C1E32,2B82C9,2DADF1,53628F,FAFAFA
[palette]EDEAE9,705B64,892F28,9C7A68,1C161C
[palette]F6F7F5,80D138,5FB930,AABFB8,263D32
[palette]202230,6CA3CD,49AEF0,747474,F6F6F5
[palette]FAFAF9,A49B43,63404E,80959C,1D2034
[palette]19171E,8E9EBD,85BCF0,895A87,F1EFED
[palette]F9F5F3,A27233,724C40,74676E,202B24
[palette]252B27,827586,84E0EE,858F7C,FAF9F9
[palette]F9F8F8,978268,735F45,9EAE96,2E3E3A
[palette]1F1A24,6BA3C1,288FE8,818383,F2F2EC
[palette]2B212C,82818D,88E2E8,B33B59,F2F6F6
[palette]F7F5F3,A17946,88482A,C18466,1F1A1F
[palette]1F2931,80B1AC,19C6F3,6F727A,EAF1F1
[palette]F5F4F5,96C29C,39B140,94C4A8,2C3632
[palette]F5F5F5,CB9D5B,8E4C32,857C90,25242D
[palette]FBFAFA,7E737E,8C4E39,9D949B,291D24
[palette]FBF9F9,8A8494,8F5635,8196AB,1B1A25
[palette]F5F6F2,A1BD95,61A330,9C889E,342F35
[palette]F7F9F7,5E2F3C,7C303A,AC5266,1E131C
[palette]F6F4F2,C1B069,64BF1B,68849D,2E312C
[palette]FBFAFB,7C655F,1FD245,72AAB5,1F1D25
[palette]1E1728,7486AE,1FAFEA,AD3E5B,F4F7F5
[palette]21232C,26B3EC,B630DB,5176BC,FAF9F8
[palette]E8E0DC,5C8A9F,3A3C3B,949495,212521
[palette]F4EFF1,786C5D,22BB26,8AAB9B,2F3334
[palette]EDE9E5,978D83,61433E,857991,201C28
[palette]F0EFE7,C27529,7A3427,7F6E6F,2C1E1F
[palette]3E3128,B19DBD,96AEE5,8E7E9B,FAFAFA
