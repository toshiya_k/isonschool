[seriename]grammar-ja-tk2
[no]1
[videotitle]ます
[box:0:x]明日、{1:[current]}{3:[current]};今、{1:[current]}{3:[current]};さっき、{1:[past]}{3:[past]};先月、{1:[past]}{3:[past]}
[box:1:x]遊びますか？(遊ぶ)[current]{3:(遊ぶ)};遊びましたか？(遊ぶ)[past]{3:(遊ぶ)};行きますか？(行く)[current]{3:(行く)};行きましたか？(行く)[past]{3:(行く)}
[box:2:y]はい、{3:[p]};いいえ、{3:[n]}
[box:3:y]遊びます(遊ぶ)[p,current];遊びました(遊ぶ)[p,past];遊びません(遊ぶ)[n,current];遊びませんでした(遊ぶ)[n,past];行きます(行く)[p,current];行きました(行く)[p,past];行きません(行く)[n,current];行きませんでした(行く)[n,past]
[par]P:0,1
[par]P:2,3
[description]{Grammatical explanation}:\n{Tae Kim's Guide to Learning Japanese}\n{http://www.guidetojapanese.org/learn/grammar/polite}
---
[seriename]grammar-ja-tk2
[no]2
[videotitle]です
[box:0:z]今、{2:[current]}{4:[current]};さっき、{2:[past]}{4:[past]}
[box:1:z]犬は;猫は;トムは
[box:2:z]静かですか？(静かだ)[current]{4:(静かだ)};静かでしたか？(静かだ)[past]{4:(静かだ)}
[box:3:x]はい、{4:[p]};いいえ、{4:[n]}
[box:4:x]静かです(静かだ)[p,current];静かでした(静かだ)[p,past];静かではありません(静かだ)[n,current];静かではありませんでした(静かだ)[n,past]
[par]P:0,1,2
[par]P:3,4
[description]{Grammatical explanation}:\n{Tae Kim's Guide to Learning Japanese}\n{http://www.guidetojapanese.org/learn/grammar/polite}
---
[seriename]grammar-ja-tk2
[no]3
[videotitle]ご主人
[box:0:y]ご両親{4:両親};奥さん{4:妻};ご主人{4:主人};お姉さん{4:姉};お兄さん{4:兄}
[box:1:y]は
[box:2:y]お元気ですか？{6:元気です};お仕事ですか？{6:仕事です};買い物ですか？{6:買い物です}
[box:3:x]はい、
[box:4:x]両親;妻;主人;姉;兄
[box:5:x]は
[box:6:x]元気です;仕事です;買い物です
[par]P:0,1,2
[par]P:3,4,5,6
[description]{Grammatical explanation}:\n{Tae Kim's Guide to Learning Japanese}\n{http://www.guidetojapanese.org/learn/grammar/people}\n https://www.youtube.com/watch?v=QK7HCBqc_-c 
---
[seriename]grammar-ja-tk2
[no]4
[videotitle]娘さん
[box:0:z]息子さん{4:息子};娘さん{4:娘};お母さん{4:母};お父さん{4:父}
[box:1:z]は
[box:2:z]お元気ですか？{6:元気です};お仕事ですか？{6:仕事です};買い物ですか？{6:買い物です}
[box:3:y]はい、
[box:4:y]息子;娘;母;父
[box:5:y]は
[box:6:y]元気です;仕事です;買い物です
[par]P:0,1,2
[par]P:3,4,5,6
[description]{Grammatical explanation}:\n{Tae Kim's Guide to Learning Japanese}\n{http://www.guidetojapanese.org/learn/grammar/people}\n https://www.youtube.com/watch?v=QK7HCBqc_-c 
---
[seriename]grammar-ja-tk2
[no]5
[videotitle]どこ
[box:0:x]お母さん{3:母};お父さん{3:父}
[box:1:x]は
[box:2:x]どこですか？
[box:3:z]母;父
[box:4:z]は
[box:5:z]買い物です;公園です;散歩です;買い物に行きました;公園に行きました;散歩に出かけました
[par]P:0,1,2
[par]P:3,4,5
[description]{Grammatical explanation}:\n{Tae Kim's Guide to Learning Japanese}\n{http://www.guidetojapanese.org/learn/grammar/people}
---
[seriename]grammar-ja-tk2
[no]6
[videotitle]か
[box:0:y]焼肉を;焼き鳥を;たこ焼きを
[box:1:y]食べますか？;食べませんか？
[box:2:z]すみません。;ごめんなさい。
[box:3:z]お腹がいっぱいです。
[par]P:0,1
[par]P:2,3
[description]{Grammatical explanation}:\n{Tae Kim's Guide to Learning Japanese}\n{http://www.guidetojapanese.org/learn/grammar/question}
---
[seriename]grammar-ja-tk2
[no]7
[videotitle]か
[box:0:y]今から，{2:[current]};昨日{2:[past]}
[box:1:y]何を
[box:2:y]食べたか(食べる)[past];食べるのか(食べる)[current];買うのか(買う)[current];買ったか(買う)[past];言ったか(言う)[past];言うのか(言う)[current]
[box:3:y]忘れた;分からない
[description]{Grammatical explanation}:\n{Tae Kim's Guide to Learning Japanese}\n{http://www.guidetojapanese.org/learn/grammar/question}
---
[seriename]grammar-ja-tk2
[no]8
[videotitle]誰も
[box:0:z]誰
[box:1:z]も[n];でも[p]
[box:2:z]分かる(分かる)[p]{1:[p]};分からない(分かる)[n]{1:[n]};知っている(知る)[p]{1:[p]};知らない(知る)[n]{1:[n]};食べる(食べる)[p]{1:[p]};できる(できる)[p]{1:[p]};できない(できる)[n]{1:[n]};持っている(持つ)[p]{1:[p]};持っていない(持つ)[n]{1:[n]};気付く(気付く)[p]{1:[p]};答える(答える)[p]{1:[p]}
[description]{Grammatical explanation}:\n{Tae Kim's Guide to Learning Japanese}\n{http://www.guidetojapanese.org/learn/grammar/question}
---
[seriename]grammar-ja-tk2
[no]9
[videotitle]いつでも
[box:0:x]いつでも;どこでも;誰とでも
[box:1:x]楽しい;遊ぶ;将棋を指す;サッカーをする
[description]{Grammatical explanation}:\n{Tae Kim's Guide to Learning Japanese}\n{http://www.guidetojapanese.org/learn/grammar/question}
