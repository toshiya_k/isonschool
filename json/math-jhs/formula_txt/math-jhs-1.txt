[seriename]math-jhs-1
[no]1
[videotitle]-3 < -2
[title]{Inequality sign}
[explanation:0]0 > -2
[explanation:1]-2 > -3
[explanation:2]2 > -3
[equation:0]-3[<:]2
[equation:1]8[>:]0
[equation:2]-2[<:>]0
[equation:3]-1[>:]-2
[equation:4]-3[=:<]-3
---
[seriename]math-jhs-1
[no]2
[videotitle]-2 > -2.1
[title]{Inequality sign}
[time]6
[explanation:0]-2 > -2.1
[explanation:1]-2 < -1.9
[equation:0]1.3[<:>]1.4
[equation:1]-2.1[>:<]-2
[equation:2]-3.3[<:]-3.1
[equation:3]-5.11[<:]-5.1
[equation:4]-2.3[=:<]-2.30
---
