[seriename]math-grade1-3
[no]1
[videotitle]3>2
[title]{Inequality sign}
[explanation:0]3>2
[explanation:1]3<4
[equation:0]3[>:<]2
[equation:1]8[>:]7
[equation:2]13[<:>]20
[equation:3]100[<:]120
[equation:4]201[=:<]201
---
