[key]Ako ay anim na taong gulang na.
[lang:fil]Ako ay anim na taong gulang na.
[lang:en]I am six years old.
[lang:ja]私は6歳です。
[lang:af]Ek is ses jaar oud.
[lang:az]Altı yaşım var.
[lang:id]Umur saya enam tahun.
[lang:ms]Saya berumur enam tahun.
[lang:bs]Imam šest godina.
[lang:ca]Tinc sis anys.
[lang:cs]Jsem šest let starý.
[lang:da]Jeg er seks år gammel.
[lang:de]Ich bin sechs Jahre alt.
[lang:et]Olen kuueaastane.
[lang:es]Tengo seis años.
[lang:eu]Sei urte ditut.
[lang:fr]J'ai six ans.
[lang:gl]Teño seis anos.
[lang:hr]Imam šest godina.
[lang:zu]Ngineminyaka eyisithupha.
[lang:is]Ég er sex ára.
[lang:it]Ho sei anni.
[lang:sw]Nina miaka sita.
[lang:lv]Man ir seši gadi.
[lang:lt]Man šešeri metai.
[lang:hu]Hat éves vagyok.
[lang:nl]Ik ben zes jaar oud.
[lang:no]Jeg er seks år gammel.
[lang:uz]Men olti yoshdaman.
[lang:pl]Mam sześć lat.
[lang:pt]Eu tenho seis anos de idade.
[lang:ro]Am sase ani.
[lang:sq]Unë jam gjashtë vjeç.
[lang:sk]Mám šesť rokov.
[lang:sl]Stara sem šest let.
[lang:sr]Имам шест година.
[lang:fi]Olen kuusi vuotta vanha.
[lang:sv]Jag är sex år gammal.
[lang:vi]Tôi sau tuổi.
[lang:tr]Altı yaşındayım.
[lang:be]Мне шэсць гадоў.
[lang:bg]Аз съм на шест години.
[lang:ky]Мен алты жаштамын.
[lang:kk]Мен алты жастамын.
[lang:mk]Имам шест години.
[lang:mn]Би зургаан настай.
[lang:ru]Мне шесть лет.
[lang:sr]Имам шест година.
[lang:uk]Мені шість років.
[lang:el]Είμαι έξι χρονών.
[lang:hy]Ես վեց տարեկան եմ:
[lang:iw]אני בן שש.
[lang:ur]میں چھ سال کا ہوں۔
[lang:ar]أنا ست سنوات من العمر.
[lang:fa]من شش سال دارم
[lang:ne]म छ वर्षको भएँ।
[lang:mr]मी सहा वर्षांचा आहे.
[lang:hi]मेरी आयु छह साल है।
[lang:bn]আমি ছয় বছর বয়সী.
[lang:pa]ਮੈਂ ਛੇ ਸਾਲਾਂ ਦਾ ਹਾਂ।
[lang:gu]હું છ વર્ષનો છું.
[lang:ta]எனக்கு ஆறு வயது.
[lang:te]నా వయసు ఆరు సంవత్సరాలు.
[lang:kn]ನನಗೆ ಆರು ವರ್ಷ.
[lang:ml]എനിക്ക് ആറ് വയസ്സായി.
[lang:si]මට වයස අවුරුදු හයයි.
[lang:th]ฉันอายุหกขวบ
[lang:lo]ຂ້ອຍມີອາຍຸຫົກປີ.
[lang:my]ကျွန်ုပ်အသက်ခြောက်နှစ်ရှိပါပြီ
[lang:ka]ექვსი წლის ვარ.
[lang:am]የስድስት ዓመት ልጅ ነኝ ፡፡
[lang:km]ខ្ញុំមានអាយុប្រាំមួយឆ្នាំ។
[lang:zh-Hans]我今年六岁。
[lang:zh-Hant]我今年六歲。
[lang:ko]나는 여섯 살입니다.
[lang:co]Aghju sei anni.
[lang:fy]Ik bin seis jier âld.
[lang:ha]Ina da shekara shida.
[lang:ig]Adị m afọ isii.
[lang:jv]Aku nem taun.
[lang:ku]Ez şeş salî me.
[lang:lb]Ech sinn sechs Joer al.
[lang:mg]Vehivavy enin-taona aho.
[lang:mt]Jiena sitt snin.
[lang:mi]Tekau taku tau.
[lang:ps]زه شپږ کلن یم
[lang:sm]E ono oʻu tausaga.
[lang:gd]Tha mi sia bliadhna a dh'aois.
[lang:st]Ke lilemo li tšeletseng.
[lang:sn]Ndine makore matanhatu.
[lang:sd]مان ڇهه سال پراڻي آهيان.
[lang:so]Waxaan ahay lix sano jir.
[lang:su]Abdi umur genep taun.
[lang:tg]Ман шаш солаам.
[lang:cy]Rwy'n chwech oed.
[lang:xh]Ndineminyaka emithandathu ubudala.
[lang:yi]איך בין זעקס יאָר אַלט.
[lang:yo]Mo jẹ ọdun mẹfa.
---

[key]Ako ay apat na taong gulang na.
[lang:fil]Ako ay apat na taong gulang na.
[lang:en]I am four years old.
[lang:ja]私は4歳です。
[lang:af]Ek is vier jaar oud.
[lang:az]Dörd yaşım var.
[lang:id]Umur saya empat tahun.
[lang:ms]Saya berumur empat tahun.
[lang:bs]Imam četiri godine.
[lang:ca]Tinc quatre anys.
[lang:cs]Mám čtyři roky.
[lang:da]Jeg er fire år gammel.
[lang:de]Ich bin vier Jahre alt.
[lang:et]Olen nelja-aastane.
[lang:es]Tengo cuatro años.
[lang:eu]Lau urte ditut.
[lang:fr]J'ai quatre ans.
[lang:gl]Teño catro anos.
[lang:hr]Imam četiri godine.
[lang:zu]Ngineminyaka emine.
[lang:is]Ég er fjögurra ára.
[lang:it]Ho quattro anni.
[lang:sw]Nina umri wa miaka nne.
[lang:lv]Man ir četri gadi.
[lang:lt]Man ketveri metai.
[lang:hu]Négy éves vagyok.
[lang:nl]Ik ben vier jaar oud.
[lang:no]Jeg er fire år gammel.
[lang:uz]Men to'rt yoshdaman.
[lang:pl]Mam cztery lata.
[lang:pt]Eu tenho quatro anos
[lang:ro]Am patru ani.
[lang:sq]Unë jam katër vjeç.
[lang:sk]Mám štyri roky.
[lang:sl]Stara sem štiri leta.
[lang:sr]Имам четири године.
[lang:fi]Olen neljä vuotta vanha.
[lang:sv]Jag är fyra år gammal.
[lang:vi]Tôi bốn tuổi.
[lang:tr]Ben dört yaşındayım.
[lang:be]Мне чатыры гады.
[lang:bg]Аз съм на четири години.
[lang:ky]Мен төрт жаштамын.
[lang:kk]Мен төрт жастамын.
[lang:mk]Јас имам четири години.
[lang:mn]Би дөрвөн настай.
[lang:ru]Мне четыре года.
[lang:sr]Имам четири године.
[lang:uk]Мені чотири роки.
[lang:el]Είμαι τέσσερα χρονών.
[lang:hy]Ես չորս տարեկան եմ:
[lang:iw]אני בת ארבע.
[lang:ur]میری عمر چار سال ہے۔
[lang:ar]أنا أربع سنوات من العمر.
[lang:fa]من چهار ساله هستم
[lang:ne]म चार बर्षको भएँ।
[lang:mr]मी चार वर्षांचा आहे.
[lang:hi]मैं चार साल का हूं।
[lang:bn]আমি চার বছর বয়সী.
[lang:pa]ਮੈਂ ਚਾਰ ਸਾਲਾਂ ਦਾ ਹਾਂ.
[lang:gu]હું ચાર વર્ષનો છું.
[lang:ta]எனக்கு நான்கு வயது.
[lang:te]నా వయసు నాలుగేళ్లు.
[lang:kn]ನನಗೆ ನಾಲ್ಕು ವರ್ಷ.
[lang:ml]എനിക്ക് നാല് വയസ്സായി.
[lang:si]මට වයස අවුරුදු හතරයි.
[lang:th]ฉันอายุสี่ขวบ
[lang:lo]ຂ້ອຍມີອາຍຸສີ່ປີ.
[lang:my]ငါလေးနှစ်ရှိပြီ
[lang:ka]მე ოთხი წლის ვარ.
[lang:am]እኔ የአራት ዓመት ልጅ ነኝ ፡፡
[lang:km]ខ្ញុំមានអាយុបួនឆ្នាំ។
[lang:zh-Hans]我今年四岁。
[lang:zh-Hant]我今年四歲。
[lang:ko]나는 4 살입니다.
[lang:co]Aghju quattru anni.
[lang:fy]Ik bin fjouwer jier âld.
[lang:ha]Ina da shekara hudu.
[lang:ig]Adị m afọ anọ.
[lang:jv]Aku umur patang taun.
[lang:ku]Ez çar salî me.
[lang:lb]Ech sinn véier Joer al.
[lang:mg]Efatra taona aho.
[lang:mt]Jiena erba 'snin.
[lang:mi]E wha oku tau.
[lang:ps]زه څلور کلن یم
[lang:sm]E fa oʻu tausaga.
[lang:gd]Tha mi ceithir bliadhna a dh'aois.
[lang:st]Ke lilemo li 'nè.
[lang:sn]Ndine makore mana.
[lang:sd]آئون چار سال پراڻي آهيان.
[lang:so]Waxaan ahay afar sano jir.
[lang:su]Abdi umur opat taun.
[lang:tg]Ман чор сола ҳастам.
[lang:cy]Rwy'n bedair oed.
[lang:xh]Ndineminyaka emine ubudala.
[lang:yi]איך בין פיר יאר אַלט.
[lang:yo]Mo jẹ ọdun mẹrin.
---

[key]Ako ay dalawampung taong gulang na.
[lang:fil]Ako ay dalawampung taong gulang na.
[lang:en]I am twenty years old.
[lang:ja]私は二十歳です。
[lang:af]Ek is twintig jaar oud.
[lang:az]Mənim iyirmi yaşım var.
[lang:id]Saya berusia dua puluh tahun.
[lang:ms]Saya berumur dua puluh tahun.
[lang:bs]Imam dvadeset godina.
[lang:ca]Tinc vint anys.
[lang:cs]Je mi dvacet let.
[lang:da]Jeg er tyve år gammel.
[lang:de]Ich bin 20 Jahre alt.
[lang:et]Ma olen kakskümmend aastat vana.
[lang:es]Tengo veinte años.
[lang:eu]Hogei urte ditut.
[lang:fr]J'ai vingt ans.
[lang:gl]Teño vinte anos.
[lang:hr]Imam dvadeset godina.
[lang:zu]Ngineminyaka engamashumi amabili.
[lang:is]Ég er tvítugur.
[lang:it]Ho vent'anni.
[lang:sw]Nina umri wa miaka ishirini.
[lang:lv]Es esmu divdesmit gadus vecs.
[lang:lt]Aš esu dvidešimties metų.
[lang:hu]Húsz éves vagyok.
[lang:nl]Ik ben twintig jaar oud.
[lang:no]Jeg er tjue år gammel.
[lang:uz]Men yigirma yoshdaman.
[lang:pl]Mam dwadzieścia lat.
[lang:pt]Eu tenho vinte anos
[lang:ro]Am douazeci de ani.
[lang:sq]Unë jam njëzet vjeç.
[lang:sk]Mám dvadsať rokov.
[lang:sl]Star sem dvajset let.
[lang:sr]Имам двадесет година.
[lang:fi]Olen kaksikymmentä vuotta vanha.
[lang:sv]Jag är tjugo år gammal.
[lang:vi]Tôi hai mươi tuổi.
[lang:tr]Ben yirmi yaşındayım.
[lang:be]Мне дваццаць гадоў.
[lang:bg]Аз съм на двадесет години.
[lang:ky]Мен жыйырма жаштамын.
[lang:kk]Мен жиырма жастамын.
[lang:mk]Јас имам дваесет години.
[lang:mn]Би хорин настай.
[lang:ru]Мне двадцать лет.
[lang:sr]Имам двадесет година.
[lang:uk]Мені двадцять років.
[lang:el]Είμαι είκοσι χρονών.
[lang:hy]Ես քսան տարեկան եմ.
[lang:iw]אני בן עשרים.
[lang:ur]میں بیس سال کا ہوں.
[lang:ar]ابلغ من العمر عشرين عاما.
[lang:fa]من بیست ساله هستم.
[lang:ne]म बीस वर्षको भएँ।
[lang:mr]मी वीस वर्षांचा आहे.
[lang:hi]मैं बीस वर्ष का हूँ।
[lang:bn]আমার বয়স বিশ বছর।
[lang:pa]ਮੈਂ ਵੀਹ ਸਾਲਾਂ ਦਾ ਹਾਂ।
[lang:gu]હું વીસ વર્ષનો છું.
[lang:ta]நான் இருபது வயதானவன்.
[lang:te]నా వయసు ఇరవై సంవత్సరాలు.
[lang:kn]ನನಗೆ ಇಪ್ಪತ್ತು ವರುಷಗಳಾಗಿವೆ.
[lang:ml]എനിക്ക് ഇരുപത് വയസ്സായി.
[lang:si]මට වයස අවුරුදු විස්සයි.
[lang:th]ฉันอายุยี่สิบปี
[lang:lo]ຂ້ອຍມີອາຍຸໄດ້ 20 ປີ.
[lang:my]ကျွန်ုပ်အသက်နှစ်ဆယ်ရှိပါပြီ
[lang:ka]Მე ოცი წლის ვარ.
[lang:am]ሃያ ዓመቴ ነው.
[lang:km]ខ្ញុំ​អាយុ​ម្ភៃ​ឆ្នាំ។
[lang:zh-Hans]我今年20岁了。
[lang:zh-Hant]我今年20歲了。
[lang:ko]나는 스무 살입니다.
[lang:co]Aghju vinti anni.
[lang:fy]Ik bin tweintich jier âld.
[lang:ha]Ina da shekara ashirin.
[lang:ig]Adị m afọ iri abụọ.
[lang:jv]Aku umur rong puluh taun.
[lang:ku]Ez bîst salî me.
[lang:lb]Ech sinn zwanzeg Joer al.
[lang:mg]Roapolo taona aho.
[lang:mt]Jiena għoxrin sena.
[lang:mi]E rua tekau oku tau.
[lang:ps]زه شل کلن یم.
[lang:sm]E luasefulu oʻu tausaga.
[lang:gd]Tha mi fichead bliadhna a dh'aois.
[lang:st]Ke lilemo li mashome a mabeli.
[lang:sn]Ini ndine makore makumi maviri.
[lang:sd]آئون ويهن سالن جي عمر آهيان.
[lang:so]Waxaan jiraa labaatan sano
[lang:su]Abdi umur dua puluh taun.
[lang:tg]Ман бистсолаам.
[lang:cy]Rwy'n ugain oed.
[lang:xh]Ndineminyaka engamashumi amabini.
[lang:yi]איך בין צוואַנציק יאָר אַלט.
[lang:yo]Mo jẹ ọdun ogún.
---

[key]Ako ay dalawang taong gulang na.
[lang:fil]Ako ay dalawang taong gulang na.
[lang:en]I am two years old.
[lang:ja]私は2歳です。
[lang:af]Ek is twee jaar oud.
[lang:az]İki yaşım var.
[lang:id]Umur saya dua tahun.
[lang:ms]Saya berumur dua tahun.
[lang:bs]Imam dvije godine.
[lang:ca]Tinc dos anys.
[lang:cs]Mám dva roky.
[lang:da]Jeg er to år gammel.
[lang:de]Ich bin zwei Jahre alt.
[lang:et]Olen kaks aastat vana.
[lang:es]Tengo dos años.
[lang:eu]Bi urte ditut.
[lang:fr]J'ai deux ans.
[lang:gl]Teño dous anos.
[lang:hr]Imam dvije godine.
[lang:zu]Ngineminyaka emibili.
[lang:is]Ég er tveggja ára.
[lang:it]Ho due anni.
[lang:sw]Nina umri wa miaka mbili.
[lang:lv]Man ir divi gadi.
[lang:lt]Man dveji metai.
[lang:hu]Két éves vagyok.
[lang:nl]Ik ben twee jaar.
[lang:no]Jeg er to år gammel.
[lang:uz]Men ikki yoshdaman.
[lang:pl]Mam dwa lata.
[lang:pt]Eu tenho dois anos
[lang:ro]Am doi ani.
[lang:sq]Jam dy vjec.
[lang:sk]Mám dva roky.
[lang:sl]Stara sem dve leti.
[lang:sr]Имам две године.
[lang:fi]Olen kahden vuoden ikäinen.
[lang:sv]Jag är två år gammal.
[lang:vi]Tôi hai tuổi
[lang:tr]İki yaşındayım.
[lang:be]Мне два гады.
[lang:bg]Аз съм на две години.
[lang:ky]Мен эки жаштамын.
[lang:kk]Мен екі жастамын.
[lang:mk]Имам две години.
[lang:mn]Би хоёр настай.
[lang:ru]Мне два года.
[lang:sr]Имам две године.
[lang:uk]Мені два роки.
[lang:el]Είμαι δύο ετών.
[lang:hy]Ես երկու տարեկան եմ:
[lang:iw]אני בת שנתיים.
[lang:ur]میری عمر دو سال ہے۔
[lang:ar]أنا سنتين من العمر.
[lang:fa]من دو ساله هستم
[lang:ne]म दुई बर्षको भएँ।
[lang:mr]मी दोन वर्षांचा आहे.
[lang:hi]मेरी उम्र दो साल है।
[lang:bn]আমার বয়স দুই বছর।
[lang:pa]ਮੈਂ ਦੋ ਸਾਲਾਂ ਦਾ ਹਾਂ
[lang:gu]હું બે વર્ષનો છું.
[lang:ta]எனக்கு இரண்டு வயது.
[lang:te]నా వయసు రెండేళ్లు.
[lang:kn]ನನಗೆ ಎರಡು ವರ್ಷ.
[lang:ml]എനിക്ക് രണ്ട് വയസ്സായി.
[lang:si]මට වයස අවුරුදු දෙකයි.
[lang:th]ฉันอายุสองปี
[lang:lo]ຂ້ອຍມີອາຍຸໄດ້ສອງປີ.
[lang:my]ငါနှစ်နှစ်ပါ
[lang:ka]ორი წლის ვარ.
[lang:am]የሁለት ዓመት ልጅ ነኝ ፡፡
[lang:km]ខ្ញុំមានអាយុពីរឆ្នាំ។
[lang:zh-Hans]我今年两岁。
[lang:zh-Hant]我今年兩歲。
[lang:ko]나는 두 살입니다.
[lang:co]Aghju dui anni.
[lang:fy]Ik bin twa jier âld.
[lang:ha]Ina shekara biyu.
[lang:ig]Adị m afọ abụọ.
[lang:jv]Aku umur rong taun.
[lang:ku]Ez du salî me.
[lang:lb]Ech sinn zwee Joer al.
[lang:mg]Roa taona aho.
[lang:mt]Jiena sentejn.
[lang:mi]E rua taku tau.
[lang:ps]زه دوه کلن یم
[lang:sm]E lua oʻu tausaga.
[lang:gd]Tha mi dà bhliadhna a dh'aois.
[lang:st]Ke lilemo li peli.
[lang:sn]Ndine makore maviri ekuzvarwa.
[lang:sd]آئون ٻه سال پراڻي آهيان.
[lang:so]Waxaan ahay laba sano jir.
[lang:su]Abdi umur dua taun.
[lang:tg]Ман ду солаам.
[lang:cy]Rwy'n ddwy flwydd oed.
[lang:xh]Ndineminyaka emibini ubudala.
[lang:yi]איך בין צוויי יאָר אַלט.
[lang:yo]Emi ni ọdun meji.
---

[key]Ako ay isang taong gulang na.
[lang:fil]Ako ay isang taong gulang na.
[lang:en]I am one year old.
[lang:ja]私は1歳です。
[lang:af]Ek is een jaar oud.
[lang:az]Bir yaşım var.
[lang:id]Saya berumur satu tahun.
[lang:ms]Saya berumur satu tahun.
[lang:bs]Imam godinu dana.
[lang:ca]Tinc un any.
[lang:cs]Je mi jeden rok.
[lang:da]Jeg er et år gammel.
[lang:de]Ich bin ein Jahr alt.
[lang:et]Olen üheaastane.
[lang:es]Tengo un año
[lang:eu]Urtebete dut.
[lang:fr]J'ai un an.
[lang:gl]Teño un ano.
[lang:hr]Imam godinu dana.
[lang:zu]Nginonyaka owodwa ubudala.
[lang:is]Ég er eins árs.
[lang:it]Io ho un anno
[lang:sw]Nina umri wa mwaka mmoja.
[lang:lv]Es esmu vienu gadu vecs.
[lang:lt]Aš vienerių metų.
[lang:hu]Egy éves vagyok.
[lang:nl]Ik ben een jaar oud.
[lang:no]Jeg er ett år gammel.
[lang:uz]Men bir yoshdaman.
[lang:pl]Mam rok.
[lang:pt]Eu tenho um ano de idade
[lang:ro]Am un an.
[lang:sq]Jam nje vjec.
[lang:sk]Mám jeden rok.
[lang:sl]Stara sem eno leto.
[lang:sr]Имам годину дана.
[lang:fi]Olen yhden vuoden vanha.
[lang:sv]Jag är ett år gammal.
[lang:vi]Tôi một tuổi
[lang:tr]Ben bir yaşındayım.
[lang:be]Мне адзін год.
[lang:bg]Аз съм на една година.
[lang:ky]Мен бир жыл жаштамын.
[lang:kk]Мен бір жастамын.
[lang:mk]Јас сум една година.
[lang:mn]Би нэг настай.
[lang:ru]Мне один год.
[lang:sr]Имам годину дана.
[lang:uk]Мені один рік.
[lang:el]Είμαι ένα έτος.
[lang:hy]Ես մեկ տարեկան եմ:
[lang:iw]אני בן שנה.
[lang:ur]میں ایک سال کا ہوں۔
[lang:ar]عمري سنة واحدة.
[lang:fa]من یک ساله هستم
[lang:ne]म एक बर्षको भएँ।
[lang:mr]मी एक वर्षाचा आहे.
[lang:hi]मैं एक साल का हूं।
[lang:bn]আমার বয়স এক বছর।
[lang:pa]ਮੈਂ ਇਕ ਸਾਲ ਦਾ ਹਾਂ
[lang:gu]હું એક વર્ષનો છું.
[lang:ta]எனக்கு ஒரு வயது.
[lang:te]నా వయసు ఒక సంవత్సరం.
[lang:kn]ನನಗೆ ಒಂದು ವರ್ಷ.
[lang:ml]എനിക്ക് ഒരു വയസ്സ്.
[lang:si]මට අවුරුද්දක් වයසයි.
[lang:th]ฉันอายุหนึ่งปี
[lang:lo]ຂ້ອຍມີອາຍຸ ໜຶ່ງ ປີ.
[lang:my]ကျွန်တော်တစ်နှစ်ပါ
[lang:ka]ერთი წლის ვარ.
[lang:am]አንድ አመት ነው ፡፡
[lang:km]ខ្ញុំមានអាយុមួយឆ្នាំ។
[lang:zh-Hans]我今年一岁。
[lang:zh-Hant]我今年一歲。
[lang:ko]나는 한 살입니다.
[lang:co]Sò un annu.
[lang:fy]Ik bin ien jier âld.
[lang:ha]Ni shekara daya.
[lang:ig]Adị m otu afọ.
[lang:jv]Aku setaun.
[lang:ku]Ez yek salî me.
[lang:lb]Ech sinn ee Joer al.
[lang:mg]Iray taona aho.
[lang:mt]Jiena sena.
[lang:mi]Ko ahau te tau kotahi.
[lang:ps]زه یو کلن یم
[lang:sm]E tasi oʻu tausaga.
[lang:gd]Tha mi aon bhliadhna a dh'aois.
[lang:st]Ke selemo se le seng.
[lang:sn]Ndine gore rimwe chete.
[lang:sd]مان هڪ سال آهيان.
[lang:so]Waxaan ahay hal sano jir
[lang:su]Abdi lami sataun.
[lang:tg]Ман яксола ҳастам.
[lang:cy]Rwy'n flwydd oed.
[lang:xh]Ndinonyaka omnye ubudala.
[lang:yi]איך בין איין יאָר אַלט.
[lang:yo]Emi ni ọdun kan.
---

[key]Ako ay limang taong gulang na.
[lang:fil]Ako ay limang taong gulang na.
[lang:en]I am five years old.
[lang:ja]私は5歳です。
[lang:af]Ek is vyf jaar oud.
[lang:az]Beş yaşım var.
[lang:id]Umur saya lima tahun.
[lang:ms]Saya berumur lima tahun.
[lang:bs]Imam pet godina.
[lang:ca]Tinc cinc anys.
[lang:cs]Je mi pět let.
[lang:da]Jeg er fem år gammel.
[lang:de]Ich bin fünf Jahre alt.
[lang:et]Olen viis aastat vana.
[lang:es]Tengo cinco años.
[lang:eu]Bost urte ditut.
[lang:fr]J'ai cinq ans.
[lang:gl]Teño cinco anos.
[lang:hr]Imam pet godina.
[lang:zu]Ngineminyaka emihlanu.
[lang:is]Ég er fimm ára.
[lang:it]Ho cinque anni.
[lang:sw]Nina umri wa miaka mitano.
[lang:lv]Man ir pieci gadi.
[lang:lt]Man penkeri metai.
[lang:hu]Öt éves vagyok.
[lang:nl]Ik ben vijf jaar oud.
[lang:no]Jeg er fem år gammel.
[lang:uz]Men besh yoshdaman.
[lang:pl]Mam pięć lat.
[lang:pt]Eu tenho cinco anos de idade.
[lang:ro]Eu am cinci ani.
[lang:sq]Jam 5 vjec.
[lang:sk]Mám päť rokov.
[lang:sl]Stara sem pet let.
[lang:sr]Имам пет година.
[lang:fi]Olen viisi vuotta vanha.
[lang:sv]Jag är fem år gammal.
[lang:vi]Tôi năm tuổi.
[lang:tr]Beş yaşındayım.
[lang:be]Мне пяць гадоў.
[lang:bg]Аз съм на пет години.
[lang:ky]Мен беш жаштамын.
[lang:kk]Мен бес жастамын.
[lang:mk]Имам пет години.
[lang:mn]Би таван настай.
[lang:ru]Мне пять лет.
[lang:sr]Имам пет година.
[lang:uk]Мені п’ять років.
[lang:el]Είμαι πέντε χρονών.
[lang:hy]Ես հինգ տարեկան եմ:
[lang:iw]אני בן חמש.
[lang:ur]میری عمر پانچ سال ہے۔
[lang:ar]عمري خمس سنوات.
[lang:fa]من پنج ساله ام.
[lang:ne]म पाँच बर्षको भएँ।
[lang:mr]मी पाच वर्षांचा आहे.
[lang:hi]मैं पांच साल का हूँ।
[lang:bn]আমি পাঁচ বছর বয়সী.
[lang:pa]ਮੈਂ ਪੰਜ ਸਾਲਾਂ ਦੀ ਹਾਂ
[lang:gu]હું પાંચ વર્ષનો છું.
[lang:ta]எனக்கு ஐந்து வயது.
[lang:te]నా వయసు ఐదేళ్లు.
[lang:kn]ನನಗೆ ಐದು ವರ್ಷ.
[lang:ml]എനിക്ക് അഞ്ച് വയസ്സായി.
[lang:si]මට වයස අවුරුදු පහයි.
[lang:th]ฉันอายุห้าขวบ
[lang:lo]ຂ້ອຍມີອາຍຸໄດ້ຫ້າປີ.
[lang:my]ကျွန်ုပ်အသက်ငါးနှစ်ရှိပါပြီ
[lang:ka]ხუთი წლის ვარ.
[lang:am]የአምስት ዓመት ልጅ ነኝ ፡፡
[lang:km]ខ្ញុំមានអាយុប្រាំឆ្នាំ។
[lang:zh-Hans]我今年五岁。
[lang:zh-Hant]我今年五歲。
[lang:ko]나는 다섯 살입니다.
[lang:co]Aghju cinque anni.
[lang:fy]Ik bin fiif jier âld.
[lang:ha]Ina da shekara biyar.
[lang:ig]Adị m afọ ise.
[lang:jv]Aku umur limang taun.
[lang:ku]Ez pênc salî me.
[lang:lb]Ech sinn fënnef Joer al.
[lang:mg]Dimy taona aho.
[lang:mt]Jiena ħames snin.
[lang:mi]E rima tau taku tau.
[lang:ps]زه پنځه کلن یم
[lang:sm]E lima oʻu tausaga.
[lang:gd]Tha mi còig bliadhna a dh'aois.
[lang:st]Ke lilemo li hlano.
[lang:sn]Ndine makore mashanu.
[lang:sd]آئون پنجن سالن جي عمر آهيان.
[lang:so]Waxaan ahay shan sano jir.
[lang:su]Abdi umur lima taun.
[lang:tg]Ман панҷсолаам.
[lang:cy]Rwy'n bum mlwydd oed.
[lang:xh]Ndineminyaka emihlanu.
[lang:yi]איך בין פינף יאָר אַלט.
[lang:yo]Mo jẹ ọdun marun.
---

[key]Ako ay pitong taong gulang na.
[lang:fil]Ako ay pitong taong gulang na.
[lang:en]I am seven years old.
[lang:ja]私は7歳です。
[lang:af]Ek is sewe jaar oud.
[lang:az]Yeddi yaşım var.
[lang:id]Aku berusia tujuh tahun.
[lang:ms]Saya berumur tujuh tahun.
[lang:bs]Imam sedam godina.
[lang:ca]Tinc set anys.
[lang:cs]Je mi sedm let.
[lang:da]Jeg er syv år gammel.
[lang:de]Ich bin sieben Jahre alt.
[lang:et]Olen seitse aastat vana.
[lang:es]Tengo siete años de edad.
[lang:eu]Zazpi urte ditut.
[lang:fr]Je suis âgé de sept ans.
[lang:gl]Teño sete anos.
[lang:hr]Imam sedam godina.
[lang:zu]Ngineminyaka eyisikhombisa.
[lang:is]Ég er sjö ára.
[lang:it]Io ho sette anni.
[lang:sw]Nina umri wa miaka saba.
[lang:lv]Man ir septiņi gadi.
[lang:lt]Man septyneri metai.
[lang:hu]Hét éves vagyok.
[lang:nl]Ik ben zeven jaar oud.
[lang:no]Jeg er syv år gammel.
[lang:uz]Men yetti yoshdaman.
[lang:pl]Mam siedem lat.
[lang:pt]Eu tenho sete anos de idade.
[lang:ro]Am șapte ani.
[lang:sq]Unë jam shtatë vjeç.
[lang:sk]Mám sedem rokov.
[lang:sl]Stara sem sedem let.
[lang:sr]Имам седам година.
[lang:fi]Olen seitsemän vuotta vanha.
[lang:sv]Jag är sju år gammal.
[lang:vi]Tôi bảy tuổi
[lang:tr]Ben yedi yaşındayım.
[lang:be]Мне сем гадоў.
[lang:bg]Аз съм на седем години.
[lang:ky]Мен жети жаштамын.
[lang:kk]Мен жеті жастамын.
[lang:mk]Имам седум години.
[lang:mn]Би долоон настай.
[lang:ru]Мне семь лет.
[lang:sr]Имам седам година.
[lang:uk]Мені сім років.
[lang:el]Ειμαι επτα χρονων.
[lang:hy]Ես յոթ տարեկան եմ:
[lang:iw]אני בן שבע.
[lang:ur]میں سات سال کا ہوں۔
[lang:ar]أنا عمري سبع سنوات.
[lang:fa]من هفت ساله هستم
[lang:ne]म सात बर्षको भएँ।
[lang:mr]मी सात वर्षांचा आहे.
[lang:hi]मैं सात वर्ष का हूँ।
[lang:bn]আমি সাত বছর বয়সী.
[lang:pa]ਮੈਂ ਸੱਤ ਸਾਲਾਂ ਦਾ ਹਾਂ.
[lang:gu]હું સાત વર્ષનો છું.
[lang:ta]எனக்கு ஏழு வயது.
[lang:te]నా వయసు ఏడు సంవత్సరాలు.
[lang:kn]ನನಗೆ ಏಳು ವರ್ಷ.
[lang:ml]എനിക്ക് ഏഴു വയസ്സായി.
[lang:si]මට වයස අවුරුදු හතයි.
[lang:th]ฉันอายุเจ็ดขวบ
[lang:lo]ຂ້ອຍມີອາຍຸໄດ້ເຈັດປີ.
[lang:my]ငါခုနစ်နှစ်ရှိပါပြီ
[lang:ka]შვიდი წლის ვარ.
[lang:am]እኔ የሰባት ዓመት ልጅ ነኝ።
[lang:km]ខ្ញុំមានអាយុ ៧ ឆ្នាំ។
[lang:zh-Hans]我今年七岁。
[lang:zh-Hant]我今年七歲。
[lang:ko]나는 일곱 살입니다.
[lang:co]Aghju sett'anni.
[lang:fy]Ik bin sân jier âld.
[lang:ha]Ina da shekara bakwai.
[lang:ig]Adị m afọ asaa.
[lang:jv]Aku umur pitung taun.
[lang:ku]Ez heft salî me.
[lang:lb]Ech sinn siwe Joer al.
[lang:mg]Fito taona aho izao.
[lang:mt]Jiena seba ’snin.
[lang:mi]Ko ahau tau e whitu.
[lang:ps]زه اوه کلن یم
[lang:sm]E fitu oʻu tausaga.
[lang:gd]Tha mi seachd bliadhna a dh'aois.
[lang:st]Ke lilemo li supileng.
[lang:sn]Ndine makore manomwe.
[lang:sd]آئون ست سال پراڻي آهيان.
[lang:so]Waxaan ahay toddoba jir.
[lang:su]Abdi umur tujuh taun.
[lang:tg]Ман ҳафтсола ҳастам.
[lang:cy]Rwy'n saith mlwydd oed.
[lang:xh]Ndineminyaka esixhenxe ubudala.
[lang:yi]איך בין זיבן יאָר אַלט.
[lang:yo]Ọmọ ọdun meje ni mi.
---

[key]Ako ay sampung taong gulang na.
[lang:fil]Ako ay sampung taong gulang na.
[lang:en]I am ten years old.
[lang:ja]私は十歳です。
[lang:af]Ek is tien jaar oud.
[lang:az]Mənim on yaşım var.
[lang:id]Saya berusia sepuluh tahun.
[lang:ms]Saya berumur sepuluh tahun.
[lang:bs]Imam deset godina.
[lang:ca]Tinc deu anys.
[lang:cs]Je mi deset let.
[lang:da]Jeg er ti år gammel.
[lang:de]Ich bin 10 Jahre alt.
[lang:et]Ma olen kümne aastane.
[lang:es]Tengo diez años.
[lang:eu]Hamar urte ditut.
[lang:fr]J'ai dix ans.
[lang:gl]Teño dez anos.
[lang:hr]Imam deset godina.
[lang:zu]Ngineminyaka eyishumi.
[lang:is]Ég er tíu ára.
[lang:it]Ho dieci anni.
[lang:sw]Nina umri wa miaka kumi.
[lang:lv]Man ir desmit gadu.
[lang:lt]Man dešimt metų.
[lang:hu]Tíz éves vagyok.
[lang:nl]Ik ben tien jaar oud.
[lang:no]Jeg er ti år gammel.
[lang:uz]Men o'n yoshdaman.
[lang:pl]Mam dziesięć lat.
[lang:pt]Eu tenho dez anos de idade.
[lang:ro]Am zece ani.
[lang:sq]Jam dhjete vjec.
[lang:sk]Mám desať rokov.
[lang:sl]Star sem deset let.
[lang:sr]Имам десет година.
[lang:fi]Olen kymmenen vuotta vanha.
[lang:sv]Jag är tio år gammal.
[lang:vi]Tôi mười tuổi.
[lang:tr]Ben on yaşındayım.
[lang:be]Мне дзесяць гадоў.
[lang:bg]На десет години съм.
[lang:ky]Мен он жаштамын.
[lang:kk]Мен он жастамын.
[lang:mk]Имам десет години.
[lang:mn]Би арван настай.
[lang:ru]Мне десять лет.
[lang:sr]Имам десет година.
[lang:uk]Мені десять років.
[lang:el]Είμαι δέκα χρονών.
[lang:hy]Ես տաս տարեկան եմ.
[lang:iw]אני בן עשר.
[lang:ur]میری عمر دس سال ہے۔
[lang:ar]عمري عشر سنوات.
[lang:fa]من ده ساله هستم.
[lang:ne]म दस बर्षको भएँ।
[lang:mr]मी दहा वर्षाचा आहे.
[lang:hi]मेरी आयु दस वर्ष है।
[lang:bn]আমি দশ বছর বয়সী.
[lang:pa]ਮੈਂ ਦਸ ਸਾਲ ਦੀ ਹਾਂ.
[lang:gu]હું દસ વર્ષનો છું.
[lang:ta]எனக்கு பத்து வயது.
[lang:te]నాకు పది సంవత్సరాలు.
[lang:kn]ನನಗೆ ಹತ್ತು ವರ್ಷ.
[lang:ml]എനിക്ക് പത്ത് വയസ്സ്.
[lang:si]මට වයස අවුරුදු දහයයි.
[lang:th]ผมอายุสิบขวบ.
[lang:lo]ຂ້ອຍມີອາຍຸສິບປີ.
[lang:my]ကျွန်ုပ်အသက်ဆယ်နှစ်ပါ
[lang:ka]ათი წლის ვარ.
[lang:am]የአስር ዓመት ልጅ ነኝ ፡፡
[lang:km]ខ្ញុំ​មាន​អាយុ​ដប់​ឆ្នាំ។
[lang:zh-Hans]我是十岁。
[lang:zh-Hant]我是十歲。
[lang:ko]나는 열살입니다.
[lang:co]Aghju dece anni.
[lang:fy]Ik bin tsien jier âld.
[lang:ha]Ina da shekara goma.
[lang:ig]Adị m afọ iri.
[lang:jv]Aku umur sepuluh taun.
[lang:ku]Ez deh salî me.
[lang:lb]Ech sinn zéng Joer al.
[lang:mg]Folo taona aho izao.
[lang:mt]Għandi għaxar snin.
[lang:mi]Tekau tau oku tau.
[lang:ps]زه لس کلن یم.
[lang:sm]E sefulu oʻu tausaga.
[lang:gd]Tha mi deich bliadhna a dh'aois.
[lang:st]Ke lilemo li leshome.
[lang:sn]Ndine makore gumi.
[lang:sd]آئون ڏهن سالن جي عمر آهيان.
[lang:so]Waxaan ahay toban sano
[lang:su]Abdi umur sapuluh taun.
[lang:tg]Ман даҳсолаам.
[lang:cy]Rwy'n ddeg oed.
[lang:xh]Ndineminyaka elishumi.
[lang:yi]איך בין צען יאָר אַלט.
[lang:yo]Mo jẹ ọdun mẹwa.
---

[key]Ako ay siyam na taong gulang na.
[lang:fil]Ako ay siyam na taong gulang na.
[lang:en]I am nine years old.
[lang:ja]私は9歳です。
[lang:af]Ek is nege jaar oud.
[lang:az]Doqquz yaşım var.
[lang:id]Saya berumur sembilan tahun.
[lang:ms]Saya berumur sembilan tahun.
[lang:bs]Imam devet godina.
[lang:ca]Tinc nou anys.
[lang:cs]Je mi devět.
[lang:da]Jeg er ni år gammel.
[lang:de]Ich bin neun Jahre alt.
[lang:et]Olen üheksa aastat vana.
[lang:es]Tengo nueve años.
[lang:eu]Bederatzi urte ditut.
[lang:fr]J'ai neuf ans.
[lang:gl]Teño nove anos.
[lang:hr]Imam devet godina.
[lang:zu]Ngineminyaka eyisishiyagalolunye.
[lang:is]Ég er níu ára.
[lang:it]Ho nove anni.
[lang:sw]Nina umri wa miaka tisa.
[lang:lv]Man ir deviņi gadi.
[lang:lt]Man devyneri metai.
[lang:hu]Kilenc éves vagyok.
[lang:nl]Ik ben negen jaar oud.
[lang:no]Jeg er ni år gammel.
[lang:uz]Men to'qqiz yoshdaman.
[lang:pl]Mam dziewięć lat.
[lang:pt]Eu tenho nove anos de idade.
[lang:ro]Am noua ani.
[lang:sq]Unë jam nëntë vjeç.
[lang:sk]Mám deväť rokov.
[lang:sl]Stara sem devet let.
[lang:sr]Ја имам девет година.
[lang:fi]Olen yhdeksänvuotias.
[lang:sv]Jag är nio år gammal.
[lang:vi]Tôi chín tuổi.
[lang:tr]Dokuz yaşındayım.
[lang:be]Мне дзевяць гадоў.
[lang:bg]Аз съм на девет години.
[lang:ky]Мен тогуз жаштамын.
[lang:kk]Мен тоғыз жастамын.
[lang:mk]Имам девет години.
[lang:mn]Би есөн настай.
[lang:ru]Мне девять лет.
[lang:sr]Ја имам девет година.
[lang:uk]Мені дев'ять років.
[lang:el]Είμαι εννιά ετών.
[lang:hy]Ես ինը տարեկան եմ:
[lang:iw]אני בן תשע.
[lang:ur]میں نو سال کا ہوں۔
[lang:ar]أنا فى التاسعة من عمرى.
[lang:fa]من نه ساله هستم
[lang:ne]म नौ बर्षको भएँ।
[lang:mr]मी नऊ वर्षांचा आहे.
[lang:hi]मैं नौ साल का हूँ।
[lang:bn]আমি নয় বছর বয়সী.
[lang:pa]ਮੈਂ ਨੌਂ ਸਾਲਾਂ ਦਾ ਹਾਂ.
[lang:gu]હું નવ વર્ષનો છું.
[lang:ta]எனக்கு ஒன்பது வயது.
[lang:te]నా వయసు తొమ్మిది.
[lang:kn]ನನಗೆ ಒಂಬತ್ತು ವರ್ಷ.
[lang:ml]എനിക്ക് ഒമ്പത് വയസ്സായി.
[lang:si]මට වයස අවුරුදු නවයයි.
[lang:th]ฉันอายุเก้าขวบ
[lang:lo]ຂ້ອຍມີອາຍຸເກົ້າປີ.
[lang:my]ကျွန်တော်ကိုးနှစ်ပါမယ်
[lang:ka]Ცხრა წლის ვარ.
[lang:am]የዘጠኝ ዓመት ልጅ ነኝ ፡፡
[lang:km]ខ្ញុំមានអាយុ ៩ ឆ្នាំហើយ។
[lang:zh-Hans]我今年九岁。
[lang:zh-Hant]我今年九歲。
[lang:ko]저는 아홉 살입니다.
[lang:co]Aghju nove anni.
[lang:fy]Ik bin njoggen jier âld.
[lang:ha]Ni shekara tara.
[lang:ig]Adị m afọ itoolu.
[lang:jv]Aku umur sangang taun.
[lang:ku]Ez neh salî me.
[lang:lb]Ech sinn néng Joer al.
[lang:mg]Sivy taona aho.
[lang:mt]Jiena disa 'snin.
[lang:mi]E iva oku tau.
[lang:ps]زه نهه کلن يم.
[lang:sm]E iva oʻu tausaga.
[lang:gd]Tha mi naoi bliadhna a dh'aois.
[lang:st]Ke lilemo li robong.
[lang:sn]Ndine makore mapfumbamwe.
[lang:sd]مان نو سال آهيان.
[lang:so]Waxaan ahay sagaal sano jir.
[lang:su]Abdi yuswa salapan taun.
[lang:tg]Ман нӯҳсолаам.
[lang:cy]Rwy'n naw mlwydd oed.
[lang:xh]Ndineminyaka elithoba.
[lang:yi]איך בין נייַן יאָר אַלט.
[lang:yo]Ọmọ ọdun mẹsan ni mi.
---

[key]Ako ay tatlong taong gulang na.
[lang:fil]Ako ay tatlong taong gulang na.
[lang:en]I am three years old.
[lang:ja]私は3歳です。
[lang:af]Ek is drie jaar oud.
[lang:az]Üç yaşım var.
[lang:id]Umur saya tiga tahun.
[lang:ms]Saya berumur tiga tahun.
[lang:bs]Imam tri godine.
[lang:ca]Tinc tres anys.
[lang:cs]Mám tři roky.
[lang:da]Jeg er tre år gammel.
[lang:de]Ich bin drei Jahre alt.
[lang:et]Olen kolm aastat vana.
[lang:es]Tengo tres años.
[lang:eu]Hiru urte ditut.
[lang:fr]J'ai trois ans.
[lang:gl]Teño tres anos.
[lang:hr]Imam tri godine.
[lang:zu]Ngineminyaka emithathu ubudala.
[lang:is]Ég er þriggja ára.
[lang:it]Ho tre anni.
[lang:sw]Nina miaka mitatu.
[lang:lv]Man ir trīs gadi.
[lang:lt]Man treji metai.
[lang:hu]Három éves vagyok.
[lang:nl]Ik ben drie jaar oud.
[lang:no]Jeg er tre år gammel.
[lang:uz]Men uch yoshdaman.
[lang:pl]Mam trzy lata.
[lang:pt]Eu tenho tres anos
[lang:ro]Am trei ani.
[lang:sq]Jam tre vjec.
[lang:sk]Mám tri roky.
[lang:sl]Stara sem tri leta.
[lang:sr]Имам три године.
[lang:fi]Olen kolme vuotta vanha.
[lang:sv]Jag är tre år.
[lang:vi]Tôi ba tuổi.
[lang:tr]Ben üç yaşındayım.
[lang:be]Мне тры гады.
[lang:bg]Аз съм на три години.
[lang:ky]Мен үч жаштамын.
[lang:kk]Мен үш жастамын.
[lang:mk]Имам три години.
[lang:mn]Би гурван настай.
[lang:ru]Мне три года.
[lang:sr]Имам три године.
[lang:uk]Мені три роки.
[lang:el]Είμαι τριών ετών.
[lang:hy]Ես երեք տարեկան եմ:
[lang:iw]אני בת שלוש.
[lang:ur]میری عمر تین سال ہے۔
[lang:ar]أنا من العمر ثلاث سنوات.
[lang:fa]من سه ساله هستم
[lang:ne]म तीन वर्षको भएँ।
[lang:mr]मी तीन वर्षांचा आहे.
[lang:hi]मैं तीन साल का हूं।
[lang:bn]আমার বয়স তিন বছর।
[lang:pa]ਮੈਂ ਤਿੰਨ ਸਾਲਾਂ ਦੀ ਹਾਂ
[lang:gu]હું ત્રણ વર્ષનો છું.
[lang:ta]எனக்கு மூன்று வயது.
[lang:te]నా వయసు మూడేళ్లు.
[lang:kn]ನನಗೆ ಮೂರು ವರ್ಷ.
[lang:ml]എനിക്ക് മൂന്ന് വയസ്സ്.
[lang:si]මට වයස අවුරුදු තුනක්.
[lang:th]ฉันอายุสามขวบ
[lang:lo]ຂ້ອຍມີອາຍຸສາມປີ.
[lang:my]ငါသုံးနှစ်ရှိပြီ
[lang:ka]სამი წლის ვარ.
[lang:am]የሦስት ዓመት ልጅ ነኝ ፡፡
[lang:km]ខ្ញុំមានអាយុបីឆ្នាំ។
[lang:zh-Hans]我今年三岁。
[lang:zh-Hant]我今年三歲。
[lang:ko]저는 3 살입니다.
[lang:co]Aghju trè anni.
[lang:fy]Ik bin trije jier âld.
[lang:ha]Ni shekara uku.
[lang:ig]Adị m afọ atọ.
[lang:jv]Aku umur telung taun.
[lang:ku]Ez sê salî me.
[lang:lb]Ech sinn dräi Joer al.
[lang:mg]Telo taona aho.
[lang:mt]Jiena tliet snin.
[lang:mi]Toru oku tau.
[lang:ps]زه درې کلن يم.
[lang:sm]E tolu oʻu tausaga.
[lang:gd]Tha mi trì bliadhna a dh'aois.
[lang:st]Ke lilemo li tharo.
[lang:sn]Ndine makore matatu ekuzvarwa.
[lang:sd]آئون ٽي سال پراڻي آهيان.
[lang:so]Waxaan ahay seddex sano jir.
[lang:su]Abdi umur tilu taun.
[lang:tg]Ман се солаам.
[lang:cy]Rwy'n dair oed.
[lang:xh]Ndineminyaka emithathu ubudala.
[lang:yi]איך בין דריי יאָר אַלט.
[lang:yo]Emi ni ọdun mẹta.
---

[key]Ako ay walong taong gulang na.
[lang:fil]Ako ay walong taong gulang na.
[lang:en]I am eight years old.
[lang:ja]私は8歳です。
[lang:af]Ek is agt jaar oud.
[lang:az]Səkkiz yaşım var.
[lang:id]Umur saya delapan tahun.
[lang:ms]Saya berumur lapan tahun.
[lang:bs]Imam osam godina.
[lang:ca]Tinc vuit anys.
[lang:cs]Je mi osm.
[lang:da]Jeg er otte år gammel.
[lang:de]Ich bin acht Jahre alt.
[lang:et]Olen kaheksa-aastane.
[lang:es]Tengo ocho años.
[lang:eu]Zortzi urte ditut.
[lang:fr]J'ai huit ans.
[lang:gl]Teño oito anos.
[lang:hr]Imam osam godina.
[lang:zu]Ngineminyaka eyisishiyagalombili.
[lang:is]Ég er átta ára.
[lang:it]Ho otto anni.
[lang:sw]Nina umri wa miaka nane.
[lang:lv]Man ir astoņi gadi.
[lang:lt]Man aštuoneri metai.
[lang:hu]Nyolc éves vagyok.
[lang:nl]Ik ben acht jaar oud.
[lang:no]Jeg er åtte år gammel.
[lang:uz]Men sakkiz yoshdaman.
[lang:pl]Mam osiem lat.
[lang:pt]Eu tenho oito anos.
[lang:ro]Am opt ani.
[lang:sq]Unë jam tetë vjeç.
[lang:sk]Mám osem rokov.
[lang:sl]Stara sem osem let.
[lang:sr]Имам осам година.
[lang:fi]Olen kahdeksan vuotta vanha.
[lang:sv]Jag är åtta år gammal.
[lang:vi]Tôi tám tuổi.
[lang:tr]Sekiz yaşındayım.
[lang:be]Мне восем гадоў.
[lang:bg]Аз съм на осем години.
[lang:ky]Мен сегиз жаштамын.
[lang:kk]Мен сегіз жастамын.
[lang:mk]Имам осум години.
[lang:mn]Би найман настай.
[lang:ru]Мне восемь лет.
[lang:sr]Имам осам година.
[lang:uk]Мені вісім років.
[lang:el]Είμαι οχτώ χρονών.
[lang:hy]Ես ութ տարեկան եմ:
[lang:iw]אני בת שמונה.
[lang:ur]میری عمر آٹھ سال ہے۔
[lang:ar]عمري ثمانية سنين.
[lang:fa]من هشت سالم است.
[lang:ne]म आठ बर्षको भएँ।
[lang:mr]मी आठ वर्षांचा आहे.
[lang:hi]मैं आठ साल का हूँ।
[lang:bn]আমি আট বছর বয়সী.
[lang:pa]ਮੈਂ ਅੱਠ ਸਾਲਾਂ ਦਾ ਹਾਂ
[lang:gu]હું આઠ વર્ષનો છું.
[lang:ta]எனக்கு எட்டு வயது.
[lang:te]నా వయసు ఎనిమిది సంవత్సరాలు.
[lang:kn]ನನಗೆ ಎಂಟು ವರ್ಷ.
[lang:ml]എനിക്ക് എട്ട് വയസ്സായി.
[lang:si]මට වයස අවුරුදු අටයි.
[lang:th]ฉันอายุแปดขวบ
[lang:lo]ຂ້ອຍມີອາຍຸແປດປີ.
[lang:my]ငါရှစ်နှစ်ရှိပါပြီ
[lang:ka]რვა წლის ვარ.
[lang:am]የስምንት ዓመት ልጅ ነኝ።
[lang:km]ខ្ញុំមានអាយុប្រាំបីឆ្នាំ។
[lang:zh-Hans]我八岁了。
[lang:zh-Hant]我八歲了。
[lang:ko]저는 8 살입니다.
[lang:co]Aghju ottu anni.
[lang:fy]Ik bin acht jier âld.
[lang:ha]Ina da shekara takwas.
[lang:ig]Adị m afọ asatọ.
[lang:jv]Aku wolung taun.
[lang:ku]Ez heşt salî me.
[lang:lb]Ech sinn aacht Joer.
[lang:mg]Valo taona aho.
[lang:mt]Jiena tmien snin.
[lang:mi]Kei te waru oku tau oku tau.
[lang:ps]زه اته کلن يم.
[lang:sm]E valu oʻu tausaga.
[lang:gd]Tha mi ochd bliadhna a dh'aois.
[lang:st]Ke lilemo li robeli.
[lang:sn]Ndine makore masere.
[lang:sd]آئون اٺ سال پراڻي آهيان.
[lang:so]Waxaan ahay sideed sano jir.
[lang:su]Abdi yuswa dalapan taun.
[lang:tg]Ман ҳаштсолаам.
[lang:cy]Rwy'n wyth mlwydd oed.
[lang:xh]Ndineminyaka esibhozo ubudala.
[lang:yi]איך בין אַכט יאָר אַלט.
[lang:yo]Ọmọ ọdun mẹjọ ni mi.
---

[key]linker
[lang:fil]linker
[lang:en]linker
[lang:ja]リンカ
[lang:af]linkerkant
[lang:az]bağlayıcı
[lang:id]linker
[lang:ms]penyambung
[lang:bs]povezivač
[lang:ca]linker
[lang:cs]linker
[lang:da]linker
[lang:de]Linker
[lang:et]linker
[lang:es]enlazador
[lang:eu]estekatzailearen
[lang:fr]éditeur de liens
[lang:gl]ligazón
[lang:hr]linker
[lang:zu]isixhumanisi
[lang:is]tengi
[lang:it]linker
[lang:sw]kiunga
[lang:lv]linkeris
[lang:lt]linkeris
[lang:hu]linker
[lang:nl]linker
[lang:no]linker
[lang:uz]bog'lovchi
[lang:pl]linker
[lang:pt]vinculador
[lang:ro]agent de legătură
[lang:sq]linker
[lang:sk]linker
[lang:sl]povezovalec
[lang:sr]линкер
[lang:fi]linkkeri
[lang:sv]linker
[lang:vi]liên kết
[lang:tr]bağlayıcı
[lang:be]лінкер
[lang:bg]линкер
[lang:ky]Азаттык
[lang:kk]байланыстырушы
[lang:mk]врска
[lang:mn]холбогч
[lang:ru]линкер
[lang:sr]линкер
[lang:uk]лінкер
[lang:el]linker
[lang:hy]կապող
[lang:iw]מקשר
[lang:ur]لنکر
[lang:ar]رابط
[lang:fa]پیوند دهنده
[lang:ne]लिंकर
[lang:mr]दुवा साधणारा
[lang:hi]लिंकर
[lang:bn]linker
[lang:pa]ਲਿੰਕਰ
[lang:gu]કડી કરનાર
[lang:ta]இணைப்பான்
[lang:te]లింకర్
[lang:kn]ಲಿಂಕರ್
[lang:ml]ലിങ്കർ
[lang:si]සම්බන්ධකය
[lang:th]ลิงเกอร์
[lang:lo]linker
[lang:my]linker
[lang:ka]ბმული
[lang:am]አገናኝ
[lang:km]linker
[lang:zh-Hans]连接器
[lang:zh-Hant]連接器
[lang:ko]링커
[lang:co]ligame
[lang:fy]linker
[lang:ha]mahaɗa
[lang:ig]njikọta
[lang:jv]panyambung
[lang:ku]girêdan
[lang:lb]Linker
[lang:mg]linker
[lang:mt]linker
[lang:mi]honohono
[lang:ps]تړونکی
[lang:sm]sootaga
[lang:gd]inneal-ceangail
[lang:st]sehokelo
[lang:sn]linker
[lang:sd]لنڪ
[lang:so]xiriiriyaha
[lang:su]linker
[lang:tg]пайвандкунанда
[lang:cy]cysylltydd
[lang:xh]ikhonkco
[lang:yi]לינקער
[lang:yo]aladapo
---

