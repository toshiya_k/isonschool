const fs = require("fs");
const svg2png = require("svg2png");
var mjAPI = require("mathjax-node");

mjAPI.config({
	MathJax: {
		SVG: {
			//font: "Latin-Modern"
			font: "Asana-Math"
			//font: "STIX-web"
		},
	}
});
mjAPI.start();

const WIDTH = 3;
const HEIGHT = 4;

(async function () {

	var filepaths = [];
	var width = 1280;
	var height = 720;

	var status = 0;

	for (var i = 2; i < process.argv.length; i++) {
		var a = process.argv[i];
		if (a == "--width" || a == "-w") {
			status = WIDTH;
		} else if (a == "--height" || a == "-h") {
			status = HEIGHT;
		} else if (status == 0) {
			filepaths.push(a);
		} else if (status == WIDTH) {
			width = a - 0;
			status = 0;
		} else if (status == HEIGHT) {
			height = a - 0;
			status = 0;
		} else {
			console.error("??" + a);
		}
	}

	for (var i = 0; i < filepaths.length; i += 2) {
		var input = fs.readFileSync(filepaths[i], "utf8");

		//input = "\\usepackage{cmbright}\n\n" + input
		//input = "\\usepackage{sansmath}\n\n" + input
		const data = await mjAPI.typeset({
			math: input,
			format: "TeX", // "inline-TeX", "MathML"
			svg: true,
		});

		var s = data.svg.replace(/#ffffff/g, "rgba(0,0,0,0)");
		writePng(s, width, height, filepaths[i + 1]);
	}

})();

function writePng(svg, width, height, filepath) {
	svg2png(svg, { width: width, height: height })
		.then(buffer => fs.writeFileSync(filepath, buffer))
		.catch(e => console.error(e));
}