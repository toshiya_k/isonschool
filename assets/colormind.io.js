if (!window.a) {
	window.a = [];
}

var f = function() {
	var t = '';
	for (h of document.getElementsByTagName('h3')) {
		t += ',' + h.innerText;
	}
	t = t.substr(1);

	if (window.a.indexOf(t) < 0) {
		window.a.push(t);
	}
	window.setTimeout(f, 1000);

	if (document.getElementsByClassName('color-progress').length == 0) {
		document.getElementsByClassName('randomize')[0].click();
	}
	var s = '';
	for (u of window.a) {
		s += u + "\n";
	}
	console.log(s);
}

window.setTimeout(f, 1000);
