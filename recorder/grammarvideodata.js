var data = [
  {
    "Exercise": {
      "Name": "A",
      "YoutubeLang": "es",
      "YoutubeDescription": "",
      "Problems": [
        {
          "ProblemNo": 0,
          "OriginalCandidates": [
            {
              "Word": "They",
              "Ruby": "ðe"
            },
            {
              "Word": "drink",
              "Ruby": "drɪŋk"
            },
            {
              "Word": "tea",
              "Ruby": "ti"
            }
          ],
          "ChangeCandidatePos": 0,
          "ChangeCandidates": [
            {
              "Word": "I",
              "Ruby": "aɪ"
            },
            {
              "Word": "drink",
              "Ruby": "drɪŋk"
            },
            {
              "Word": "",
              "Ruby": ""
            }
          ],
          "ChangeMorphemes": [
            {
              "Word": "I",
              "Root": "",
              "FeatureSet": null
            },
            {
              "Word": "",
              "Root": "",
              "FeatureSet": [
                "ns"
              ]
            },
            {
              "Word": "",
              "Root": "",
              "FeatureSet": null
            }
          ],
          "AnswerCandidates": [
            {
              "Word": "I",
              "Ruby": "aɪ"
            },
            {
              "Word": "drink",
              "Ruby": "drɪŋk"
            },
            {
              "Word": "tea",
              "Ruby": "ti"
            }
          ]
        },
        {
          "ProblemNo": 1,
          "OriginalCandidates": [
            {
              "Word": "I",
              "Ruby": "aɪ"
            },
            {
              "Word": "drink",
              "Ruby": "drɪŋk"
            },
            {
              "Word": "tea",
              "Ruby": "ti"
            }
          ],
          "ChangeCandidatePos": 0,
          "ChangeCandidates": [
            {
              "Word": "She",
              "Ruby": "ʃi"
            },
            {
              "Word": "drinks",
              "Ruby": "drɪŋks"
            },
            {
              "Word": "",
              "Ruby": ""
            }
          ],
          "ChangeMorphemes": [
            {
              "Word": "She",
              "Root": "",
              "FeatureSet": null
            },
            {
              "Word": "",
              "Root": "",
              "FeatureSet": [
                "s"
              ]
            },
            {
              "Word": "",
              "Root": "",
              "FeatureSet": null
            }
          ],
          "AnswerCandidates": [
            {
              "Word": "She",
              "Ruby": "ʃi"
            },
            {
              "Word": "drinks",
              "Ruby": "drɪŋks"
            },
            {
              "Word": "tea",
              "Ruby": "ti"
            }
          ]
        },
        {
          "ProblemNo": 2,
          "OriginalCandidates": [
            {
              "Word": "She",
              "Ruby": "ʃi"
            },
            {
              "Word": "drinks",
              "Ruby": "drɪŋks"
            },
            {
              "Word": "tea",
              "Ruby": "ti"
            }
          ],
          "ChangeCandidatePos": 0,
          "ChangeCandidates": [
            {
              "Word": "We",
              "Ruby": "wi"
            },
            {
              "Word": "drink",
              "Ruby": "drɪŋk"
            },
            {
              "Word": "",
              "Ruby": ""
            }
          ],
          "ChangeMorphemes": [
            {
              "Word": "We",
              "Root": "",
              "FeatureSet": null
            },
            {
              "Word": "",
              "Root": "",
              "FeatureSet": [
                "ns"
              ]
            },
            {
              "Word": "",
              "Root": "",
              "FeatureSet": null
            }
          ],
          "AnswerCandidates": [
            {
              "Word": "We",
              "Ruby": "wi"
            },
            {
              "Word": "drink",
              "Ruby": "drɪŋk"
            },
            {
              "Word": "tea",
              "Ruby": "ti"
            }
          ]
        },
        {
          "ProblemNo": 3,
          "OriginalCandidates": [
            {
              "Word": "We",
              "Ruby": "wi"
            },
            {
              "Word": "drink",
              "Ruby": "drɪŋk"
            },
            {
              "Word": "tea",
              "Ruby": "ti"
            }
          ],
          "ChangeCandidatePos": 0,
          "ChangeCandidates": [
            {
              "Word": "You",
              "Ruby": "ju"
            },
            {
              "Word": "drink",
              "Ruby": "drɪŋk"
            },
            {
              "Word": "",
              "Ruby": ""
            }
          ],
          "ChangeMorphemes": [
            {
              "Word": "You",
              "Root": "",
              "FeatureSet": null
            },
            {
              "Word": "",
              "Root": "",
              "FeatureSet": [
                "ns"
              ]
            },
            {
              "Word": "",
              "Root": "",
              "FeatureSet": null
            }
          ],
          "AnswerCandidates": [
            {
              "Word": "You",
              "Ruby": "ju"
            },
            {
              "Word": "drink",
              "Ruby": "drɪŋk"
            },
            {
              "Word": "tea",
              "Ruby": "ti"
            }
          ]
        },
        {
          "ProblemNo": 4,
          "OriginalCandidates": [
            {
              "Word": "You",
              "Ruby": "ju"
            },
            {
              "Word": "drink",
              "Ruby": "drɪŋk"
            },
            {
              "Word": "tea",
              "Ruby": "ti"
            }
          ],
          "ChangeCandidatePos": 2,
          "ChangeCandidates": [
            {
              "Word": "",
              "Ruby": ""
            },
            {
              "Word": "",
              "Ruby": ""
            },
            {
              "Word": "coffee",
              "Ruby": "ˈkɔfi"
            }
          ],
          "ChangeMorphemes": [
            {
              "Word": "",
              "Root": "",
              "FeatureSet": null
            },
            {
              "Word": "",
              "Root": "",
              "FeatureSet": null
            },
            {
              "Word": "coffee",
              "Root": "",
              "FeatureSet": null
            }
          ],
          "AnswerCandidates": [
            {
              "Word": "You",
              "Ruby": "ju"
            },
            {
              "Word": "drink",
              "Ruby": "drɪŋk"
            },
            {
              "Word": "coffee",
              "Ruby": "ˈkɔfi"
            }
          ]
        },
        {
          "ProblemNo": 5,
          "OriginalCandidates": [
            {
              "Word": "You",
              "Ruby": "ju"
            },
            {
              "Word": "drink",
              "Ruby": "drɪŋk"
            },
            {
              "Word": "coffee",
              "Ruby": "ˈkɔfi"
            }
          ],
          "ChangeCandidatePos": 0,
          "ChangeCandidates": [
            {
              "Word": "He",
              "Ruby": "hi"
            },
            {
              "Word": "drinks",
              "Ruby": "drɪŋks"
            },
            {
              "Word": "",
              "Ruby": ""
            }
          ],
          "ChangeMorphemes": [
            {
              "Word": "He",
              "Root": "",
              "FeatureSet": null
            },
            {
              "Word": "",
              "Root": "",
              "FeatureSet": [
                "s"
              ]
            },
            {
              "Word": "",
              "Root": "",
              "FeatureSet": null
            }
          ],
          "AnswerCandidates": [
            {
              "Word": "He",
              "Ruby": "hi"
            },
            {
              "Word": "drinks",
              "Ruby": "drɪŋks"
            },
            {
              "Word": "coffee",
              "Ruby": "ˈkɔfi"
            }
          ]
        },
        {
          "ProblemNo": 6,
          "OriginalCandidates": [
            {
              "Word": "He",
              "Ruby": "hi"
            },
            {
              "Word": "drinks",
              "Ruby": "drɪŋks"
            },
            {
              "Word": "coffee",
              "Ruby": "ˈkɔfi"
            }
          ],
          "ChangeCandidatePos": 0,
          "ChangeCandidates": [
            {
              "Word": "They",
              "Ruby": "ðe"
            },
            {
              "Word": "drink",
              "Ruby": "drɪŋk"
            },
            {
              "Word": "",
              "Ruby": ""
            }
          ],
          "ChangeMorphemes": [
            {
              "Word": "They",
              "Root": "",
              "FeatureSet": null
            },
            {
              "Word": "",
              "Root": "",
              "FeatureSet": [
                "ns"
              ]
            },
            {
              "Word": "",
              "Root": "",
              "FeatureSet": null
            }
          ],
          "AnswerCandidates": [
            {
              "Word": "They",
              "Ruby": "ðe"
            },
            {
              "Word": "drink",
              "Ruby": "drɪŋk"
            },
            {
              "Word": "coffee",
              "Ruby": "ˈkɔfi"
            }
          ]
        },
        {
          "ProblemNo": 7,
          "OriginalCandidates": [
            {
              "Word": "They",
              "Ruby": "ðe"
            },
            {
              "Word": "drink",
              "Ruby": "drɪŋk"
            },
            {
              "Word": "coffee",
              "Ruby": "ˈkɔfi"
            }
          ],
          "ChangeCandidatePos": 1,
          "ChangeCandidates": [
            {
              "Word": "",
              "Ruby": ""
            },
            {
              "Word": "like",
              "Ruby": "laɪk"
            },
            {
              "Word": "",
              "Ruby": ""
            }
          ],
          "ChangeMorphemes": [
            {
              "Word": "",
              "Root": "",
              "FeatureSet": null
            },
            {
              "Word": "like",
              "Root": "like",
              "FeatureSet": [
                "ns"
              ]
            },
            {
              "Word": "",
              "Root": "",
              "FeatureSet": null
            }
          ],
          "AnswerCandidates": [
            {
              "Word": "They",
              "Ruby": "ðe"
            },
            {
              "Word": "like",
              "Ruby": "laɪk"
            },
            {
              "Word": "coffee",
              "Ruby": "ˈkɔfi"
            }
          ]
        },
        {
          "ProblemNo": 8,
          "OriginalCandidates": [
            {
              "Word": "They",
              "Ruby": "ðe"
            },
            {
              "Word": "like",
              "Ruby": "laɪk"
            },
            {
              "Word": "coffee",
              "Ruby": "ˈkɔfi"
            }
          ],
          "ChangeCandidatePos": 0,
          "ChangeCandidates": [
            {
              "Word": "She",
              "Ruby": "ʃi"
            },
            {
              "Word": "likes",
              "Ruby": "laɪks"
            },
            {
              "Word": "",
              "Ruby": ""
            }
          ],
          "ChangeMorphemes": [
            {
              "Word": "She",
              "Root": "",
              "FeatureSet": null
            },
            {
              "Word": "",
              "Root": "",
              "FeatureSet": [
                "s"
              ]
            },
            {
              "Word": "",
              "Root": "",
              "FeatureSet": null
            }
          ],
          "AnswerCandidates": [
            {
              "Word": "She",
              "Ruby": "ʃi"
            },
            {
              "Word": "likes",
              "Ruby": "laɪks"
            },
            {
              "Word": "coffee",
              "Ruby": "ˈkɔfi"
            }
          ]
        },
        {
          "ProblemNo": 9,
          "OriginalCandidates": [
            {
              "Word": "She",
              "Ruby": "ʃi"
            },
            {
              "Word": "likes",
              "Ruby": "laɪks"
            },
            {
              "Word": "coffee",
              "Ruby": "ˈkɔfi"
            }
          ],
          "ChangeCandidatePos": 0,
          "ChangeCandidates": [
            {
              "Word": "You",
              "Ruby": "ju"
            },
            {
              "Word": "like",
              "Ruby": "laɪk"
            },
            {
              "Word": "",
              "Ruby": ""
            }
          ],
          "ChangeMorphemes": [
            {
              "Word": "You",
              "Root": "",
              "FeatureSet": null
            },
            {
              "Word": "",
              "Root": "",
              "FeatureSet": [
                "ns"
              ]
            },
            {
              "Word": "",
              "Root": "",
              "FeatureSet": null
            }
          ],
          "AnswerCandidates": [
            {
              "Word": "You",
              "Ruby": "ju"
            },
            {
              "Word": "like",
              "Ruby": "laɪk"
            },
            {
              "Word": "coffee",
              "Ruby": "ˈkɔfi"
            }
          ]
        }
      ],
      "VideoFile": "",
      "AnimeticCaptions": null
    },
    "Sentence": {
      "SerieName": "grammar-en-beginner",
      "No": 6,
      "VideoTitle": "s",
      "Boxes": [
        {
          "Pos": 0,
          "PersonName": "z",
          "Candidates": [
            {
              "Word": "I",
              "Root": "",
              "FeatureSet": null,
              "Conditions": [
                {
                  "Pos": 1,
                  "Morphemes": [
                    {
                      "Word": "",
                      "Root": "",
                      "FeatureSet": [
                        "ns"
                      ]
                    }
                  ]
                }
              ],
              "PersonConditions": null
            },
            {
              "Word": "He",
              "Root": "",
              "FeatureSet": null,
              "Conditions": [
                {
                  "Pos": 1,
                  "Morphemes": [
                    {
                      "Word": "",
                      "Root": "",
                      "FeatureSet": [
                        "s"
                      ]
                    }
                  ]
                }
              ],
              "PersonConditions": null
            },
            {
              "Word": "She",
              "Root": "",
              "FeatureSet": null,
              "Conditions": [
                {
                  "Pos": 1,
                  "Morphemes": [
                    {
                      "Word": "",
                      "Root": "",
                      "FeatureSet": [
                        "s"
                      ]
                    }
                  ]
                }
              ],
              "PersonConditions": null
            },
            {
              "Word": "They",
              "Root": "",
              "FeatureSet": null,
              "Conditions": [
                {
                  "Pos": 1,
                  "Morphemes": [
                    {
                      "Word": "",
                      "Root": "",
                      "FeatureSet": [
                        "ns"
                      ]
                    }
                  ]
                }
              ],
              "PersonConditions": null
            },
            {
              "Word": "We",
              "Root": "",
              "FeatureSet": null,
              "Conditions": [
                {
                  "Pos": 1,
                  "Morphemes": [
                    {
                      "Word": "",
                      "Root": "",
                      "FeatureSet": [
                        "ns"
                      ]
                    }
                  ]
                }
              ],
              "PersonConditions": null
            },
            {
              "Word": "You",
              "Root": "",
              "FeatureSet": null,
              "Conditions": [
                {
                  "Pos": 1,
                  "Morphemes": [
                    {
                      "Word": "",
                      "Root": "",
                      "FeatureSet": [
                        "ns"
                      ]
                    }
                  ]
                }
              ],
              "PersonConditions": null
            }
          ]
        },
        {
          "Pos": 1,
          "PersonName": "z",
          "Candidates": [
            {
              "Word": "like",
              "Root": "like",
              "FeatureSet": [
                "ns"
              ],
              "Conditions": null,
              "PersonConditions": null
            },
            {
              "Word": "likes",
              "Root": "like",
              "FeatureSet": [
                "s"
              ],
              "Conditions": null,
              "PersonConditions": null
            },
            {
              "Word": "drink",
              "Root": "drink",
              "FeatureSet": [
                "ns"
              ],
              "Conditions": null,
              "PersonConditions": null
            },
            {
              "Word": "drinks",
              "Root": "drink",
              "FeatureSet": [
                "s"
              ],
              "Conditions": null,
              "PersonConditions": null
            }
          ]
        },
        {
          "Pos": 2,
          "PersonName": "z",
          "Candidates": [
            {
              "Word": "coffee",
              "Root": "",
              "FeatureSet": null,
              "Conditions": null,
              "PersonConditions": null
            },
            {
              "Word": "tea",
              "Root": "",
              "FeatureSet": null,
              "Conditions": null,
              "PersonConditions": null
            }
          ]
        }
      ],
      "Exercises": [
        {
          "Name": "A",
          "YoutubeLang": "es",
          "YoutubeDescription": "",
          "Problems": [
            {
              "ProblemNo": 0,
              "OriginalCandidates": [
                {
                  "Word": "They",
                  "Ruby": "ðe"
                },
                {
                  "Word": "drink",
                  "Ruby": "drɪŋk"
                },
                {
                  "Word": "tea",
                  "Ruby": "ti"
                }
              ],
              "ChangeCandidatePos": 0,
              "ChangeCandidates": [
                {
                  "Word": "I",
                  "Ruby": "aɪ"
                },
                {
                  "Word": "drink",
                  "Ruby": "drɪŋk"
                },
                {
                  "Word": "",
                  "Ruby": ""
                }
              ],
              "ChangeMorphemes": [
                {
                  "Word": "I",
                  "Root": "",
                  "FeatureSet": null
                },
                {
                  "Word": "",
                  "Root": "",
                  "FeatureSet": [
                    "ns"
                  ]
                },
                {
                  "Word": "",
                  "Root": "",
                  "FeatureSet": null
                }
              ],
              "AnswerCandidates": [
                {
                  "Word": "I",
                  "Ruby": "aɪ"
                },
                {
                  "Word": "drink",
                  "Ruby": "drɪŋk"
                },
                {
                  "Word": "tea",
                  "Ruby": "ti"
                }
              ]
            },
            {
              "ProblemNo": 1,
              "OriginalCandidates": [
                {
                  "Word": "I",
                  "Ruby": "aɪ"
                },
                {
                  "Word": "drink",
                  "Ruby": "drɪŋk"
                },
                {
                  "Word": "tea",
                  "Ruby": "ti"
                }
              ],
              "ChangeCandidatePos": 0,
              "ChangeCandidates": [
                {
                  "Word": "She",
                  "Ruby": "ʃi"
                },
                {
                  "Word": "drinks",
                  "Ruby": "drɪŋks"
                },
                {
                  "Word": "",
                  "Ruby": ""
                }
              ],
              "ChangeMorphemes": [
                {
                  "Word": "She",
                  "Root": "",
                  "FeatureSet": null
                },
                {
                  "Word": "",
                  "Root": "",
                  "FeatureSet": [
                    "s"
                  ]
                },
                {
                  "Word": "",
                  "Root": "",
                  "FeatureSet": null
                }
              ],
              "AnswerCandidates": [
                {
                  "Word": "She",
                  "Ruby": "ʃi"
                },
                {
                  "Word": "drinks",
                  "Ruby": "drɪŋks"
                },
                {
                  "Word": "tea",
                  "Ruby": "ti"
                }
              ]
            },
            {
              "ProblemNo": 2,
              "OriginalCandidates": [
                {
                  "Word": "She",
                  "Ruby": "ʃi"
                },
                {
                  "Word": "drinks",
                  "Ruby": "drɪŋks"
                },
                {
                  "Word": "tea",
                  "Ruby": "ti"
                }
              ],
              "ChangeCandidatePos": 0,
              "ChangeCandidates": [
                {
                  "Word": "We",
                  "Ruby": "wi"
                },
                {
                  "Word": "drink",
                  "Ruby": "drɪŋk"
                },
                {
                  "Word": "",
                  "Ruby": ""
                }
              ],
              "ChangeMorphemes": [
                {
                  "Word": "We",
                  "Root": "",
                  "FeatureSet": null
                },
                {
                  "Word": "",
                  "Root": "",
                  "FeatureSet": [
                    "ns"
                  ]
                },
                {
                  "Word": "",
                  "Root": "",
                  "FeatureSet": null
                }
              ],
              "AnswerCandidates": [
                {
                  "Word": "We",
                  "Ruby": "wi"
                },
                {
                  "Word": "drink",
                  "Ruby": "drɪŋk"
                },
                {
                  "Word": "tea",
                  "Ruby": "ti"
                }
              ]
            },
            {
              "ProblemNo": 3,
              "OriginalCandidates": [
                {
                  "Word": "We",
                  "Ruby": "wi"
                },
                {
                  "Word": "drink",
                  "Ruby": "drɪŋk"
                },
                {
                  "Word": "tea",
                  "Ruby": "ti"
                }
              ],
              "ChangeCandidatePos": 0,
              "ChangeCandidates": [
                {
                  "Word": "You",
                  "Ruby": "ju"
                },
                {
                  "Word": "drink",
                  "Ruby": "drɪŋk"
                },
                {
                  "Word": "",
                  "Ruby": ""
                }
              ],
              "ChangeMorphemes": [
                {
                  "Word": "You",
                  "Root": "",
                  "FeatureSet": null
                },
                {
                  "Word": "",
                  "Root": "",
                  "FeatureSet": [
                    "ns"
                  ]
                },
                {
                  "Word": "",
                  "Root": "",
                  "FeatureSet": null
                }
              ],
              "AnswerCandidates": [
                {
                  "Word": "You",
                  "Ruby": "ju"
                },
                {
                  "Word": "drink",
                  "Ruby": "drɪŋk"
                },
                {
                  "Word": "tea",
                  "Ruby": "ti"
                }
              ]
            },
            {
              "ProblemNo": 4,
              "OriginalCandidates": [
                {
                  "Word": "You",
                  "Ruby": "ju"
                },
                {
                  "Word": "drink",
                  "Ruby": "drɪŋk"
                },
                {
                  "Word": "tea",
                  "Ruby": "ti"
                }
              ],
              "ChangeCandidatePos": 2,
              "ChangeCandidates": [
                {
                  "Word": "",
                  "Ruby": ""
                },
                {
                  "Word": "",
                  "Ruby": ""
                },
                {
                  "Word": "coffee",
                  "Ruby": "ˈkɔfi"
                }
              ],
              "ChangeMorphemes": [
                {
                  "Word": "",
                  "Root": "",
                  "FeatureSet": null
                },
                {
                  "Word": "",
                  "Root": "",
                  "FeatureSet": null
                },
                {
                  "Word": "coffee",
                  "Root": "",
                  "FeatureSet": null
                }
              ],
              "AnswerCandidates": [
                {
                  "Word": "You",
                  "Ruby": "ju"
                },
                {
                  "Word": "drink",
                  "Ruby": "drɪŋk"
                },
                {
                  "Word": "coffee",
                  "Ruby": "ˈkɔfi"
                }
              ]
            },
            {
              "ProblemNo": 5,
              "OriginalCandidates": [
                {
                  "Word": "You",
                  "Ruby": "ju"
                },
                {
                  "Word": "drink",
                  "Ruby": "drɪŋk"
                },
                {
                  "Word": "coffee",
                  "Ruby": "ˈkɔfi"
                }
              ],
              "ChangeCandidatePos": 0,
              "ChangeCandidates": [
                {
                  "Word": "He",
                  "Ruby": "hi"
                },
                {
                  "Word": "drinks",
                  "Ruby": "drɪŋks"
                },
                {
                  "Word": "",
                  "Ruby": ""
                }
              ],
              "ChangeMorphemes": [
                {
                  "Word": "He",
                  "Root": "",
                  "FeatureSet": null
                },
                {
                  "Word": "",
                  "Root": "",
                  "FeatureSet": [
                    "s"
                  ]
                },
                {
                  "Word": "",
                  "Root": "",
                  "FeatureSet": null
                }
              ],
              "AnswerCandidates": [
                {
                  "Word": "He",
                  "Ruby": "hi"
                },
                {
                  "Word": "drinks",
                  "Ruby": "drɪŋks"
                },
                {
                  "Word": "coffee",
                  "Ruby": "ˈkɔfi"
                }
              ]
            },
            {
              "ProblemNo": 6,
              "OriginalCandidates": [
                {
                  "Word": "He",
                  "Ruby": "hi"
                },
                {
                  "Word": "drinks",
                  "Ruby": "drɪŋks"
                },
                {
                  "Word": "coffee",
                  "Ruby": "ˈkɔfi"
                }
              ],
              "ChangeCandidatePos": 0,
              "ChangeCandidates": [
                {
                  "Word": "They",
                  "Ruby": "ðe"
                },
                {
                  "Word": "drink",
                  "Ruby": "drɪŋk"
                },
                {
                  "Word": "",
                  "Ruby": ""
                }
              ],
              "ChangeMorphemes": [
                {
                  "Word": "They",
                  "Root": "",
                  "FeatureSet": null
                },
                {
                  "Word": "",
                  "Root": "",
                  "FeatureSet": [
                    "ns"
                  ]
                },
                {
                  "Word": "",
                  "Root": "",
                  "FeatureSet": null
                }
              ],
              "AnswerCandidates": [
                {
                  "Word": "They",
                  "Ruby": "ðe"
                },
                {
                  "Word": "drink",
                  "Ruby": "drɪŋk"
                },
                {
                  "Word": "coffee",
                  "Ruby": "ˈkɔfi"
                }
              ]
            },
            {
              "ProblemNo": 7,
              "OriginalCandidates": [
                {
                  "Word": "They",
                  "Ruby": "ðe"
                },
                {
                  "Word": "drink",
                  "Ruby": "drɪŋk"
                },
                {
                  "Word": "coffee",
                  "Ruby": "ˈkɔfi"
                }
              ],
              "ChangeCandidatePos": 1,
              "ChangeCandidates": [
                {
                  "Word": "",
                  "Ruby": ""
                },
                {
                  "Word": "like",
                  "Ruby": "laɪk"
                },
                {
                  "Word": "",
                  "Ruby": ""
                }
              ],
              "ChangeMorphemes": [
                {
                  "Word": "",
                  "Root": "",
                  "FeatureSet": null
                },
                {
                  "Word": "like",
                  "Root": "like",
                  "FeatureSet": [
                    "ns"
                  ]
                },
                {
                  "Word": "",
                  "Root": "",
                  "FeatureSet": null
                }
              ],
              "AnswerCandidates": [
                {
                  "Word": "They",
                  "Ruby": "ðe"
                },
                {
                  "Word": "like",
                  "Ruby": "laɪk"
                },
                {
                  "Word": "coffee",
                  "Ruby": "ˈkɔfi"
                }
              ]
            },
            {
              "ProblemNo": 8,
              "OriginalCandidates": [
                {
                  "Word": "They",
                  "Ruby": "ðe"
                },
                {
                  "Word": "like",
                  "Ruby": "laɪk"
                },
                {
                  "Word": "coffee",
                  "Ruby": "ˈkɔfi"
                }
              ],
              "ChangeCandidatePos": 0,
              "ChangeCandidates": [
                {
                  "Word": "She",
                  "Ruby": "ʃi"
                },
                {
                  "Word": "likes",
                  "Ruby": "laɪks"
                },
                {
                  "Word": "",
                  "Ruby": ""
                }
              ],
              "ChangeMorphemes": [
                {
                  "Word": "She",
                  "Root": "",
                  "FeatureSet": null
                },
                {
                  "Word": "",
                  "Root": "",
                  "FeatureSet": [
                    "s"
                  ]
                },
                {
                  "Word": "",
                  "Root": "",
                  "FeatureSet": null
                }
              ],
              "AnswerCandidates": [
                {
                  "Word": "She",
                  "Ruby": "ʃi"
                },
                {
                  "Word": "likes",
                  "Ruby": "laɪks"
                },
                {
                  "Word": "coffee",
                  "Ruby": "ˈkɔfi"
                }
              ]
            },
            {
              "ProblemNo": 9,
              "OriginalCandidates": [
                {
                  "Word": "She",
                  "Ruby": "ʃi"
                },
                {
                  "Word": "likes",
                  "Ruby": "laɪks"
                },
                {
                  "Word": "coffee",
                  "Ruby": "ˈkɔfi"
                }
              ],
              "ChangeCandidatePos": 0,
              "ChangeCandidates": [
                {
                  "Word": "You",
                  "Ruby": "ju"
                },
                {
                  "Word": "like",
                  "Ruby": "laɪk"
                },
                {
                  "Word": "",
                  "Ruby": ""
                }
              ],
              "ChangeMorphemes": [
                {
                  "Word": "You",
                  "Root": "",
                  "FeatureSet": null
                },
                {
                  "Word": "",
                  "Root": "",
                  "FeatureSet": [
                    "ns"
                  ]
                },
                {
                  "Word": "",
                  "Root": "",
                  "FeatureSet": null
                }
              ],
              "AnswerCandidates": [
                {
                  "Word": "You",
                  "Ruby": "ju"
                },
                {
                  "Word": "like",
                  "Ruby": "laɪk"
                },
                {
                  "Word": "coffee",
                  "Ruby": "ˈkɔfi"
                }
              ]
            }
          ],
          "VideoFile": "",
          "AnimeticCaptions": null
        }
      ],
      "CannotChange": false
    },
    "Serie": {
      "SerieName": "grammar-en-beginner",
      "ExerciseName": "A",
      "NextPublishAt": "",
      "PublishHours": 12,
      "ProblemNum": 10,
      "AudioLang": "en",
      "VideoLang": "en",
      "Voices": [
        {
          "PersonName": "x",
          "VoiceKeys": [
            "F1",
            "M1",
            "F2"
          ]
        },
        {
          "PersonName": "y",
          "VoiceKeys": [
            "M1",
            "M2",
            "F3"
          ]
        },
        {
          "PersonName": "z",
          "VoiceKeys": [
            "F2",
            "M2",
            "F3"
          ]
        },
        {
          "PersonName": "v",
          "VoiceKeys": [
            "M2",
            "M1",
            "F1"
          ]
        },
        {
          "PersonName": "w",
          "VoiceKeys": [
            "F3",
            "M2",
            "F1"
          ]
        }
      ],
      "SerieTitle": "English grammar exercises",
      "YoutubeTitle": "{Speak English!} ⭐️ {Grammar} #[1] [:VideoTitle:]",
      "YoutubeDescription": "{Learn English grammar!} {Change a word in a sentence.}\n\n[:GrammarDescriptionWithRuby:]",
      "YoutubeTags": "English grammar,英语语法,Grammar bahasa inggris,gramática inglesa,grammatica inglese,английская грамматика,हिंदी व्याकरण,የእንግሊዝኛ ሰዋስው,영문법,英文法,Grammaire Anglaise,ngữ pháp tiếng Anh,انگریزی قواعد,قواعد اللغة الإنجليزية",
      "RubyFunc": "enipa",
      "HidesRuby": true,
      "CaptionKeyNames": [
        {
          "CaptionKey": "ruby",
          "CaptionName": "Pronunciation - IPA"
        }
      ],
      "VoiceKeyNames": [
        {
          "VoiceKey": "M1",
          "VoiceName": "en-US-Wavenet-B"
        },
        {
          "VoiceKey": "M2",
          "VoiceName": "en-US-Wavenet-A"
        },
        {
          "VoiceKey": "M3",
          "VoiceName": "en-US-Wavenet-D"
        },
        {
          "VoiceKey": "F1",
          "VoiceName": "en-US-Wavenet-F"
        },
        {
          "VoiceKey": "F2",
          "VoiceName": "en-US-Wavenet-C"
        },
        {
          "VoiceKey": "F3",
          "VoiceName": "en-US-Wavenet-E"
        }
      ],
      "Palettes": [
        {
          "Colours": [
            "EDE9E0",
            "729FBE",
            "A45664",
            "BD734D",
            "203040"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "EEF0E6",
            "9EA09F",
            "857973",
            "989895",
            "353235"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "F5F4F1",
            "D8B851",
            "8AA5BA",
            "92595B",
            "455265"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "F1F3F3",
            "718594",
            "5C758E",
            "648390",
            "1B1F27"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "F1F0E8",
            "98AABA",
            "737B8C",
            "8E939D",
            "292133"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "EFEFEB",
            "9FA0A1",
            "757588",
            "6674AC",
            "2131C0"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "F9F8F5",
            "7EA0B4",
            "93D4D4",
            "A68E5E",
            "4A6556"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "F9F8F7",
            "94938E",
            "BF4C42",
            "A77B74",
            "272939"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "EFECE9",
            "8991A0",
            "643957",
            "7E6D26",
            "142036"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "FDFDFD",
            "E6D09D",
            "DAA734",
            "CA97EB",
            "89291F"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "EEF1EE",
            "9197C9",
            "CD679B",
            "955877",
            "472121"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "F5F5F8",
            "A4B2DD",
            "8C5ED7",
            "A18DA7",
            "373543"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "FBFBFC",
            "EED0A0",
            "EFAB64",
            "EDC591",
            "A5652B"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "E8EEDB",
            "EEB58F",
            "E77AA5",
            "E7938F",
            "E2586B"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "F1F5F2",
            "9CA49B",
            "83878D",
            "698DA9",
            "2F394F"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "F2EEF0",
            "9C6F41",
            "8E9089",
            "745942",
            "493E3A"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "E9E2DF",
            "E19A67",
            "BE4963",
            "5B8EB7",
            "3A2F51"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "F3F1EA",
            "96806A",
            "9F9B97",
            "847B7B",
            "49423E"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "F9F9F9",
            "7D7984",
            "96846D",
            "775D5D",
            "37333A"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "FCFAFB",
            "B3B9C2",
            "D99355",
            "A7AAA5",
            "4A4F68"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "E9E3DA",
            "EAAC48",
            "B395CB",
            "958278",
            "936B5A"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "FBFBFA",
            "A1D9E0",
            "ECBC2C",
            "B0A3D4",
            "E52A65"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "FAF8F9",
            "9A92A5",
            "BB7D6F",
            "B66F61",
            "2F2F3B"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "C7A795",
            "B22A43",
            "902148",
            "592725",
            "1C2022"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "DEDFD4",
            "96B089",
            "6D6D7B",
            "7F7372",
            "2C2830"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "F6F7F3",
            "B1E3E9",
            "F4A941",
            "9D7A7E",
            "46798B"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "E5CDD8",
            "85A6B4",
            "307A63",
            "17ABDA",
            "24283C"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "E5E0E5",
            "81B9E6",
            "3A79B4",
            "2185DB",
            "253B68"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "EEC693",
            "8B6B71",
            "B9955B",
            "844944",
            "242935"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "F0F3F2",
            "E4646E",
            "C4554B",
            "93646C",
            "3F2317"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "F9F4F2",
            "99D1D7",
            "5084A0",
            "9D455C",
            "221723"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "F8F5F2",
            "E0BD66",
            "E56E35",
            "D09C8E",
            "8B6267"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "F2F0EC",
            "9E8E84",
            "CE5D2F",
            "C68142",
            "383237"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "F9F9F9",
            "8EA1B7",
            "9BB1C2",
            "6493D1",
            "283E67"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "E9DCD0",
            "7C6A4C",
            "C1945B",
            "584039",
            "2A2A31"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "F4F7F5",
            "EC9E28",
            "9CABBE",
            "7B8096",
            "344F76"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "F6F6F6",
            "A7AAC5",
            "F59D4B",
            "C64460",
            "4F6C86"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "F3F7F6",
            "988BA2",
            "CC5862",
            "B63F53",
            "422A3F"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "FDFBF9",
            "BDD7E5",
            "C9DEE8",
            "9ECFE6",
            "3FA8D7"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "F0E7CB",
            "8A9C96",
            "868562",
            "9E9C7B",
            "403F4C"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "EC734F",
            "C76353",
            "965350",
            "95666B",
            "1A1D20"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "F3F5F1",
            "E28E5D",
            "D95552",
            "A69191",
            "48312D"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "F1F2F0",
            "807E88",
            "96A0A3",
            "7B737F",
            "3E5668"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "F6F6F6",
            "E79291",
            "DB7876",
            "947E95",
            "46486B"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "FAF9F8",
            "B38597",
            "A57437",
            "8D625C",
            "3E302E"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "F3F6F4",
            "F3B053",
            "ECA692",
            "9A9BB9",
            "42637F"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "F5F5F1",
            "989690",
            "AB7B5F",
            "B87362",
            "272D31"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "F3F6F7",
            "5485AD",
            "9890A3",
            "AD425C",
            "641E36"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "F9F9F9",
            "ABA2B1",
            "988FAE",
            "8B8B9E",
            "2E2C62"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "F9F8F8",
            "9EAEEC",
            "A5A3EA",
            "7D7AE4",
            "1B23E2"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "F3E1C8",
            "997D5C",
            "8FC3C4",
            "9E6C60",
            "236EA4"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "F8F7F8",
            "A0C3E5",
            "4D8CD8",
            "3B59A3",
            "35528A"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "FAF9F9",
            "A59DA0",
            "DDB660",
            "E97734",
            "544C55"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "F9F8F7",
            "A3C4D3",
            "559ABA",
            "707B81",
            "376071"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "3493EA",
            "737291",
            "3249A4",
            "5B2584",
            "19161E"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "F7F7F7",
            "868F8A",
            "C0AC89",
            "7B7B77",
            "35403B"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "F7F6F4",
            "D36A51",
            "B5705D",
            "70312A",
            "402222"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "F6F5F6",
            "8CB1B3",
            "BDAB9E",
            "A19A9D",
            "715B5C"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "F7F4EF",
            "DB9D80",
            "CA909C",
            "7E8BA1",
            "97876D"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "F5F6F7",
            "A88C7B",
            "B9B5B4",
            "4E7A84",
            "935578"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "FAF9FA",
            "7C717D",
            "B68F74",
            "833D4F",
            "282539"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "F0F3F3",
            "A6A173",
            "7F67A0",
            "7281A0",
            "2F2E3D"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "EAEDE9",
            "7D7763",
            "25A7AD",
            "6D5C55",
            "1F232A"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "F6FAF9",
            "F1CCB4",
            "DC948C",
            "3D89B9",
            "995561"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "EBF0EF",
            "5B9B9F",
            "7F7F5F",
            "75797D",
            "131617"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "F2F3F4",
            "30D5D5",
            "B74C69",
            "90857F",
            "21293F"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "EFE8CA",
            "E6BF5F",
            "A6519E",
            "899EB5",
            "26385F"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "ECEBE3",
            "8CA6DE",
            "476BA7",
            "557BCC",
            "1D3065"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "E8DCB0",
            "789F84",
            "449482",
            "AD744F",
            "1F1B1F"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "F8F7F6",
            "C48C61",
            "A19B95",
            "A4583D",
            "35383A"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "F3F5F7",
            "AB9C99",
            "9C9798",
            "818080",
            "373141"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "F8FAFB",
            "D2C88D",
            "EED53F",
            "AD5C59",
            "343C44"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "F5F6F6",
            "B1903B",
            "8E849B",
            "A94154",
            "39292B"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "F2F1EA",
            "75AB9E",
            "36A194",
            "796E64",
            "2B2A33"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "F1F0E8",
            "7F704B",
            "819F81",
            "75613F",
            "343023"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "F1F1EC",
            "836C78",
            "B3305D",
            "635556",
            "311F33"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "F5F6F4",
            "93C0D9",
            "C18D83",
            "B7928D",
            "447E8D"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "F3F2F3",
            "9B99A6",
            "CF845C",
            "81838D",
            "4F4349"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "EFEBED",
            "837B7E",
            "5E4E56",
            "716B72",
            "140F11"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "EDF0F3",
            "C39563",
            "789BCD",
            "8A84CB",
            "453F7C"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "FAF7F7",
            "C6BCCC",
            "ACC0DB",
            "7D95BE",
            "497CDB"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "F5F2F5",
            "A98153",
            "8AB8C9",
            "906152",
            "475652"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "F6F9F7",
            "F2B497",
            "EC8561",
            "B18CE4",
            "4D6FED"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "E8E3D9",
            "9EADA5",
            "BC6E37",
            "7C959A",
            "242A39"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "F1F5F7",
            "B3A8A2",
            "3B9DDE",
            "7B6F83",
            "17315C"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "F4F4F3",
            "B3A59F",
            "C99E89",
            "94838F",
            "77556B"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "F0E6C9",
            "CB9F88",
            "EB5C5F",
            "877F9B",
            "23435F"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "F5F5F5",
            "A1A4AC",
            "E2BD9A",
            "8FA0C0",
            "7A93AC"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "F5F5EF",
            "777F71",
            "928B88",
            "95686C",
            "292B2C"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "EFF1F0",
            "9C9271",
            "AF8457",
            "9D593F",
            "191B24"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "F3F1F4",
            "817F7E",
            "99A2AD",
            "7C738D",
            "4A3E5E"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "FBF9FA",
            "AD9490",
            "BDB17C",
            "8C8B9F",
            "8E7D4E"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "F9F7F7",
            "B88173",
            "D89A78",
            "AC472A",
            "364853"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "F0EFE7",
            "898380",
            "E86969",
            "7C332E",
            "343138"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "E6E1E2",
            "8AAAC2",
            "3F7AB0",
            "4863AA",
            "203249"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "F2EFEF",
            "9AB8C1",
            "5E7191",
            "A1C6E2",
            "16141A"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "DFE1E3",
            "4D82B7",
            "5887A3",
            "565D73",
            "202E42"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "E0E6C7",
            "DEAF56",
            "E06D47",
            "C05CA2",
            "E14A30"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "F6F5F6",
            "7C7D75",
            "8BB4C3",
            "7A747C",
            "3F3B47"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "F5F6F7",
            "728394",
            "7C9597",
            "647781",
            "142833"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "F6F3F1",
            "6CBFEB",
            "A9573D",
            "AA8A90",
            "2B3156"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "EBEBE3",
            "B0A796",
            "ED7735",
            "9B7663",
            "313548"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "F9F9F8",
            "B7BDC1",
            "A0AAB5",
            "818CD0",
            "1384E6"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "F1F0EE",
            "AF8A77",
            "98655E",
            "7F5648",
            "77363E"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "F8FAF8",
            "7F7C7D",
            "A8A7A5",
            "6D7F9F",
            "4F5062"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "F2F5F2",
            "929592",
            "EEDC30",
            "6F7453",
            "2C2A1F"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "FAFAF7",
            "DEAAA5",
            "EEC1CA",
            "D8A999",
            "F46D6A"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "F7DF85",
            "D7AFED",
            "AD7FF1",
            "B564D3",
            "3C6FBC"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "F4F4F1",
            "8395A7",
            "686D7A",
            "7C8AA6",
            "213150"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "F5F1EE",
            "989190",
            "A54C42",
            "9D8163",
            "1D191B"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "F5F4F3",
            "BB7D63",
            "A09BB8",
            "65728A",
            "8C665F"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "F3F9F3",
            "EBBCAD",
            "F0B099",
            "CD9597",
            "DB6472"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "F0F2F2",
            "6F938D",
            "B09157",
            "8C6758",
            "282C2F"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "EFE0E0",
            "3C9E99",
            "966B70",
            "98676B",
            "302732"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "F5F7F1",
            "B09E89",
            "F7D777",
            "97707D",
            "4A627D"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "F0F2F0",
            "A0AFB0",
            "CC4F3D",
            "BC8C74",
            "3B3346"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "F9F8F7",
            "9F8775",
            "A0B1B9",
            "886A64",
            "495F58"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "F5F6F5",
            "ECDF22",
            "88C544",
            "97673E",
            "2F3140"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "F4F9F5",
            "EF8B71",
            "D36578",
            "946955",
            "292229"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "F4F5F8",
            "A4C6C8",
            "8B93B8",
            "65C6B9",
            "3E73B3"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "F2F0F2",
            "57BAE4",
            "19649C",
            "3FA5CC",
            "1D2042"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "F7F9F9",
            "F4D37B",
            "9FACBB",
            "C07377",
            "3C7E96"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "F9FCF9",
            "F6D25B",
            "EF7EB5",
            "D275E7",
            "4846A5"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "F9F9FA",
            "94958D",
            "C8C8B7",
            "828893",
            "3C3D56"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "F7F8F7",
            "F2AA32",
            "B8B3A8",
            "8F9294",
            "3E3751"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "F3F6F7",
            "E3BE66",
            "C56A8B",
            "EC807E",
            "637095"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "EFEFE7",
            "9C8064",
            "867E9F",
            "91567A",
            "3E496E"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "F7F5F6",
            "B28855",
            "C3AA8E",
            "8C8373",
            "5F3D26"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "F8F4F5",
            "7F886D",
            "B2A5B6",
            "6C7687",
            "45819E"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "F9F6F6",
            "9D7363",
            "9A959C",
            "826961",
            "4A3C3A"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "F9F8F7",
            "9BAAAD",
            "C5422D",
            "CC8D6C",
            "4F4C6D"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "F5F1F0",
            "886B4A",
            "B4A791",
            "7E433B",
            "532C41"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "F5F6F8",
            "D67056",
            "D4916F",
            "A2354A",
            "28233B"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "F4FBF2",
            "847159",
            "758285",
            "EE6E6C",
            "332D33"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "F1ECDB",
            "867668",
            "6B625F",
            "585754",
            "2D2328"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "F7F7F7",
            "857F81",
            "96B1BA",
            "7B757A",
            "46444E"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "F2F2F1",
            "E6A3D2",
            "F0AFE7",
            "4C8CEC",
            "B3649F"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "E9DBE6",
            "CCA56C",
            "C89AAE",
            "3390A7",
            "ED7A69"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "F6F3F6",
            "94BED8",
            "7EAFB0",
            "8795A1",
            "44808E"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "F8F5F3",
            "62A3A6",
            "668EAC",
            "964259",
            "231725"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "E2E8E7",
            "F57663",
            "F13B53",
            "8E7F8C",
            "2E1C29"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "F9F9FB",
            "C69F99",
            "DDB79B",
            "857B84",
            "E43729"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "F8FAFB",
            "EFC398",
            "F2C79E",
            "AEA7A6",
            "EF7618"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "F2ECE5",
            "C87671",
            "97CCD3",
            "856266",
            "3B3954"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "FBFBFA",
            "ACB6BA",
            "CCA98B",
            "9D8F90",
            "5A637E"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "F5F8F9",
            "CE6F68",
            "847D79",
            "875951",
            "231828"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "F2F1EE",
            "9A9B9E",
            "A53D47",
            "CF8A58",
            "2C343B"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "F1ECE2",
            "B4AEE2",
            "6378C2",
            "7779EB",
            "3B52C5"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "FCFBFB",
            "BBD1F6",
            "85AAE2",
            "9FBCE9",
            "3F79D9"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "F5F3F0",
            "8F9C9C",
            "E7B645",
            "894D3C",
            "422C2C"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "D69FE1",
            "417ECA",
            "5A3DB5",
            "352D7D",
            "221B3F"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "F9F9F7",
            "A08D80",
            "65C6F7",
            "948B91",
            "2B8ECB"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "F1F1EE",
            "82BAC2",
            "5F6F87",
            "8096A5",
            "213A48"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "F4F5F5",
            "A09E9E",
            "A48F73",
            "8C8483",
            "33373C"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "FAFAFC",
            "DAA9ED",
            "A777EB",
            "9283D9",
            "5F57D9"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "F5F3F1",
            "9DA6A3",
            "A34440",
            "BE8845",
            "2C222B"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "F5F6F5",
            "D5A499",
            "D54E3B",
            "A38B95",
            "712E16"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "FEFEFC",
            "8AAFC4",
            "D5739B",
            "8EA6BC",
            "261E3D"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "F5F8F8",
            "9A7D69",
            "92C082",
            "70686B",
            "585D44"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "F9F9F7",
            "9A9299",
            "B0948C",
            "7A7780",
            "422D40"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "FAFAF9",
            "92869E",
            "A39AB0",
            "8C8C8C",
            "2D2C2F"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "F7F4EF",
            "878781",
            "B1989A",
            "888C94",
            "56526D"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "F5F6F4",
            "959EB0",
            "F5A344",
            "AA575B",
            "3B3034"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "F9F5F6",
            "CC9056",
            "9F9AA5",
            "A35C4D",
            "41353A"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "F7F7F4",
            "B48A5C",
            "8B8594",
            "786F7B",
            "252F47"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "E2E8E7",
            "9DA9BF",
            "7B7A7A",
            "2965BB",
            "22212D"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "F4F3F3",
            "9CBAC4",
            "74609D",
            "1FC6D4",
            "252F38"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "EBEBEB",
            "BC9D51",
            "8F9497",
            "846A6D",
            "4F4E3F"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "F4F5F4",
            "CE7E90",
            "E2755A",
            "5B8EBB",
            "55465E"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "F7F5F8",
            "A39A7A",
            "8CB2BB",
            "738DB0",
            "3070A6"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "F0EDEB",
            "A68181",
            "9DA84B",
            "795C45",
            "2B2838"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "EBE6D7",
            "CE6C5D",
            "CD9A28",
            "A7372F",
            "34272D"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "F9FAF9",
            "F4D66A",
            "F0AF18",
            "CE9AA9",
            "293B53"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "F3F8F7",
            "F0B833",
            "8ECED9",
            "E75A3F",
            "1B294E"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "F1EADD",
            "B78B7F",
            "A4987C",
            "776764",
            "434337"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "F5F5F8",
            "9D8E7B",
            "8E9AA0",
            "8D7483",
            "4D6687"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "F2F4F2",
            "78776F",
            "9A9CA1",
            "7F6B66",
            "30323D"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "F5F5F3",
            "3EE3DA",
            "DF4D41",
            "DD8674",
            "293A52"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "E5DDB2",
            "E8BA4C",
            "9572B1",
            "559A9B",
            "24142F"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "F2F2F0",
            "D69239",
            "807574",
            "5E5B56",
            "8F313C"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "F2F3F4",
            "64A4BB",
            "BC9364",
            "B1566C",
            "1D2738"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "FCFBFB",
            "8C8C84",
            "ACB4A9",
            "838B83",
            "33433C"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "F2F4F3",
            "CAB1BC",
            "ED9D60",
            "8C8EA9",
            "AA4C9D"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "F1F7F2",
            "EBCD98",
            "E7846A",
            "A39F7E",
            "9F3624"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "F2F1F2",
            "A68A5F",
            "97989D",
            "876F76",
            "61656A"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "F6F9FB",
            "E3AD57",
            "E1A7A2",
            "A48099",
            "C83025"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "F8F6F5",
            "72CEEA",
            "2EBCEA",
            "F076B5",
            "4663B1"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "F5F4F0",
            "CA8753",
            "939498",
            "BE412B",
            "37383B"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "F5F3F0",
            "DB6E75",
            "8991A5",
            "494147",
            "382F33"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "F7F6F6",
            "BFAE75",
            "64A6CE",
            "AA7A8B",
            "3F6587"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "F4F3F3",
            "BB98A0",
            "9D9EA2",
            "734EE7",
            "3F4BA8"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "F5F2F3",
            "40A7A7",
            "4598BF",
            "195EA7",
            "20232F"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "F5F7F6",
            "F194AB",
            "F14545",
            "CB9187",
            "442F2E"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "F2F3F3",
            "998168",
            "808282",
            "7D746E",
            "2F3338"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "F8F9F9",
            "B1C1C4",
            "E2857C",
            "E09C78",
            "77767B"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "F5F0F5",
            "BC956A",
            "D8A693",
            "957572",
            "BD5B21"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "F9F7F6",
            "C87350",
            "94A595",
            "826645",
            "2B2D3E"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "F4F2F2",
            "858E89",
            "5D5353",
            "858781",
            "141417"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "F7F7F9",
            "EEA869",
            "46BBAB",
            "AE8B84",
            "9E606E"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "F8F8F6",
            "DD9292",
            "ACD85D",
            "E54555",
            "2C3453"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "EDF0EE",
            "8F9E66",
            "7A8797",
            "877067",
            "31363D"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "F3F7F8",
            "DF7A6E",
            "4A9951",
            "947232",
            "241725"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "F9FAFA",
            "A3A5AF",
            "F59481",
            "B48988",
            "685453"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "F0F6F5",
            "E3B8BA",
            "F18DB3",
            "EF71A3",
            "C672A3"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "EFF0F1",
            "868DAB",
            "796E91",
            "6869A8",
            "191732"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "FEFDFC",
            "A4BE49",
            "D9B2C3",
            "BA5548",
            "4B523B"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "EBDAD5",
            "958CAE",
            "B34745",
            "782DB3",
            "19214A"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "F4F5F5",
            "7B7C7C",
            "83959C",
            "7C747A",
            "201D20"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "F3F5F4",
            "8B8288",
            "A09176",
            "87787C",
            "222025"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "F0F2ED",
            "797B7A",
            "838D8C",
            "6C7178",
            "1F2721"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "F6F5F5",
            "A29D9C",
            "DFBD49",
            "8A8390",
            "4E5260"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "F5F5F7",
            "A9989B",
            "DBC29C",
            "9C8589",
            "6B87A9"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "E3EBD2",
            "68A16C",
            "64BB7D",
            "406D47",
            "2E3935"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "EDEDE5",
            "948C8C",
            "A26049",
            "8A7B7C",
            "231E23"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "F7F9F8",
            "F46E66",
            "6975A0",
            "AF6B6F",
            "3D333C"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "F9F8F7",
            "DDA051",
            "9FB5BB",
            "946E60",
            "585563"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "EFF2F5",
            "B49B73",
            "B88871",
            "84837B",
            "734B36"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "F2F4F4",
            "9A94A9",
            "8E646E",
            "908892",
            "2C2533"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "F6F7F7",
            "BCAFA5",
            "83BEE3",
            "8E8AA8",
            "777495"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "ECE9D6",
            "8F9F8D",
            "8EDCF7",
            "6B81B5",
            "346487"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "F9F9F7",
            "E9B6A2",
            "93A9CD",
            "A799E7",
            "74799E"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "F8F8F3",
            "F899BA",
            "EFA29B",
            "D88B84",
            "E0284D"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "ECEEEA",
            "7F7765",
            "808283",
            "605C5A",
            "29202B"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "F2EEEA",
            "AD8052",
            "8E9599",
            "7E5D49",
            "3F3736"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "FAF8F7",
            "D5A9B2",
            "99B4D4",
            "7F7DEE",
            "3256B0"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "F9FAF9",
            "D0A77F",
            "B0A8A9",
            "DD6340",
            "393335"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "FBF9F9",
            "A0999D",
            "AAB2B9",
            "AA7680",
            "403862"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "FBF9FB",
            "C3BB7E",
            "6B87D3",
            "8A99A6",
            "1E50A1"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "F0F2EE",
            "E7A551",
            "C4866B",
            "9D7D74",
            "672823"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "F0EEF2",
            "848D86",
            "706596",
            "834198",
            "191A22"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "F2F0E6",
            "A5A49E",
            "E54E36",
            "D4727A",
            "2C2B41"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "FAF9F9",
            "90974F",
            "8DADB3",
            "91654B",
            "4F596F"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "E9E1D4",
            "8CB582",
            "7B7E86",
            "736DA8",
            "281F32"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "F8F9F8",
            "8F989A",
            "52839F",
            "78686D",
            "2B2533"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "F7F6F4",
            "8E8682",
            "BE775D",
            "8C7164",
            "2D3331"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "E5E3E7",
            "B18E44",
            "909092",
            "7A5B62",
            "4A3D47"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "F2F2F5",
            "A4AAAD",
            "9B859A",
            "7D9DC7",
            "232E25"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "F4ECF3",
            "E97D85",
            "478485",
            "E8988D",
            "EA6C57"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "F1EEEE",
            "EC8248",
            "908691",
            "6B8DA2",
            "413F4F"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "EFF2F1",
            "7F9397",
            "778699",
            "8B7554",
            "272F35"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "F6F2ED",
            "CBC644",
            "ACAE8C",
            "B74C37",
            "212A2F"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "F5F6F4",
            "DB90C6",
            "C69378",
            "3EA7A7",
            "424C52"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "F1EFF0",
            "808372",
            "949B8E",
            "73766B",
            "2C332E"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "F8F8F8",
            "D7A3B8",
            "6F62A8",
            "7692BF",
            "403F83"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "F1E8E3",
            "717DA0",
            "E3868F",
            "6E5D7D",
            "323044"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "F9F7F9",
            "E0AB7F",
            "CD6E6C",
            "92839E",
            "285C8E"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "F3F5F8",
            "E3BA9B",
            "739DC3",
            "EE8783",
            "6B6B83"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "DDD6CD",
            "58C2D2",
            "137491",
            "6698B0",
            "0F161E"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "EFDA88",
            "A378A4",
            "D99571",
            "C4564D",
            "421B32"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "F9F7F9",
            "CCCDCE",
            "A9B6D2",
            "1DC8EA",
            "8792C3"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "F3F4EA",
            "887969",
            "958E86",
            "77675E",
            "332E2B"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "FBFAF9",
            "E09EAA",
            "9DAFC5",
            "A198B5",
            "B24B51"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "F1F1EF",
            "92958C",
            "C69345",
            "BC6342",
            "18201A"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "F9FAF9",
            "BCA49E",
            "D69E99",
            "9C8580",
            "6E6360"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "F1F7F8",
            "A18178",
            "A4A5A7",
            "AD3E5C",
            "4A414A"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "F8F7F4",
            "A6B8A9",
            "BD8B4B",
            "D8C734",
            "27352F"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "F4F5F6",
            "9BA2A7",
            "797F87",
            "818992",
            "2E3A4C"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "BDE96D",
            "938881",
            "979EA5",
            "4C4E51",
            "1C1D35"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "F8F7F6",
            "9F7362",
            "9683AE",
            "984954",
            "3B3C5B"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "F2F3EE",
            "B9B080",
            "AD9B37",
            "BA603E",
            "3B3D1F"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "F9FBFA",
            "C7BFBB",
            "F8B21A",
            "C098B6",
            "5B4F61"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "F3F7F8",
            "E8A47D",
            "95B2CB",
            "F45A4F",
            "4B526A"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "F5F5F5",
            "956C5B",
            "9393A9",
            "AC4259",
            "433E5C"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "F7F7F7",
            "747575",
            "8C9EB5",
            "7F736E",
            "486479"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "DFF0EE",
            "64888A",
            "526871",
            "68849C",
            "232229"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "F5F7F6",
            "EEE744",
            "1885A5",
            "84A4BE",
            "212024"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "EEDFE2",
            "B39F57",
            "757E92",
            "865C79",
            "282735"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "FAF9F9",
            "9D9CA8",
            "997789",
            "8584A5",
            "292A36"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "F6F8F8",
            "B5C0D5",
            "C96BC7",
            "F8A325",
            "463A51"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "EEE2CF",
            "86A472",
            "75917F",
            "80775E",
            "262D2D"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "EEDECF",
            "9E949D",
            "C66F31",
            "828A8D",
            "2D232B"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "F6F7F6",
            "DCBB84",
            "19BEBF",
            "E58739",
            "CC271A"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "FBF8F3",
            "92B9F4",
            "438CE7",
            "8084F3",
            "12275D"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "F3F3EF",
            "9F9792",
            "B08664",
            "8C7668",
            "373035"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "F9F9F9",
            "8D8C8C",
            "B0BBCE",
            "868298",
            "4D6083"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "F2F1EE",
            "9FB4B0",
            "B97029",
            "7A909E",
            "1A2125"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "F7F4F5",
            "548DB2",
            "C2A7AF",
            "985A57",
            "BB3B62"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "F5F5F4",
            "CD826F",
            "89A4D5",
            "A96B98",
            "6A58B5"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "F5FAF4",
            "EA947D",
            "B77F99",
            "7C7B82",
            "261829"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "F5F1EC",
            "989D4D",
            "75A0A0",
            "865E52",
            "363334"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "F3F7F6",
            "7F7A81",
            "9E9C9C",
            "B13E55",
            "281D2F"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "F2F6EF",
            "EC9F58",
            "D34032",
            "9A9D97",
            "4D344E"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "F5F4F6",
            "52D6AC",
            "547C7C",
            "84987F",
            "2D3B43"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "E9E4DC",
            "B57455",
            "8687AA",
            "4E4341",
            "2D2C2D"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "F5F7F7",
            "E87358",
            "E6674B",
            "C9894A",
            "45565D"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "F6F8F7",
            "7C7D76",
            "9F9691",
            "7E7373",
            "1B1F1F"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "F5F8F7",
            "91A595",
            "7A817F",
            "9393A5",
            "171C2A"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "F5F5F3",
            "9EA591",
            "EBC01B",
            "887D57",
            "323B33"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "F9FAF9",
            "EB9D56",
            "B19391",
            "9B9A9A",
            "5D4C47"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "F8F7F5",
            "968FA4",
            "54BDF0",
            "7C8396",
            "927F6C"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "E8E7DE",
            "B4A07E",
            "B89A8B",
            "A94363",
            "383334"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "E6E3DD",
            "B1C0D3",
            "5A82F0",
            "F29B5E",
            "433C5A"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "F8F8F7",
            "F1CA69",
            "A3B1C0",
            "E05A62",
            "4F5B7B"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "E7E3DE",
            "6D758A",
            "A18698",
            "7D466C",
            "2B3945"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "F5F5F3",
            "80887B",
            "A5A7AE",
            "7E7C7A",
            "3B4644"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "E2E1E5",
            "D38E42",
            "8B554B",
            "8D6676",
            "3E2624"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "F5F6F2",
            "E0B660",
            "C16D90",
            "DD5981",
            "5B668B"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "F6DDD4",
            "978482",
            "DBAE32",
            "7F5D2F",
            "303037"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "F9FAFA",
            "E0B9A6",
            "EFA11F",
            "809ACA",
            "7570A5"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "FBFAFB",
            "A4ACA9",
            "83A3BA",
            "7B8487",
            "283541"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "F7F8F7",
            "959096",
            "905D5A",
            "5A9379",
            "2B2A32"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "E8F4F6",
            "E8C5B0",
            "E490A7",
            "F3B8B9",
            "E36770"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "FAF8F7",
            "EE8847",
            "8DA4E6",
            "4CB8CB",
            "3168AC"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "F0D587",
            "698686",
            "D99149",
            "976338",
            "473346"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "F5F5F1",
            "ADACAC",
            "DFC18F",
            "BF8E98",
            "898788"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "F4F6F5",
            "979F97",
            "9E7C61",
            "937C73",
            "49626F"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "FAF7F7",
            "DCD930",
            "C4C2A9",
            "74997A",
            "D2D31E"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "F6F5F3",
            "E39F1A",
            "9FA7BE",
            "8F7D6D",
            "484646"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "F5F2F5",
            "7F9989",
            "999D76",
            "34B54E",
            "2A3629"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "FBFAFA",
            "96819D",
            "8CAED4",
            "8473AD",
            "3C656E"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "F1F2EE",
            "8C9182",
            "A68B72",
            "8A5E64",
            "272F2B"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "F3F7F5",
            "8E96A4",
            "EDB246",
            "BB3D58",
            "195C81"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "EAEAEE",
            "A5874D",
            "77869A",
            "725B62",
            "3F3945"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "F1D582",
            "8D8784",
            "60413E",
            "7B6698",
            "1E1A21"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "F8FAF9",
            "AEB9AD",
            "F197D1",
            "8D92A2",
            "C4665F"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "F6F7F5",
            "9F9B8D",
            "AA976F",
            "928282",
            "32303A"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "F7F8F7",
            "A1A7B8",
            "EFC299",
            "958AA4",
            "86657B"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "F4F4F2",
            "77A9BA",
            "75CBDD",
            "69713F",
            "223346"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "F2F6F8",
            "9B9187",
            "6BCAD0",
            "AD3D65",
            "1E5E94"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "F4F4F7",
            "E18150",
            "94CFCD",
            "E25F42",
            "2D3969"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "F4EEDD",
            "A8976E",
            "AF9B8B",
            "935766",
            "516D67"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "F2F3EF",
            "7C746A",
            "9E9892",
            "7D6C6B",
            "272C28"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "F5F7F6",
            "867F68",
            "989EA9",
            "847A7F",
            "494B56"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "ECD6D4",
            "7A807D",
            "305C61",
            "767A76",
            "28282A"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "FBFAFA",
            "918DA8",
            "A2A3B9",
            "7B8B9D",
            "253048"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "F4F2F4",
            "33C4DC",
            "2A469B",
            "5B7FB8",
            "1A1E3C"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "F5F6EB",
            "F79F71",
            "A95E75",
            "CB6C6E",
            "2A181D"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "F5F8F7",
            "D59095",
            "A187B4",
            "847CAD",
            "6D3475"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "F6F5F2",
            "9DB0C0",
            "D16A48",
            "A58C75",
            "34314B"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "ECE6D7",
            "727F75",
            "ABC0BC",
            "746C6F",
            "403841"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "F0F0E0",
            "B1C99B",
            "ECBB54",
            "C0557A",
            "613C52"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "F9FAF9",
            "90877D",
            "9C9899",
            "7C737C",
            "3C3D3D"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "FAF9F9",
            "B4B69C",
            "71BBE5",
            "7A96C9",
            "1B87E1"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "EAEEF0",
            "717FA0",
            "8C8994",
            "6D5E7C",
            "1C2130"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "F5F7F9",
            "D17E6F",
            "D8973D",
            "8E6B82",
            "4B1C31"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "EAE0D7",
            "4D9B8E",
            "786377",
            "8F636B",
            "18161F"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "E4F1EC",
            "BBA5E9",
            "E8AA57",
            "9F387D",
            "282C52"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "F8F8F8",
            "88CFB1",
            "89ABA5",
            "82ABBB",
            "29496A"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "FAF8F6",
            "AD895E",
            "9DA4B9",
            "987450",
            "3C394F"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "F0F3F1",
            "848D8C",
            "8F90A9",
            "646885",
            "232B33"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "FCFCFD",
            "B5AAA4",
            "C9BEB6",
            "F09775",
            "80AEE1"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "F4F2F2",
            "D79E56",
            "89867F",
            "56A2A3",
            "50494B"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "FBF6F4",
            "D0E7EF",
            "67D1D7",
            "A09AA4",
            "5D7AC1"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "FAF9F9",
            "BC9FA4",
            "E03B3B",
            "DA9754",
            "221527"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "EEF4E7",
            "8E4467",
            "DFBD8F",
            "C23A62",
            "24192F"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "FAFAFA",
            "A1BDD2",
            "D2A16C",
            "C986B9",
            "2D5890"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "F2F4EE",
            "B9965E",
            "C19C7A",
            "8A7778",
            "29282D"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "F4F7F7",
            "F3D23F",
            "29B1D7",
            "87A2C6",
            "7E82CA"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "F5F7F9",
            "F3E145",
            "E99B46",
            "8B4494",
            "4C2867"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "F9F5F5",
            "9A9AA2",
            "9E8DA8",
            "847784",
            "2B2335"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "F5F6F5",
            "9C9796",
            "989BAD",
            "837483",
            "3D3A63"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "F6F4F5",
            "968FA1",
            "9C8FA5",
            "9A65AB",
            "2D304F"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "F6F5F5",
            "B6895E",
            "A2ADB5",
            "936968",
            "344C7A"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "F1F6F1",
            "6F7CB5",
            "4497E7",
            "6C6497",
            "2E3C74"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "F5E294",
            "9E7659",
            "8D8963",
            "A6483C",
            "2D2B33"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "F9FBF9",
            "9B9085",
            "D04252",
            "7D8297",
            "1F1E26"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "F2F5F0",
            "8D9C8D",
            "EFBF51",
            "A55B3D",
            "35352D"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "F4E6F3",
            "DAAE63",
            "D79692",
            "869083",
            "A07526"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "F1F6F7",
            "B1B9BE",
            "EAB456",
            "EA6E5C",
            "3C556E"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "F4F2F6",
            "988D85",
            "AC998B",
            "5E8899",
            "4E4B4A"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "E4DBDE",
            "6DB6C5",
            "104588",
            "2A4A9E",
            "16141B"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "F3F5F1",
            "9B9C9B",
            "F1BE40",
            "92722F",
            "25332E"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "FBF7F6",
            "BFC198",
            "8EC3E6",
            "53A2E7",
            "4BA9E8"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "F3F2F5",
            "8AB7E1",
            "4355E3",
            "898AE0",
            "1D2458"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "F9FAF9",
            "D8CCDA",
            "F27E6F",
            "B3A9CF",
            "6E9ED7"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "F5F6F5",
            "A8AFBF",
            "DDAB8C",
            "AE80CF",
            "6C7788"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "EDECED",
            "5D4569",
            "C24FA1",
            "6F25B1",
            "28244E"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "F6F6F4",
            "EE884A",
            "CF80A2",
            "92758B",
            "84343F"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "F7F8F2",
            "F5ACA6",
            "97C1F1",
            "AC78A3",
            "6B69D1"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "F9F9F9",
            "937A76",
            "DECE9F",
            "984A35",
            "584830"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "F7F2F6",
            "67BAC9",
            "1BAE9F",
            "797E65",
            "222636"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "E5E4EA",
            "7E859B",
            "E59C3C",
            "CA464D",
            "484361"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "E9E9EA",
            "898C81",
            "717272",
            "7A7B7C",
            "2A272A"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "F0EFEE",
            "99A39D",
            "864A4D",
            "90879F",
            "21151E"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "F4F3F0",
            "7A8384",
            "738388",
            "7A7B84",
            "1E323C"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "F9F8F7",
            "D27451",
            "87989B",
            "7D5D57",
            "4C5256"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "F2F5F2",
            "778290",
            "99937A",
            "82787F",
            "24252E"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "E5E7F2",
            "7C98C5",
            "332965",
            "5363D1",
            "151426"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "EEF3F1",
            "7CA6A3",
            "EACC70",
            "6C7B7A",
            "594948"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "F3F4F4",
            "8D8294",
            "B26342",
            "A17B62",
            "271E29"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "F4DF69",
            "7E817A",
            "90713F",
            "7B736D",
            "17151B"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "F4F7F5",
            "EB72A0",
            "F0A4C1",
            "8E6181",
            "814369"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "F5F4F3",
            "85A5B1",
            "E88474",
            "388C8E",
            "402D3A"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "F5FAF8",
            "F3BF76",
            "F3A049",
            "F2924A",
            "71483C"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "F3F5F3",
            "DF7D7C",
            "C890B5",
            "CD5B61",
            "45617B"
          ],
          "DarkMode": true
        },
        {
          "Colours": [
            "EEEDE7",
            "74C0D8",
            "979775",
            "626F4F",
            "1B3443"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "F6F8F8",
            "82857C",
            "5F6167",
            "828B8E",
            "282B32"
          ],
          "DarkMode": false
        },
        {
          "Colours": [
            "FCFBF9",
            "ADC1D2",
            "88D0E7",
            "7D9CD1",
            "3F89C4"
          ],
          "DarkMode": true
        }
      ]
    },
    "Category": {
      "ID": "grammar-en",
      "JSONFolder": "/Users/juny/go/src/isonschool/json/grammar-en/",
      "BinaryFolder": "/Users/juny/go/src/yossie/binary/grammar-en/",
      "Kind": "grammar"
    },
    "Title": "English grammar exercises",
    "Subtitle": "s"
  }
];
