var data = {
	"Sentences": [
		{
			"No": 1,
			"Title": "Time 時間",
			"Boxes": [
				{
					"Pos": 0,
					"Candidates": [
						{ "Word": "学校は", "Ruby": "がっこう は" },
						{ "Word": "会議は", "Ruby": "かいぎ は" },
					],
				},
				{
					"Pos": 1,
					"Candidates": [
						{ "Word": "午前10時に", "Ruby": "ごぜん じゅうじ に" },
						{ "Word": "午前10時半に", "Ruby": "ごぜん じゅうじ はん に" },
						{ "Word": "午後1時20分に", "Ruby": "ごご いちじ にじゅっぷん に" },
						{ "Word": "午後7時15分に", "Ruby": "ごご しちじ じゅうごふん に" },
					],
				},
				{
					"Pos": 2,
					"Candidates": [
						{ "Word": "始まります。", "Ruby": "はじまります" },
						{ "Word": "終わります。", "Ruby": "おわります" },
						{ "Word": "終了します。", "Ruby": "しゅうりょうします" },
					],
				},
			],
		},
		{
			"No": 2,
			"Title": "Number 数",
			"Boxes": [
				{
					"Pos": 0,
					"Candidates": [
						{ "Word": "私は", "Ruby": "わたしは" },
					],
				},
				{
					"Pos": 1,
					"Weight": 10,
					"Candidates": [
						{ "Word": "本を", "Ruby": "ほん を", "Conditions": [{"Pos":2, "Words":['1冊','2冊','3冊'] }]},
						{ "Word": "ペンを", "Ruby": "ぺん を", "Conditions": [{"Pos":2, "Words":['1本','2本','3本'] }] },
						{ "Word": "写真を", "Ruby": "しゃしん を", "Conditions": [{"Pos":2, "Words":['1枚','2枚','3枚']}]  },
					],
				},
				{
					"Pos": 2,
					"Candidates": [
						{ "Word": "1冊", "Ruby": "いっさつ", "Root": "1" },
						{ "Word": "2冊", "Ruby": "にさつ", "Root": "2" },
						{ "Word": "3冊", "Ruby": "さんさつ", "Root": "3" },
						{ "Word": "1本", "Ruby": "いっぽん", "Root": "1" },
						{ "Word": "2本", "Ruby": "にほん", "Root": "2" },
						{ "Word": "3本", "Ruby": "さんぼん", "Root": "3" },
						{ "Word": "1枚", "Ruby": "いちまい", "Root": "1" },
						{ "Word": "2枚", "Ruby": "にまい", "Root": "2" },
						{ "Word": "3枚", "Ruby": "さんまい", "Root": "3" },
					],
				},
				{
					"Pos": 3,
					"Candidates": [
						{ "Word": "持っています。", "Ruby": "もっています" },
						{ "Word": "買います。", "Ruby": "かいます" },
					],
				},
			],
		},
	],
};

class VideoData {
	constructor(){}
}

class Problem {
	constructor(originalWords, changeWordPos, changeWords, newWords) {
		this.originalWords = originalWords;
		this.changeWordPos = changeWordPos;
		this.changeWords = changeWords;
		this.newWords = newWords;
	}
}

class ProblemGenerator {
	passConditions(words) {
		for (var w of words) {
			if (w.Conditions) {
				for (var c of w.Conditions) {
					if (c.Words.indexOf(words[c.Pos].Word) < 0) {
						return false;
					}
				}
			}
		}
		return true;
	}

	generate(sentence, num) {
		var words = sentence.Boxes;
		var weights = [];
		for (var i = 0; i < words.length; i++) {
			if (words[i].Weight) {
				for (var w = 0; w < words[i].Weight; w++) {
					weights.push(i);
				}
			} else {
				for (var w = 1; w < words[i].Candidates.length; w++) {
					weights.push(i);
				}
			}
		}
		for (var i = 0; i < 10000; i++) {
			var ws = new Array(words.length);
			for (var w = 0; w < words.length; w++) {
				ws[w] = words[w].Candidates[Math.floor(Math.random() * words[w].Candidates.length)];
			}
			if (!this.passConditions(ws)) {
				continue;
			}
			var ret = [];
			for (var m = 0; m < 1000; m++) {
				var changeWords = new Array(words.length);
				w = weights[Math.floor(Math.random() * weights.length)];
				var newCand;
				for (var ii = 0; ii < 10; ii++) {
					newCand = words[w].Candidates[Math.floor(Math.random() * words[w].Candidates.length)];
					if (ws[w].Word == newCand.Word) {
						continue;
					}
				}
				if (ws[w].Word == newCand.Word) {
					continue;
				}
				changeWords[w] = newCand;
				if (newCand.Conditions) {
					for (var c of newCand.Conditions) {
						if (ws[c.Pos].Root) {
							var root = ws[c.Pos].Root;
							for (var cand of sentence.Boxes[c.Pos].Candidates) {
								if (cand.Root == root && c.Words.indexOf(cand.Word) >= 0) {
									changeWords[c.Pos] = cand;
									break;
								}
							}
						}
					}
				}
				var newWords = new Array(...ws);
				for (var ci = 0; ci < changeWords.length; ci++) {
					if (changeWords[ci]) {
						newWords[ci] = changeWords[ci];
					}
				}
				if (!this.passConditions(newWords)) {
					continue;
				}
				var ok = true;
				for (var r of ret) {
					var same = true;
					for (var ri = 0; ri < r.originalWords.length; ri++) {
						if (r.originalWords[ri].Word != newWords[ri].Word) {
							same = false;
						}
					}
					if (same) {
						ok = false;
						break;
					}
				}
				if (!ok) {
					continue;
				}
				ret.push(new Problem(new Array(...ws), w, new Array(...changeWords), new Array(...newWords)));
				ws = newWords;
				if (ret.length >= num) {
					return ret;
				}
			}
		}
		return ret;
	}
}

var fileHandle;
var filenameHandle;

const myWidth = 1280;
const myHeight = 720;

const speeds = [1, 0.5];

class VideoDuration {
	constructor(titleDuration, qDuration, aDuration, blankDuration, endingDuration) {
		this.titleDuration = titleDuration;
		this.qDuration = qDuration;
		this.aDuration = aDuration;
		this.blankDuration = blankDuration;
		this.endingDuration = endingDuration;
	}
	getQDuration(n) {
		return this.qDuration * speeds[n];
	}
	getADuration(n) {
		return this.aDuration * speeds[n];
	}
}

class VideoLayer {
	constructor(stage){
		this.stage = stage;
		// this.makeTitleLayer();
		// this.makeEndingLayer();
		// this.makeBlankLayer();
		//this.makeQLayer();
	}

	/*
	makeTitleLayer() {
		this.titleLayer = new Konva.Layer();
		var titleRect = new Konva.Rect({
			fill: '#222222',
			width: myWidth,
			height: myHeight,
		});
		this.titleLayer.add(titleRect);

		this.stage.add(this.titleLayer);
	}

	makeEndingLayer() {
		this.endingLayer = new Konva.Layer();
		var endingRect = new Konva.Rect({
			fill: '#222222',
			width: myWidth,
			height: myHeight,
		});
		this.endingLayer.add(endingRect);

		this.stage.add(this.endingLayer);
	}

	makeBlankLayer() {
		this.blankLayer = new Konva.Layer();
		var rect = new Konva.Rect({
			fill: '#222222',
			width: myWidth,
			height: myHeight,
		});
		this.blankLayer.add(rect);

		this.stage.add(this.blankLayer);
	}*/

	makeQLayer(sentence) {
		if (this.qLayer) {
			this.stage.remove(this.qLayer);
		}
		this.qLayer = new Konva.Layer();
		var qRect = new Konva.Rect({
			fill: '#222222',
			width: myWidth,
			height: myHeight,
		});
		this.qLayer.add(qRect);
		var sumWidth = 0;
		for (var i = 0; i < sentence.Boxes.length; i++) {
			sumWidth += sentence.Boxes[i].Width;
		}
		var myFontSize =  (myWidth - 100) / sumWidth;
		this.originalTexts = new Array(sentence.Boxes.length);
		this.newTexts = new Array(sentence.Boxes.length);
		console.log(sentence);
		var x = 50;
		for (var i = 0; i < sentence.Boxes.length; i++) {
			console.log(x);
			this.originalTexts[i] = new Konva.Text({
				x: x,
				y: 0,
				fontSize: myFontSize,
				fontFamily: 'UD デジタル 教科書体 NP',
				text: 'テキスト',
				fill: 'white',
				align: 'center',
				verticalAlign: 'middle',
				width: myFontSize * sentence.Boxes[i].Width,
				height: myHeight / 2,
			});
			this.newTexts[i] = new Konva.Text({
				x: x,
				y: myHeight / 2,
				fontSize: myFontSize,
				fontFamily: 'UD デジタル 教科書体 NP',
				text: 'テキスト',
				fill: 'white',
				align: 'center',
				verticalAlign: 'middle',
				width: myFontSize * sentence.Boxes[i].Width,
				height: myHeight / 2,
			});
			x += sentence.Boxes[i].Width * myFontSize;
			this.qLayer.add(this.originalTexts[i]);
			this.qLayer.add(this.newTexts[i]);
		}
		this.stage.add(this.qLayer);
	}

	setTitleLayer() {
		this.setBlankLayer();
	}

	setEndingLayer() {
		this.setBlankLayer();
	}

	setBlankLayer() {
		for (var i = 0; i < this.originalTexts.length; i++) {
			this.originalTexts[i].text('');
			this.newTexts[i].text('');
		}
		this.qLayer.draw();
	}

	setQLayer(problem) {
		return () => {
			for (var i = 0; i < problem.originalWords.length; i++) {
				this.originalTexts[i].text(problem.originalWords[i].Word);
				this.newTexts[i].text(problem.newWords[i].Word);
			}
			this.qLayer.draw();
		};
	}
}

class Video {
	constructor(id, sentence, problems, videoLayer, videoDuration) {
		this.id = id;
		this.sentence = sentence;
		this.problems = problems;
		this.videoLayer = videoLayer;
		this.videoDuration = videoDuration;
		this.frames = [];
		// this.frames.push({make:()=>videoLayer.setTitleLayer(), duration: videoDuration.titleDuration});
		this.frames.push({make:videoLayer.setQLayer(this.problems[0]), duration: videoDuration.titleDuration});
		for (var n = 0; n < speeds.length; n++) {
			for (var p of this.problems) {
				this.frames.push({make:videoLayer.setQLayer(p), duration: videoDuration.getQDuration(n)});
				this.frames.push({make:videoLayer.setQLayer(p), duration: videoDuration.getADuration(n)});
				this.frames.push({make:()=>videoLayer.setBlankLayer(), duration: videoDuration.blankDuration});
			}
		}
		this.frames.push({make:()=>videoLayer.setEndingLayer(), duration: videoDuration.endingDuration});
	}

	start() {
		this.videoLayer.makeQLayer(this.sentence);

		this.nextFrame(0);
		var canvas = document.querySelector("canvas");

		// Optional frames per second argument.
		var stream = canvas.captureStream(25);
		var recordedChunks = [];
	
		var options = { mimeType: "video/webm; codecs=vp9" };
		this.mediaRecorder = new MediaRecorder(stream, options);
	
		this.mediaRecorder.ondataavailable = (event) => {
			if (event.data.size > 0) {
				recordedChunks.push(event.data);
				var blob = new Blob(recordedChunks, {
					type: "video/webm"
				});
				writeFile(blob);
				writeFilename(this.id);
			} else {
				console.log("event.data.size == 0");
			}
		};
		this.mediaRecorder.start();
	}
	
	nextFrame(frameNo) {
		if (this.timeoutId) {
			window.clearTimeout(this.timeoutId);
		}
		console.log(frameNo);
		if (frameNo == this.frames.length) {
			this.mediaRecorder.stop();
			nextVideo();
			return;
		}
		this.frames[frameNo].make();
		this.timeoutId = window.setTimeout(event => {
			this.nextFrame(frameNo+1);
		}, this.frames[frameNo].duration);
	}
}

var videoList = [];
var videoNo = 0;

async function writeFile(contents) {
	try {
		const writer = await fileHandle.createWriter();
		await writer.truncate(0);
		await writer.write(0, contents);
		await writer.close();
	} catch (err) {
		console.error(err.message);
	}
}

async function writeFilename(contents) {
	try {
		const writer = await filenameHandle.createWriter();
		await writer.truncate(0);
		await writer.write(0, contents);
		await writer.close();
	} catch (err) {
		console.error(err.message);
	}
}

async function setVideoFile() {
	try {
		// FileSystemFileHandleを取得
		fileHandle = await window.chooseFileSystemEntries({type: 'saveFile'});
	} catch (err) {
		console.error(err.message);
	}
}

async function setFilenameFile() {
	try {
		// FileSystemFileHandleを取得
		filenameHandle = await window.chooseFileSystemEntries({type: 'saveFile'});
	} catch (err) {
		console.error(err.message);
	}
}

function load(m, n) {
	var stage = new Konva.Stage({
		container: 'container',
		width: myWidth,
		height: myHeight,
		color: '#222222',
	});
	var videoLayer = new VideoLayer(stage);
	var videoDuration = new VideoDuration(1000, 4000, 2000, 1000, 12000);
	
	for (var i = 0; i < data.Sentences.length; i++) {
		if ((m && i < m) || (n && i >= n)) {
			continue;
		}
		var sentence = data.Sentences[i];
		for (var j = 0; j < sentence.Boxes.length; j++) {
			var max = 1;
			for (var c = 0; c < sentence.Boxes[j].Candidates.length; c++) {
				if (sentence.Boxes[j].Candidates[c].Word.length > max) {
					max = sentence.Boxes[j].Candidates[c].Word.length;
				}
			}
			sentence.Boxes[j].Width = max;
		}
		var problems = new ProblemGenerator().generate(sentence, 10);
		if (problems.length < 10) {
			console.error("problems.length < 10");
		}
		console.log(problems);
		videoList.push(((idLocal, problems, sentence) => (() => {
			new Video(idLocal, sentence, problems, videoLayer, videoDuration).start();
		}))('japanese-grammar_' + sentence.No +'.webm', problems, sentence));
	}
	nextVideo();
}

function nextVideo() {
	if (videoNo >= videoList.length) {
		return;
	}
	//if (videoNo >= 3) return;
	videoList[videoNo]();
	videoNo++;
}
