const myWidth = 1280;
const myHeight = 720;
var stage;

function calStrWidthHeight(text, fontsize) {
	var e = document.getElementById("widthcheck");
	e.textContent = text;
	e.style.fontSize = fontsize + "px";
	return { strWidth: e.offsetWidth, strHeight: e.offsetHeight };
}

function loadMask(l, source) {
	stage = new Konva.Stage({
		container: 'container',
		width: l,
		height: l,
	});
	var layer = new Konva.Layer();
	stage.add(layer);
	var qRect = new Konva.Rect({
		fill: 'rgba(0,0,0,0)',
		width: l,
		height: l,
	});
	layer.add(qRect);
	var imageObj = new Image();
	imageObj.onload = () => {
		var image = new Konva.Image({
			width: l,
			height: l,
			image: imageObj,
		});
		layer.add(image);
		layer.draw();
	};
	imageObj.src = source;
}

function loadSayMask(l) {
	stage = new Konva.Stage({
		container: 'container',
		width: l,
		height: l,
	});
	var layer = new Konva.Layer();
	stage.add(layer);
	var qRect = new Konva.Rect({
		fill: 'rgba(0,0,0,0)',
		width: l,
		height: l,
	});
	layer.add(qRect);
	var imageObj = new Image();
	imageObj.onload = () => {
		var image = new Konva.Image({
			width: l,
			height: l,
			image: imageObj,
		});
		layer.add(image);
		layer.draw();
	};
	imageObj.src = 'say-mask.svg';
}

function loadSayMask(l) {
	stage = new Konva.Stage({
		container: 'container',
		width: l,
		height: l,
	});
	var layer = new Konva.Layer();
	stage.add(layer);
	var qRect = new Konva.Rect({
		fill: 'rgba(0,0,0,0)',
		width: l,
		height: l,
	});
	layer.add(qRect);
	var imageObj = new Image();
	imageObj.onload = () => {
		var image = new Konva.Image({
			width: l,
			height: l,
			image: imageObj,
		});
		layer.add(image);
		layer.draw();
	};
	imageObj.src = 'say-mask.svg';
}

function loadPersonMask(l) {
	stage = new Konva.Stage({
		container: 'container',
		width: l,
		height: l,
	});
	var layer = new Konva.Layer();
	stage.add(layer);
	var qRect = new Konva.Rect({
		fill: 'rgba(0,0,0,0)',
		width: l,
		height: l,
	});
	layer.add(qRect);
	var imageObj = new Image();
	imageObj.onload = () => {
		var image = new Konva.Image({
			width: l,
			height: l,
			image: imageObj,
		});
		layer.add(image);
		layer.draw();
	};
	imageObj.src = 'person-neutral-mask.svg';
}

function loadMaleMask(l) {
	stage = new Konva.Stage({
		container: 'container',
		width: l,
		height: l,
	});
	var layer = new Konva.Layer();
	stage.add(layer);
	var qRect = new Konva.Rect({
		fill: 'rgba(0,0,0,0)',
		width: l,
		height: l,
	});
	layer.add(qRect);
	var imageObj = new Image();
	imageObj.onload = () => {
		var image = new Konva.Image({
			width: l,
			height: l,
			image: imageObj,
		});
		layer.add(image);
		layer.draw();
	};
	imageObj.src = 'person-male-mask.svg';
}

function loadFemaleMask(l) {
	stage = new Konva.Stage({
		container: 'container',
		width: l,
		height: l,
	});
	var layer = new Konva.Layer();
	stage.add(layer);
	var qRect = new Konva.Rect({
		fill: 'rgba(0,0,0,0)',
		width: l,
		height: l,
	});
	layer.add(qRect);
	var imageObj = new Image();
	imageObj.onload = () => {
		var image = new Konva.Image({
			width: l,
			height: l,
			image: imageObj,
		});
		layer.add(image);
		layer.draw();
	};
	imageObj.src = 'person-female-mask.svg';
}

function loadIconMask() {
	stage = new Konva.Stage({
		container: 'container',
		width: myWidth,
		height: myHeight,
	});
	var layer = new Konva.Layer();
	stage.add(layer);
	var qRect = new Konva.Rect({
		fill: 'rgba(0,0,0,0)',
		width: myWidth,
		height: myHeight,
	});
	layer.add(qRect);
	var imageObj = new Image();
	imageObj.onload = () => {
		var l = 160;
		var image = new Konva.Image({
			x: 110,
			y: (myHeight - l) / 2.0 - 130,
			width: l,
			height: l,
			image: imageObj,
		});
		layer.add(image);
		layer.draw();
	};
	imageObj.src = 'ichou-mask.svg';
}

function loadTitleIsonSchoolMask() {
	var l = 160;
	stage = new Konva.Stage({
		container: 'container',
		width: myWidth,
		height: myHeight,
	});
	var layer = new Konva.Layer();
	stage.add(layer);
	var qRect = new Konva.Rect({
		fill: 'rgba(0,0,0,0)',
		width: myWidth,
		height: myHeight,
	});
	layer.add(qRect);
	var text = new Konva.Text({
		x: 260,
		y: (myHeight - l)/ 2.0 - 120,
		fontSize: 140,
		fontFamily: 'UD デジタル 教科書体 NP',
		text: 'Ison School',
		fill: 'rgba(0,0,0,1)',
		align: 'center',
		verticalAlign: 'middle',
		width: (myWidth - 300),
		height: l,
	});
	layer.add(text);
	layer.draw();
}

function loadIsonSchoolMask(fontsize) {
	var wh = calStrWidthHeight("Ison School", fontsize);
	stage = new Konva.Stage({
		container: 'container',
		width: wh.strWidth,
		height: wh.strHeight,
	});
	var layer = new Konva.Layer();
	stage.add(layer);
	var qRect = new Konva.Rect({
		fill: 'rgba(0,0,0,0)',
		width: wh.strWidth,
		height: wh.strHeight,
	});
	layer.add(qRect);
	var text = new Konva.Text({
		x: 0,
		y: 0,
		fontSize: fontsize,
		fontFamily: 'UD デジタル 教科書体 NP',
		text: 'Ison School',
		fill: 'rgba(0,0,0,1)',
	});
	layer.add(text);
	layer.draw();
}

function loadTitle() {
	stage = new Konva.Stage({
		container: 'container',
		width: myWidth,
		height: myHeight,
		color: '#222222',
	});
	var layer = new Konva.Layer();
	stage.add(layer);
	var qRect = new Konva.Rect({
		fill: '#222222',
		width: myWidth,
		height: myHeight,
	});
	layer.add(qRect);
	var imageObj = new Image();
	imageObj.onload = () => {
		var l = 160;
		var image = new Konva.Image({
			x: 110,
			y: (myHeight - l) / 2.0,
			width: l,
			height: l,
			image: imageObj,
		});
		layer.add(image);
		layer.draw();
	};
	imageObj.src = 'ichou.svg';

	var text = new Konva.Text({
		x: 260,
		y: 0,
		fontSize: 140,
		fontFamily: 'UD デジタル 教科書体 NP',
		text: 'Ison School',
		fill: 'white',
		align: 'center',
		verticalAlign: 'middle',
		width: (myWidth - 300),
		height: myHeight,
	});
	layer.add(text);
}

function loadTitleOnlyImg() {
	stage = new Konva.Stage({
		container: 'container',
		width: myWidth,
		height: myHeight,
		color: '#222222',
	});
	var layer = new Konva.Layer();
	stage.add(layer);
	var qRect = new Konva.Rect({
		fill: '#222222',
		width: myWidth,
		height: myHeight,
	});
	layer.add(qRect);
	var imageObj = new Image();
	imageObj.onload = () => {
		var l = 160;
		var image = new Konva.Image({
			x: (myWidth - l) / 2.0,
			y: (myHeight - l) / 2.0,
			width: l,
			height: l,
			image: imageObj,
		});
		layer.add(image);
		layer.draw();
	};
	imageObj.src = 'ichou.svg';
}

function loadTitleOnlyImgMask(l) {
	stage = new Konva.Stage({
		container: 'container',
		width: l,
		height: l,
	});
	var layer = new Konva.Layer();
	stage.add(layer);
	var qRect = new Konva.Rect({
		fill: 'rgba(0,0,0,0)',
		width: l,
		height: l,
	});
	layer.add(qRect);
	var imageObj = new Image();
	imageObj.onload = () => {
		var image = new Konva.Image({
			x: 0,
			y: 0,
			width: l,
			height: l,
			image: imageObj,
		});
		layer.add(image);
		layer.draw();
	};
	imageObj.src = 'ichou-mask.svg';
}

function loadTitleOnlyImg2() {
	stage = new Konva.Stage({
		container: 'container',
		width: myWidth,
		height: myHeight,
		color: '#222222',
	});
	var layer = new Konva.Layer();
	stage.add(layer);
	var qRect = new Konva.Rect({
		fill: '#222222',
		width: myWidth,
		height: myHeight,
	});
	layer.add(qRect);
	var imageObj = new Image();
	imageObj.onload = () => {
		var l = 160;
		var image = new Konva.Image({
			x: (myWidth - l) / 2.0 - l*0.75,
			y: (myHeight - l) / 2.0,
			width: l,
			height: l,
			image: imageObj,
		});
		layer.add(image);
		var image2 = new Konva.Image({
			x: (myWidth - l) / 2.0 + l*0.75,
			y: (myHeight - l) / 2.0,
			width: l,
			height: l,
			image: imageObj,
		});
		layer.add(image2);
		layer.draw();
	};
	imageObj.src = 'ichou.svg';
}

function loadTitleOnlyImg3() {
	stage = new Konva.Stage({
		container: 'container',
		width: myWidth,
		height: myHeight,
		color: '#222222',
	});
	var layer = new Konva.Layer();
	stage.add(layer);
	var qRect = new Konva.Rect({
		fill: '#222222',
		width: myWidth,
		height: myHeight,
	});
	layer.add(qRect);
	var imageObj = new Image();
	imageObj.onload = () => {
		var l = 160;
		var image = new Konva.Image({
			x: (myWidth - l) / 2.0 - 1.5 * l,
			y: (myHeight - l) / 2.0,
			width: l,
			height: l,
			image: imageObj,
		});
		layer.add(image);
		var image2 = new Konva.Image({
			x: (myWidth - l) / 2.0,
			y: (myHeight - l) / 2.0,
			width: l,
			height: l,
			image: imageObj,
		});
		layer.add(image2);
		var image3 = new Konva.Image({
			x: (myWidth - l) / 2.0 + 1.5 * l,
			y: (myHeight - l) / 2.0,
			width: l,
			height: l,
			image: imageObj,
		});
		layer.add(image3);
		layer.draw();
	};
	imageObj.src = 'ichou.svg';
}

function loadBrand() {
	var wid = 2560;
	var hei = 1440;
	stage = new Konva.Stage({
		container: 'container',
		width: wid,
		height: hei,
		color: '#222222',
	});
	var layer = new Konva.Layer();
	stage.add(layer);
	var qRect = new Konva.Rect({
		fill: '#222222',
		width: wid,
		height: hei,
	});
	layer.add(qRect);
	var imageObj = new Image();
	imageObj.onload = () => {
		var l = 300;
		var image = new Konva.Image({
			x: 200,
			y: (hei - l) / 2.0 - 150,
			width: l,
			height: l,
			image: imageObj,
		});
		layer.add(image);
		layer.draw();
	};
	imageObj.src = 'ichou.svg';

	var text = new Konva.Text({
		x: 400,
		y: -150,
		fontSize: 300,
		fontFamily: 'UD デジタル 教科書体 NP',
		text: 'Ison School',
		fill: 'white',
		align: 'center',
		verticalAlign: 'middle',
		width: (wid - 400),
		height: hei,
	});
	layer.add(text);

	var subtext = new Konva.Text({
		x: 0,
		y: hei / 2 - 150,
		fontSize: 200,
		fontFamily: 'UD デジタル 教科書体 NP',
		text: 'School for everyone!',
		fill: 'lightblue',
		align: 'center',
		verticalAlign: 'middle',
		width: wid,
		height: hei / 2,
	});
	layer.add(subtext);

}

var qRectW;
var qLayerW;
function loadChannelArt() {
	var allwid = 2560;
	var allhei = 1440;
	var wid = 1546;
	var hei = 423;
	var leftspace = (allwid - wid)/2;
	var topspace = (allhei - hei)/2;
	stage = new Konva.Stage({
		container: 'container',
		width: allwid,
		height: allhei,
		color: '#222222',
	});
	var layer = new Konva.Layer();
	stage.add(layer);
	var qRect = new Konva.Rect({
		fill: '#222222',
		width: allwid,
		height: allhei,
	});
	qRectW = qRect;
	layer.add(qRect);
	var imageWidth = 160;
	var space = 200;
	var textOfffsetY = -50;
	var imageObj = new Image();
	imageObj.onload = () => {
		var image = new Konva.Image({
			x: leftspace + space,
			y: topspace + (hei - imageWidth) / 2.0 + textOfffsetY,
			width: imageWidth,
			height: imageWidth,
			image: imageObj,
		});
		layer.add(image);
		layer.draw();
	};
	imageObj.src = 'ichou.svg';

	var text = new Konva.Text({
		x: leftspace + imageWidth + space,
		y: topspace + 0 + textOfffsetY,
		fontSize: 160,
		fontFamily: 'UD デジタル 教科書体 NP',
		text: 'Ison School',
		fill: 'white',
		align: 'right',
		verticalAlign: 'middle',
		width: wid - imageWidth - space*2,
		height: hei,
	});
	layer.add(text);

	var subtextOffsetY = -10;
	var subtext = new Konva.Text({
		x: leftspace,
		y: topspace + hei/2 + subtextOffsetY,
		fontSize: 80,
		fontFamily: 'UD デジタル 教科書体 NP',
		text: 'School for everyone!',
		fill: 'lightblue',
		align: 'center',
		verticalAlign: 'middle',
		width: wid,
		height: hei/2,
	});
	layer.add(subtext);
}

function loadBlank() {
	stage = new Konva.Stage({
		container: 'container',
		width: myWidth,
		height: myHeight,
		color: '#222222',
	});
	var layer = new Konva.Layer();
	stage.add(layer);
	var qRect = new Konva.Rect({
		fill: '#222222',
		width: myWidth,
		height: myHeight,
	});
	layer.add(qRect);
	layer.draw();
}

function loadSubscribe() {
	stage = new Konva.Stage({
		container: 'container',
		width: myWidth,
		height: myHeight,
		color: '#222222',
	});
	var layer = new Konva.Layer();
	stage.add(layer);
	var qRect = new Konva.Rect({
		fill: '#222222',
		width: myWidth,
		height: myHeight,
	});
	layer.add(qRect);
	var offsetX = 0;
	var offsetY = 0;
	var imageObj = new Image();
	imageObj.onload = () => {
		var l = 160;
		var image = new Konva.Image({
			x: 130 + offsetX,
			y: 105 + offsetY,
			width: l,
			height: l,
			image: imageObj,
		});
		layer.add(image);
		layer.draw();
	};
	imageObj.src = 'ichou.svg';

	var subscribeButton = new Konva.Rect({
		x: 475 + offsetX,
		y: 110 + offsetY,
		fill: 'rgb(204, 0, 0)',
		width: 520,
		height: 150,
		cornerRadius: 10,
	});
	layer.add(subscribeButton);
	var text = new Konva.Text({
		x: 550 + offsetX,
		y: 155 + offsetY,
		fontSize: 70,
		fontFamily: 'Roboto',
		text: 'SUBSCRIBE',
		fill: 'white',
		align: 'left',
		verticalAlign: 'top',
		width: 700,
		height: 100,
	});
	layer.add(text);
	var arrowText = new Konva.Text({
		x: myWidth - 250 + offsetX,
		y: 115 + offsetY,
		fontSize: 140,
		fontFamily: 'UD デジタル 教科書体 NP',
		text: '↓',
		fill: 'white',
		align: 'center',
		verticalAlign: 'middle',
		width: 140,
		height: 140,
	});
	layer.add(arrowText);
}

function loadIcon() {
	stage = new Konva.Stage({
		container: 'container',
		width: myWidth,
		height: myHeight,
		color: '#BBDEFB',
	});
	var w = 720;
	var h = 800;
	var layer = new Konva.Layer();
	stage.add(layer);
	var qRect = new Konva.Rect({
		fill: '#E3F2FD',		//'#F9F9F9',
		width: w,
		height: h,
	});
	layer.add(qRect);
	var imageObj = new Image();
	imageObj.onload = () => {
		var l = 480;
		var image = new Konva.Image({
			x: (w - l) / 2,
			y: h / 2 - l / 2 - 30,
			width: l,
			height: l,
			image: imageObj,
		});
		layer.add(image);
		layer.draw();
	};
	imageObj.src = 'ichou2.svg';
}

function loadChannelArtCute() {
	var allwid = 2560;
	var allhei = 1440;
	var wid = 1546;
	var hei = 423;
	var leftspace = (allwid - wid)/2;
	var topspace = (allhei - hei)/2;
	stage = new Konva.Stage({
		container: 'container',
		width: allwid,
		height: allhei,
		color: '#FFFDE7',
	});
	var layer = new Konva.Layer();
	stage.add(layer);
	var qRect = new Konva.Rect({
		fill: '#FFFDE7',
		width: allwid,
		height: allhei,
	});
	qRectW = qRect;
	layer.add(qRect);
	//var imageWidth = allwid;
	var space = 0;//200;
	var text = new Konva.Text({
		x: leftspace,
		y: topspace,
		fontSize: 160,
		fontFamily: 'UD デジタル 教科書体 NP',
		text: 'Ison School',
		fill: '#004D40',
		align: 'center',
		verticalAlign: 'middle',
		width: wid,
		height: hei,
	});
	//layer.add(text);

	var subtext = new Konva.Text({
		x: leftspace,
		y: topspace + 230,
		fontSize: 80,
		fontFamily: 'UD デジタル 教科書体 NP',
		text: 'School for everyone!',
		fill: '#00695C',
		align: 'center',
		verticalAlign: 'middle',
		width: wid,
		height: hei/2,
	});
	var imageObj = new Image();
	imageObj.onload = () => {
		var image = new Konva.Image({
			x: (allwid-2800)/2,//leftspace + space,
			y: (allhei-4200)/2+1000,//0,//topspace, //+ (hei - imageWidth) / 2.0 + textOfffsetY,
			width: 2800,
			height: 4200,
			image: imageObj,
		});
		layer.add(image);
		//layer.add(text);
		//layer.add(subtext);
		layer.draw();
	};
	imageObj.src = 'ichou-pic.jpg';

}
