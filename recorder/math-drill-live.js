class VideoData {
	constructor(){}
}

class Problem {
	constructor(a, b, c, operator) {
		this.a = a;
		this.b = b;
		this.c = c;
		this.operator = operator;
	}
	operationStr() {
		return this.operator;
	}
}

class ProblemGenerator {
	generate(aList, bList, num, condition, creator) {
		var ret = [];
		for (var m = 0; m < 100000; m++) {
			var a = aList[Math.floor(Math.random() * aList.length)];
			var b = bList[Math.floor(Math.random() * bList.length)];
			var ok = true;
			for (var r of ret) {
				if (r.a == a && r.b == b) {
					ok = false;
				}
			}
			if (!condition(a,b)) {
				ok = false;
			}
			if (ok) {
				ret.push(creator(a, b));
			}
			if (ret.length >= num) {
				return ret;
			}
		}
		return ret;
	}
}

var fileHandle;
var filenameHandle;

const myFontSize = 200;
const myWidth = 1280;
const myHeight = 720;

const speeds = [1, 0.5];

class VideoDuration {
	constructor(titleDuration, qDuration, aDuration, blankDuration, endingDuration) {
		this.titleDuration = titleDuration;
		this.qDuration = qDuration;
		this.aDuration = aDuration;
		this.blankDuration = blankDuration;
		this.endingDuration = endingDuration;
	}
	getQDuration(n) {
		return this.qDuration * speeds[n];
	}
	getADuration(n) {
		return this.aDuration * speeds[n];
	}
}

class VideoLayer {
	constructor(stage){
		this.stage = stage;
		// this.makeTitleLayer();
		// this.makeEndingLayer();
		// this.makeBlankLayer();
		this.makeQLayer();
	}

	/*
	makeTitleLayer() {
		this.titleLayer = new Konva.Layer();
		var titleRect = new Konva.Rect({
			fill: '#222222',
			width: myWidth,
			height: myHeight,
		});
		this.titleLayer.add(titleRect);

		this.stage.add(this.titleLayer);
	}

	makeEndingLayer() {
		this.endingLayer = new Konva.Layer();
		var endingRect = new Konva.Rect({
			fill: '#222222',
			width: myWidth,
			height: myHeight,
		});
		this.endingLayer.add(endingRect);

		this.stage.add(this.endingLayer);
	}

	makeBlankLayer() {
		this.blankLayer = new Konva.Layer();
		var rect = new Konva.Rect({
			fill: '#222222',
			width: myWidth,
			height: myHeight,
		});
		this.blankLayer.add(rect);

		this.stage.add(this.blankLayer);
	}*/

	makeQLayer(problem) {
		if (this.qLayer) {
			this.qLayer.removeFromParent();
		}
		this.qLayer = new Konva.Layer();
		var qRect = new Konva.Rect({
			fill: '#222222',
			width: myWidth,
			height: myHeight,
		});
		this.qLayer.add(qRect);
		var initX = 50;
		this.aText = getText('', initX, 1.5);
		this.operatorText = getText('+', initX + myFontSize * 1.5, 1);
		this.bText = getText('', initX + myFontSize * 2.5, 1);
		this.equalText = getText('=', initX + myFontSize * 3.5, 1);
		this.cText = getText('', initX + myFontSize * 4.5, 1.5, 'lightblue');
		this.qLayer.add(this.aText);
		this.qLayer.add(this.operatorText);
		this.qLayer.add(this.bText);
		this.qLayer.add(this.equalText);
		this.qLayer.add(this.cText);
	
		this.stage.add(this.qLayer);
	}

	adjustPlace(a) {
		if (this.lastA == a) {
			return;
		}
		if (a >= 10) {
			var initX = 45;
			this.aText.x(initX);
			this.aText.width(myFontSize * 1.5);
			this.operatorText.x(initX+myFontSize * 1.5);
			this.bText.x(initX+myFontSize*2.5);
			this.equalText.x(initX+myFontSize*3.5);
			this.cText.x(initX+myFontSize*4.5);
		} else {
			var initX = 70;
			this.aText.x(initX);
			this.aText.width(myFontSize);
			this.operatorText.x(initX+myFontSize * 1);
			this.bText.x(initX+myFontSize*2);
			this.equalText.x(initX+myFontSize*3);
			this.cText.x(initX+myFontSize*4);
		}
		this.lastA = a;
	}

	setTitleLayer() {
		this.setBlankLayer();
	}

	setEndingLayer() {
		this.setBlankLayer();
	}

	setBlankLayer() {
		this.aText.text('');
		this.operatorText.text('');
		this.bText.text('');
		this.equalText.text('');
		this.cText.text('');
		this.qLayer.draw();
	}

	setQLayer(a, b, c, operation) {
		return () => {
			this.adjustPlace(a);
			this.aText.text(''+a);
			this.bText.text(''+b);
			this.cText.text(''+c);
			this.operatorText.text(operation);
			this.equalText.text('=');
			this.qLayer.draw();
		};
	}
}

class Video {
	constructor(id, problems, videoLayer, videoDuration) {
		this.id = id;
		this.problems = problems;
		this.videoLayer = videoLayer;
		this.videoDuration = videoDuration;
		this.frames = [];
		// this.frames.push({make:()=>videoLayer.setTitleLayer(), duration: videoDuration.titleDuration});
		this.frames.push({make:videoLayer.setQLayer(this.problems[0].a, this.problems[0].b, '', this.problems[0].operator), duration: videoDuration.titleDuration});
		for (var n = 0; n < speeds.length; n++) {
			for (var p of this.problems) {
				this.frames.push({make:videoLayer.setQLayer(p.a, p.b, '', p.operator), duration: videoDuration.getQDuration(n)});
				this.frames.push({make:videoLayer.setQLayer(p.a, p.b, p.c, p.operator), duration: videoDuration.getADuration(n)});
				this.frames.push({make:()=>videoLayer.setBlankLayer(), duration: videoDuration.blankDuration});
			}
		}
		this.frames.push({make:()=>videoLayer.setEndingLayer(), duration: videoDuration.endingDuration});
	}
	
	start() {
		this.nextFrame(0);
	}

	nextFrame(frameNo) {
		if (this.timeoutId) {
			window.clearTimeout(this.timeoutId);
		}
		console.log(frameNo);
		if (frameNo == this.frames.length) {
			nextVideo();
			return;
		}
		this.frames[frameNo].make();
		this.timeoutId = window.setTimeout(event => {
			this.nextFrame(frameNo+1);
		}, this.frames[frameNo].duration);
	}
}

function getText(text, x, widthEm, color) {
	var t = new Konva.Text({
		x: x,
		y: 0,
		fontSize: myFontSize,
		fontFamily: 'UD デジタル 教科書体 NP',
		text: text,
		fill: color ? color : 'white',
		align: 'center',
		verticalAlign: 'middle',
		width: myFontSize * widthEm,
		height: myHeight,
	}); 
	return t;
}

var videoList = [];
var videoNo = 0;

function load() {
	var stage = new Konva.Stage({
		container: 'container',
		width: myWidth,
		height: myHeight,
		color: '#222222',
	});
	var videoLayer = new VideoLayer(stage);
	var videoDuration = new VideoDuration(1000, 4000, 2000, 1000, 12000);
	
	for (var i = 1; i < 100; i++) {
		var problems = [];
		problems.push(...new ProblemGenerator().generate([i], [0,1,2,3,4,5,6,7,8,9], 10, (a, b) => a + b < 100, (a, b) => new Problem(a, b, a+b, "+")));
		problems.push(...new ProblemGenerator().generate([i], [0,1,2,3,4,5,6,7,8,9], 10, (a, b) => a - b >= 0, (a, b) => new Problem(a, b, a-b, "-")));
		problems.push(...new ProblemGenerator().generate([i], [0,1,2,3,4,5,6,7,8,9], 10, (a, b) => a * b < 100, (a, b) => new Problem(a, b, a*b, "×")));
		problems.push(...new ProblemGenerator().generate([i], [1,2,3,4,5,6,7,8,9], 10, (a, b) => Math.floor(a/b) * b == a, (a, b) => new Problem(a, b, a/b, "÷")));
		problems.sort((a,b)=>Math.random() - 0.5);
		problems.splice(10);
		videoList.push(((idLocal, problems) => (() => {
			new Video(idLocal, problems, videoLayer, videoDuration).start();
		}))('', problems));
	}
	nextVideo();
}

function nextVideo() {
	if (videoNo >= videoList.length) {
		return;
	}
	//if (videoNo >= 3) return;
	videoList[videoNo]();
	videoNo++;
}
