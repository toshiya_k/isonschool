var fileHandle;
var filenameHandle;

const myWidth = 1280;
const myHeight = 720;
const maxBox = 10;
const maxLine = 2;

const GenderNeutral = 0;
const GenderMale = 1;
const GenderFemale = 2;

var allData = "";

function calStrWidthHeight(text) {
	var e = document.getElementById("widthcheck");
	e.textContent = text;
	return { strWidth: e.offsetWidth, strHeight: e.offsetHeight };
}

class VideoLayer {
	constructor(stage){
		this.stage = stage;
		this.qLayer = new Konva.Layer();
		this.qRect = new Konva.Rect({
			fill: '#222222',
			width: myWidth,
			height: myHeight,
		});
		this.qLayer.add(qRect);

		this.oriTexts = new Array(maxBox);
		this.oriRubies = new Array(maxBox);
		this.ansTexts = new Array(maxBox);
		this.ansRubies = new Array(maxBox);
		for (var j = 0; j < maxBox; j++) {
			this.oriTexts[j] = new Konva.Text({
				fill: 'white',
				align: 'center',
				verticalAlign: 'middle',
				fontFamily: 'UD デジタル 教科書体 NP',
			});	
			this.oriRubies[j] = new Konva.Text({
				fill: 'white',
				align: 'center',
				verticalAlign: 'middle',
				fontFamily: 'UD デジタル 教科書体 NP',
			});	
			this.ansTexts[j] = new Konva.Text({
				fill: 'white',
				align: 'center',
				verticalAlign: 'middle',
				fontFamily: 'UD デジタル 教科書体 NP',
			});	
			this.ansRubies[j] = new Konva.Text({
				fill: 'white',
				align: 'center',
				verticalAlign: 'middle',
				fontFamily: 'UD デジタル 教科書体 NP',
			});	
			this.qLayer.add(this.oriTexts[j]);
			this.qLayer.add(this.oriRubies[j]);
			this.qLayer.add(this.ansTexts[j]);
			this.qLayer.add(this.ansRubies[j]);

		}
		this.oriImageObjects = new Array(maxLine);
		this.oriPersonImages = new Array(maxLine);
		this.ansImageObjects = new Array(maxLine);
		this.ansPersonImages = new Array(maxLine);
		for (var p = 0; p < maxLine; p++) {
			this.oriImageObjects[p] = new Array(3);
			this.oriPersonImages[p] = new Array(3);
			for (var g = 0; g < 3; g++) {
				this.oriImageObjects[p][g] = new Image();
				this.oriImageObjects[p][g].onload = ((p, g) => (() => {
					this.oriPersonImages[p][g] = new Konva.Image({
						image: this.oriImageObjects[p][g],
					});
					this.oriPersonImages[p][g].visible(false);
					this.qLayer.add(this.oriPersonImages[p][g]);
				}))(p,g);
				if (g == 0) {
					this.oriImageObjects[p][g].src = 'person-neutral' + p + '.svg';
				} else if (g == 1) {
					this.oriImageObjects[p][g].src = 'person-male' + p + '.svg';
				} else if (g == 2) {
					this.oriImageObjects[p][g].src = 'person-female' + p + '.svg';
				}
			}
			this.ansImageObjects[p] = new Array(3);
			this.ansPersonImages[p] = new Array(3);
			for (var g = 0; g < 3; g++) {
				this.ansImageObjects[p][g] = new Image();
				this.ansImageObjects[p][g].onload = ((p, g) => (() => {
					this.ansPersonImages[p][g] = new Konva.Image({
						image: this.ansImageObjects[p][g],
					});
					this.ansPersonImages[p][g].visible(false);
					this.qLayer.add(this.ansPersonImages[p][g]);
				}))(p,g);
				if (g == 0) {
					this.ansImageObjects[p][g].src = 'person-neutral' + p + '.svg';
				} else if (g == 1) {
					this.ansImageObjects[p][g].src = 'person-male' + p + '.svg';
				} else if (g == 2) {
					this.ansImageObjects[p][g].src = 'person-female' + p + '.svg';
				}
			}
		}
		var imageObj = new Image();
		imageObj.onload = () => {
			var l = 44;
			this.sayImage = new Konva.Image({
				x: (myWidth - l) / 2,
				y: 6,
				width: l,
				height: l,
				image: imageObj,
			});
			this.qLayer.add(this.sayImage);
		};
		imageObj.src = 'icon.svg';
		
		this.stage.add(this.qLayer);

		this.iconImageObj = new Image();
		var l = 160;
		this.iconImageObj.onload = () => {
			this.iconImage = new Konva.Image({
				x: 110, //(myWidth - l) / 2.0,//110,
				y: (myHeight - l) / 2.0 - 120,
				width: l,
				height: l,
				image: this.iconImageObj,
			});
			this.qLayer.add(this.iconImage);
			this.qLayer.draw();
		};
		this.iconImageObj.src = 'ichou.svg';
	
		this.isonSchoolText = new Konva.Text({
			x: 260,
			y: (myHeight - l) / 2.0 - 120,
			fontSize: 140,
			fontFamily: 'UD デジタル 教科書体 NP',
			text: 'Ison School',
			fill: 'white',
			align: 'center',
			verticalAlign: 'middle',
			width: (myWidth - 300),
			height: l,
		});
		this.qLayer.add(this.isonSchoolText);
		this.titleText = new Konva.Text({
			x: 50,
			y: (myHeight - l)/ 2.0 + 60,
			fontSize: 60,
			fontFamily: 'UD デジタル 教科書体 NP',
			text: 'Japanese grammar exercise #1000',
			fill: 'white',
			align: 'center',
			verticalAlign: 'middle',
			width: myWidth - 100,
			height: l,
		});
		this.qLayer.add(this.titleText);
		this.subtitleText = new Konva.Text({
			x: 50,
			y: (myHeight - l)/ 2.0 + 160,
			fontSize: 60,
			fontFamily: 'UD デジタル 教科書体 NP',
			text: 'Japanese grammar exercise #1000',
			fill: 'lightblue',
			align: 'center',
			verticalAlign: 'middle',
			width: myWidth - 100,
			height: l,
		});
		this.qLayer.add(this.subtitleText);
	}

	makeQLayer(serie, sentence, exercise) {
		var strWidthW = calStrWidthHeight("W").strWidth;
		exercise.BoxNum = exercise.Problems[0].OriginalCandidates.length;
		var lines = getLines(sentence);
		var lineNum = lines.length;
		this.lines = lines;
		this.lineNum = lineNum;
		console.log("lineNum:"+lineNum);

		var showsPersonImages = lineNum > 1;
		for (var p of exercise.Problems) {
			if (showsPersonImages) { break; }
			var gmo = genderMap(sentence, p.OriginalCandidates);
			var gma = genderMap(sentence, p.AnswerCandidates);
			if (Object.keys(gmo).length != Object.keys(gma).length) {
				showsPersonImages = true;
				break;
			}
			for (var k of Object.keys(gmo)) {
				if (gmo[k] != gma[k]) {
					showsPersonImages = true;
					break;
				}
			}
		}
		this.showsPersonImages = showsPersonImages;

		var strSumWidths = new Array(lineNum);
		var maxStrSumWidth = 0;
		var maxTextStrHeight = 0;
		var maxRubyStrHeight = 0;
		exercise.MaxStrWidths = new Array(exercise.BoxNum);
		for (var p = 0; p < lineNum; p++) {
			strSumWidths[p] = 0;
			for (var i = 0; i < exercise.BoxNum; i++) {
				if (sentence.Boxes[i].PersonName != lines[p]) {
					continue;
				}
				var max = 0;
				for (var j = 0; j < exercise.Problems.length; j++) {
					var wh = calStrWidthHeight(exercise.Problems[j].OriginalCandidates[i].Word);
					if (max < wh.strWidth) {
						max = wh.strWidth;
					}
					if (maxTextStrHeight < wh.strHeight) {
						maxTextStrHeight = wh.strHeight;
					}
					wh = calStrWidthHeight(exercise.Problems[j].AnswerCandidates[i].Word);
					if (max < wh.strWidth) {
						max = wh.strWidth;
					}
					if (maxTextStrHeight < wh.strHeight) {
						maxTextStrHeight = wh.strHeight;
					}
					wh = calStrWidthHeight(exercise.Problems[j].OriginalCandidates[i].Ruby);
					if (maxRubyStrHeight < wh.strHeight) {
						maxRubyStrHeight = wh.strHeight;
					}
					wh = calStrWidthHeight(exercise.Problems[j].AnswerCandidates[i].Ruby);
					if (maxRubyStrHeight < wh.strHeight) {
						maxRubyStrHeight = wh.strHeight;
					}
				}
				exercise.MaxStrWidths[i] = max + strWidthW;
				strSumWidths[p] += exercise.MaxStrWidths[i];
			}
			if (strSumWidths[p] > maxStrSumWidth) {
				maxStrSumWidth = strSumWidths[p];
			}
		}
		var imageStrWidth = calStrWidthHeight("あ").strWidth; // だいたい1文字ぶん。画像の部分用。
		if (!showsPersonImages) {
			imageStrWidth = 0;
		}
		maxTextStrHeight *= 1.05;
		maxRubyStrHeight *= 1.05;
		var textFontSize = 100 * (myWidth - 100) / (maxStrSumWidth + imageStrWidth * 2);
		var rubyFontSize = 100000;
		for (var i = 0; i < exercise.BoxNum; i++) {
			var strWidth = exercise.MaxStrWidths[i];
			for (var p of exercise.Problems) {
				var w = 100 * strWidth / (calStrWidthHeight(p.OriginalCandidates[i].Ruby).strWidth + strWidthW);
				if (w < rubyFontSize) {
					rubyFontSize = w;
				}
				w = 100 * strWidth / (calStrWidthHeight(p.AnswerCandidates[i].Ruby).strWidth + strWidthW);
				if (w < rubyFontSize) {
					rubyFontSize = w;
				}
			}
		}
		if (serie.HidesRuby) {
			rubyFontSize = 0;
		}
		var textHeight = maxTextStrHeight * textFontSize / 100.0;
		var rubyHeight = maxRubyStrHeight * rubyFontSize / 100.0;

		const paddingTop = 50;
		if ((myHeight-paddingTop*2.0)/lineNum - 2.0*(textHeight + rubyHeight) < 0) {
			var hasToDecrease = textHeight + rubyHeight - (myHeight - paddingTop * 2.0) / (2.0 * lineNum);
			textHeight -= hasToDecrease * textFontSize / (textFontSize + rubyFontSize);
			rubyHeight -= hasToDecrease * rubyFontSize / (textFontSize + rubyFontSize);
			textFontSize = textHeight * 100.0 / maxTextStrHeight;
			rubyFontSize = rubyHeight * 100.0 / maxRubyStrHeight;
		}
		var extraHeight = (myHeight-paddingTop*2.0)/lineNum - 2.0*(textHeight + rubyHeight);
		var extraHeightPortion = extraHeight / 10.0;

		var personImageWidth = textFontSize * imageStrWidth / 100.0;

		var topMargin = paddingTop + extraHeightPortion * 3;
		var l = topMargin * 0.6;
		if (l > 80) {
			l = 80;
		}
		this.sayImage.x((myWidth - l) / 2);
		this.sayImage.y(topMargin * 0.2);
		this.sayImage.width(l);
		this.sayImage.height(l);
		for (var p = 0; p < lineNum; p++) {
			var x = imageStrWidth * textFontSize / 100.0 / 2 + (myWidth - maxStrSumWidth * textFontSize / 100) / 2;
			console.log("imageStrWidth:"+imageStrWidth+",x:"+x+",textFontSize:"+textFontSize + ", rubyFontSize:"+rubyFontSize + ", extraHeight" + extraHeight+",lineNum:"+lineNum);
			var oriRubyY = paddingTop + p*(myHeight-paddingTop*2)/lineNum + extraHeightPortion * 3;
			var oriTextY = oriRubyY + rubyHeight + extraHeightPortion * 1;
			var ansTextY = oriTextY + textHeight + extraHeightPortion * 2;
			var ansRubyY = ansTextY + textHeight + extraHeightPortion * 1;
			console.log("x:"+x+",oriRubyY:"+oriRubyY);
			if (showsPersonImages) {
				for (var g = 0; g < 3; g++) {
					this.oriPersonImages[p][g].width(personImageWidth);
					this.oriPersonImages[p][g].height(personImageWidth);
					this.oriPersonImages[p][g].x(50);
					this.oriPersonImages[p][g].y(oriTextY);
					this.oriPersonImages[p][g].visible(false);
					this.ansPersonImages[p][g].width(personImageWidth);
					this.ansPersonImages[p][g].height(personImageWidth);
					this.ansPersonImages[p][g].x(50);
					this.ansPersonImages[p][g].y(ansTextY);
					this.ansPersonImages[p][g].visible(false);
				}
			}
			for (var i = 0; i < exercise.BoxNum; i++) {
				if (lines[p] != sentence.Boxes[i].PersonName) {
					continue;
				}
				this.oriTexts[i].x(x);
				this.oriTexts[i].y(oriTextY);
				this.oriTexts[i].fontSize(textFontSize);
				this.oriTexts[i].width(exercise.MaxStrWidths[i] * textFontSize / 100);
				this.oriTexts[i].height(textHeight);
				this.oriRubies[i].x(x);
				this.oriRubies[i].y(oriRubyY);
				this.oriRubies[i].fontSize(rubyFontSize);
				this.oriRubies[i].width(exercise.MaxStrWidths[i] * textFontSize / 100);
				this.oriRubies[i].height(rubyHeight);
				this.oriRubies[i].visible(!exercise.HidesRuby);
				this.ansTexts[i].x(x);
				this.ansTexts[i].y(ansTextY);
				this.ansTexts[i].fontSize(textFontSize);
				this.ansTexts[i].width(exercise.MaxStrWidths[i] * textFontSize / 100);
				this.ansTexts[i].height(textHeight);
				this.ansRubies[i].x(x);
				this.ansRubies[i].y(ansRubyY);
				this.ansRubies[i].fontSize(rubyFontSize);
				this.ansRubies[i].width(exercise.MaxStrWidths[i] * textFontSize / 100);
				this.ansRubies[i].height(rubyHeight);
				this.ansRubies[i].visible(!exercise.HidesRuby);
				x += exercise.MaxStrWidths[i] * textFontSize / 100;
			}
			console.log("A " + x);
		}
		for (var i = exercise.BoxNum; i < maxBox; i++) {
			this.oriTexts[i].text('');
			this.oriRubies[i].text('');
			this.ansTexts[i].text('');
			this.ansRubies[i].text('');
		}
		this.hintText.text('');
	}

	setTitle(datum) {
		this.makeQLayer(datum.Serie, datum.Sentence, datum.Exercise);
		this.hintText.text('');
		this.sayImage.visible(false);
		for (var i = 0; i < datum.Exercise.BoxNum; i++) {
			this.oriTexts[i].text("");
			this.oriRubies[i].text("");
			this.ansTexts[i].text("");
			this.ansRubies[i].text("");
		}
		this.iconImage.visible(true);
		this.isonSchoolText.text('');
		this.titleText.text(datum.Title + " #" + datum.Sentence.No);
		this.subtitleText.text(datum.Subtitle);
		this.qLayer.draw();
		return datum.Category.ID + "/" + datum.Sentence.SerieName + "/" + datum.Sentence.No + "/" + datum.Exercise.Name + "/0/title";
	}

	setQLayer(category, serie, sentence, exercise, problem, status) {
		this.iconImage.visible(false);
		this.isonSchoolText.text('');
		this.titleText.text('');
		this.subtitleText.text('');
		this.makeQLayer(serie, sentence, exercise);
		if (status[2] == 'a') {
			this.sayImage.visible(false);
		} else {
			this.sayImage.visible(true);
		}
		if (status[1] == 'c') {
			console.log('cは使えません。'+status)
		}
		for (var i = 0; i < exercise.BoxNum; i++) {
			if (status[0] == '0' || status[0] == 'p') {
				this.oriTexts[i].text("");
				this.oriRubies[i].text("");
			} else if (status[0] == '1') {
				this.oriTexts[i].text(problem.OriginalCandidates[i].Word);
				this.oriRubies[i].text(problem.OriginalCandidates[i].Ruby);
			}
			if (status[1] == '0' || status[1] == 'p') {
				this.ansTexts[i].text("");
				this.ansRubies[i].text("");
			} else if (status[1] == '1') {
				this.ansTexts[i].text(problem.ChangeCandidatePos == i ? problem.ChangeCandidates[i].Word : "");
				this.ansRubies[i].text(problem.ChangeCandidatePos == i ? problem.ChangeCandidates[i].Ruby : "");
			} else if (status[1] == '2') {
				this.ansTexts[i].text(problem.ChangeCandidates[i].Word);
				this.ansRubies[i].text(problem.ChangeCandidates[i].Ruby);
			} else if (status[1] == '3') {
				this.ansTexts[i].text(problem.AnswerCandidates[i].Word);
				this.ansRubies[i].text(problem.AnswerCandidates[i].Ruby);
			}
			if (problem.ChangeCandidates[i].Word) {
				this.ansTexts[i].fill('lightblue');
				this.ansRubies[i].fill('lightblue');
			} else {
				this.ansTexts[i].fill('white');
				this.ansRubies[i].fill('white');
			}
		}

		if (this.showsPersonImages) {
			var gmo = genderMap(sentence, problem.OriginalCandidates);
			var gma = genderMap(sentence, problem.AnswerCandidates);
			for (var p = 0; p < this.lineNum; p++) {
				for (var g = 0; g < 3; g++) {
					this.oriPersonImages[p][g].visible(status[0] != '0' && g == gmo[this.lines[p]]);
					this.ansPersonImages[p][g].visible(status[1] != '0' && g == gma[this.lines[p]] && gmo[this.lines[p]] != gma[this.lines[p]]);
				}
			}
		}

		this.qLayer.draw();
		return category.ID + "/" + sentence.SerieName + "/" + sentence.No + "/" + exercise.Name + "/" + problem.ProblemNo + "/" + status;
	}
}

async function writeFile(contents, filename, iNo) {
	try {
		const writer = await fileHandle.createWriter();
		await writer.truncate(0);
		await writer.write(0, contents);
		await writer.close();
	} catch (err) {
		console.error(err.message + filename + iNo);
		images.push(images[iNo]);
	}
}

async function writeFilename(contents, iNo) {
	try {
		const writer = await filenameHandle.createWriter();
		await writer.truncate(0);
		await writer.write(0, contents);
		await writer.close();
	} catch (err) {
		console.error(err.message + contents + iNo);
		images.push(images[iNo]);
	}
}

async function setVideoFile() {
	try {
		// FileSystemFileHandleを取得
		fileHandle = await window.chooseFileSystemEntries({type: 'saveFile'});
	} catch (err) {
		console.error(err.message);
	}
}

async function setFilenameFile() {
	try {
		// FileSystemFileHandleを取得
		filenameHandle = await window.chooseFileSystemEntries({type: 'saveFile'});
	} catch (err) {
		console.error(err.message);
	}
}

var images = [];

var statuses = [
	"00aA", // まっくろ
	"p0aA", // 問題文の顔のみ
	"13aA", // すべて
	"10aA", // 問題文のみ
	"12aA", // 変化するところすべて含めて
	"12bA", // 変化するところをすべてみせて，シンキングタイム
	"1paA", // 変化するところは全く見せない
	"1pbA", // 変化するところは全く見せない，シンキングタイム
	"p1aA", // もとの文なし。主に変化するところだけ。
	"p1bA", // もとの文なし。主に変化するところだけ。シンキングタイム
];

function load(m, n) {
	images = [];
	var stage = new Konva.Stage({
		container: 'container',
		width: myWidth,
		height: myHeight,
		color: '#222222',
	});
	var videoLayer = new VideoLayer(stage);
	for (var i = 0; i < data.length; i++) {
		var datum = data[i];
		//videoLayer.makeQLayer(data[i].Exercise);
		//videoLayer.makeQLayer(data[i].Sentence, data[i].Exercise);
		images.push(((datum)=>()=>videoLayer.setTitle(datum))(datum));
		for (var n = 0; n < datum.Exercise.Problems.length; n++) {
			for (var status of statuses) {
				images.push(((videoLayer, datum, n, status)=>(()=>videoLayer.setQLayer(datum.Category, datum.Serie, datum.Sentence, datum.Exercise, datum.Exercise.Problems[n], status)))(videoLayer, datum, n, status));
			}
		}
	}
	window.setTimeout(nextImage, 100);
}

function saveImage(filename, iNo) {
	var canvas = document.querySelector("canvas");
	//var f = (filename, iNo) => ((blob) => {writeFile(blob, filename, iNo); writeFilename(filename, iNo)});
	//canvas.toBlob(f(filename, iNo), "image/png");
	allData += filename+"\n";
	allData += canvas.toDataURL()+"\n";
}

var imageNo = -100;
var blob;
function nextImage() {
	if (imageNo >= images.length) {
		blob = new Blob([ allData ], { "type" : "text/plain" });
		var a = document.createElement('a');
		a.download = 'pngfiles.txt';
		a.href = window.URL.createObjectURL(blob);
		a.style.display = 'none';
		document.body.appendChild(a);
		a.click();
		document.body.removeChild(a);
		console.log("終了");
		return;
	}
	if (imageNo >= 0) {
		var filename = images[imageNo]();
		saveImage(filename, imageNo);
	}
	// window.setTimeout(((filename)=>()=>saveImage(filename))(filename), 100);
	imageNo++;
	//nextImage();
	window.setTimeout(nextImage, 10);
}

// GenderMap は，性別を返します。
function genderMap(sentence, candidates) {
	var genderMap = {};
	for (var i = 0; i < sentence.Boxes.length; i++) {
		genderMap[sentence.Boxes[i].PersonName] = GenderNeutral;
	}

	for (var i = 0; i < sentence.Boxes.length; i++) {
		for (var c of sentence.Boxes[i].Candidates) {
			if (c.Word == candidates[i].Word && c.PersonConditions) {
				for (var pc of c.PersonConditions) {
					if (genderMap[pc.PersonName] && genderMap[pc.PersonName] != pc.Gender) {
						console.error("Genderがいっちしません。" + pc.PersonName)
					}
					genderMap[pc.PersonName] = pc.Gender
				}
			}
		}
	}
	return genderMap
}


function getLines(sentence) {
	var lines = [];
	var lastPersonName = '!!!!!';
	for (var i = 0; i < sentence.Boxes.length; i++) {
		if (sentence.Boxes[i].PersonName != lastPersonName) {
			lines.push(sentence.Boxes[i].PersonName);
			lastPersonName = sentence.Boxes[i].PersonName;
		}
	}
	return lines;
}