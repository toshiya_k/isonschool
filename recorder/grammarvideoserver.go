package main

import "net/http"

func main() {
	fs := http.FileServer(http.Dir("/Users/juny/go/src/isonschool/recorder/"))
	http.Handle("/", fs)
	http.ListenAndServe(":8082", nil)
}
