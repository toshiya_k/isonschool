# The Question Marker
1. Questions in polite form
2. The question marker in casual speech
3. 「か」 used in relative clauses
4. Using question words

## Questions in polite form

### Vocabulary
1. 田中 【た・なか】 – Tanaka (last name)
2. お母さん【お・かあ・さん】 – mother (polite)
3. どこ – where
4. 鈴木 【すず・き】 – Suzuki (last name)
5. 母 【はは】 – mother
6. 買い物 【か・い・もの】 – shopping
7. 行く 【い・く】 (u-verb) – to go
8. イタリア – Italy
9. 料理 【りょう・り】 – cooking; cuisine; dish
10. 食べる 【た・べる】 (ru-verb) – to eat
11. すみません – sorry (polite)
12. ちょっと – a little
13. お腹 【お・なか】 – stomach
14. いっぱい – full
15. ごめんなさい – sorry (polite)
16. ごめん – sorry

The question marker is covered here because it is primarily used to clearly indicate a question in polite sentences. While it is entirely possible to express a question even in polite form using just intonation, the question marker is often attached to the very end of the sentence to indicate a question. The question marker is simply the hiragana character 「か」 and you don’t need to add a question mark. For previously explained reasons, you must not use the declarative 「だ」 with the question marker.

### Example 1
```
田中さん：お母さんはどこですか。
Tanaka-san: Where is (your) mother?

鈴木さん：母は買い物に行きました。
Suzuki-san: (My) mother went shopping.
```

### Example 2
```
キムさん：イタリア料理を食べに行きませんか。
Kim-san: Go to eat Italian food?

鈴木さん：すみません。ちょっと、お腹がいっぱいです。
Suzuki-san: Sorry. (My) stomach is a little full.
```

Here the question is actually being used as an invitation just like how in English we say, “Won’t you come in for a drink?” 「すみません」 is a polite way of apologizing. Slightly less formal is 「ごめんなさい」 while the casual version is simply 「ごめん」.

## The question marker in casual speech
### Vocabulary
1. こんな – this sort of
2. 本当 【ほん・とう】 – real
3. 食べる 【た・べる】 (ru-verb) – to eat
4. そんな – that sort of
5. ある (u-verb) – to exist (inanimate)

It makes sense to conclude that the question marker would work in exactly the same way in casual speech as it does in polite speech. However, this is not the case. The question marker 「か」 is usually not used with casual speech to make actual questions. It is often used to consider whether something is true or not. Depending on the context and intonation, it can also be used to make rhetorical questions or to express sarcasm. It can sound quite rough so you might want to be careful about using 「か」 for questions in the plain casual form.

### Examples
```
こんなのを本当に食べるか？
Do you think [he/she] will really eat this type of thing?

そんなのは、あるかよ！
Do I look like I would have something like that?!
```

Instead of 「か」, real questions in casual speech are usually asked with the explanatory の particle or nothing at all except for a rise in intonation, as we have already seen in previous sections.

```
こんなのを本当に食べる？
Are you really going to eat something like this?

そんなのは、あるの？
Do you have something like that?
```

## 「か」 used in relative clauses
### Vocabulary
1. 昨日【きのう】 – yesterday
2. 何【なに】 – what
3. 食べる 【た・べる】 (ru-verb) – to eat
4. 忘れる 【わす・れる】 (ru-verb) – to forget
5. 彼 【かれ】 – he; boyfriend
6. 言う 【い・う】 (u-verb) – to say
7. 分かる 【わ・かる】 (u-verb) – to understand
8. 先生 【せん・せい】 – teacher
9. 学校 【がっ・こう】 – school
10. 行く 【い・く】 (u-verb) – to go
11. 教える 【おし・える】 (ru-verb) – to teach; to inform
12. どう – how
13. 知る 【し・る】 (u-verb) – to know

Another use of the question marker is simply grammatical and has nothing to do with the politeness. A question marker attached to the end of a relative clause makes a mini-question inside a larger sentence. This allows the speaker to talk about the question. For example, you can talk about the question, “What did I eat today?” In the following examples, the question that is being considered is in red.

```
昨日何を食べたか忘れた。
Forgot what I ate yesterday.

彼は何を言ったか分からない。
Don’t understand what he said.

先生が学校に行ったか教えない？
Won’t you inform me whether teacher went to school?
```

In sentences like example 3 where the question being considered has a yes/no answer, it is common (but not necessary) to attach 「どうか」. This is roughly equivalent to saying, “whether or not” in English. You can also include the alternative as well to mean the same thing.

```
先生が学校に行ったかどうか知らない。
Don’t know whether or not teacher went to school.

先生が学校に行ったか行かなかったか知らない。
Don’t know whether teacher went to school or didn’t.
```

## Using question words
### Vocabulary
1. おいしい (i-adj) – tasty
1. クッキー – cookie
1. 全部 【ぜん・ぶ】 – everything
1. 食べる 【た・べる】 (ru-verb) – to eat
1. 誰 【だれ】 – who
1. 盗む 【ぬす・む】 (u-verb) – to steal
1. 知る 【し・る】 (u-verb) – to know
1. 犯人 【はん・にん】 – criminal
1. 見る 【み・る】 (ru-verb) – to see
1. この – this （abbr. of これの）
1. 中 【なか】 – inside
1. ～から (particle) – from ～
1. 選ぶ 【えら・ぶ】 (u-verb) – to select

While we’re on the topic of questions, this is a good time to go over question words (where, who, what, etc.) and what they mean in various contexts. Take a look at what adding the question marker does to the meaning of the words.

## Question Words
Word+Question Marker | Meaning
---|---
誰か|Someone
何か|Something
いつか|Sometime
どこか|Somewhere
どれか|A certain one from many

## Examples
As you can see by the following examples, you can treat these words just like any regular nouns.

```
誰かがおいしいクッキーを全部食べた。
Someone ate all the delicious cookies.

誰が盗んだのか、誰か知りませんか。
Doesn’t anybody know who stole it?

犯人をどこかで見ましたか。
Did you see the criminal somewhere?

この中からどれかを選ぶの。
(Explaining) You are to select a certain one from inside this (selection).
```


## Question words with inclusive meaning
### Vocabulary
1. 全部 【ぜん・ぶ】 – everything
皆 【みんな】 – everybody
皆さん 【みな・さん】 – everybody (polite)
この – this （abbr. of これの）
質問 【しつ・もん】 – question
答え 【こた・え】 – answer
知る 【し・る】 (u-verb) – to know
友達 【とも・だち】 – friend
遅れる 【おく・れる】 (ru-verb) – to be late
ここ – here
ある (u-verb) – to exist (inanimate)
レストラン – restaurant
おいしい (i-adj) – tasty
今週末 【こん・しゅう・まつ】 – this weekend
行く 【い・く】 (u-verb) – to go

The same question words in the chart above can be combined with 「も」 in a negative sentence to mean “nobody” （誰も）, “nothing” （何も）, “nowhere” （どこも）, etc.

「誰も」 and 「何も」 are primarily used only for negative sentences. Curiously, there is no way to say “everything” with question words. Instead, it is conventional to use other words like 「全部」. And although 「誰も」 can sometimes be used to mean “everybody”, it is customary to use 「皆」 or 「皆さん」

The remaining three words 「いつも」 (meaning “always”) and 「どれも」 (meaning “any and all”), and 「どこも」 (meaning everywhere) can be used in both negative and positive sentences.

## Inclusive Words
Word+も|Meaning
---|---
誰も|Everybody/Nobody
何も|Nothing (negative only)
いつも|Always
どこも|Everywhere
どれも|Any and all

### Examples
```
この質問の答えは、誰も知らない。
Nobody knows the answer of this question.

友達はいつも遅れる。
Friend is always late.

ここにあるレストランはどれもおいしくない 。
Any and all restaurants that are here are not tasty.

今週末は、どこにも行かなかった。
Went nowhere this weekend.
(Grammatically, this 「も」 is the same as the topic particle 「も」 so the target particle 「に」 must go before the topic particle 「も」 in ordering.)
```

## Question words to mean “any”
### Vocabulary
1. この – this （abbr. of これの）
質問 【しつ・もん】 – question
答え 【こた・え】 – answer
分かる 【わ・かる】 (u-verb) – to understand
昼ご飯 【ひる・ご・はん】 – lunch
いい (i-adj) – good
あの – that (over there) （abbr. of あれの）
人 【ひと】 – person
本当 【ほん・とう】 – real
食べる 【た・べる】 (ru-verb) – to eat

The same question words combined with 「でも」 can be used to mean “any”. One thing to be careful about is that 「何でも」 is read as 「なんでも」 and not 「なにでも」

## Words for “Any”
Word+でも|Meaning
---|---
誰でも|Anybody
何でも|Anything
いつでも|Anytime
どこでも|Anywhere
どれでも|Whichever

### Examples
```
この質問の答えは、誰でも分かる。
Anybody understands the answer of this question.

昼ご飯は、どこでもいいです。
About lunch, anywhere is good.

あの人は、本当に何でも食べる。
That person really eats anything.
```
