package hatena

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"isonschool/cli/ison"
	"log"
	"net/url"
	"os"

	"github.com/gomodule/oauth1/oauth"
)

// Client ...
type Client struct {
	Credentials oauth.Credentials
	OAuthClient *oauth.Client
}

const (
	hatenaSecretFilepath          = "/Users/juny/go/src/yossie/hatena/secret.json"
	hatenaTempCredentialsFilepath = "/Users/juny/go/src/yossie/hatena/tempcredentials.json"
)

// Credentials represents Token and Secret credential.
type Credentials struct {
	Token  string
	Secret string
}

// NewClient returns a new client.
func NewClient() *Client {
	var credentials Credentials
	ison.ReadJSON(&credentials, hatenaSecretFilepath)
	scope := url.Values{"scope": {"read_public,write_public"}}

	oauthClient := &oauth.Client{
		Credentials: oauth.Credentials{
			Token:  credentials.Token,
			Secret: credentials.Secret,
		},
		TemporaryCredentialRequestURI: "https://www.hatena.com/oauth/initiate",
		ResourceOwnerAuthorizationURI: "https://www.hatena.com/oauth/authorize",
		TokenRequestURI:               "https://www.hatena.com/oauth/token",
	}

	var tmpCredentials Credentials
	ison.ReadJSON(&tmpCredentials, hatenaTempCredentialsFilepath)

	if tmpCredentials.Token == "" {
		tempCredentials, err := oauthClient.RequestTemporaryCredentials(nil, "oob", scope)
		if err != nil {
			log.Fatal("RequestTemporaryCredentials:", err)
		}

		u := oauthClient.AuthorizationURL(tempCredentials, nil)
		fmt.Printf("1. Go to %s\n2. Authorize the application\n3. Enter verification code:\n", u)

		var code string
		fmt.Scanln(&code)

		fmt.Println("InputCode: ", code)

		tokenCard, _, err := oauthClient.RequestToken(nil, tempCredentials, code)
		if err != nil {
			log.Fatal("RequestToken:", err)
		}

		fmt.Println("Token: ", tokenCard.Token)
		fmt.Println("Secret: ", tokenCard.Secret)

		tmpCredentials.Token = tokenCard.Token
		tmpCredentials.Secret = tokenCard.Secret
		ison.WriteJSON(&tmpCredentials, hatenaTempCredentialsFilepath)
	}

	var token oauth.Credentials
	token.Token = tmpCredentials.Token
	token.Secret = tmpCredentials.Secret

	return &Client{Credentials: token, OAuthClient: oauthClient}

}

// Bookmark is a hatena bookmark object.
type Bookmark struct {
	Comment   string `json:"comment"`
	Permalink string `json:"permalink"`
	Private   bool   `json:"private"`
}

// PostBookmarkOfVideo ...
func (c *Client) PostBookmarkOfVideo(id, title string) string {
	resp, err := c.OAuthClient.Post(nil, &c.Credentials, "https://bookmark.hatenaapis.com/rest/1/my/bookmark", url.Values{"url": {"https://youtu.be/" + id}, "comment": {title}})
	if err != nil {
		fmt.Fprintln(os.Stderr, err)
	}
	defer resp.Body.Close()
	body, _ := ioutil.ReadAll(resp.Body)
	var bookmark Bookmark
	json.Unmarshal(body, &bookmark)
	fmt.Printf("%+v\n", bookmark)
	return bookmark.Permalink
}
