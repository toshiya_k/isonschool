package twitter

import (
	"bytes"
	"isonschool/cli/ison"
	"log"
	"math/rand"
	"strings"
	"time"

	"github.com/ChimeraCoder/anaconda"
)

// Credentials ...
type Credentials struct {
	AccessToken       string
	AccessTokenSecret string
	ConsumerKey       string
	ConsumerSecret    string
}

const credentialsJSONFilepath = "/Users/juny/go/src/yossie/twitter/credentials.json"

// Client ..
type Client struct {
	api *anaconda.TwitterApi
}

// NewClient returns new client.
func NewClient() *Client {
	rand.Seed(time.Now().Unix())

	var c Credentials
	ison.ReadJSON(&c, credentialsJSONFilepath)
	api := anaconda.NewTwitterApiWithCredentials(c.AccessToken, c.AccessTokenSecret, c.ConsumerKey, c.ConsumerSecret)
	return &Client{api: api}
}

// TweetVideo updates status.
func (t *Client) TweetVideo(id, title, description string) int64 {
	s := title
	link := "\nhttps://youtu.be/" + id
	if len(title)+len(link) > 280 {
		var b bytes.Buffer
		for _, c := range title {
			b.WriteRune(c)
			if len(b.Bytes())+len(link) > 280-3 {
				s = b.String()
				break
			}
		}
	}
	var desc string
	ds := strings.Split(description, "\n")
	start := rand.Intn(len(ds))
	for i := 0; i < len(ds); i++ {
		d := ds[(start+i)%len(ds)]
		if len(d) == 0 || d[0] == ' ' || (d[0] >= '0' && d[0] <= '9') {
			continue
		}
		if len(title)+len(link)+len(desc)+len(d) < 280-3 {
			desc += "\n" + d
		}
	}
	tweet, err := t.api.PostTweet(s+desc+link, nil)
	if err != nil && strings.Contains(err.Error(), "Status is a duplicate.") {
		return -1
	} else if err != nil {
		log.Fatalf("Error in tweeting. %s %v", s, err)
	}
	log.Println("tweeted:", tweet.Text, tweet.Id)
	return tweet.Id
}
