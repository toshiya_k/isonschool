package ruby_test

import (
	"isonschool/cli/ruby"
	"testing"
)

var yomi = map[string]string{
	"実装":   "じっそう",
	"家に帰る": "いえ に かえる",
	"空":    "そら",
}

func TestJumanpp(t *testing.T) {
	for k, v := range yomi {
		r := ruby.Jumanpp(k)
		if r != v {
			t.Fatalf("%s, %s", r, v)
		}
	}
}

var engipa = map[string]string{
	"This is the pen": "ðɪs ɪz ðə pɛn",
}

func TestEngToIPA(t *testing.T) {
	for k, v := range engipa {
		r := ruby.EngToIPA(k)
		if r != v {
			t.Fatalf("[%s], [%s]", r, v)
		}
	}
}

var pinyin = map[string]string{
	"你们好吗？我们都很好": "nǐmen hǎo ma ？ wǒmen dōu hěn hǎo",
}

func TestPinyin(t *testing.T) {
	for k, v := range pinyin {
		r := ruby.ConvertToPinyin(k)
		if r != v {
			t.Fatalf("[%s], [%s]", r, v)
		}
	}
}

type pair struct {
	alphanum string
	pinyin   string
}

var pinyinPhonetic = []pair{
	pair{alphanum: "wu2_", pinyin: "wú"},
	pair{alphanum: "che1_", pinyin: "chē"},
	pair{alphanum: "qiu2_", pinyin: "qiú"},
	pair{alphanum: "zuo4_", pinyin: "zuò"},
	pair{alphanum: "tang2_", pinyin: "táng"},
	pair{alphanum: "yan2_", pinyin: "yán"},
	pair{alphanum: "qian3_", pinyin: "qiǎn"},
	pair{alphanum: "lie4_", pinyin: "liè"},
	pair{alphanum: "bei4_", pinyin: "bèi"},
	pair{alphanum: "heng1_", pinyin: "hēng"},
	pair{alphanum: "kuai4_", pinyin: "kuài"},
	pair{alphanum: "o2_", pinyin: "ó"},
	pair{alphanum: "xiong1_", pinyin: "xiōng"},
	pair{alphanum: "n2_", pinyin: "ń"},
	pair{alphanum: "luu4_", pinyin: "lǜ"},
	pair{alphanum: "zhou4_mie1_", pinyin: "zhòumiē"},
}

func TestReplacePinyinPhonetic(t *testing.T) {
	for _, r := range pinyinPhonetic {
		n := ruby.ReplacePinyinPhonetic(r.alphanum)
		if n != r.pinyin {
			t.Fatalf("入力:%s 理想:%s 出力:%s", r.alphanum, r.pinyin, n)
		}
	}
}
