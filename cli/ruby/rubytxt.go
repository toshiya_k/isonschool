package ruby

import (
	"isonschool/cli/ison"
)

// TxtRuby は，ルビテキストです。
type TxtRuby struct {
	RubyFunc RubyFunc
	Pairs    []Pair
}

// MergeTxtRubies は，マージします。
func MergeTxtRubies(category ison.Category) {
	for _, file := range category.TxtRubyFiles() {
		m := ison.NewTxtMarshallerFromFile(file)
		for _, txtRuby := range ParseTxtRubies(m) {
			f := category.JSONRubyFile(txtRuby.RubyFunc.String())
			var data Data
			ison.ReadJSON(&data, f)
			for _, txt := range txtRuby.Pairs {
				data.Update(txt)
			}
			ison.WriteJSON(&data, f)
		}
	}
}

// ParseTxtRubies は，ペアをパースします
func ParseTxtRubies(m *ison.TxtMarshaller) []TxtRuby {
	m.Add("rubyfunc", func(d interface{}, k, v string) {
		d.(*TxtRuby).RubyFunc = RubyFunc(v)
	})
	m.Add("ruby", func(d interface{}, k, v string) {
		d.(*TxtRuby).Pairs = append(d.(*TxtRuby).Pairs, Pair{Word: k, Ruby: v})
	})
	var rubies []TxtRuby
	var success, isEOF bool
	for !isEOF {
		var d TxtRuby
		success, isEOF = m.Unmarshal(&d)
		if success {
			rubies = append(rubies, d)
		}
	}
	return rubies
}
