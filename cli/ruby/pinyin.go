package ruby

import (
	"bytes"
	"log"
	"os/exec"
	"strings"

	"github.com/mozillazg/go-pinyin"
)

var cache = make(map[string]string)

// ConvertToPinyin は，ピンインに変換します。
func ConvertToPinyin(text string) string {
	if c, ok := cache[text]; ok {
		return c
	}
	t := segments(text)
	a := pinyin.NewArgs()
	a.Fallback = func(r rune, a pinyin.Args) []string {
		if r == '？' || r == '。' || r == '、' || r == '，' || r == ' ' {
			return []string{string(r)}
		}
		log.Fatalf("ピンインがありません。%s", string(r))
		return []string{}
	}
	a.Style = pinyin.Tone
	a.Heteronym = true
	ret := pinyin.Pinyin(t, a)
	var b bytes.Buffer
	for _, r := range ret {
		if len(r) == 0 {
			continue
		}
		if len(r) > 1 {
			log.Printf("多音字! %+v", r)
		}
		b.WriteString(r[0])
	}
	cache[text] = b.String()
	return b.String()
}

func segments(s string) string {
	cmd := exec.Command("java", "-Xmx1024m", "-Dfile.encoding=UTF-8", "-classpath", "fnlp-core/target/fnlp-core-2.1-SNAPSHOT.jar:libs/trove4j-3.0.3.jar:libs/commons-cli-1.2.jar", "org.fnlp.nlp.cn.tag.CWSTagger", "-s", "models/seg.m", s)
	cmd.Dir = "/Users/juny/fnlp-master"
	var b bytes.Buffer
	cmd.Stdout = &b
	err := cmd.Run()
	if err != nil {
		log.Panicf("%v %s", err, s)
	}
	return strings.TrimSpace(b.String())
}
