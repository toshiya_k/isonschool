package ruby

import (
	"strings"

	"github.com/mattn/go-pipeline"
)

// EngToIPA は，英語を国際発音記号に変換します。
func EngToIPA(text string) string {
	out, _ := pipeline.Output([]string{"echo", text}, []string{"python3", "/Users/juny/go/src/isonschool/cli/ruby/engtoipa.py"})
	return strings.TrimSpace(string(out))
}
