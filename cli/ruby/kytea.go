package ruby

import (
	"bytes"
	"strings"

	"github.com/mattn/go-pipeline"
)

// KyteaJa は，読みを取得します。
func KyteaJa(text string) string {
	out, _ := pipeline.Output([]string{"echo", text}, []string{"kytea", "-model", "/Users/juny/go/src/isonschool/assets/kyteamodel/jp-0.4.7-1.mod"})
	var words = strings.Split(string(out), " ")
	var b bytes.Buffer
	var nospace bool
	for _, word := range words {
		terms := strings.Split(word, "/")
		if len(terms) <= 2 {
			continue
		}
		if b.Len() > 0 && !nospace {
			b.WriteString(" ")
		}
		b.WriteString(terms[2])
		nospace = terms[1] == "助動詞" || terms[1] == "語尾"
	}
	return b.String()
}

// KyteaZhHans は，読みを取得します。
func KyteaZhHans(text string) string {
	out, _ := pipeline.Output([]string{"echo", text}, []string{"kytea", "-model", "/Users/juny/go/src/isonschool/assets/kyteamodel/lcmc-0.4.0-1.mod"})
	var words = strings.Split(strings.TrimSpace(string(out)), " ")
	var b bytes.Buffer
	for _, word := range words {
		terms := strings.Split(word, "/")
		if len(terms) <= 1 {
			continue
		}
		if b.Len() > 0 {
			b.WriteString(" ")
		}
		b.WriteString(ReplacePinyinPhonetic(terms[1]))
	}
	return b.String()
}

// ReplacePinyinPhonetic は，声調の表示方法を変えます。
func ReplacePinyinPhonetic(s string) string {
	for _, r := range replaces {
		s = strings.ReplaceAll(s, r.old, r.new)
	}
	return s
}

// ReplacePinyinPhonetic2 は，声調の表示方法を変えます。
func ReplacePinyinPhonetic2(s string) string {
	for _, r := range replaceSymbols {
		s = strings.ReplaceAll(s, r.old, r.new)
	}
	for _, r := range exceptionSymbols {
		s = strings.ReplaceAll(s, r.old, r.new)
	}
	return s
}

// 带音标字符。
var phoneticSymbol = `
ā:a1
á:a2
ǎ:a3
à:a4
a:a5
ē:e1
é:e2
ě:e3
è:e4
e:e5
ō:o1
ó:o2
ǒ:o3
ò:o4
o:o5
ǘ:uu2
ǚ:uu3
ǜ:uu4
ü:uu5
ū:u1
ú:u2
ǔ:u3
ù:u4
u:u5
ī:i1
í:i2
ǐ:i3
ì:i4
i:i5
ń:n2
ň:n3
ǹ:n4
ḿ:m2
`

var exceptionSymbols = []replace{
	replace{old: "n2_", new: "ń"},
	replace{old: "n3_", new: "ň"},
	replace{old: "n4_", new: "ǹ"},
	replace{old: "ng2_", new: "ńg"},
	replace{old: "ng3_", new: "ňg"},
	replace{old: "ng4_", new: "ǹg"},
	replace{old: "m1_", new: "m̄"},
	replace{old: "m2_", new: "ḿ"},
	replace{old: "m4_", new: "m̀"},
}

var vowelWeights = map[string]int{
	"a":  0,
	"e":  1,
	"o":  2,
	"uu": 3,
	"u":  3,
	"i":  4,
}

var vowels = []string{
	"",
	"a",
	"e",
	"o",
	"uu",
	"u",
	"i",
}

type replace struct {
	old string
	new string
}

var replaceSymbols = func(s string) []replace {
	var ret []replace
	for _, p := range strings.Split(s, "\n") {
		p = strings.TrimSpace(p)
		if p == "" {
			continue
		}
		ss := strings.Split(p, ":")
		phonetic := ss[0]
		alphanum := ss[1]
		alpha := alphanum[:len(alphanum)-1]
		num := alphanum[len(alphanum)-1:]
		for _, vowel := range vowels {
			if vowel == "" || vowelWeights[vowel] > vowelWeights[alpha] {
				for _, g := range []string{"", "n", "ng"} {
					ret = append(ret, replace{
						old: vowel + alpha + g + num + "_",
						new: vowel + phonetic + g,
					})
					ret = append(ret, replace{
						old: alpha + vowel + g + num + "_",
						new: phonetic + vowel + g,
					})
				}
			}
		}
	}
	return ret
}(phoneticSymbol)
