package ruby

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"isonschool/cli/ison"
	"log"
	"sort"
)

// RubyFunc は，ルビの関数名です。
type RubyFunc string

func (r RubyFunc) String() string {
	if r == "" || r == RubyFuncFurigana || r == RubyFuncPinyin || r == RubyFuncEnIPA || r == RubyFuncRomaji {
		return string(r)
	}
	log.Panicf("RubyFunc.Sring 知られていない RubyFunc です。%s", string(r))
	return ""
}

// Get は，ルビを取得します。
func (r RubyFunc) Get(text string) string {
	switch r {
	case RubyFuncFurigana:
		return Jumanpp(text)
	case RubyFuncRomaji:
		return ToRomaji(text)
	case RubyFuncPinyin:
		return KyteaZhHans(text)
	case RubyFuncEnIPA:
		return EngToIPA(text)
	}
	log.Panicf("知られていない RubyFunc です。%s", string(r))
	return ""
}

// RubyFunc の種類です。
const (
	RubyFuncFurigana RubyFunc = "furigana"
	RubyFuncRomaji   RubyFunc = "romaji"
	RubyFuncEnIPA    RubyFunc = "enipa"
	RubyFuncPinyin   RubyFunc = "pinyin"
)

// Rubier は，ルビをふります。
type Rubier struct {
	data                      *Data
	needsRubyTexts            []string
	machineGeneratedRubyTexts map[string]string
	category                  ison.Category
	rubyFunc                  RubyFunc
}

// HasError は，エラーがあるかどうかを返します。
func (r *Rubier) HasError() bool {
	if r == nil {
		return false
	}
	return len(r.needsRubyTexts) > 0
}

func (r *Rubier) appendNeedsRubyText(text string) {
	if r == nil {
		return
	}
	for _, s := range r.needsRubyTexts {
		if s == text {
			return
		}
	}
	r.needsRubyTexts = append(r.needsRubyTexts, text)
}

// CheckNeedsRuby は，ルビが必要かチェックします。
func CheckNeedsRuby() {
	var need bool
	for _, r := range rubiers {
		if r.checkNeedsRuby() {
			need = true
		}
	}
	if need {
		log.Panicf("ルビが足りません。終了します。")
	}
}

// checkNeedsRuby は，ルビが必要かチェックします。
func (r *Rubier) checkNeedsRuby() bool {
	if r == nil {
		return false
	}
	if len(r.machineGeneratedRubyTexts) > 0 {
		original, _ := ioutil.ReadFile(r.category.TxtMachineRubyFile().String())
		var mb bytes.Buffer
		mb.Write(original)
		for k, v := range r.machineGeneratedRubyTexts {
			mb.WriteString("[ruby:" + k + "]" + v + "\n")
		}
		ioutil.WriteFile(r.category.TxtMachineRubyFile().String(), mb.Bytes(), 0666)
		log.Printf("ルビが足りないため，機械的に生成しました。確認してください。 %s", r.category.TxtMachineRubyFile())
		return true
	}
	return false
}

var rubiers = make(map[string]*Rubier)

// NewRubier は，データをファイルから読み込んで，新しい Rubier を返します。
func NewRubier(category ison.Category, rubyFunc RubyFunc) *Rubier {
	if rubyFunc == "" {
		return nil
	}
	if r, ok := rubiers[category.ID+string(rubyFunc)]; ok {
		return r
	}
	var data Data
	ison.ReadJSON(&data, category.JSONRubyFile(rubyFunc.String()))
	r := &Rubier{
		data:                      &data,
		category:                  category,
		machineGeneratedRubyTexts: make(map[string]string),
		rubyFunc:                  rubyFunc,
	}
	rubiers[category.ID+string(rubyFunc)] = r
	return r
}

// GetRuby は，ルビを取得します。
func (r *Rubier) GetRuby(text, pre, post string) string {
	if r == nil {
		return ""
	}
	if text == "" {
		return ""
	}
	for _, p := range r.data.Pairs {
		if p.Word == text {
			return p.Ruby
		}
	}
	if mr, ok := r.machineGeneratedRubyTexts[text]; ok {
		return mr
	}
	ru := r.rubyFunc.Get(text)
	r.machineGeneratedRubyTexts[text] = ru
	fmt.Printf("rubyFunc: %s, ", ru)
	r.appendNeedsRubyText(text)
	return ""
}

// Data は，ルビデータを表します。
type Data struct {
	Pairs Pairs
}

// Update は，アップデートします。
func (d *Data) Update(pair Pair) {
	var has bool
	for i, p := range d.Pairs {
		if p.Word == pair.Word {
			d.Pairs[i].Ruby = pair.Ruby
			has = true
		}
	}
	if !has {
		d.Pairs = append(d.Pairs, Pair{Word: pair.Word, Ruby: pair.Ruby})
	}
}

// Sort は，ソートします。
func (d *Data) Sort() {
	sort.Sort(d.Pairs)
}

// Pair は，ルビのペアです。
type Pair struct {
	Word string
	Ruby string
}

// Pairs は，ソートできます。
type Pairs []Pair

func (p Pairs) Len() int      { return len(p) }
func (p Pairs) Swap(i, j int) { p[i], p[j] = p[j], p[i] }
func (p Pairs) Less(i, j int) bool {
	if p[i].Word < p[j].Word {
		return true
	} else if p[i].Word > p[j].Word {
		return false
	} else if p[i].Ruby < p[j].Ruby {
		return true
	}
	return false
}
