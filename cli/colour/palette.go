package colour

import (
	"fmt"
	"image/color"
	"log"
	"math"
	"strconv"
	"strings"
)

// Colour は，色です。
type Colour string

// RGB returns like 0.1,0.2,0.3
func (c Colour) RGB() string {
	r, g, b := float32(c.R())/255.0, float32(c.G())/255.0, float32(c.B())/255.0
	return fmt.Sprintf("%f,%f,%f", r, g, b)
}

// Empty は，空の色かどうかを返します。
func (c Colour) Empty() bool {
	return len(c) == 0
}

// Color は，color.Colorに変換します。
func (c Colour) Color() color.Color {
	return color.NRGBA{R: c.R(), G: c.G(), B: c.B(), A: 255}
}

// ColorA は，color.Colorに変換します。
func (c Colour) ColorA() color.Color {
	return color.NRGBA{R: c.R(), G: c.G(), B: c.B(), A: 100}
}

func (c Colour) String() string {
	if len(c) == 0 {
		return "[empty]"
	}
	if len(c) != 6 {
		log.Panicf("不正な色です。%v", string(c))
	}
	return string(c)
}

// R は，Red 成分です。
func (c Colour) R() uint8 {
	return toUint8(string(c)[0:2])
}

// B は，Blue 成分です。
func (c Colour) B() uint8 {
	return toUint8(string(c)[2:4])
}

// G は，Green 成分です。
func (c Colour) G() uint8 {
	return toUint8(string(c)[4:6])
}

// Palette は，色のパレットです。
type Palette struct {
	PaletteName string
	Colours     [5]Colour
}

func (p Palette) String() string {
	return fmt.Sprintf(
		"%s,%s,%s,%s,%s",
		p.Colours[0],
		p.Colours[1],
		p.Colours[2],
		p.Colours[3],
		p.Colours[4],
	)
}

// IsDarkMode は，背景色が黒っぽいかどうかをかえします。
func (p Palette) IsDarkMode() bool {
	return color.Palette([]color.Color{color.Black, color.White}).Index(p.Background().Color()) == 0
}

// Palette の Colours に入っている値が何を表すか
const (
	ColourBackground = 0
	ColourSecondary  = 1
	ColourPrimary    = 2
	ColourThirdary   = 3
	ColourFore       = 4
)

// ToPalette は，文字列からパレットを作成します。
func ToPalette(s string) Palette {
	ss := strings.Split(s, ",")
	if len(ss) != 5 {
		log.Fatalf("5色ではありません。%s", s)
	}
	var ret Palette
	for i := 0; i < 5; i++ {
		ret.Colours[i] = Colour(ss[i])
	}
	return ret
}

// CreatePalettes は，文字列から適しているパレットを作成します。
func CreatePalettes(s string) []Palette {
	ss := strings.Split(s, ",")
	if len(ss) != 5 {
		log.Fatalf("5色ではありません。%s", s)
	}
	var p1, p2 Palette
	white := Colour(ss[0])
	light := Colour(ss[1])
	primary := Colour(ss[2])
	dark := Colour(ss[3])
	black := Colour(ss[4])
	if ContrastRatio(white, light) > ContrastRatio(white, dark) {
		p1.Colours = [5]Colour{white, light, primary, dark, black}
	} else {
		p1.Colours = [5]Colour{white, dark, primary, light, black}
	}
	if ContrastRatio(black, light) > ContrastRatio(black, dark) {
		p2.Colours = [5]Colour{black, light, primary, dark, white}
	} else {
		p2.Colours = [5]Colour{black, dark, primary, light, white}
	}
	p1.PaletteName = p1.String()
	p2.PaletteName = p2.String()
	var ret []Palette
	if p1.Check() {
		ret = append(ret, p1)
	}
	if p2.Check() {
		ret = append(ret, p2)
	}
	return ret
}

// Check は，適しているかどうかをかえします。
func (p Palette) Check() bool {
	backCR := ContrastRatio(p.Colours[ColourBackground], p.Colours[ColourPrimary])
	backForeCR := ContrastRatio(p.Colours[ColourBackground], p.Colours[ColourFore])
	sat := Saturation(p.Colours[ColourBackground])
	ok := backCR > 7.0 && backForeCR > 10.0 && sat < 140
	if p.IsDarkMode() {
		ok = backCR > 9.0 && backForeCR > 11.0 && sat < 80
	}
	if ok {
		fmt.Println(backCR, backForeCR, "sat", sat, Saturation(p.Colours[ColourBackground]), Saturation(p.Colours[ColourFore]), Saturation(p.Colours[ColourPrimary]))
	}
	return ok
}

// Saturation は，彩度です。
// 収束値 CNT = (MAX + MIN) ÷ 2
// 収束値 CNTが127以下の場合 彩度 S = (MAX - MIN) ÷ (MAX + MIN)
// 収束値 CNTが127以上の場合 彩度 S = (MAX - MIN) ÷ (510 - MAX - MIN)
func Saturation(colour Colour) float64 {
	max := float64(maxUint8(maxUint8(colour.R(), colour.G()), colour.B()))
	min := float64(minUint8(minUint8(colour.R(), colour.G()), colour.B()))
	cnt := (max + min) / float64(2)
	if cnt <= 127 {
		return 255 * (max - min) / (max + min)
	}
	return 255 * (max - min) / (510 - max - min)
}

func maxUint8(a, b uint8) uint8 {
	if a > b {
		return a
	}
	return b
}

func minUint8(a, b uint8) uint8 {
	if a < b {
		return a
	}
	return b
}

func toUint8(s string) uint8 {
	r, err := strconv.ParseUint(s, 16, 8)
	if err != nil {
		log.Panicf("16進数 %s をパースできません。%v", s, err)
	}
	return uint8(r)
}

// Background は，背景色です。
func (p *Palette) Background() Colour {
	return p.Colours[ColourBackground]
}

// Primary は， メインカラーです。
func (p *Palette) Primary() Colour {
	return p.Colours[ColourPrimary]
}

// Secondary は， 2番目のカラーです。
func (p *Palette) Secondary() Colour {
	return p.Colours[ColourSecondary]
}

// Thirdary は， 3番目のカラーです。
func (p *Palette) Thirdary() Colour {
	return p.Colours[ColourThirdary]
}

// Fore は，文字の色です。
func (p *Palette) Fore() Colour {
	return p.Colours[ColourFore]
}

// CheckContrastRatio は，適合レベルAAAに達しているかを返します。
// AAA 7.0 以上
// AA 4.5 以上
func CheckContrastRatio(a, b Colour) bool {
	return ContrastRatio(a, b) > 7.0
}

// ContrastRatio は，コントラスト比です。
func ContrastRatio(a, b Colour) float64 {
	l1, l2 := RelativeLuminance(a), RelativeLuminance(b)
	if l1 < l2 {
		l1, l2 = l2, l1
	}
	return (l1 + 0.05) / (l2 + 0.05)
}

// RelativeLuminance は，相対輝度を計算します。
func RelativeLuminance(c Colour) float64 {
	r8, g8, b8 := c.R(), c.G(), c.B()
	sr, sg, sb := float64(r8)/255.0, float64(g8)/255.0, float64(b8)/255.0
	var f = func(sa float64) float64 {
		if sa <= 0.03928 {
			return sa / 12.92
		}
		return math.Pow((sa+0.055)/1.055, 2.4)
	}
	r, g, b := f(sr), f(sg), f(sb)
	return 0.2126*r + 0.7152*g + 0.0722*b
}

/*
// BrightnessDifference は，明度差を計算します。二つの色の明度差（color brightness difference）が１２５以上（最大値は２５５）となるのが推奨されています。
func BrightnessDifference(a, b color.Colour) float64 {
	return math.Abs(Brightness(a) - Brightness(b))
}

// Brightness は，明度を返します。
// 二つの色の明度差（color brightness difference）が１２５以上（最大値は２５５）となるのが推奨されています。
func Brightness(c color.Colour) float64 {
	r, g, b, _ := c.RGBA()
	return float64(r*299+g*587+b*114) / 1000.0
}

// Difference は，色の差を求めます。色の差（color difference）が５００以上（最大値は７６５）であるとコントラストが良いとされています。
func Difference(a, b color.Colour) float64 {
	r1, g1, b1, _ := a.RGBA()
	r2, g2, b2, _ := b.RGBA()
	return math.Abs(float64(r1-r2)) + math.Abs(float64(g1-g2)) + math.Abs(float64(b1-b2))
}
*/
