package colour

import (
	"bytes"
	"image"
	"image/png"
	"isonschool/cli/ison"
	"log"
	"sort"

	"golang.org/x/image/math/fixed"

	"github.com/golang/freetype/truetype"
	"golang.org/x/image/draw"
	"golang.org/x/image/font"
)

// Screen は，スクリーンです。
type Screen struct {
	Area
	Width  int
	Height int
}

// NewScreen は，新しいスクリーンです。
func NewScreen(width, height int, subDirection Direction, subAreas []Area) *Screen {
	return &Screen{
		Width:  width,
		Height: height,
		Area: Area{
			AreaName:     "main",
			direction:    DirectionH,
			Width:        []Term{Term{C: float64(width)}},
			Height:       []Term{Term{C: float64(height)}},
			SubDirection: DirectionH,
			SubAreas:     subAreas,
		},
	}
}

// Area は，スクリーンの領域です。
type Area struct {
	AreaName     string
	Width        Terms
	Height       Terms
	SubDirection Direction
	SubAreas     []Area
	VAlign       VAlign
	HAlign       HAlign
	FontTerm     FontTerm
	direction    Direction // 自分自身のLengthの方向
	width        float64
	height       float64
	bounds       image.Rectangle
	dot          image.Point
	ftsize       float64
}

// FontTerm は，フォント用の項です。
type FontTerm struct {
	Font        ison.Filepath
	Name        string  // 変数名
	C           float64 // 係数
	W           float64 // 文字 W
	R           float64 // 全角文字 1 文字分
	maxAdvanceR fixed.Int26_6
}

// Term は，変数です。定数の場合，Name はから文字です。 C がゼロで，Name がから文字でない場合，1を意味します。
type Term struct {
	Name string
	C    float64
}

// Terms は，Term のスライスです。
type Terms []Term

// Coeff は，変数係数です。
func (v Term) Coeff() float64 {
	if v.Name == "" {
		log.Panicf("Coeff: 変数名がありません。変数名がない場合は定数なので，C を直接使ってください。 %+v", v)
	}
	if v.C == 0 {
		return 1
	}
	return v.C
}

// Direction は，方向です。
type Direction bool

// V は縦方向，H は横方向です。
const (
	DirectionV Direction = false
	DirectionH Direction = true
)

// Perpendicular は，垂直方向です。
func (d Direction) Perpendicular() Direction {
	return !d
}

const (
	screenWidth  = 1280
	screenHeight = 720
)

const ftsizeR = 100

// Setup は，計算して，Area のエクスポートされていない変数に値を入れます。
func (s *Screen) Setup(contents map[string][]Content) {
	s.cal(contents)
	var se SimEquations
	s.Area.AddEq(&se)
	values := se.Solve()
	s.set(values)
}

// SimEquations は，連立方程式です。
type SimEquations struct {
	Eqs []*Equation
}

// Add は，方程式を追加します。
func (s *SimEquations) Add(e *Equation) {
	s.Eqs = append(s.Eqs, e)
}

// Solve は，連立方程式をときます。
func (s *SimEquations) Solve() map[string]float64 {
	var vnames []string
	for _, e := range s.Eqs {
		for v := range e.Vars {
			vnames = append(vnames, v)
		}
	}
	if len(vnames) != len(s.Eqs) {
		log.Panicf("変数の数と方程式の数が一致しないため，解けません。変数の数:%d, 方程式の数:%d, 変数:%+v, 方程式:%+v", len(vnames), len(s.Eqs), vnames, s.Eqs)
	}
	sort.Slice(s.Eqs, func(i, j int) bool {
		for _, v := range vnames {
			if s.Eqs[i].Vars[v] != 0 {
				return true
			}
			if s.Eqs[j].Vars[v] != 0 {
				return false
			}
		}
		return false
	})
	for i, v := range vnames {
		s.Eqs[i].Canon(v)
		for j := range s.Eqs {
			if i == j {
				continue
			}
			s.Eqs[j].SubEq(s.Eqs[i], s.Eqs[j].Vars[v])
		}
	}
	ans := make(map[string]float64)
	for i, v := range vnames {
		ans[v] = s.Eqs[i].C
	}
	return ans
}

// Equation は，方程式です。
type Equation struct {
	Vars map[string]float64
	C    float64
}

// SubEq は，coe をかけてから引き算します。
func (e *Equation) SubEq(eq *Equation, coe float64) {
	for v := range eq.Vars {
		e.Vars[v] -= eq.Vars[v] * coe
	}
	e.C -= eq.C * coe
}

// Canon は， v の変数名の係数を1にします。
func (e *Equation) Canon(v string) {
	d, ok := e.Vars[v]
	if !ok || d == 0 {
		log.Panicf("0 で割ることはできません。%s %+v", v, e)
	}
	for k := range e.Vars {
		e.Vars[k] /= d
	}
	e.C /= d
}

// NewEquation は，新しい方程式です。
func NewEquation(vs Terms) *Equation {
	e := &Equation{Vars: make(map[string]float64)}
	e.Sub(vs)
	return e
}

// Add は，変数の係数や定数部分を追加します。
func (e *Equation) Add(vs Terms) {
	for _, v := range vs {
		if v.Name == "" {
			e.C -= v.C
		} else {
			e.Vars[v.Name] += v.Coeff()
		}
	}
}

// Sub は，変数の係数や定数部分を追加します。
func (e *Equation) Sub(vs Terms) {
	for _, v := range vs {
		if v.Name == "" {
			e.C += v.C
		} else {
			e.Vars[v.Name] -= v.Coeff()
		}
	}
}

// PerpendicularLength は，自分自身と垂直方向の長さです。
func (a *Area) PerpendicularLength() Terms {
	return a.DirectionLength(a.direction.Perpendicular())
}

// DirectionLength は，d 方向の長さです。
func (a *Area) DirectionLength(d Direction) Terms {
	if d == DirectionH {
		return a.Width
	}
	return a.Height
}

// Length は，自分自身の方向の長さです。
func (a *Area) Length() Terms {
	return a.DirectionLength(a.direction)
}

// AddEq は，連立方程式に方程式を追加します。
func (a *Area) AddEq(se *SimEquations) {
	if len(a.SubAreas) == 0 {
		return
	}
	length := a.DirectionLength(a.SubDirection)
	f := NewEquation(length)
	for _, sa := range a.SubAreas {
		f.Add(sa.Length())
	}
	se.Add(f)
	for _, sa := range a.SubAreas {
		sa.AddEq(se)
	}
}

// cal は，Area.maxAdvanceR, direction, pLength に値を入れます。
func (a *Area) cal(contents map[string][]Content) {
	if !a.FontTerm.Font.Empty() {
		cs := contents[a.AreaName]
		for _, c := range cs {
			if c.Text == "" {
				continue
			}
			advanceR := font.MeasureString(faceR(a.FontTerm.Font), c.Text)
			if advanceR > a.FontTerm.maxAdvanceR {
				a.FontTerm.maxAdvanceR = advanceR
			}
		}
	}
	for _, sa := range a.SubAreas {
		sa.direction = a.SubDirection
		if a.SubDirection == DirectionH {
			sa.Height = a.Height
		} else {
			sa.Width = a.Width
		}
		sa.cal(contents)
	}
}

func (a *Area) set(ans map[string]float64) {
	a.width = a.Width.Cal(ans)
	a.height = a.Height.Cal(ans)
	for _, sa := range a.SubAreas {
		sa.set(ans)
	}
}

// Cal は，変数に値を代入します。
func (t Terms) Cal(ans map[string]float64) float64 {
	var ret float64
	for _, u := range t {
		if u.Name == "" {
			ret += u.C
		} else {
			ret += u.Coeff() * ans[u.Name]
		}
	}
	return ret
}

var metricsRCache map[ison.Filepath]font.Metrics

func metricsR(fnt ison.Filepath) font.Metrics {
	if m, ok := metricsRCache[fnt]; ok {
		return m
	}
	metricsRCache[fnt] = faceR(fnt).Metrics()
	return metricsRCache[fnt]
}

// Image は，png画像データを返します。
func (s *Screen) Image(contents Contents) []byte {
	img := image.NewRGBA(image.Rect(0, 0, s.Width, s.Height))
	s.Area.draw(img, contents)
	var buf bytes.Buffer
	err := png.Encode(&buf, img)
	if err != nil {
		log.Panicf("%v", err)
	}
	return buf.Bytes()
}

func (a *Area) draw(img draw.Image, contents Contents) {
	c := contents[a.AreaName]
	if !c.Colour.Empty() {
		if c.Mask != "" { // 画像を描画する。
			m := mask(c.Mask)
			b := m.Bounds().Add(a.bounds.Min).Add(img.Bounds().Min)
			if a.VAlign == VAlignMiddle {
				b = b.Add(image.Point{Y: (a.bounds.Dy() - m.Bounds().Dy()) / 2})
			} else if a.VAlign == VAlignBottom {
				b = b.Add(image.Point{Y: a.bounds.Dy() - m.Bounds().Dy()})
			}
			if a.HAlign == HAlignCenter {
				b = b.Add(image.Point{X: (a.bounds.Dx() - m.Bounds().Dx()) / 2})
			} else if a.HAlign == HAlignEnd {
				b = b.Add(image.Point{X: a.bounds.Dx() - m.Bounds().Dx()})
			}
			draw.DrawMask(img, b, image.NewUniform(c.Colour.Color()), image.Point{}, m, image.Point{}, draw.Over)
		} else if c.Text != "" { // テキストを書き込む
			dr := &font.Drawer{
				Dst:  img,
				Src:  image.NewUniform(c.Colour.Color()),
				Face: face(a.FontTerm.Font, a.ftsize),
				Dot:  fixed.Point26_6{X: fixed.I(a.dot.X), Y: fixed.I(a.dot.Y)},
			}
			dr.DrawString(c.Text)
		} else { // 一色を書き込む。
			draw.Draw(img, a.bounds.Add(img.Bounds().Min), image.NewUniform(c.Colour.Color()), image.Point{}, draw.Over)
		}
	}
	for _, sa := range a.SubAreas {
		sa.draw(img, contents)
	}
}

var fontCache = make(map[ison.Filepath]*truetype.Font)
var faceCache = make(map[ison.Filepath]map[float64]font.Face)

func faceR(fnt ison.Filepath) font.Face {
	return face(fnt, ftsizeR)
}

func face(fnt ison.Filepath, ftsize float64) font.Face {
	if faceCache[fnt] == nil {
		faceCache[fnt] = make(map[float64]font.Face)
	}
	if f, ok := faceCache[fnt][ftsize]; ok {
		return f
	}
	if _, ok := fontCache[fnt]; !ok {
		ft, err := truetype.Parse(ison.ReadFile(fnt))
		if err != nil {
			log.Panicf("%v", err)
		}
		fontCache[fnt] = ft
	}
	faceCache[fnt][ftsize] = truetype.NewFace(fontCache[fnt], &truetype.Options{Size: ftsize})
	return faceCache[fnt][ftsize]
}

// Content は， Area で描画するデータです。
type Content struct {
	Colour Colour
	Text   string
	Mask   ison.Filepath
}

// VAlign は，縦方向のアライメントです。
type VAlign int

// HAlign は，横方向のアライメントです。
type HAlign int

// デフォルトは，Middle です。
const (
	VAlignMiddle VAlign = iota
	VAlignTop
	VAlignBottom
)

// デフォルトは Center です。
const (
	HAlignCenter HAlign = iota
	HAlignStart
	HAlignEnd
)

// Contents は，AreaName とそれに対応する Content を保存します。
type Contents map[string]Content

var maskCache = make(map[ison.Filepath]image.Image)

func mask(file ison.Filepath) image.Image {
	if m, ok := maskCache[file]; ok {
		return m
	}
	b := ison.ReadFile(file)
	m, err := png.Decode(bytes.NewBuffer(b))
	if err != nil {
		log.Panicf("%s %v", file, err)
	}
	maskCache[file] = m
	return m
}

// LogoScreenContents は，ロゴ画面の Contents を作ります。
func LogoScreenContents(palette Palette, logoMask ison.Filepath) Contents {
	return Contents{
		"main": Content{
			Colour: palette.Background(),
		},
		"logo": Content{
			Colour: palette.Primary(),
			Mask:   logoMask,
		},
	}
}

// LogoScreen は，ロゴ画面の Screen を作ります。
func LogoScreen(logoNum, logoWidth int) *Screen {
	var logoAreas []Area = make([]Area, 2*logoNum+1)
	logoAreas[0] = Area{
		AreaName: "leftspace",
		Width: Terms{
			Term{Name: "x"},
		},
	}
	ai := 1
	for i := 0; i < logoNum; i++ {
		logoAreas[ai] = Area{
			AreaName: "logo",
			Width: Terms{
				Term{C: float64(logoWidth)},
			},
		}
		ai++
		if i == logoNum-1 {
			break
		}
		logoAreas[ai] = Area{
			AreaName: "space",
			Width: Terms{
				Term{C: float64(logoWidth) * 0.5},
			},
		}
		ai++
	}
	logoAreas[ai] = Area{
		AreaName: "rightspace",
		Width: Terms{
			Term{Name: "x"},
		},
	}
	return NewScreen(screenWidth, screenHeight, DirectionH, logoAreas)
}

// TitleScreen は，タイトルスクリーンです。
func TitleScreen(titleFont, subtitleFont ison.Filepath) *Screen {
	areas := []Area{
		Area{
			Height: Terms{Term{C: 100}},
		},
		Area{
			Height:       Terms{Term{C: 10}},
			SubDirection: DirectionH,
			SubAreas: []Area{
				Area{
					Width: Terms{Term{C: 100}, Term{Name: "logospace"}},
				},
				Area{
					AreaName: "logo",
					Width:    Terms{Term{C: 100}},
				},
				Area{
					AreaName: "logoText",
					Width:    Terms{Term{C: 100}},
				},
				Area{
					Width: Terms{Term{C: 100}, Term{Name: "logospace"}},
				},
			},
		},
		Area{
			Height:       Terms{Term{C: 150}},
			SubDirection: DirectionH,
			SubAreas: []Area{
				Area{
					Width: Terms{Term{C: 100}},
				},
				Area{
					AreaName: "title",
					Width:    Terms{Term{Name: "title"}},
					FontTerm: FontTerm{
						Font: titleFont,
						Name: "titlefnt",
					},
				},
				Area{
					Width: Terms{Term{C: 100}},
				},
			},
		},
	}
	return NewScreen(screenWidth, screenHeight, DirectionV, areas)
}

// ProblemScreen は，問題画面を作成します。
func ProblemScreen() *Screen {
	var areas = []Area{}
	return NewScreen(screenWidth, screenHeight, DirectionV, areas)
}
