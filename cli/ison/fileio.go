package ison

import (
	"bufio"
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"strconv"
	"strings"
)

// Sort インターフェースは，ソートできることを示します。
type Sort interface {
	Sort()
}

var filecontents = make(map[Filepath][]byte)

// Filepath は，絶対ファイルパスです。
type Filepath string

// Exist は，そのファイルが存在するかどうかをかえします。
func (f Filepath) Exist() bool {
	_, err := os.Stat(f.String())
	if os.IsNotExist(err) {
		return false
	} else if err != nil {
		log.Panicf("%s %v", f, err)
	}
	return true
}

// ToFilepath は，文字列 f を Filepath に変換します。
func ToFilepath(f string) Filepath {
	if f == "" {
		return Filepath("")
	}
	if !strings.Contains(f, "/") || !strings.Contains(f, ".") {
		log.Panicf("Filepath が不正です。 %s", f)
	}
	return Filepath(f)
}

// Empty は，からかどうかをあらわします。
func (f Filepath) Empty() bool {
	return f == ""
}

// Join は，filepath.Joinです。
func Join(elem ...string) Filepath {
	// ファイル名がからの場合は，Filepath もからにする。
	if elem[len(elem)-1] == "" {
		return ""
	}
	return ToFilepath(filepath.Join(elem...))
}

// Base は，filepath.Base です。
func (f Filepath) Base() Filename {
	return ToFilename(filepath.Base(f.String()))
}

// Dir returns directory.
func (f Filepath) Dir() string {
	return filepath.Dir(f.String())
}

func (f Filepath) String() string {
	s := string(f)
	if s == "" {
		return s
	}
	if !strings.Contains(s, "/") || !strings.Contains(s, ".") {
		log.Panicf("Filepath が不正です。 %s", s)
	}
	return s
}

// Filename は，ファイル名，Base です。
type Filename string

// Empty は，からかどうかを判別します。
func (f Filename) Empty() bool {
	return f == ""
}

// ToFilename は，文字列を Filename に変換します。
func ToFilename(f string) Filename {
	if f == "" {
		return Filename("")
	}
	if !strings.Contains(f, ".") {
		log.Panicf("Filename が不正です。 %s", f)
	}
	return Filename(f)
}

func (f Filename) String() string {
	s := string(f)
	if s == "" {
		return s
	}
	if !strings.Contains(s, ".") {
		log.Panicf("Filename が不正です。 %s", s)
	}
	return s
}

// AppendExtJSON は，拡張子 .json を加えます。
func (f Filename) AppendExtJSON() Filename {
	s := f.String()
	if strings.HasSuffix(s, ".json") {
		return f
	}
	return ToFilename(s + ".json")
}

// TrimExtJSON は，拡張子 .json を取り除きます。
func (f Filename) TrimExtJSON() Filename {
	s := f.String()
	return ToFilename(strings.TrimSuffix(s, ".json"))
}

// SerieNameAndSentenceNo は，ファイル名から，SerieName と SentenceNo を抽出します。
func (f Filename) SerieNameAndSentenceNo() (string, int) {
	s := f.String()
	ss := strings.Split(s, "_")
	no, err := strconv.Atoi(ss[1])
	if err != nil {
		log.Panicf("%v", err)
	}
	return ss[0], no
}

// SerieNameAndNoAndExerciseName は，ファイル名から，SerieName と No と ExerciseName を抽出します。
func (f Filename) SerieNameAndNoAndExerciseName() (string, int, string) {
	serieName, nos := f.SerieNameAndNos()
	var no int
	exerciseName := ""
	if len(nos) >= 2 {
		exerciseName = nos[1]
		n, err := strconv.Atoi(nos[0])
		if err != nil {
			panic(err.Error())
		}
		no = n
	}
	return serieName, no, exerciseName
}

// SerieNameAndExerciseName は，ファイル名から，SerieName と ExerciseName を抽出します。
func (f Filename) SerieNameAndExerciseName() (string, string) {
	serieName, nos := f.SerieNameAndNos()
	exerciseName := ""
	if len(nos) >= 2 {
		exerciseName = nos[1]
	}
	return serieName, exerciseName
}

// SerieNameAndNos は，ファイル名から，SerieName と SerieNos を抽出します。
func (f Filename) SerieNameAndNos() (string, []string) {
	s := f.String()
	if strings.Index(s, "_") < 0 {
		log.Panicf("ファイル名が不正です。 %s", f)
	}
	if strings.Index(s, ".") >= 0 {
		s = s[:strings.Index(s, ".")]
	}
	return s[:strings.Index(s, "_")], strings.Split(s[strings.Index(s, "_")+1:], "_")
}

// WriteFile は，ファイルを読み込みます。
func WriteFile(filePath Filepath, data []byte) {
	os.MkdirAll(filepath.Dir(filePath.String()), 0755)
	b, ok := filecontents[filePath]
	if !ok || b == nil {
		fmt.Printf("%s を作成しました。datalen=%d\n", filePath, len(data))
	} else if bytes.Compare(b, data) != 0 {
		fmt.Printf("%s を変更しました。datalen=%d\n", filePath, len(data))
	} else {
		return
	}
	err := ioutil.WriteFile(filePath.String(), data, 0644)
	if err != nil {
		log.Panicf("WriteFile %s %v", filePath, err)
	}
}

// ReadFile は，ファイルを読み込みます。
func ReadFile(filepath Filepath) []byte {
	b, err := ioutil.ReadFile(filepath.String())
	if err != nil && !os.IsNotExist(err) {
		log.Panicf("%s %v", filepath, err)
	}
	filecontents[filepath] = b
	return b
}

// WriteJSON は，データを JSON 形式で保存します。
func WriteJSON(v interface{}, filepath Filepath) {
	if vs, ok := v.(Sort); ok {
		vs.Sort()
	}
	b, err := json.MarshalIndent(&v, "", "  ")
	if err != nil {
		log.Panicf("Failed marshal: %v", err)
	}
	WriteFile(filepath, b)
}

// ReadJSON は，JSON データを読み込みます。データが読み込まれたかどうかがかえります。
func ReadJSON(v interface{}, filepath Filepath) (dataRead bool) {
	// ファイルが存在しない場合には何もしない
	if _, err := os.Stat(filepath.String()); os.IsNotExist(err) {
		return false
	}
	b := ReadFile(filepath)
	err := json.Unmarshal(b, &v)
	if err != nil {
		log.Panicf("cannot unmarshal: %s, %s, %v", filepath, string(b), err)
	}
	return true
}

// CheckYes は，標準入力に y が入力されたかを返します。
func CheckYes(format string, a ...interface{}) bool {
	fmt.Printf(format+" 同意する場合は y を入力してください。", a...)
	scanner := bufio.NewScanner(os.Stdin)
	scanner.Scan()
	return strings.TrimSpace(scanner.Text()) == "y"
}
