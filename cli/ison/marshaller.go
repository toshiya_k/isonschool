package ison

import (
	"log"
	"strings"
)

// TxtMarshaller は，テキストファイルを解析します。
type TxtMarshaller struct {
	setters  map[string]Setter
	terms    [][]string
	lineNo   int
	columnNo int
	file     Filepath
}

// File は，ファイル名を返します。
func (m *TxtMarshaller) File() Filepath {
	return m.file
}

// NewTxtMarshallerFromString は，バイト列から作成します。
func NewTxtMarshallerFromString(s string) *TxtMarshaller {
	var m TxtMarshaller
	m.split(s)
	return &m
}

// NewTxtMarshallerFromFile は，ファイルから作成します。
func NewTxtMarshallerFromFile(file Filepath) *TxtMarshaller {
	var m TxtMarshaller
	m.file = file
	b := ReadFile(file)
	m.split(string(b))
	return &m
}

// SetFile は，解析元のファイルを設定します。
func (m *TxtMarshaller) split(s string) {
	m.terms = [][]string{}
	lines := strings.Split(s, "\n")
	for _, line := range lines {
		line := strings.Trim(line, " 　")
		if line == "" {
			continue
		}
		ms := strings.Split(line, "\t")
		m.terms = append(m.terms, ms)
	}
	m.lineNo = 0
	m.columnNo = 0
}

// Unmarshal は，d にデコードします。success は，デコードされたかを表します。ファイルの終わりで isEOF がtrueになります。
func (m *TxtMarshaller) Unmarshal(d interface{}) (success, isEOF bool) {
	for i := m.lineNo; i < len(m.terms); i++ {
		// 行のはじめに // がある場合，終了となる。
		if strings.HasPrefix(strings.TrimSpace(m.terms[i][0]), "//") {
			if m.columnNo+1 < len(m.terms[m.lineNo]) {
				m.columnNo++
				return success, false
			}
			return success, true
		}
		// 行のはじめに --- がある場合，区切りとなる。
		if strings.TrimSpace(m.terms[i][0]) == "---" {
			if m.columnNo+1 < len(m.terms[m.lineNo]) {
				m.columnNo++
				return success, false
			}
			m.lineNo = i + 1
			m.columnNo = 0
			return success, false
		}
		term := m.terms[i][m.columnNo]
		term = strings.TrimSpace(term)
		if strings.HasPrefix(term, "[") {
			ci := strings.Index(term, "]")
			name := term[1:ci]
			k := ""
			li := strings.Index(term, ":")
			if li >= 0 && li < ci {
				name = term[1:li]
				k = term[li+1 : ci]
			}
			v := term[ci+1:]
			setter, ok := m.setters[name]
			if !ok {
				log.Panicf("name=%s の読み込みが設定されていません。", name)
			}
			setter(d, k, strings.ReplaceAll(v, `\n`, "\n"))
			success = true
		}
	}
	if m.lineNo < len(m.terms) && m.columnNo+1 < len(m.terms[m.lineNo]) {
		m.columnNo++
		return success, false
	}
	return success, true
}

// Add は，セッターを追加します。
func (m *TxtMarshaller) Add(name string, setter Setter) {
	if m.setters == nil {
		m.setters = make(map[string]Setter)
	}
	m.setters[name] = setter
}

// Setter は，セッターです。
type Setter func(d interface{}, k, v string)
