package ison

import (
	"bytes"
	"log"
	"math/rand"
	"strconv"
	"strings"
)

// FirstLetter は，s の最初の文字を返します。
func FirstLetter(s string) string {
	var b bytes.Buffer
	for _, r := range s {
		b.WriteRune(r)
		return b.String()
	}
	return ""
}

// FirstLetterToLower は，最初の文字を小文字に変えます。
func FirstLetterToLower(s string) string {
	var first = true
	var b bytes.Buffer
	for _, r := range s {
		if first {
			b.WriteString(strings.ToLower(FirstLetter(s)))
			first = false
		} else {
			b.WriteRune(r)
		}
	}
	return b.String()
}

// FirstLetterToTitle は，最初の文字を大文字に変えます。
func FirstLetterToTitle(s string) string {
	var first = true
	var b bytes.Buffer
	for _, r := range s {
		if first {
			b.WriteString(strings.ToTitle(string(r)))
			if !(r == '¡' || r == '¿') {
				first = false
			}
		} else {
			b.WriteRune(r)
		}
	}
	return b.String()
}

// SplitTags は，重複タグをのぞいたスライスを返します。
func SplitTags(tags string) []string {
	ss := strings.Split(tags, ",")
	var ret []string
	for _, s := range ss {
		if !Contains(ret, s) {
			ret = append(ret, s)
		}
	}
	return ret
}

// Contains は，スライスに文字列が含まれているかを返します。
func Contains(ss []string, s string) bool {
	for _, u := range ss {
		if u == s {
			return true
		}
	}
	return false
}

// SetRandSeed は，rand の seed を文字列から設定します。
func SetRandSeed(s string) {
	var seed int64 = 13
	for _, r := range s {
		seed = seed*int64(r) + 17
	}
	rand.Seed(seed)
}

// Atoi は，文字列をintに変換します。
func Atoi(s string) int {
	i, err := strconv.Atoi(s)
	if err != nil {
		log.Panicf("%v s:%s", err, s)
	}
	return i
}

// ParseBool は，文字列をbool に変換します。
func ParseBool(s string) bool {
	b, err := strconv.ParseBool(s)
	if err != nil {
		log.Panicf("%v %s", err, s)
	}
	return b
}
