package ison

import (
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"sort"
	"strconv"
	"strings"
)

// Master は，マスターデータです。
// Master の json ファイルは，手で編集します。
type Master struct {
	Categories            []Category
	CommonTranslationFile Filepath
}

// Category は，カテゴリーデータです。
type Category struct {
	ID           string
	FontFolder   string
	JSONFolder   string
	BinaryFolder string
	Kind         string
	AudioLang    Lang
	Master       *Master
}

// FontFile は，フォントのファイルパスを返します。
func (c *Category) FontFile(fontFile Filename) Filepath {
	return Join(c.FontFolder, fontFile.String())
}

// JSONSerieFileFromVideoFile は，シリーズデータのファイルパスを返します。
func (c *Category) JSONSerieFileFromVideoFile(videoFile Filename) Filepath {
	serieName, exerciseName := videoFile.SerieNameAndExerciseName()
	return c.JSONSerieFile(serieName, exerciseName)
}

// SerieExerciseNames は，シリーズのエクササイズ名のスライスを返します。
func (c *Category) SerieExerciseNames(serieName string) []string {
	var ret []string
	for _, f := range c.JSONSerieFiles(serieName) {
		s := strings.TrimSuffix(f.String(), ".json")
		s = s[strings.LastIndex(s, "_")+1:]
		ret = append(ret, s)
	}
	return ret
}

// JSONFormulaFiles は，数学データのファイルパスを返します。
func (c *Category) JSONFormulaFiles() []Filepath {
	return folderFilesWithPred(filepath.Join(c.JSONFolder, "formula"), nil)
}

// JSONSerieFiles は，シリーズデータのファイルパスを返します。
func (c *Category) JSONSerieFiles(serieName string) []Filepath {
	return folderFilesWithPred(filepath.Join(c.JSONFolder, "serie"), func(isDir bool, name string) bool {
		return strings.HasPrefix(name, serieName+"_")
	})
}

// JSONFormulaFile は，シリーズデータのファイルパスを返します。
func (c *Category) JSONFormulaFile(serieName string, no int) Filepath {
	return Join(c.JSONFolder, "formula", serieName, serieName+"_"+strconv.Itoa(no)+".json")
}

// YAMLMthFile は，Mthデータのファイルパスを返します。
func (c *Category) YAMLMthFile(serieName string) Filepath {
	return Join(c.JSONFolder, "mth", serieName+".yaml")
}

// JSONSerieFile は，シリーズデータのファイルパスを返します。
func (c *Category) JSONSerieFile(serieName, exerciseName string) Filepath {
	return Join(c.JSONFolder, "serie", serieName+"_"+exerciseName+".json")
}

// TxtMachineRubyFile は，機械生成されたルビーファイルのパスです。
func (c *Category) TxtMachineRubyFile() Filepath {
	return Join(c.JSONFolder, "ruby_txt", "machine_generated.txt")
}

// BinaryNeedsRubyFile は，翻訳が必要なファイルを出力します。
func (c *Category) BinaryNeedsRubyFile() Filepath {
	return Join(c.BinaryFolder, "needsruby.txt")
}

// BinaryVideoIDsFile は，ビデオIDファイルのパスです。
func (c *Category) BinaryVideoIDsFile() Filepath {
	return Join(c.BinaryFolder, "videoids.txt")
}

// BinaryEnMachineTranslationFile は，英語に翻訳されたファイルのパスです。
func (c *Category) BinaryEnMachineTranslationFile() Filepath {
	return Join(c.BinaryFolder, "entranslation.txt")
}

// TxtMachineTranslationFile は，機械生成されたルビーファイルのパスです。
func (c *Category) TxtMachineTranslationFile() Filepath {
	return Join(c.JSONFolder, "translation_txt", "machine_generated.txt")
}

// BinaryNeedsTranslationFile は，翻訳が必要なファイルを出力します。
func (c *Category) BinaryNeedsTranslationFile() Filepath {
	return Join(c.BinaryFolder, "needstranslation.txt")
}

// JSONTranslationFile は，翻訳データのファイルパスを返します。
func (c *Category) JSONTranslationFile() Filepath {
	return Join(c.JSONFolder, "translation", "translation.json")
}

// JSONRubyFile は，ルビデータのファイルパスを返します。
func (c *Category) JSONRubyFile(rubyFunc string) Filepath {
	return Join(c.JSONFolder, "ruby", rubyFunc+".json")
}

// JSONChannelFile は，
func (c *Category) JSONChannelFile() Filepath {
	return Join(c.JSONFolder, "channel", "channel.json")
}

// TxtChannelFiles は，
func (c *Category) TxtChannelFiles() []Filepath {
	return folderFiles(filepath.Join(c.JSONFolder, "channel_txt"))
}

// TxtFormulaFiles は，
func (c *Category) TxtFormulaFiles() []Filepath {
	return folderFiles(filepath.Join(c.JSONFolder, "formula_txt"))
}

// TxtFormulaFile は，
func (c *Category) TxtFormulaFile(serieName string) Filepath {
	return Join(c.JSONFolder, "formula_txt", serieName+".txt")
}

// TxtSerieFiles は，
func (c *Category) TxtSerieFiles() []Filepath {
	return folderFiles(filepath.Join(c.JSONFolder, "serie_txt"))
}

// TxtRubyFiles は，新しい翻訳テキストファイルパスを返します。
func (c *Category) TxtRubyFiles() []Filepath {
	return folderFiles(filepath.Join(c.JSONFolder, "ruby_txt"))
}

// TxtTranslationFiles は，新しい翻訳テキストファイルパスを返します。
func (c *Category) TxtTranslationFiles() []Filepath {
	return append(folderFiles(filepath.Join(c.JSONFolder, "translation_txt")), c.Master.CommonTranslationFile)
}

// HasVideo は，ビデオが存在するカテゴリかを返します。
func (c *Category) HasVideo() bool {
	return c.Kind != "channel"
}

// IsChannel は，チャンネル用のカテゴリかを返します。
func (c *Category) IsChannel() bool {
	return c.Kind == "channel"
}

// IsGrammar は，文法用のカテゴリかを返します。
func (c *Category) IsGrammar() bool {
	return c.Kind == "grammar"
}

// IsFormula は，数学用のカテゴリかを返します。
func (c *Category) IsFormula() bool {
	return c.Kind == "formula"
}

// BinaryAudioFile は，オーディオファイルのフルパスを返します。
func (c *Category) BinaryAudioFile(text, voiceName string) Filepath {
	err := os.MkdirAll(filepath.Join(c.BinaryFolder, "audio", voiceName), 0755)
	if err != nil {
		log.Panicf("BinaryAudioFile error %v", err)
	}
	text = strings.ReplaceAll(text, " ", "_")
	return Join(c.BinaryFolder, "audio", voiceName, text+".mp3")
}

// BinaryCaptionSBVFile は，キャプションファイルのフルパスを返します。
func (c *Category) BinaryCaptionSBVFile(videoFilename Filename, captionKey string, lang Lang) Filepath {
	serieName, _ := videoFilename.SerieNameAndNos()
	err := os.MkdirAll(filepath.Join(c.BinaryFolder, "caption_sbv", serieName), 0755)
	if err != nil {
		log.Panicf("BinaryCaptionSBVFile error %v", err)
	}
	return Join(c.BinaryFolder, "caption_sbv", serieName, videoFilename.String()+"."+captionKey+"."+lang.String()+".sbv")
}

// BinaryThumbnailFile は，キャプションファイルのフルパスを返します。
func (c *Category) BinaryThumbnailFile(videoFilename Filename) Filepath {
	err := os.MkdirAll(filepath.Join(c.BinaryFolder, "thumbnail"), 0755)
	if err != nil {
		log.Panicf("BinaryThumbnailFile error %v", err)
	}
	return Join(c.BinaryFolder, "thumbnail", videoFilename.String()+".png")
}

// JSONCaptionSetFile は，キャプションファイルのフルパスを返します。
func (c *Category) JSONCaptionSetFile(videoFilename Filename) Filepath {
	if !strings.HasSuffix(videoFilename.String(), ".json") {
		videoFilename = Filename(videoFilename.String() + ".json")
	}
	ss := strings.Split(videoFilename.String(), "_")
	serieName := ss[0]
	return Join(c.JSONFolder, "caption", serieName, videoFilename.String())
}

// BinaryImageFile は，画像のフルパスを返します。
func (c *Category) BinaryImageFile(serieName string, serieNo int, exerciseName string, problemNo int, status string) Filepath {
	err := os.MkdirAll(filepath.Join(c.BinaryFolder, "image"), 0755)
	if err != nil {
		log.Panicf("BinaryImageFile error %v", err)
	}
	return Join(c.BinaryFolder, "image", serieName+"_"+strconv.Itoa(serieNo)+"_"+exerciseName+"_"+strconv.Itoa(problemNo)+"_"+status+".png")
}

// RemoveAllBinaryImageFiles は，画像をすべて削除します。
func (c *Category) RemoveAllBinaryImageFiles() {
	err := os.RemoveAll(filepath.Join(c.BinaryFolder, "image"))
	if err != nil {
		log.Panicf("RemoveAllBinaryImageFiles error %v", err)
	}
}

// Lang は，言語コードを保持します。
type Lang string

// ToLang converts a string to ison.Lang.
func ToLang(s string) Lang {
	ret := Lang(s)
	ret.check()
	return ret
}

func (l Lang) check() {
	if l == "" {
		return
	}
	for _, lang := range VideoLocalizationLangs {
		if l == lang {
			return
		}
	}
	log.Panicf("%s は不正な言語です。", string(l))
}

func (l Lang) String() string {
	l.check()
	return string(l)
}

// IsEnglish は，英語かどうかを返します。
func (l Lang) IsEnglish() bool {
	return l == "en"
}

// IsChinese は，中国語かどうかを返します。
func (l Lang) IsChinese() bool {
	return l == "zh-Hans" || l == "zh-Hant"
}

// IsDisplayLang は，Youtubeの表示言語かを返します。
func (l Lang) IsDisplayLang() bool {
	for _, lang := range DisplayLocalizationLangs {
		if l == lang {
			return true
		}
	}
	return false
}

var noSpaceLangs = []Lang{"ja", "zh-Hans", "zh-Hant"}

// LangEn は，英語です。
var LangEn = Lang("en")

// HasSpace は，単語間にスペースのある言語かを返します。
func (l Lang) HasSpace() bool {
	for _, lang := range noSpaceLangs {
		if lang == l {
			return false
		}
	}
	return true
}

// JSONVideoFiles は，ビデオデータのJSONが入っているフォルダの中にあるファイル名を返します。
func (c *Category) JSONVideoFiles() []Filepath {
	return folderFiles(filepath.Join(c.JSONFolder, "video"))
}

// JSONVideoSerieFiles は，ビデオデータのJSONが入っているフォルダの中にあるファイル名を返します。
func (c *Category) JSONVideoSerieFiles(serieName string) []Filepath {
	return folderFiles(filepath.Join(c.JSONFolder, "video", serieName))
}

// JSONVideoFile は，ビデオデータのJSONが入っているフォルダの中にあるファイル名を返します。
func (c *Category) JSONVideoFile(filename Filename) Filepath {
	if !strings.HasSuffix(filename.String(), ".json") {
		filename += ".json"
	}
	serieName, _ := filename.SerieNameAndNos()
	return Join(c.JSONFolder, "video", serieName, filename.String())
}

// TxtVideoFiles は，ローカライゼーションのJSONが入っているフォルダの中にあるファイル名を返します。
func (c *Category) TxtVideoFiles() []Filepath {
	return folderFiles(filepath.Join(c.JSONFolder, "video_txt"))
}

// JSONPlaylistFiles は， プレイリストデータ のJSONが入っているフォルダの中にあるファイル名を返します。
func (c *Category) JSONPlaylistFiles() []Filepath {
	return folderFiles(filepath.Join(c.JSONFolder, "playlist"))
}

// JSONPlaylistFile は， プレイリストデータ のJSONが入っているフォルダの中にあるファイル名を返します。
func (c *Category) JSONPlaylistFile(name string, lang Lang) Filepath {
	return Join(c.JSONFolder, "playlist", name, name+"~"+lang.String()+".json")
}

// TxtPlaylistFiles は，プレイリストテキストファイルのリストを返します。
func (c *Category) TxtPlaylistFiles() []Filepath {
	return folderFiles(filepath.Join(c.JSONFolder, "playlist_txt"))
}

// JSONGrammarFiles は，ローカライゼーションのJSONが入っているフォルダの中にあるファイル名を返します。
func (c *Category) JSONGrammarFiles() []Filepath {
	return folderFiles(filepath.Join(c.JSONFolder, "grammar"))
}

// TxtGrammarFiles は，
func (c *Category) TxtGrammarFiles() []Filepath {
	return folderFiles(filepath.Join(c.JSONFolder, "grammar_txt"))
}

// JSONGrammarFile は，
func (c *Category) JSONGrammarFile(serieName string, sentenceNo int) Filepath {
	return Join(c.JSONFolder, "grammar", serieName, serieName+"_"+strconv.Itoa(sentenceNo)+".json")
}

// TxtVideoFile は，ローカライゼーションのJSONが入っているフォルダの中にあるファイル名を返します。
func (c *Category) TxtVideoFile(filename Filename) Filepath {
	return Join(c.JSONFolder, "video_txt", filename.String())
}

// BinaryVideoFile は，ビデオのフルパスを返します。
func (c *Category) BinaryVideoFile(filename Filename) Filepath {
	return Join(c.BinaryFolder, "video", filename.String())
}

// folderFiles は，フォルダの中にあるファイルを検索します。返り値は，絶対パスとなっています。
func folderFiles(folder string) []Filepath {
	return folderFilesWithPred(folder, nil)
}

// folderFilesWithPred は，フォルダの中にあるファイルを検索します。返り値は，絶対パスとなっています。
func folderFilesWithPred(folder string, pred func(isDir bool, name string) bool) []Filepath {
	var files = []Filepath{}
	fis, err := ioutil.ReadDir(folder)
	if err != nil {
		log.Panicf("%v", err)
	}
	for _, fi := range fis {
		if strings.HasPrefix(fi.Name(), ".") { // 隠しファイルは除く
			continue
		}
		if pred != nil && !pred(fi.IsDir(), fi.Name()) {
			continue
		}
		if fi.IsDir() {
			files = append(files, folderFilesWithPred(filepath.Join(folder, fi.Name()), pred)...)
		} else {
			files = append(files, Filepath(filepath.Join(folder, fi.Name())))
		}
	}
	sort.Slice(files, func(i, j int) bool {
		if !strings.Contains(files[i].Base().String(), "_") || !strings.Contains(files[j].Base().String(), "_") {
			return files[i].String() < files[j].String()
		}
		is, ii := files[i].Base().SerieNameAndNos()
		js, jj := files[j].Base().SerieNameAndNos()
		if is < js {
			return true
		} else if is > js {
			return false
		}
		for n := 0; n < len(ii) && n < len(jj); n++ {
			ii0, _ := strconv.Atoi(ii[n])
			jj0, _ := strconv.Atoi(jj[n])
			if ii0 < jj0 {
				return true
			} else if ii0 > jj0 {
				return false
			}
			if ii[n] < jj[n] {
				return true
			} else if ii[n] > jj[n] {
				return false
			}
		}
		return len(ii) < len(jj)
	})
	return files
}

// VideoLocalizationLangsExceptEn は，en を除いた言語です。
var VideoLocalizationLangsExceptEn = VideoLocalizationLangs[1:]

// VideoLocalizationLangs は，
var VideoLocalizationLangs = []Lang{
	"en",
	"ja",
	"af",
	"az",
	"id",
	"ms",
	"bs",
	"ca",
	"cs",
	"da",
	"de",
	"et",
	"es",
	"eu",
	"fil",
	"fr",
	"gl",
	"hr",
	"zu",
	"is",
	"it",
	"sw",
	"lv",
	"lt",
	"hu",
	"nl",
	"no",
	"uz",
	"pl",
	"pt",
	"ro",
	"sq",
	"sk",
	"sl",
	"sr",
	"fi",
	"sv",
	"vi",
	"tr",
	"be",
	"bg",
	"ky",
	"kk",
	"mk",
	"mn",
	"ru",
	"sr",
	"uk",
	"el",
	"hy",
	"iw",
	"ur",
	"ar",
	"fa",
	"ne",
	"mr",
	"hi",
	"bn",
	"pa",
	"gu",
	"ta",
	"te",
	"kn",
	"ml",
	"si",
	"th",
	"lo",
	"my",
	"ka",
	"am",
	"km",
	"zh-Hans",
	"zh-Hant",
	"ko",
	"co",
	"fy",
	"ha",
	"ig",
	"jv",
	"ku",
	"lb",
	"mg",
	"mt",
	"mi",
	"ps",
	"sm",
	"gd",
	"st",
	"sn",
	"sd",
	"so",
	"su",
	"tg",
	"cy",
	"xh",
	"yi",
	"yo",
}

// DisplayLocalizationLangs は，Youtubeの表示に使われる言語です。
var DisplayLocalizationLangs = []Lang{
	"en",
	"ja",
	"af",
	"az",
	"id",
	"ms",
	"bs",
	"ca",
	"cs",
	"da",
	"de",
	"et",
	"es",
	"eu",
	"fil",
	"fr",
	"gl",
	"hr",
	"zu",
	"is",
	"it",
	"sw",
	"lv",
	"lt",
	"hu",
	"nl",
	"no",
	"uz",
	"pl",
	"pt",
	"ro",
	"sq",
	"sk",
	"sl",
	"sr",
	"fi",
	"sv",
	"vi",
	"tr",
	"be",
	"bg",
	"ky",
	"kk",
	"mk",
	"mn",
	"ru",
	"sr",
	"uk",
	"el",
	"hy",
	"iw",
	"ur",
	"ar",
	"fa",
	"ne",
	"mr",
	"hi",
	"bn",
	"pa",
	"gu",
	"ta",
	"te",
	"kn",
	"ml",
	"si",
	"th",
	"lo",
	"my",
	"ka",
	"am",
	"km",
	"zh-Hans",
	"zh-Hant",
	"ko",
}
