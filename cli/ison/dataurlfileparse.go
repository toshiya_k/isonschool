package ison

import (
	"encoding/base64"
	"io/ioutil"
	"log"
	"strconv"
	"strings"
)

// ParseDataURL は，HTMLで作成したDataURLをパースし，それぞれのファイルとして生成します。
func ParseDataURL(c Category, dataurlfile Filepath) {
	c.RemoveAllBinaryImageFiles()
	b := ReadFile(dataurlfile)
	lines := strings.Split(string(b), "\n")
	for i := 0; i < len(lines); i += 2 {
		ls := strings.Split(lines[i], "/")
		if c.ID != ls[0] {
			continue
		}
		sentenceNo, err := strconv.Atoi(ls[2])
		if err != nil {
			log.Fatal(err)
		}
		problemNo, err := strconv.Atoi(ls[4])
		if err != nil {
			log.Fatal(err)
		}
		f := c.BinaryImageFile(ls[1], sentenceNo, ls[3], problemNo, ls[5])
		b, err := base64.StdEncoding.DecodeString(lines[i+1][len("data:image/png;base64,"):])
		if err != nil {
			log.Fatal(err)
		}
		err = ioutil.WriteFile(f.String(), b, 0644)
		if err != nil {
			log.Fatal(err)
		}
	}
}
