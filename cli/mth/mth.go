package mth

import (
	"bytes"
	"errors"
	"fmt"
	"isonschool/cli/ison"
	"log"
	"math/rand"
	"regexp"
	"sort"
	"strconv"
	"strings"
	"text/template"

	"gopkg.in/yaml.v2"
)

// Ordered returns if this ProblemType is a greater or less problem.
func Ordered(p string) bool {
	return strings.Contains(p, "o")
}

// Reversed returns if this ProblemType is left-right reversed.
func Reversed(p string) bool {
	return strings.Contains(p, "r")
}

// ChangeVar returns a changed variable name of this problem.
func ChangeVar(p string) string {
	var v string
	ind := strings.Index(p, "-")
	if ind < 0 {
		v = p
	} else {
		v = p[:ind]
	}
	return v
}

// funcMap is used for processing templates.
var funcMap = template.FuncMap{
	"add":     add,
	"sub":     sub,
	"mul":     mul,
	"div":     div,
	"repeat1": repeat1,
	"pow10":   pow10,
}

func add(a ...int) int {
	var ret int
	for _, v := range a {
		ret += v
	}
	return ret
}

func sub(a, b int) int {
	return a - b
}

func mul(a ...int) int {
	var ret int = 1
	for _, v := range a {
		ret *= v
	}
	return ret
}

func div(a, b int) float64 {
	return float64(a) / float64(b)
}

func repeat1(n int) []int {
	var res []int
	for i := 0; i < n-1; i++ {
		res = append(res, i+1)
	}
	return res
}

func pow10(n, k int) int {
	var ret int = 1
	for i := 0; i < k; i++ {
		ret *= 10
	}
	return ret * n
}

// AMap is a map of data.
type AMap map[string]int

// Copy copies AMap.
func (m AMap) Copy(changeKey string) AMap {
	r := AMap(make(map[string]int))
	for k, v := range m {
		if k == changeKey {
			r[k] = m[k+"_"]
		} else {
			r[k] = v
		}
	}
	return r
}

// Execute execute template
func (m AMap) Execute(tmpl string) string {
	t := template.Must(template.New("mth").Funcs(funcMap).Parse(tmpl))
	var b bytes.Buffer
	if err := t.Execute(&b, &m); err != nil {
		log.Panicf("error in executeTemplate. %v %s %+v", err, tmpl, m)
	}
	return b.String()
}

const minint = -2 ^ 32

// ExecuteIntWrong calculates tmpl
func (m AMap) ExecuteIntWrong(tmpl string) int {
	tmpl = regexp.MustCompile(`(\.[a-zA-Z0-9]+)`).ReplaceAllString(tmpl, "${1}_")
	log.Printf("ExecuteIntWrong tmpl:%s", tmpl)
	return m.ExecuteInt(tmpl)
}

// ExecuteInt calculates tmpl
func (m AMap) ExecuteInt(tmpl string) int {
	if tmpl == "" {
		return minint
	}
	s := m.Execute("{{" + tmpl + "}}")
	val, err := strconv.Atoi(s)
	if err != nil {
		log.Panicf("cannot execute Atoi. %s Cal:%s %v", s, tmpl, err)
	}
	return val
}

// ExecuteFloat calculates
func (m AMap) ExecuteFloat(tmpl string) float64 {
	s := m.Execute("{{" + tmpl + "}}")
	val, err := strconv.ParseFloat(s, 64)
	if err != nil {
		log.Panicf("error in ParseFloat. %v %s %s", err, s, tmpl)
	}
	return val
}

type equality int

const (
	equal   equality = 0
	greater equality = 1
	less    equality = -1
)

func (e equality) String() string {
	switch e {
	case equal:
		return "="
	case greater:
		return ">"
	case less:
		return "<"
	}
	log.Panicf("should not come here.")
	return ""
}

func compare(left, right float64) equality {
	const delta = 0.000001
	if left == right || (left > right && left-right < delta) || (left < right && right-left < delta) {
		return equal
	} else if right < left {
		return greater
	} else if right > left {
		return less
	}
	log.Panicf("this should not happen. %f %f", left, right)
	return 0
}

// Equation is an equation.
type Equation interface {
	Problem() string
	Answer() string
	Explanation() string
}

type equation struct {
	problem, answer, explanation string
}

func (e *equation) Problem() string {
	return e.problem
}

func (e *equation) Answer() string {
	return e.answer
}

func (e *equation) Explanation() string {
	return e.explanation
}

func percentageSum(m map[string]int) int {
	var sum int
	for _, v := range m {
		sum += v
	}
	if sum == 0 {
		log.Fatalf("percentageSum is 0. %+v", m)
	}
	return sum
}

// Prob is a strategy
type Prob interface {
	Choose() string
}

type prob struct {
	dist map[string]int
	sum  int
}

// NewProb returns a prob strategy.
func NewProb(dist map[string]int) Prob {
	return &prob{
		dist: dist,
		sum:  percentageSum(dist),
	}
}

func (p *prob) Choose() string {
	return p.choose(p.dist, p.sum)
}

func (p *prob) choose(m map[string]int, sum int) string {
	r := rand.Intn(sum)
	for k, v := range m {
		r -= v
		if r <= 0 {
			return k
		}
	}
	log.Fatalf("cannot choose: %+v", p)
	return ""
}

var errNoConstraint = errors.New("errNoConstraint")

// TmplData is a data of template.
type TmplData struct {
	SerieName    string
	No           int
	VideoTitle   string
	Title        string
	Time         int
	Explanations []string
	Problems     []string
}

const outTemplateStr = `[seriename]{{.SerieName}}
[no]{{.No}}
[videotitle]{{.VideoTitle}}
[title]{{.Title}}
[time]{{.Time}}
{{range $ind, $val := .Explanations}}[explanation]{{$val}}
{{end}}{{range $ind, $val := .Problems}}[equation:{{$ind}}]{{$val}}
{{end}}---
`

var outTemplate = template.Must(template.New("mth").Parse(outTemplateStr))

// ErrUnableToGenerate is an error
var ErrUnableToGenerate error = errors.New("ErrUnableToGenerate")

// Generator generates problem string.
// When a problem cannot be generated, it returns ErrUnableToGenerate.
type Generator interface {
	Generate() (Equation, error)
	Next() error
}

// GeneratorData is a yaml file data.
type GeneratorData struct {
	ConstraintStrategies map[string]Strategy `yaml:"ConstraintStrategies"`
	Tmpl                 Tmpl                `yaml:"Tmpl"`
	ProblemTypeProb      map[string]int      `yaml:"ProblemTypeProb"`
}

type generator struct {
	gd   GeneratorData
	ces  map[string]ConstraintEnv
	prob Prob
}

func (g *generator) Generate() (Equation, error) {
	cs := make(map[string]*constraint)
	for k, v := range g.gd.ConstraintStrategies {
		cs[k] = v.constraint()
		g.ces[k].Env(cs[k])
	}
	type keypri struct {
		key      string
		priority int
	}
	var keys []keypri
	for k := range cs {
		keys = append(keys, keypri{key: k, priority: cs[k].Priority()})
	}
	sort.Slice(keys, func(i, j int) bool { return keys[i].priority < keys[j].priority })
	for i := 0; i < 10000; i++ {
		var err error
		m := AMap(make(map[string]int))
		for _, kp := range keys {
			k := kp.key
			m[k], m[k+"_"], err = cs[k].Get(m)
			log.Printf("m[k]:%d m[k_]:%d", m[k], m[k+"_"])
			if err != nil {
				break
			}
		}
		if err == nil {
			return g.MakeEquation(m, g.prob.Choose(), cs), nil
		}
		log.Printf("Generate: err %v", err)
	}
	return nil, ErrUnableToGenerate
}

func (g *generator) MakeEquation(m AMap, problemType string, cs map[string]*constraint) Equation {
	var problem, answer, explanation string

	ck := ChangeVar(problemType)
	l, r := g.gd.Tmpl.LeftTemplate, g.gd.Tmpl.RightTemplate
	if Reversed(problemType) {
		l, r = r, l
	}
	la, ra := l, r
	explanation = m.Execute(g.gd.Tmpl.ExplanationTemplate)
	for k := range m {
		ko := "[." + k + "]"
		kv := "{{." + k + "}}"
		kvn := "{{." + k + "_}}"
		kvno := "[" + kv + ":" + kvn + "]"
		if k == ck {
			if Ordered(problemType) {
				l, r = strings.ReplaceAll(l, ko, kvn), strings.ReplaceAll(r, ko, kvn)
			} else {
				l, r = strings.ReplaceAll(l, ko, kvno), strings.ReplaceAll(r, ko, kvno)
			}
		} else {
			l, r = strings.ReplaceAll(l, ko, kv), strings.ReplaceAll(r, ko, kv)
		}
		la, ra = strings.ReplaceAll(la, ko, kv), strings.ReplaceAll(ra, ko, kv)
	}
	answer = m.Execute(fmt.Sprintf("%s = %s", la, ra))
	if Ordered(problemType) {
		var mo AMap = m.Copy(ck)
		lv, rv := mo.ExecuteFloat(g.gd.Tmpl.LeftCalc), mo.ExecuteFloat(g.gd.Tmpl.RightCalc)
		if Reversed(problemType) {
			lv, rv = rv, lv
		}
		problem = m.Execute(fmt.Sprintf("%s [%s:=] %s", l, compare(lv, rv).String(), r))
	} else {
		problem = m.Execute(fmt.Sprintf("%s = %s", l, r))
	}
	return &equation{
		problem:     problem,
		answer:      answer,
		explanation: explanation,
	}
}

func (g *generator) Next() error {
	for k := range g.ces {
		if err := g.ces[k].Next(); err != nil {
			return err
		}
	}
	return nil
}

// NewGenerator returns a new genertor.
func NewGenerator(gd GeneratorData) Generator {
	c := make(map[string]ConstraintEnv)
	for k, v := range gd.ConstraintStrategies {
		c[k] = v.ConstraintEnv()
	}
	pr := make(map[string]int)
	for k, v := range gd.ProblemTypeProb {
		pr[string(k)] = v
	}
	p := NewProb(pr)
	return &generator{
		gd:   gd,
		ces:  c,
		prob: p,
	}
}

// ConstraintTypes.
const (
	ConstraintTypeUniform = "U" // 一様分布
	ConstraintTypeNormal  = "N" // 正規分布
	ConstraintTypeChoose  = "C" // 選択肢から選ぶ
)

// constraint is a constraint
type constraint struct {
	ConstraintType string
	Min            string
	Max            string
	StdDev         int
	WrongDev       int
	WrongVar       string
	Cal            string
	priority       int
	//Mean           int
	//Choices        []int
}

//var errCannotCal = fmt.Errorf("errCannotCal")
var errNotSatisfied = fmt.Errorf("notSatisfied")

func (c *constraint) Get(m AMap) (val int, wrongVal int, err error) {
	if c.Cal == "" {
		min, max := m.ExecuteInt(c.Min), m.ExecuteInt(c.Max)
		if min > max {
			return 0, 0, errNotSatisfied
		}
		val = c.get(min, max)
		if c.HasWrong() {
			wrongVal = c.Wrong(val, m)
		} else {
			wrongVal = val
		}
	} else {
		val = m.ExecuteInt(c.Cal)
		wrongVal = m.ExecuteIntWrong(c.Cal)
	}
	if !c.In(val, m) {
		return 0, 0, errNotSatisfied
	}
	return val, wrongVal, nil
}

func (c *constraint) get(min, max int) int {
	switch c.ConstraintType {
	case ConstraintTypeUniform:
		//log.Printf("min:%d, max:%d", min, max)
		return min + rand.Intn(max-min+1)
		/*
			case ConstraintTypeNormal:
				for i := 0; i < 10000; i++ {
					r := int(c.rnd.NormFloat64()*float64(c.StdDev)) + c.Mean
					if c.Min <= r && r <= c.Max {
						return r
					}
				}
				log.Printf("Get norm used mean, min:%d, max:%d, mean:%d\n", c.Min, c.Max, c.Mean)
				return c.Mean
			case ConstraintTypeChoose:
				return c.Choices[c.rnd.Intn(len(c.Choices))]
		*/
	default:
		log.Fatalf("Invalid ConstraintType %s", c.ConstraintType)
		return 0
	}
}

func (c *constraint) In(n int, m AMap) bool {
	return (c.Min == "" || m.ExecuteInt(c.Min) <= n) && (c.Max == "" || n <= m.ExecuteInt(c.Max))
}

func (c *constraint) Wrong(right int, m AMap) int {
	if rand.Intn(4) == 0 {
		return right
	}
	for i := 0; i < 10000; i++ {
		r := int(rand.NormFloat64()*float64(c.WrongDev)) + right
		if c.In(r, m) {
			return r
		}
	}
	log.Printf("Wrong used right. min:%s, max:%s, right:%d\n", c.Min, c.Max, right)
	return right
}

func (c *constraint) HasWrong() bool {
	return c.WrongDev > 0
}

func (c *constraint) Priority() int {
	return c.priority
}

func (c *constraint) SetMinMax(n int) {
	c.Min, c.Max = strconv.Itoa(n), strconv.Itoa(n)
}

func contains(es []Equation, e Equation) bool {
	for _, q := range es {
		if q.Answer() == e.Answer() || q.Problem() == e.Problem() {
			return true
		}
	}
	return false
}

// Strategy is a struct read from yaml file.
type Strategy struct {
	Strategy       string `yaml:"Strategy"`
	ConstraintType string `yaml:"ConstraintType"`
	Min            string `yaml:"Min"`
	Max            string `yaml:"Max"`
	Start          int    `yaml:"Start"`
	End            int    `yaml:"End"`
	StdDev         int    `yaml:"StdDev"`
	WrongDev       int    `yaml:"WrongDev"`
	WrongVar       string `yaml:"WrongVar"`
	Cal            string `yaml:"Cal"`
	Priority       int    `yaml:"Priority"`
}

// ConstraintEnv returns a ConstraintEnv
func (s Strategy) ConstraintEnv() ConstraintEnv {
	switch s.Strategy {
	case "sequential":
		return &SequentialConstraintEnv{n: s.Start, end: s.End}
	case "constant":
		return &ConstantConstraintEnv{}
	default:
		log.Fatalf("Unknown ConstraintStrategy %s", s.Strategy)
		return nil
	}
}

func (s Strategy) constraint() *constraint {
	return &constraint{
		ConstraintType: s.ConstraintType,
		Min:            s.Min,
		Max:            s.Max,
		StdDev:         s.StdDev,
		WrongDev:       s.WrongDev,
		WrongVar:       s.WrongVar,
		Cal:            s.Cal,
		priority:       s.Priority,
	}
}

// Tmpl is a struct used for reading yaml file.
type Tmpl struct {
	LeftTemplate        string `yaml:"LeftTemplate"`
	RightTemplate       string `yaml:"RightTemplate"`
	LeftCalc            string `yaml:"LeftCalc"`
	RightCalc           string `yaml:"RightCalc"`
	ExplanationTemplate string `yaml:"ExplanationTemplate"`
}

// Mth is a base struct for yaml file.
type Mth struct {
	SerieName     string                   `yaml:"SerieName"`
	Title         string                   `yaml:"Title"`
	Time          int                      `yaml:"Time"`
	ProblemNum    int                      `yaml:"ProblemNum"`
	Generators    map[string]GeneratorData `yaml:"Generators"`
	GeneratorProb map[string]int           `yaml:"GeneratorProb"`
	StartVideoNo  int                      `yaml:"StartVideoNo"`
	VideoNum      int                      `yaml:"VideoNum"`
}

// ConstraintEnv is an interface for wrapping Constraint for each usage.
type ConstraintEnv interface {
	Env(c *constraint)
	Next() error
}

// SequentialConstraintEnv is to make a sequential constraint.
type SequentialConstraintEnv struct {
	n   int
	end int
}

// Env makes changes to a constraint.
func (e *SequentialConstraintEnv) Env(c *constraint) {
	c.SetMinMax(e.n)
}

// Next advances one.
func (e *SequentialConstraintEnv) Next() error {
	e.n++
	if e.n > e.end {
		return errNoConstraint
	}
	return nil
}

// ConstantConstraintEnv is a constant constraint
type ConstantConstraintEnv struct {
}

// Env does nothing
func (e *ConstantConstraintEnv) Env(c *constraint) {
}

// Next does nothing
func (e *ConstantConstraintEnv) Next() error {
	return nil
}

// MakeFormulaFile makes a file.
func MakeFormulaFile(filepath ison.Filepath) []byte {
	var m Mth
	err := yaml.Unmarshal(ison.ReadFile(filepath), &m)
	if err != nil {
		log.Fatalf("MakeFormulaFile error %v %v", filepath, err)
	}
	fmt.Printf("%+v\n", m)
	gs := make(map[string]Generator)
	for k, v := range m.Generators {
		gs[k] = NewGenerator(v)
	}
	prob := NewProb(m.GeneratorProb)
	return makeFormulaFile(m.SerieName, m.Title, m.Time, m.ProblemNum, gs, prob, m.StartVideoNo, m.VideoNum)
}

func makeFormulaFile(seriename, title string, time int, problemNum int, gs map[string]Generator, gp Prob, startVideoNo, videoNum int) []byte {
	log.Printf("seriename:%s title:%s time:%d problemNum:%d startVideoNo:%d videoNum:%d", seriename, title, time, problemNum, startVideoNo, videoNum)
	var b bytes.Buffer
	var equations []Equation
	no := startVideoNo
	for w := 0; w < 10000; w++ {
		log.Printf("w:%d", w)
		g := gs[gp.Choose()]
		for i := 0; i < 10000; i++ {
			if equation, err := g.Generate(); err == nil {
				log.Printf("equation: %+v", equation)
				if !contains(equations, equation) {
					equations = append(equations, equation)
				}
			} else {
				log.Printf("# %v", err)
				break
			}
			if len(equations) == problemNum {
				break
			}
		}
		if len(equations) == problemNum {
			n := rand.Intn(problemNum)
			e := equations[n]
			var problems []string
			for _, q := range equations {
				problems = append(problems, q.Problem())
			}
			data := TmplData{
				SerieName:    seriename,
				No:           no,
				VideoTitle:   e.Answer(),
				Title:        title,
				Time:         time,
				Explanations: strings.Split(e.Explanation(), "\n"),
				Problems:     problems,
			}
			err := outTemplate.Execute(&b, data)
			if err != nil {
				log.Fatalf("%v", err)
			}
			//log.Println(b.String())
			equations = nil
			no++
		}
		if g.Next() != nil {
			break
		}
		if no-startVideoNo >= videoNum {
			break
		}
	}
	return b.Bytes()
}
