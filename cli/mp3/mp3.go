package mp3

import (
	"fmt"
	"io"
	"isonschool/cli/ison"
	"log"
	"math"
	"os"
	"runtime"

	"github.com/dmulholl/mp3lib"
	"golang.org/x/crypto/ssh/terminal"
)

// Data は，MP3ファイルのデータです。
type Data struct {
	SampleCount  int
	SamplingRate int
	FirstBitRate int
}

// Seconds は，秒数です。
func (d Data) Seconds() float64 {
	return float64(d.SampleCount) / float64(d.SamplingRate)
}

// GetData は，file のデータを取得します。
func GetData(file ison.Filepath) Data {
	infile, err := os.Open(file.String())
	if err != nil {
		log.Panicf("mp3Length error open file:%s %v", file, err)
	}
	var totalFrames int
	var totalBytes int
	var totalCount int
	var isFirstFrame = true
	var firstBitRate int
	var firstFrame *mp3lib.MP3Frame
	for {
		// Read the next frame from the input file.
		frame := mp3lib.NextFrame(infile)
		if frame == nil {
			break
		}

		// Skip the first frame if it's a VBR header.
		if isFirstFrame {
			isFirstFrame = false
			if mp3lib.IsXingHeader(frame) || mp3lib.IsVbriHeader(frame) {
				continue
			}
		}

		// If we detect more than one bitrate we'll need to add a VBR
		// header to the output file.
		if firstBitRate == 0 {
			firstBitRate = frame.BitRate
			firstFrame = frame
		}

		totalFrames++
		totalBytes += len(frame.RawBytes)
		totalCount += frame.SampleCount
	}

	return Data{
		SampleCount:  totalCount,
		SamplingRate: firstFrame.SamplingRate,
		FirstBitRate: firstFrame.BitRate,
	}
}

func getFirstFrame(file string) *mp3lib.MP3Frame {
	infile, err := os.Open(file)
	if err != nil {
		log.Panicf("getFirstFrame error open file:%s %v", file, err)
	}
	var isFirstFrame = true
	var firstBitRate int
	for {
		// Read the next frame from the input file.
		frame := mp3lib.NextFrame(infile)
		if frame == nil {
			break
		}

		// Skip the first frame if it's a VBR header.
		if isFirstFrame {
			isFirstFrame = false
			if mp3lib.IsXingHeader(frame) || mp3lib.IsVbriHeader(frame) {
				continue
			}
		}

		if firstBitRate == 0 {
			return frame
		}

	}
	return nil
}

// Check that all the files in the list exist.
func validateFiles(files []string) {
	for _, file := range files {
		if _, err := os.Stat(file); err != nil {
			fmt.Fprintf(
				os.Stderr,
				"Error: the file '%v' does not exist.\n", file)
			os.Exit(1)
		}
	}
}

// Interlace a spacer file between each file in the list.
func interlace(files []string, spacer string) []string {
	var interlaced []string
	for _, file := range files {
		interlaced = append(interlaced, file)
		interlaced = append(interlaced, spacer)
	}
	return interlaced[:len(interlaced)-1]
}

// Merge creates a new file at the specified output path containing the merged
// contents of the list of input files.
func Merge(outpath string, inpaths []string, nosound string, nosoundlengths []float64) {

	var force, quiet, tag = true, true, false
	var nosoundFrame = getFirstFrame(nosound)
	var nosoundLength = float64(nosoundFrame.SampleCount) / float64(nosoundFrame.SamplingRate)

	var totalFrames uint32
	var totalBytes uint32
	var totalFiles int
	var firstBitRate int
	var isVBR bool
	var extra float64

	// Only overwrite an existing file if the --force flag has been used.
	if _, err := os.Stat(outpath); err == nil {
		if !force {
			fmt.Fprintf(
				os.Stderr,
				"Error: the file '%v' already exists.\n", outpath)
			os.Exit(1)
		}
	}

	// If the list of input files includes the output file we'll end up in an
	// infinite loop.
	for _, filepath := range inpaths {
		if filepath == outpath {
			fmt.Fprintln(
				os.Stderr,
				"Error: the list of input files includes the output file.")
			os.Exit(1)
		}
	}

	// Create the output file.
	outfile, err := os.Create(outpath)
	if err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}

	if !quiet {
		printLine()
	}

	// Loop over the input files and append their MP3 frames to the output
	// file.
	for i, inpath := range inpaths {
		if !quiet {
			fmt.Println("+", inpath)
		}

		infile, err := os.Open(inpath)
		if err != nil {
			fmt.Fprintln(os.Stderr, err)
			os.Exit(1)
		}

		isFirstFrame := true

		for {
			// Read the next frame from the input file.
			frame := mp3lib.NextFrame(infile)
			if frame == nil {
				break
			}

			// Skip the first frame if it's a VBR header.
			if isFirstFrame {
				isFirstFrame = false
				if mp3lib.IsXingHeader(frame) || mp3lib.IsVbriHeader(frame) {
					continue
				}
			}

			// If we detect more than one bitrate we'll need to add a VBR
			// header to the output file.
			if firstBitRate == 0 {
				firstBitRate = frame.BitRate
			} else if frame.BitRate != firstBitRate {
				isVBR = true
				fmt.Printf("VBR")
			}

			// Write the frame to the output file.
			_, err := outfile.Write(frame.RawBytes)
			if err != nil {
				fmt.Fprintln(os.Stderr, err)
				os.Exit(1)
			}

			totalFrames++
			totalBytes += uint32(len(frame.RawBytes))
		}

		infile.Close()

		var m = int(math.Floor((extra + nosoundlengths[i]) / nosoundLength))
		extra = extra + nosoundlengths[i] - float64(m)*nosoundLength
		for n := 0; n < m; n++ {
			_, err := outfile.Write(nosoundFrame.RawBytes)
			if err != nil {
				fmt.Fprintln(os.Stderr, err)
				os.Exit(1)
			}

		}
		totalFiles++
	}

	outfile.Close()
	if !quiet {
		printLine()
	}

	// If we detected multiple bitrates, prepend a VBR header to the file.
	if isVBR {
		if !quiet {
			fmt.Println("• Multiple bitrates detected. Adding VBR header.")
		}
		addXingHeader(outpath, totalFrames, totalBytes)
	}

	// Copy the ID3v2 tag from the first input file if requested. Order of
	// operations is important here. The ID3 tag must be the first item in
	// the file - in particular, it must come *before* any VBR header.
	if tag {
		if !quiet {
			fmt.Println("• Adding ID3 tag.")
		}
		addID3v2Tag(outpath, inpaths[0])
	}

	// Print a count of the number of files merged.
	if !quiet {
		fmt.Printf("• %v files merged.\n", totalFiles)
		printLine()
	}
}

// Prepend an Xing VBR header to the specified MP3 file.
func addXingHeader(filepath string, totalFrames, totalBytes uint32) {

	outputFile, err := os.Create(filepath + ".mp3cat.tmp")
	if err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}

	inputFile, err := os.Open(filepath)
	if err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}

	xingHeader := mp3lib.NewXingHeader(totalFrames, totalBytes)

	_, err = outputFile.Write(xingHeader.RawBytes)
	if err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}

	_, err = io.Copy(outputFile, inputFile)
	if err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}

	outputFile.Close()
	inputFile.Close()

	err = os.Remove(filepath)
	if err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}

	err = os.Rename(filepath+".mp3cat.tmp", filepath)
	if err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}
}

// Prepend an ID3v2 tag to the MP3 file at mp3Path, copying from tagPath.
func addID3v2Tag(mp3Path, tagPath string) {

	tagFile, err := os.Open(tagPath)
	if err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}

	id3tag := mp3lib.NextID3v2Tag(tagFile)
	tagFile.Close()

	if id3tag != nil {
		outputFile, err := os.Create(mp3Path + ".mp3cat.tmp")
		if err != nil {
			fmt.Fprintln(os.Stderr, err)
			os.Exit(1)
		}

		inputFile, err := os.Open(mp3Path)
		if err != nil {
			fmt.Fprintln(os.Stderr, err)
			os.Exit(1)
		}

		_, err = outputFile.Write(id3tag.RawBytes)
		if err != nil {
			fmt.Fprintln(os.Stderr, err)
			os.Exit(1)
		}

		_, err = io.Copy(outputFile, inputFile)
		if err != nil {
			fmt.Fprintln(os.Stderr, err)
			os.Exit(1)
		}

		outputFile.Close()
		inputFile.Close()

		err = os.Remove(mp3Path)
		if err != nil {
			fmt.Fprintln(os.Stderr, err)
			os.Exit(1)
		}

		err = os.Rename(mp3Path+".mp3cat.tmp", mp3Path)
		if err != nil {
			fmt.Fprintln(os.Stderr, err)
			os.Exit(1)
		}
	}
}

// Print a line to stdout if we're running in a terminal.
func printLine() {
	if terminal.IsTerminal(int(os.Stdout.Fd())) {
		width, _, err := terminal.GetSize(int(os.Stdout.Fd()))
		if err == nil {
			if runtime.GOOS == "windows" {
				for i := 0; i < width; i++ {
					fmt.Print("-")
				}
				fmt.Println()
			} else {
				fmt.Print("\u001B[90m")
				for i := 0; i < width; i++ {
					fmt.Print("─")
				}
				fmt.Println("\u001B[0m")
			}
		}
	}
}
