package main

import (
	"flag"
	"isonschool/cli/ison"
	"isonschool/cli/yout"
)

func main() {
	var masterfile string
	var categoryID string
	var disableUploading bool
	flag.StringVar(&masterfile, "m", "/Users/juny/go/src/isonschool/json/master.json", "")
	flag.StringVar(&categoryID, "c", "", "")
	flag.BoolVar(&disableUploading, "d", false, "")
	flag.Parse()
	var master ison.Master
	ison.ReadJSON(&master, masterfile)
	for _, c := range master.Categories {
		if c.HasVideo() && (categoryID == "" || categoryID == c.ID) {
			yout.UploadVideo(c, disableUploading)
		}
	}
}
