package main

import (
	"flag"
	"isonschool/cli/ison"
)

func main() {
	var masterfile string
	var categoryID string
	var jsfilename string
	var exerciseName string
	var problemNum int
	var sentenceNos string
	flag.StringVar(&masterfile, "m", "/Users/juny/go/src/isonschool/json/master.json", "")
	flag.StringVar(&categoryID, "c", "", "")
	flag.StringVar(&jsfilename, "j", "/Users/juny/go/src/isonschool/recorder/grammarvideodata.js", "")
	flag.StringVar(&exerciseName, "e", "", "")
	flag.IntVar(&problemNum, "p", 10, "")
	flag.StringVar(&sentenceNos, "s", "", "カンマ区切り")
	flag.Parse()
	var master ison.Master
	ison.ReadJSON(&master, masterfile)
	yout.MakeHTMLRecorderJSON(master, categoryID, jsfilename)
}
