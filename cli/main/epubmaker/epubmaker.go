package main

import (
	"archive/zip"
	"bytes"
	"encoding/json"
	"flag"
	"fmt"
	"image/jpeg"
	"io/ioutil"
	"log"
	"path"
	"path/filepath"
	"strings"
	"text/template"

	"github.com/nfnt/resize"
)

const templatesDir = "/Users/juny/go/src/isonschool/cli/main/epubmaker/templates"

// Data is a data structure
type Data struct {
	Title     string
	Modified  string
	UUID      string
	Publisher string
	Images    map[string]string
	Output    string
}

// EpubWriter makes epub file.
type EpubWriter struct {
	basedir string
	data    interface{}
	buf     *bytes.Buffer
	w       *zip.Writer
}

// NewEpubWriter returns a new EpubWriter.
func NewEpubWriter(data interface{}) *EpubWriter {
	buf := new(bytes.Buffer)
	w := zip.NewWriter(buf)

	e := &EpubWriter{
		basedir: templatesDir,
		data:    data,
		buf:     buf,
		w:       w,
	}
	e.writeMimetype()
	return e
}

func (e *EpubWriter) writeMimetype() {
	mtheader := &zip.FileHeader{
		Name:   "mimetype",
		Method: zip.Store,
	}
	f, err := e.w.CreateHeader(mtheader)
	if err != nil {
		log.Fatalf("CreateHeader mimetype %v", err)
	}
	_, err = f.Write([]byte("application/epub+zip"))
	if err != nil {
		log.Fatalf("Write mimetype %v", err)
	}
}

// AddFiles adds files
func (e *EpubWriter) AddFiles() {
	e.addFiles("")
}

func (e *EpubWriter) addFiles(dir string) {
	ts, err := ioutil.ReadDir(filepath.Join(e.basedir, dir))
	if err != nil {
		log.Fatalf("ReadDir %s %v", dir, err)
	}
	for _, t := range ts {
		if t.IsDir() {
			name := path.Join(dir, t.Name()) + "/"
			_, err := e.w.Create(name)
			if err != nil {
				log.Fatalf("Create folder name:%s %v", name, err)
			}
			e.addFiles(path.Join(dir, t.Name()))
		} else {
			e.writeFile(path.Join(dir, t.Name()), filepath.Join(e.basedir, dir, t.Name()))
		}
	}
}

// WriteFile write a file to the epub.
func (e *EpubWriter) WriteFile(name, file string) {
	e.writeFile(name, file)
}

func (e *EpubWriter) writeFile(name, file string) {
	f, err := e.w.Create(strings.TrimSuffix(name, ".tmpl"))
	if err != nil {
		log.Fatalf("Create name:%s, file:%s %v", name, file, err)
	}
	b, err := ioutil.ReadFile(file)
	if err != nil {
		log.Fatalf("ReadFile name:%s, file:%s %v", name, file, err)
	}
	if strings.HasSuffix(file, ".tmpl") {
		t, err := template.New("template").Parse(string(b))
		if err != nil {
			log.Fatalf("Template Parse Error: name:%s file:%s %v", name, file, err)
		}
		err = t.Execute(f, e.data)
		if err != nil {
			log.Fatalf("Template Execute Error: name:%s, file:%s %v", name, file, err)
		}
	} else if strings.HasSuffix(file, ".jpg") {
		m, err := jpeg.Decode(bytes.NewReader(b))
		if err != nil {
			log.Fatalf("jpeg.Decode Error %s %v", file, err)
		}
		m = resize.Thumbnail(600, 1500, m, resize.Lanczos3)
		err = jpeg.Encode(f, m, nil)
		if err != nil {
			log.Fatalf("jpeg.Encode error name:%s, file:%s %v", name, file, err)
		}
	} else {
		_, err = f.Write(b)
		if err != nil {
			log.Fatalf("Write name:%s, file:%s %v", name, file, err)
		}
	}
}

// WriteEpub outputs the epub.
func (e *EpubWriter) WriteEpub(output string) {
	err := e.w.Close()
	if err != nil {
		log.Fatalf("Close %v", err)
	}

	err = ioutil.WriteFile(output, e.buf.Bytes(), 0644)
	if err != nil {
		log.Fatalf("Write output %s %v", output, err)
	}
}

func main() {
	var datafilepath string
	flag.StringVar(&datafilepath, "f", "", "")
	flag.Parse()

	b, err := ioutil.ReadFile(datafilepath)
	if err != nil {
		log.Fatalf("file:%s %v", datafilepath, err)
	}
	var data Data
	err = json.Unmarshal(b, &data)
	if err != nil {
		log.Fatalf("file:%s %v", datafilepath, err)
	}

	e := NewEpubWriter(&data)

	e.AddFiles()

	for newfile, originalfile := range data.Images {
		e.WriteFile(path.Join("OEBPS/images", newfile), originalfile)
	}

	e.WriteEpub(data.Output)

	fmt.Printf("File output: %s\n", data.Output)
}
