package main

import (
	"bytes"
	"io/ioutil"
	"isonschool/cli/yout"
)

func main() {
	var b bytes.Buffer
	yout.ListVoices(&b)
	ioutil.WriteFile("/Users/juny/go/src/isonschool/assets/voices.txt", b.Bytes(), 0644)
}
