package main

import (
	"flag"
	"isonschool/cli/ison"
)

func main() {
	var masterfile string
	flag.StringVar(&masterfile, "m", "/Users/juny/go/src/isonschool/json/master.json", "")
	var dataurlfile string
	flag.StringVar(&dataurlfile, "t", "/Users/juny/Downloads/pngfiles.txt", "")
	flag.Parse()
	var master ison.Master
	ison.ReadJSON(&master, masterfile)
	ison.ParseDataURL(master, dataurlfile)
}
