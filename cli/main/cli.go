package main

import (
	"bytes"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strings"
	"time"

	"isonschool.com/isonschool/cli/colour"
	"isonschool.com/isonschool/cli/hatena"
	"isonschool.com/isonschool/cli/ison"
	"isonschool.com/isonschool/cli/mth"
	"isonschool.com/isonschool/cli/ruby"
	"isonschool.com/isonschool/cli/translation"
	"isonschool.com/isonschool/cli/twitter"
	"isonschool.com/isonschool/cli/yout"
)

// KeyEn is a pair of strings
type KeyEn struct {
	Key, En string
}

var (
	availableTyps = map[string]bool{
		"serve":            true,
		"voicelist":        true,
		"filterpalette":    true,
		"tra":              true,
		"playlistids":      true,
		"videoids":         true,
		"printtranslation": true,
		"formula":          true,
		"grammar":          true,
		"makevideo":        true,
		"makecaption":      true,
		"makethumbnail":    true,
		"thumbnail":        true,
		"merge":            true,
		"update":           true,
		"upload":           true,
		"playlist":         true,
		"uploadplaylist":   true,
		"playlistitem":     true,
		"caption":          true,
		"print":            true,
		"tweet":            true,
		"hatena":           true,
		"makeformula":      true,
	}
)

func main() {
	var masterfile string
	var categoryID string
	var seriename string
	var exname string
	var typ string
	var jsfilename string
	var dataurlfile string
	var texts string
	var printtranslationfile string
	var no int
	var file string
	flag.StringVar(&masterfile, "m", "/Users/juny/go/src/isonschool/json/master.json", "")
	flag.StringVar(&categoryID, "c", "", "")
	flag.StringVar(&seriename, "s", "", "")
	flag.StringVar(&exname, "e", "", "")
	flag.IntVar(&no, "n", 0, "problem no.")
	flag.StringVar(&typ, "typ", "", "")
	flag.StringVar(&texts, "texts", "", ";で区切る")
	flag.StringVar(&jsfilename, "jsfile", "/Users/juny/go/src/isonschool/recorder/grammarvideodata.js", "")
	flag.StringVar(&dataurlfile, "dataURLFile", "/Users/juny/Downloads/pngfiles.txt", "")
	flag.StringVar(&printtranslationfile, "printtranslationfile", "/Users/juny/go/src/yossie/translationdump.txt", "")
	flag.StringVar(&file, "f", "", "")
	flag.Parse()
	fmt.Printf("typ:%s\n", typ)
	if !availableTyps[typ] {
		log.Printf("typ is invalid")
		return
	}
	if typ == "serve" {
		fs := http.FileServer(http.Dir("/Users/juny/go/src/isonschool/recorder/"))
		http.Handle("/", fs)
		http.ListenAndServe(":8082", nil)
		return
	} else if typ == "voicelist" {
		var b bytes.Buffer
		yout.ListVoices(&b)
		ioutil.WriteFile("/Users/juny/go/src/isonschool/assets/voices.txt", b.Bytes(), 0644)
		return
	} else if typ == "filterpalette" {
		var paletteNum int
		var darkmodeNum int
		var b bytes.Buffer
		m := make(map[string]colour.Palette)
		for _, p := range strings.Split(string(ison.ReadFile("/Users/juny/go/src/yossie/palettes.txt")), "\n") {
			p = strings.TrimSpace(strings.TrimPrefix(p, "[palette]"))
			if p == "" {
				continue
			}
			for _, l := range colour.CreatePalettes(p) {
				if _, ok := m[l.PaletteName]; ok {
					continue
				}
				m[l.PaletteName] = l
				paletteNum++
				if l.IsDarkMode() {
					darkmodeNum++
				}
			}
		}
		var n int
		for _, l := range m {
			b.WriteString(fmt.Sprintf("[palette]%s\n", l.String()))
			n++
			if n == 100 {
				break
			}
		}
		ioutil.WriteFile("/Users/juny/go/src/yossie/palettes-ok.txt", b.Bytes(), 0644)
		fmt.Println("paletteNum", paletteNum, "normal", paletteNum-darkmodeNum, "darkmode", darkmodeNum)
		fmt.Println("/Users/juny/go/src/yossie/palettes-ok.txt")
		return
	} else if typ == "makeformula" {
		ioutil.WriteFile("/Users/juny/go/src/yossie/formula.txt", mth.MakeFormulaFile(ison.ToFilepath(file)), 0644)
		fmt.Println("/Users/juny/go/src/yossie/formula.txt")
	} /* else if typ == "makeformula-div" {
		rnd := rand.New(rand.NewSource(0))
		//ioutil.WriteFile("/Users/juny/go/src/yossie/formula-div.txt", mth.MakeFormulaFile("math-e-div-1", "÷", 6, 7, mth.NewDivGenerator(rnd), mth.NewSequentialConstraintStrategy(2, 400), mth.NewDivProbStrategy(50, 20, 10, 10, 10, 30, 10, 5, 5, 5, rnd), 1, 99), 0644)

		ioutil.WriteFile("/Users/juny/go/src/yossie/formula-div.txt", mth.MakeFormulaFile("math-e-div-4th-1", "÷", 6, 7, mth.NewDivGenerator(rnd), mth.NewConstantConstraintStrategy(2, 100), mth.NewDivProbStrategy(50, 20, 20, 10, 10, 10, 30, 10, 10, 5, 5, 5, rnd), 1, 50), 0644)
		fmt.Println("/Users/juny/go/src/yossie/formula-div.txt")
		return
	}*/
	var master ison.Master
	ison.ReadJSON(&master, ison.ToFilepath(masterfile))
	for i := range master.Categories {
		master.Categories[i].Master = &master
	}

	var printtranslationBuffer bytes.Buffer
	for _, c := range master.Categories {
		if typ == "tweet" {
			ticker := time.NewTicker(100 * time.Second)
			if c.HasVideo() && (categoryID == "" || categoryID == c.ID) {
				client := twitter.NewClient()
				for _, f := range c.JSONVideoFiles() {
					var video yout.Video
					ison.ReadJSON(&video, f)
					if video.ID != "" {
						for i, loc := range video.Localizations {
							if loc.TwitterStatusID == 0 {
								video.Localizations[i].TwitterStatusID = client.TweetVideo(video.ID, loc.Title, loc.Description)
								ison.WriteJSON(&video, f)
								<-ticker.C
							}
						}
					}
				}
			}
		}
		if typ == "hatena" {
			if c.HasVideo() && (categoryID == "" || categoryID == c.ID) {
				client := hatena.NewClient()
				for _, f := range c.JSONVideoFiles() {
					var video yout.Video
					ison.ReadJSON(&video, f)
					if video.ID != "" && video.HatenaPermalink == "" {
						title := video.Title
						for _, loc := range video.Localizations {
							if loc.Lang.String() == "ja" {
								title = loc.Title
							}
						}
						video.HatenaPermalink = client.PostBookmarkOfVideo(video.ID, title)
						ison.WriteJSON(&video, f)
					}
				}
			}
		}
		if typ == "tra" && (categoryID == "" || categoryID == c.ID) {
			f := ison.ReadFile(c.BinaryEnMachineTranslationFile())
			var sources []string
			var ens []string
			add := func(s, e string) {
				if s == "" || e == "" {
					return
				}
				sources = append(sources, s)
				ens = append(ens, e)
			}
			var key, en string
			for _, k := range strings.Split(string(f), "\n") {
				if strings.HasPrefix(k, "[key]") {
					key = k[len("[key]"):]
				} else if strings.HasPrefix(k, "[lang:en]") {
					en = k[len("[lang:en]"):]
				} else if strings.HasPrefix(k, "---") {
					add(key, en)
					key, en = "", ""
				}
			}
			add(key, en)
			translation.GoogleTranslateFromEnToAll(sources, ens, c.AudioLang, c.TxtMachineTranslationFile())
			ison.WriteFile(c.BinaryEnMachineTranslationFile(), []byte{})
		}
		if typ == "playlistids" && (categoryID == "" || categoryID == c.ID) && c.HasVideo() {
			var plidsfile ison.Filepath = "/Users/juny/go/src/yossie/playlistids.txt"
			var ids []string
			for _, id := range strings.Split(string(ison.ReadFile(plidsfile)), "\n") {
				id = strings.TrimSpace(id)
				if id == "" {
					continue
				}
				if strings.Index(id, "=") >= 0 {
					id = id[strings.Index(id, "=")+1:]
				}
				var has bool
				for _, d := range ids {
					if d == id {
						has = true
					}
				}
				if !has {
					ids = append(ids, id)
				}
			}
			log.Printf("%+v", ids)
			if len(ids) > 0 {
				var i int
				for _, f := range c.JSONPlaylistFiles() {
					var pl yout.Playlist
					ison.ReadJSON(&pl, f)
					if pl.ID == "" {
						pl.ID = ids[i]
						ison.WriteJSON(&pl, f)
						i++
						if i >= len(ids) {
							break
						}
					}
				}
				var b bytes.Buffer
				for ; i < len(ids); i++ {
					b.WriteString(ids[i] + "\n")
				}
				ison.WriteFile(plidsfile, b.Bytes())
			}
		}
		if typ == "videoids" && (categoryID == "" || categoryID == c.ID) {
			var b bytes.Buffer
			for _, s := range strings.Split(string(ison.ReadFile(c.BinaryVideoIDsFile())), "\n") {
				ss := strings.Split(s, " ")
				if len(ss) < 2 {
					b.WriteString(s + "\n")
					continue
				}
				id := strings.TrimPrefix(strings.TrimSpace(ss[0]), "https://youtu.be/")
				file := strings.TrimSpace(ss[1])
				var written bool
				for _, f := range c.JSONVideoFiles() {
					var video yout.Video
					ison.ReadJSON(&video, f)
					if video.File.String() == file {
						if video.ID != "" && video.ID != id {
							log.Printf("Warning!! id is not the same. File:%s, oldId:%s, newId:%s", file, video.ID, id)
						}
						video.ID = id
						ison.WriteJSON(&video, f)
						written = true
						break
					}
				}
				if !written {
					b.WriteString(s + "\n")
				}
			}
			ison.WriteFile(c.BinaryVideoIDsFile(), b.Bytes())
		}
		if typ == "printtranslation" && (categoryID == "" || categoryID == c.ID) {
			translation.PrintTranslation(c, strings.Split(texts, ","), categoryID == "", &printtranslationBuffer)
		}
		if typ == "formula" {
			if c.IsFormula() && (categoryID == "" || categoryID == c.ID) {
				fmt.Printf("%s\n", c.ID)
				// 翻訳
				translation.MergeTxtTranslation(c)
				fmt.Printf("MergeTxtTranslation がおわりました。\n")
				yout.MergeTxtSerieFiles(c, seriename)
				fmt.Printf("MergeTxtSerieFiles がおわりました。\n")
				yout.MergeTxtFormulaFiles(c, seriename)
				fmt.Printf("MergeTxtFormulaFiles がおわりました。\n")
				yout.MakeFormulaImages(c, seriename, no)
				fmt.Printf("MakeFormulaImages がおわりました。\n")
				yout.MakeFormulaVideo(c, seriename, no, exname)
				fmt.Printf("MakeFormulaVideo がおわりました。\n")
				yout.MakeJSONVideoFilesFromFormula(c, seriename, no, exname)
				fmt.Printf("MakeJSONVideoFilesFromFormula がおわりました。\n")
				yout.MakeFormulaThumbnailImage(c, seriename, no, exname, false)
				fmt.Printf("MakeFormulaThumbnailImage がおわりました。\n")
			}
		}
		if typ == "print" {
			if c.IsGrammar() && (categoryID == "" || categoryID == c.ID) {
				printExercises(c, seriename, no, exname)
			}
		}
		if typ == "grammar" {
			if c.IsGrammar() && (categoryID == "" || categoryID == c.ID) {
				fmt.Printf("Start: %s\n", c.ID)
				// 翻訳
				translation.MergeTxtTranslation(c)
				fmt.Printf("MergeTxtTranslation がおわりました。\n")
				// ルビ
				ruby.MergeTxtRubies(c)
				fmt.Printf("MergeTxtRubies がおわりました。\n")
				yout.MergeTxtSerieFiles(c, seriename)
				fmt.Printf("MergeTxtSerieFiles がおわりました。\n")
				// センテンス
				yout.MergeTxtGrammarFiles(c, seriename, no)
				fmt.Printf("MergeTxtGrammarFiles がおわりました。\n")
				// 問題作成
				yout.MakeProblems(c, seriename, no, exname)
				fmt.Printf("MakeProblems がおわりました。\n")
				printExercises(c, seriename, no, exname)
				yout.MakeImages(c, seriename, no, exname)
				fmt.Printf("MakeImages がおわりました。\n")
				// if yout.MakeHTMLRecorderJSON(c, seriename, jsfilename) {
				// 	fmt.Printf("HTMLのJSONが作成されました。 http://localhost:8082/grammarvideo.html?a=%d ブラウザで保存したあと，エンターキーを押してください。", time.Now().Unix())
				// 	bufio.NewScanner(os.Stdin).Scan()
				// }
				// fmt.Printf("MakeHTMLRecorderJSON がおわりました。\n")
				//ison.ParseDataURL(c, ison.ToFilepath(dataurlfile))
				//fmt.Printf("ParseDataURL がおわりました。\n")
				yout.DownloadTextToSpeechMP3(c, seriename, no, exname)
				fmt.Printf("DownloadTextToSpeechMP3 がおわりました。\n")
				yout.MakeVideo(c, seriename, no, exname)
				fmt.Printf("MakeVideo がおわりました。\n")
				yout.MakeCaption(c, seriename, no, exname)
				fmt.Printf("MakeCaption がおわりました。\n")
				yout.MakeJSONVideoFilesFromGrammar(c, seriename, no, exname)
				fmt.Printf("MakeJSONVideoFilesFromGrammar がおわりました。\n")
				yout.MakeThumbnailImage(c, seriename, no, exname, false)
				fmt.Printf("MakeThumbnailImage がおわりました。\n")
			}
		}
		if typ == "makevideo" {
			if c.IsGrammar() && (categoryID == "" || categoryID == c.ID) {
				fmt.Printf("Start: %s\n", c.ID)
				// 翻訳
				translation.MergeTxtTranslation(c)
				fmt.Printf("MergeTxtTranslation がおわりました。\n")
				// ルビ
				ruby.MergeTxtRubies(c)
				fmt.Printf("MergeTxtRubies がおわりました。\n")
				yout.MergeTxtSerieFiles(c, seriename)
				fmt.Printf("MergeTxtSerieFiles がおわりました。\n")
				// ビデオ作成
				yout.DownloadTextToSpeechMP3(c, seriename, no, exname)
				fmt.Printf("DownloadTextToSpeechMP3 がおわりました。\n")
				yout.MakeVideo(c, seriename, no, exname)
				fmt.Printf("MakeVideo がおわりました。\n")
				yout.MakeCaption(c, seriename, no, exname)
				fmt.Printf("MakeCaption がおわりました。\n")
				yout.MakeJSONVideoFilesFromGrammar(c, seriename, no, exname)
				fmt.Printf("MakeJSONVideoFilesFromGrammar がおわりました。\n")
				yout.MakeThumbnailImage(c, seriename, no, exname, false)
				fmt.Printf("MakeThumbnailImage がおわりました。\n")
			}
		}
		if typ == "makecaption" {
			if c.IsGrammar() && (categoryID == "" || categoryID == c.ID) {
				fmt.Printf("Start: %s\n", c.ID)
				// 翻訳
				translation.MergeTxtTranslation(c)
				fmt.Printf("MergeTxtTranslation がおわりました。\n")
				// ルビ
				ruby.MergeTxtRubies(c)
				fmt.Printf("MergeTxtRubies がおわりました。\n")
				// ビデオ作成
				yout.MakeCaption(c, seriename, no, exname)
				fmt.Printf("MakeCaption がおわりました。\n")
			}
		}
		if typ == "makethumbnail" && (categoryID == "" || categoryID == c.ID) {
			if c.IsGrammar() {
				yout.MakeThumbnailImage(c, seriename, no, exname, true)
			}
		}
		if typ == "merge" && (categoryID == "" || categoryID == c.ID) {
			translation.MergeTxtTranslation(c)
			if c.IsChannel() {
				yout.MergeTxtChannel(c)
			} else if c.IsGrammar() {
				ruby.MergeTxtRubies(c)
				yout.MergeTxtSerieFiles(c, seriename)
				yout.MergeTxtGrammarFiles(c, seriename, no)
				yout.MakeJSONVideoFilesFromGrammar(c, seriename, no, exname)
				yout.MergeTxtPlaylists(c)
			} else if c.IsFormula() {
				yout.MergeTxtSerieFiles(c, seriename)
				yout.MergeTxtFormulaFiles(c, seriename)
				yout.MakeJSONVideoFilesFromFormula(c, seriename, no, exname)
				yout.MergeTxtPlaylists(c)
			} else {
				yout.MergeTxtVideoFiles(c)
				yout.MergeTxtPlaylists(c)
			}
		}
		if typ == "update" && (categoryID == "" || categoryID == c.ID) {
			if c.IsChannel() {
				yout.UpdateChannel(c)
			} else if c.IsGrammar() {
				yout.UploadVideo(c, true, seriename, no, exname)
				yout.UploadThumbnail(c, seriename, no, exname)
			} else {
				yout.UploadVideo(c, true, seriename, no, exname)
			}
		}
		if typ == "thumbnail" && c.HasVideo() && (categoryID == "" || categoryID == c.ID) {
			yout.UploadThumbnail(c, seriename, no, exname)
		}
		if typ == "upload" && c.HasVideo() && (categoryID == "" || categoryID == c.ID) {
			yout.UploadVideo(c, false, seriename, no, exname)
			yout.UploadThumbnail(c, seriename, no, exname)
			yout.UploadCaption(c, seriename, no, exname)
		}
		if typ == "uploadplaylist" && c.HasVideo() && (categoryID == "" || categoryID == c.ID) {
			translation.MergeTxtTranslation(c)
			yout.MergeTxtPlaylists(c)
			yout.UploadPlaylist(c, true)
		}
		if typ == "playlist" && c.HasVideo() && (categoryID == "" || categoryID == c.ID) {
			translation.MergeTxtTranslation(c)
			yout.MergeTxtPlaylists(c)
			yout.UploadPlaylist(c, false)
		}
		if typ == "playlistitem" && c.HasVideo() && (categoryID == "" || categoryID == c.ID) {
			translation.MergeTxtTranslation(c)
			yout.MergeTxtPlaylists(c)
			yout.UploadPlaylistItem(c)
		}
		if typ == "caption" && c.HasVideo() && (categoryID == "" || categoryID == c.ID) {
			translation.MergeTxtTranslation(c)
			yout.UploadCaption(c, seriename, no, exname)
		}
	}
	if typ == "printtranslation" {
		fmt.Printf("printtranslation がおわりました。texts=%s\n", texts)
		ison.WriteFile(ison.ToFilepath(printtranslationfile), printtranslationBuffer.Bytes())
	}
}

func printExercises(c ison.Category, seriename string, no int, exname string) {
	var b bytes.Buffer
	for _, f := range c.JSONGrammarFiles() {
		var sentence yout.Sentence
		ison.ReadJSON(&sentence, f)
		if (no == 0 || sentence.No == no) && (seriename == "" || seriename == sentence.SerieName) {
			b.WriteString(fmt.Sprintf("%d\n", sentence.No))
			for _, e := range sentence.Exercises {
				if exname != "" && e.Name != exname {
					continue
				}
				b.WriteString(e.Problems.String())
			}
		}
	}
	fmt.Println(strings.ReplaceAll(b.String(), " ?", "?"))
}
