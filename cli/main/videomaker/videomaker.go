package main

import (
	"flag"
	"isonschool/cli/ison"
)

func main() {
	var masterfile string
	var categoryID string
	flag.StringVar(&masterfile, "m", "/Users/juny/go/src/isonschool/json/master.json", "")
	flag.StringVar(&categoryID, "c", "", "")
	flag.Parse()
	var master ison.Master
	ison.ReadJSON(&master, masterfile)
	for _, c := range master.Categories {
		if c.IsGrammar() && (categoryID == "" || categoryID == c.ID) {
			yout.MakeVideo(c)
		}
	}
}
