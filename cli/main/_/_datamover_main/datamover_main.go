package main

import (
	"log"
	"os"

	"github.com/dmulholl/mp3lib"
)

var videodatafile string = "/Users/juny/go/src/isonschool/videodata/videodata.json"

func main() {
	file := "/Users/juny/go/src/yossie/binary/grammar-ja/audio/grammar-ja-1/1/会議は午後7時15分に終わります.mp3"
	infile, err := os.Open(file)
	if err != nil {
		log.Panicf("mp3Length error open file:%s %v", file, err)
	}
	var totalFrames int
	var totalBytes int
	var totalCount int
	var isFirstFrame = true
	var firstBitRate int
	//var isVBR bool
	for {
		// Read the next frame from the input file.
		frame := mp3lib.NextFrame(infile)
		if frame == nil {
			break
		}

		// Skip the first frame if it's a VBR header.
		if isFirstFrame {
			isFirstFrame = false
			if mp3lib.IsXingHeader(frame) || mp3lib.IsVbriHeader(frame) {
				continue
			}
		}

		// If we detect more than one bitrate we'll need to add a VBR
		// header to the output file.
		if firstBitRate == 0 {
			firstBitRate = frame.BitRate
		} else if frame.BitRate != firstBitRate {
			//isVBR = true
		}

		totalFrames++
		totalBytes += len(frame.RawBytes)
		totalCount += frame.SampleCount
	}

}

/*
サンプリングレート（Hz）×　ビット深度（bit）＝　ビットレート（bps）
サンプリングレート=24000
ビットレート=32000
サンプルカウント576
フレーム長さ=96
   //     bytes_per_sample = (bit_rate / sampling_rate) / 8
    //     frame_length = sample_count * bytes_per_sample + padding
    //
    // In practice we need to rearrange this formula to avoid rounding errors.
    //
    // I can't find any definitive statement on whether this length is
    // supposed to include the 4-byte header and the optional 2-byte CRC.
    // Experimentation on mp3 files captured from the wild indicates that it
    // includes the header at least.
    frame.FrameLength =
        (frame.SampleCount / 8) * frame.BitRate / frame.SamplingRate + padding
*/
