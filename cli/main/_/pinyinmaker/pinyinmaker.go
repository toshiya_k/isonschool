package main

import (
	"bytes"
	"io/ioutil"
	"sort"
	"strings"

	"github.com/mozillazg/go-pinyin"
)

type pair struct {
	old string
	new string
}

func main() {
	var m = make(map[string]string)
	for r := range pinyin.PinyinDict {
		a1 := pinyin.NewArgs()
		a1.Heteronym = true
		a1.Style = pinyin.Tone
		p1 := pinyin.SinglePinyin(rune(r), a1)
		a2 := pinyin.NewArgs()
		a2.Heteronym = true
		a2.Style = pinyin.Tone3
		p2 := pinyin.SinglePinyin(rune(r), a2)
		for i := range p1 {
			m[p1[i]] = strings.ReplaceAll(p2[i], "v", "uu")
		}
	}
	var ret []pair
	for n, o := range m {
		ret = append(ret, pair{old: o, new: n})
	}
	sort.Slice(ret, func(i, j int) bool { return len(ret[i].old) > len(ret[j].old) })
	var b bytes.Buffer
	b.WriteString("package ruby\n\n")
	b.WriteString("var replaces = []replace{\n")
	for _, r := range ret {
		b.WriteString("\treplace{old: \"" + r.old + `_", new: "` + r.new + "\"},\n")
		if r.old == r.new {
			b.WriteString("\treplace{old: \"" + r.old + `5_", new: "` + r.new + "\"},\n")
		}
	}
	b.WriteString("}\n")
	ioutil.WriteFile("/Users/juny/go/src/isonschool/cli/ruby/kyteadic.go", b.Bytes(), 0644)
}
