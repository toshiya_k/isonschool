package main

import (
	"io/ioutil"
	"log"
	"os"
	"strconv"
)

func main() {
	dir := "/Users/juny/go/src/yossie/binary/grammar-ja/audio/grammar-ja-tk/"
	for i := 1; i <= 52; i++ {
		d := dir + strconv.Itoa(i) + "/"
		fs, err := ioutil.ReadDir(d)
		if err != nil {
			log.Panicf("%v", err)
		}
		for _, f := range fs {
			os.Rename(d+f.Name(), dir+f.Name())
		}
	}
}
