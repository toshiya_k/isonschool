package main

import (
	"flag"
	"isonschool/cli/ison"
	"isonschool/cli/yout"
)

func main() {
	var masterfile string
	var categoryID string
	flag.StringVar(&masterfile, "m", "/Users/juny/go/src/isonschool/json/master.json", "")
	flag.StringVar(&categoryID, "c", "grammar-en", "")
	flag.Parse()
	var master ison.Master
	ison.ReadJSON(&master, ison.ToFilepath(masterfile))
	for _, c := range master.Categories {
		if categoryID == "" || categoryID == c.ID {
			yout.MakeImages(c, "")
		}
	}
	/*
		palette, _ := colour.ToPalette("EFE0E0,3C9E99,966B70,98676B,302732")
		drawer := yout.NewDrawer(palette, "/Users/juny/Library/Fonts/NotoSansDisplay-SemiBold.ttf", "/Users/juny/Library/Fonts/NotoSansDisplay-SemiBold.ttf", "/Users/juny/Library/Fonts/NotoSansDisplay-Condensed.ttf", "/Users/juny/Library/Fonts/NotoSansDisplay-Condensed.ttf")
		//b := drawer.DrawTitle("English grammar exercises #10", "is/am/are")
		var master ison.Master
		ison.ReadJSON(&master)
		var serie serie
		ison.ReadFile()
		b := drawer.SetupData()
		ison.WriteFile("/Users/juny/go/src/yossie/test.png", b)
		//ftBinary := ison.ReadFile("/Users/juny/Library/Fonts/NotoSansDisplay-SemiBold.ttf")
		//ftBinary, err := ioutil.ReadFile("/Users/juny/Library/Fonts/NotoSansDisplay-Condensed.ttf")
	*/
}
