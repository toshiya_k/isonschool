package main

import (
	"bytes"
	"io/ioutil"
	"isonschool/cli/ison"
)

type url struct {
	page            string
	translationPage string
}

var baseURLs = map[string]string{
	"en":      "http://www.guidetojapanese.org/learn/grammar/",
	"de":      "http://www.guidetojapanese.org/german/",
	"es":      "http://www.guidetojapanese.org/spanish/",
	"fr":      "http://www.guidetojapanese.org/french/",
	"id":      "http://www.guidetojapanese.org/indonesian/",
	"it":      "http://www.guidetojapanese.org/italian/",
	"pl":      "http://www.guidetojapanese.org/polish/",
	"pt":      "http://www.guidetojapanese.org/portuguese/",
	"ru":      "http://vandal.sdf-eu.org/JapaneseGuide/",
	"fi":      "http://www.guidetojapanese.org/finnish/",
	"tr":      "http://www.guidetojapanese.org/turkish/",
	"ko":      "http://www.guidetojapanese.org/korean/index.html",
	"zh-Hans": "http://res.wokanxing.info/jpgramma/",
	"zh-Hant": "http://res.wokanxing.info/jpgramma/",
}

var pages = map[string]string{
	"stateofbeing":    "copula.html",
	"particlesintro":  "particles.html",
	"adjectives":      "adjectives.html",
	"verbs":           "verbs.html",
	"negativeverbs":   "negverb.html",
	"past_tense":      "pastverb.html",
	"verbparticles":   "particles2.html",
	"in-transitive":   "transtype.html",
	"clause":          "subclause.html",
	"nounparticles":   "particles3.html",
	"adverbs":         "adgobi.html",
	"polite":          "polite.html",
	"people":          "address.html",
	"question":        "question.html",
	"compound":        "compound.html",
	"teform":          "enduring.html",
	"potential":       "potential.html",
	"surunaru":        "surunaru.html",
	"conditionals":    "conditional.html",
	"must":            "haveto.html",
	"desire":          "desire.html",
	"actionclause":    "quotation.html",
	"define":          "define.html",
	"try":             "try.html",
	"favors":          "favor.html",
	"requests":        "requests.html",
	"numbers":         "numbers.html",
	"slang":           "casual.html",
	"sentence_ending": "wrapup4.html",
	"causepass":       "causepass.html",
	"honorific":       "honorhum.html",
	"unintended":      "unintended.html",
	"genericnouns":    "genericnoun.html",
	"certainty":       "certainty.html",
	"amount":          "amount.html",
	"similarity":      "similar.html",
	"comparison":      "compare.html",
	"easyhard":        "easyhard.html",
	"negativeverbs2":  "negverb2.html",
	"reasoning":       "reasoning.html",
	"timeactions":     "timeaction2.html",
	"nochange":        "nochange.html",
	"formal":          "formal.html",
	"should":          "should.html",
	"even":            "even.html",
	"signs":           "signs.html",
	"feasibility":     "feasibility.html",
	"tendency":        "tendency.html",
	"volitional2":     "adv_volitional.html",
	"covered":         "covered.html",
	"immediate":       "colose_actions.html",
	"other":           "othergrammar.html",
}

func main() {
	var b bytes.Buffer
	for page, trapage := range pages {
		if page == "" {
			continue
		}
		b.WriteString("[key]" + baseURLs["en"] + page + "\n")
		for _, lang := range ison.VideoLocalizationLangs {
			if base, ok := baseURLs[lang]; ok {
				b.WriteString("[lang:" + lang + "]" + base)
				if lang == "ko" {
					// do nothing
				} else if lang == "zh-Hans" || lang == "zh-Hant" {
					b.WriteString(page + ".html")
				} else if lang == "en" {
					b.WriteString(page)
				} else {
					b.WriteString(trapage)
				}
			} else {
				b.WriteString("[lang:" + lang + "]" + baseURLs["en"] + page)
			}
			b.WriteString("\n")
		}
		b.WriteString("---\n")
	}
	ioutil.WriteFile("/Users/juny/go/src/isonschool/json/grammar-ja/translation_txt/tkpagetranslation.txt", b.Bytes(), 0666)
}
