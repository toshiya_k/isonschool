package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"time"
)

func main() {
	var filenameFile string
	var videoFile string
	var createdVideoFolder string
	var duration time.Duration

	flag.StringVar(&videoFile, "v", "/Users/juny/Downloads/a", "")
	flag.StringVar(&filenameFile, "f", "/Users/juny/Downloads/b", "")
	flag.StringVar(&createdVideoFolder, "c", "/Users/juny/Downloads/createdvideo/", "")
	flag.DurationVar(&duration, "d", time.Second, "")
	flag.Parse()
	ticker := time.NewTicker(duration)
	for range ticker.C {
		filename, err := ioutil.ReadFile(filenameFile)
		if err != nil {
			log.Panicf("%v", err)
		}
		if len(filename) > 0 {
			os.Rename(videoFile, createdVideoFolder+string(filename))
			fmt.Printf("moved %s\n", filename)
			err := ioutil.WriteFile(filenameFile, []byte(""), 0644)
			if err != nil {
				log.Print(err)
			}
		}
	}
}
