package main

import (
	"bufio"
	"bytes"
	"io/ioutil"
	"log"
	"os"
	"strconv"
	"strings"
)

func main() {
	scanner := bufio.NewScanner(os.Stdin)
	scanner.Scan()
	text := scanner.Text()
	text = strings.ReplaceAll(text, "[Key]", "[key]")
	ss := strings.Split(text, "[key]")
	var b bytes.Buffer
	for n := 2; n <= 98; n++ {
		ns := strconv.Itoa(n)
		for _, s := range ss {
			s = strings.TrimSpace(s)
			if s == "" {
				continue
			}
			// if n == 3 {
			// 	b.WriteString(`[lang:ja]` + s)
			// 	b.WriteString("\t")

			// } else {
			b.WriteString(`=CONCATENATE(B` + ns + `,GOOGLETRANSLATE("` + s + `","en",A` + ns + `))`)
			b.WriteString("\t")
			// }
		}
		b.WriteString("\n")
	}
	ioutil.WriteFile("/Users/juny/go/src/yossie/googlespreadsheet.txt", b.Bytes(), 0666)
	log.Printf("/Users/juny/go/src/yossie/googlespreadsheet.txt")
}
