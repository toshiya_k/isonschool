package main

import (
	"bytes"
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
	"path/filepath"
	"strconv"
	"strings"
	"time"

	"github.com/mitchellh/go-homedir"
)

// Config is a configuration file
type Config struct {
	Files    []string
	Dirs     []string
	Commands []string
}

var modTime map[string]time.Time = make(map[string]time.Time)
var changedFile string

func main() {
	var configpath string
	flag.StringVar(&configpath, "c", "config.json", "Config file path")
	flag.Parse()
	args := flag.Args()
	var config Config
	b, err := ioutil.ReadFile(configpath)
	if err != nil {
		panic(err)
	}
	json.Unmarshal(b, &config)
	ticker := time.NewTicker(time.Second)

	for range ticker.C {
		var changed bool
		for _, f := range config.Files {
			f, err = homedir.Expand(f)
			if err != nil {
				panic(err)
			}
			f, err = filepath.Abs(f)
			if err != nil {
				panic(err)
			}
			name := filepath.Base(f)
			fn := func(g string) bool {
				return name == g
			}
			if strings.Contains(name, "*") && strings.Index(name, "*") == strings.LastIndex(name, "*") {
				pre := name[:strings.Index(name, "*")]
				suf := name[strings.Index(name, "*")+1:]

				fn = func(g string) bool {
					return strings.HasPrefix(g, pre) && strings.HasSuffix(g, suf) && len(g) >= len(pre)+len(suf)
				}
			}
			changed = checkDir(filepath.Dir(f), false, fn)
		}
		for _, dir := range config.Dirs {
			dir, err = homedir.Expand(dir)
			if err != nil {
				panic(err)
			}
			dir, err = filepath.Abs(dir)
			if err != nil {
				panic(err)
			}
			dir = strings.TrimSpace(dir)
			var deep bool
			if strings.HasSuffix(dir, "*") {
				deep = true
				dir = dir[:len(dir)-1]
			}
			changed = checkDir(dir, deep, nil)
		}

		if !changed {
			continue
		}

		var cmds [][]string
		for _, c := range config.Commands {
			var cmd []string
			for _, s := range strings.Split(strings.TrimSpace(c), " ") {
				if s == "" {
					continue
				}
				for i := len(args) - 1; i >= 0; i-- {
					s = strings.ReplaceAll(s, "$"+strconv.Itoa(i), args[i])
				}
				s = strings.ReplaceAll(s, "$FILE", changedFile)
				cmd = append(cmd, s)
			}
			cmds = append(cmds, cmd)
		}
		fmt.Printf("%s changed. running commands...\n", changedFile)
		for _, c := range cmds {
			fmt.Printf("%s\n", c)
			command := exec.Command(c[0], c[1:]...)
			command.Stdin = os.Stdin
			var bstdout bytes.Buffer
			command.Stdout = &bstdout
			var bstderr bytes.Buffer
			command.Stderr = &bstderr
			err = command.Start()
			if err != nil {
				fmt.Printf("start error: %v\n", err)
				break
			}
			err = command.Wait()
			if bstderr.Len() > 0 {
				fmt.Printf("stderr:\n%s\n", bstderr.Bytes())
				break
			}
			if err != nil {
				fmt.Printf("wait error: %v\n", err)
				break
			}
			fmt.Printf("%s\n", bstdout.Bytes())
		}
		fmt.Printf("--- end ---\n")
	}
}

func checkDir(dir string, deep bool, fn func(string) bool) (changed bool) {
	fis, err := ioutil.ReadDir(dir)
	if err != nil {
		panic(err)
	}
	for _, fi := range fis {
		if fi.IsDir() {
			if deep && checkDir(filepath.Join(dir, fi.Name()), deep, fn) {
				changed = true
			}
		}
		if fn != nil && !fn(fi.Name()) {
			continue
		}
		t := fi.ModTime()
		f := filepath.Join(dir, fi.Name())
		if t != modTime[f] {
			changed = true
			changedFile = f
		}
		modTime[f] = t
	}
	return
}

// ModTime returns modTime of file
func ModTime(file string) time.Time {
	f, err := os.Open(file)
	if err != nil {
		panic(err)
	}
	info, err := f.Stat()
	if err != nil {
		panic(err)
	}
	return info.ModTime()
}
