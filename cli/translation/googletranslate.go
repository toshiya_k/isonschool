package translation

import (
	"bytes"
	"context"
	"fmt"
	"io/ioutil"
	"isonschool/cli/ison"
	"log"
	"strings"

	translate "cloud.google.com/go/translate/apiv3"
	translatepb "google.golang.org/genproto/googleapis/cloud/translate/v3"
)

func translateText(text string, sourceLang ison.Lang, targetLang ison.Lang) string {
	ctx := context.Background()

	log.Printf("%s %s", targetLang.String(), text)

	client, err := translate.NewTranslationClient(ctx)
	if err != nil {
		log.Panicf("%v", err)
	}
	defer client.Close()

	req := &translatepb.TranslateTextRequest{
		Parent:             "projects/619016746727/locations/global",
		Contents:           []string{text},
		SourceLanguageCode: sourceLang.String(),
		TargetLanguageCode: targetLang.String(),
		Model:              "projects/619016746727/locations/global/models/general/nmt",
	}
	resp, err := client.TranslateText(ctx, req)
	if err != nil {
		log.Panicf("Translate: %v", err)
	}
	return resp.Translations[0].TranslatedText
}

// GoogleTranslateToEn は，Google Translate API を使って翻訳します。
func GoogleTranslateToEn(sourceTexts []string, sourceLang ison.Lang, file ison.Filepath) []string {
	var ret []string
	log.Printf("%+v", sourceTexts)
	for _, st := range sourceTexts {
		t := translateText(st, sourceLang, ison.LangEn)
		ret = append(ret, t)
		var b bytes.Buffer
		f, _ := ioutil.ReadFile(file.String())
		b.Write(f)
		b.WriteString("[key]" + st + "\n")
		s := strings.ReplaceAll(t, "&#39;", "'")
		b.WriteString("[lang:en]" + s + "\n")
		b.WriteString("---\n\n")
		ioutil.WriteFile(file.String(), b.Bytes(), 0644)
		fmt.Printf("%s\n", file.String())
	}
	return ret
}

// GoogleTranslateFromEnToAll は，Google Translate API を使って翻訳します。
func GoogleTranslateFromEnToAll(sourceTexts []string, enTexts []string, sourceLang ison.Lang, file ison.Filepath) {
	var langs []ison.Lang
	for _, l := range ison.VideoLocalizationLangsExceptEn {
		if l != sourceLang {
			langs = append(langs, l)
		}
	}
	log.Printf("%+v", enTexts)
	for i, en := range enTexts {
		var t []string = make([]string, len(langs))
		for i, lang := range langs {
			t[i] = translateText(en, ison.LangEn, lang)
		}
		var b bytes.Buffer
		f, _ := ioutil.ReadFile(file.String())
		b.Write(f)
		b.WriteString("[key]" + sourceTexts[i] + "\n")
		b.WriteString("[lang:" + sourceLang.String() + "]" + sourceTexts[i] + "\n")
		b.WriteString("[lang:en]" + en + "\n")
		for i, lang := range langs {
			s := strings.ReplaceAll(t[i], "&#39;", "'")
			b.WriteString("[lang:" + lang.String() + "]" + s + "\n")
		}
		b.WriteString("---\n\n")
		ioutil.WriteFile(file.String(), b.Bytes(), 0644)
		fmt.Printf("%s\n", file.String())
	}
}
