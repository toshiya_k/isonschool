package translation

import (
	"isonschool/cli/ison"
)

// MergeTxtTranslation は，マージします。
func MergeTxtTranslation(category ison.Category) {
	var txts []TxtTranslation
	for _, file := range category.TxtTranslationFiles() {
		m := ison.NewTxtMarshallerFromFile(file)
		txts = append(txts, ParseTxtTranslation(m)...)
	}
	f := category.JSONTranslationFile()
	var data TraData
	ison.ReadJSON(&data, f)
	for _, txt := range txts {
		for lang, target := range txt.Target {
			data.Update(txt.Key, lang, target)
		}
	}
	ison.WriteJSON(&data, f)
}

// ParseTxtTranslation は，パースします。
func ParseTxtTranslation(m *ison.TxtMarshaller) []TxtTranslation {
	m.Add("key", func(d interface{}, k, v string) {
		d.(*TxtTranslation).Key = v
	})
	m.Add("lang", func(d interface{}, k, v string) {
		d.(*TxtTranslation).Target[ison.Lang(k)] = v
	})
	var txts []TxtTranslation
	var success, isEOF bool
	for !isEOF {
		var d = TxtTranslation{Target: make(map[ison.Lang]string)}
		success, isEOF = m.Unmarshal(&d)
		if success {
			txts = append(txts, d)
		}
	}
	return txts
}

// TxtTranslation は，テキストの内容です。
type TxtTranslation struct {
	Key    string
	Target map[ison.Lang]string
}
