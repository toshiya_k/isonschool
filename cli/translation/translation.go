package translation

import (
	"bytes"
	"fmt"
	"io"
	"isonschool/cli/ison"
	"log"
	"sort"
	"strconv"
	"strings"
)

// Translator は，翻訳するために使います。
type Translator struct {
	tra                  *TraData
	category             ison.Category
	needsTranslationKeys []string
}

// HasError は，翻訳エラーがあるかどうかをかえします。
func (t *Translator) HasError() bool {
	return len(t.needsTranslationKeys) > 0
}

// CheckNeedsTranslation は，翻訳が必要かチェックします。
// 翻訳が必要な場合，key をファイルに出力してからパニックします。
func (t *Translator) CheckNeedsTranslation() {
	if len(t.needsTranslationKeys) == 0 {
		return
	}
	sort.Strings(t.needsTranslationKeys)
	var ens []string
	if t.category.AudioLang.IsEnglish() {
		ens = t.needsTranslationKeys
		GoogleTranslateFromEnToAll(t.needsTranslationKeys, t.needsTranslationKeys, t.category.AudioLang, t.category.TxtMachineTranslationFile())
	} else {
		ens = GoogleTranslateToEn(t.needsTranslationKeys, t.category.AudioLang, t.category.BinaryEnMachineTranslationFile())
	}
	var b bytes.Buffer
	for _, key := range t.needsTranslationKeys {
		b.WriteString("[key]" + key + "\t")
	}
	b.WriteString("\n")
	for _, en := range ens {
		b.WriteString("[lang:en]" + en + "\t")
	}
	b.WriteString("\n")
	for _, l := range ison.VideoLocalizationLangsExceptEn {
		for _, en := range ens {
			b.WriteString(fmt.Sprintf("=CONCATENATE(\"[lang:%s]\",GOOGLETRANSLATE(\"%s\",\"en\",\"%s\"))\t", l, en, l))
		}
		b.WriteString("\n")
	}
	file := t.category.BinaryNeedsTranslationFile()
	ison.WriteFile(file, b.Bytes())
	log.Panicf("翻訳が足りません。終了します。 %s", file)
}

// NewTranslator は，新しい Translator を返します。
func NewTranslator(category ison.Category) *Translator {
	var tra TraData
	ison.ReadJSON(&tra, category.JSONTranslationFile())
	return &Translator{tra: &tra, category: category}
}

// Translate は，{key} の部分を翻訳に置き換えた文字列を返します。
func (t *Translator) Translate(text string, lang ison.Lang) string {
	var ret string
	for {
		i := strings.Index(text, "{")
		if i < 0 {
			return ret + text
		}
		ret += text[:i]
		text = text[i+1:]
		j := strings.Index(text, "}")
		key := text[:j]
		text = text[j+1:]
		langNum := 1
		sep := " "
		if strings.Contains(key, "|") {
			langn := key[strings.Index(key, "|")+1 : strings.LastIndex(key, "|")]
			if langn == "ALL" {
				langNum = len(ison.VideoLocalizationLangs)
			} else {
				n, err := strconv.Atoi(langn)
				if err != nil {
					log.Panicf("%v %s", err, key)
				}
				langNum = n
			}
			sep = key[strings.LastIndex(key, "|")+1:]
			key = key[:strings.Index(key, "|")]
		}
		var ss = make([]string, langNum)
		for k := 0; k < langNum; k++ {
			var lInd int
			for l, la := range ison.VideoLocalizationLangs {
				if la == lang {
					lInd = l
					break
				}
			}
			lan := ison.VideoLocalizationLangs[(lInd+k*(len(ison.VideoLocalizationLangs)/langNum))%len(ison.VideoLocalizationLangs)]
			ss[k] = t.translateKey(key, lan)
		}
		ret += strings.Join(ss, sep)
		//ret += t.translateKey(key, lang)
	}
}

// Translate は，翻訳します。
func (t *Translator) translateKey(key string, lang ison.Lang) string {
	if key == "" {
		log.Panicf("key が空文字です。")
		return ""
	}
	ret := t.translateKeyInner(key, lang)
	if ret != "" {
		return ret
	}
	if ison.FirstLetter(key) == strings.ToTitle(ison.FirstLetter(key)) {
		ret = t.translateKeyInner(ison.FirstLetterToLower(key), lang)
		if ret != "" {
			return ison.FirstLetterToTitle(ret)
		}
	}
	if key == strings.ToTitle(key) {
		ret = t.translateKeyInner(strings.ToLower(key), lang)
		if ret != "" {
			return strings.ToTitle(ret)
		}
	}
	//log.Printf("%s の翻訳がありません %s\n", key, lang)
	t.appendNeedsTranslationKey(key)
	return ""
}

func (t *Translator) appendNeedsTranslationKey(key string) {
	for _, k := range t.needsTranslationKeys {
		if k == key {
			return
		}
	}
	t.needsTranslationKeys = append(t.needsTranslationKeys, key)
}

func (t *Translator) hasKey(key string) bool {
	for _, s := range t.tra.Sources {
		if s.Key == key {
			return true
		}
	}
	return false
}

func (t *Translator) translateKeyInner(key string, lang ison.Lang) string {
	for _, s := range t.tra.Sources {
		if s.Key == key {
			for _, t := range s.Targets {
				if t.Lang == lang {
					return t.Text
				}
			}
		}
	}
	return ""
}

// TraData は，翻訳済みデータです。
type TraData struct {
	Sources []Source
}

// Sort は，ソートします。
func (d *TraData) Sort() {
	sort.Sort(SourcesSort(d.Sources))
	for si := range d.Sources {
		sort.Sort(TargetsSort(d.Sources[si].Targets))
	}
}

// Update は，翻訳データをアップデートします。
// 機械翻訳が人間翻訳を上書きすることはありません。
func (d *TraData) Update(key string, lang ison.Lang, text string) {
	var si = -1
	for i, s := range d.Sources {
		if s.Key == key {
			si = i
			break
		}
	}
	if si < 0 {
		d.Sources = append(d.Sources, Source{Key: key})
		si = len(d.Sources) - 1
	}
	var ti = -1
	for i, t := range d.Sources[si].Targets {
		if t.Lang == lang {
			ti = i
			break
		}
	}
	if ti < 0 {
		d.Sources[si].Targets = append(d.Sources[si].Targets, Target{Lang: lang, Text: text})
		ti = len(d.Sources[si].Targets) - 1
	}
	if d.Sources[si].Targets[ti].Text != text {
		d.Sources[si].Targets[ti].Text = text
	}
}

// Source は，原文です。
type Source struct {
	Key     string
	Targets []Target
}

// SourcesSort は，ソート用の型です。
type SourcesSort []Source

func (s SourcesSort) Len() int      { return len(s) }
func (s SourcesSort) Swap(i, j int) { s[i], s[j] = s[j], s[i] }
func (s SourcesSort) Less(i, j int) bool {
	if s[i].Key < s[j].Key {
		return true
	}
	return false
}

// Target は，訳文です。
type Target struct {
	Lang ison.Lang
	Text string
}

// TargetsSort は，ソート用の型です。
type TargetsSort []Target

func (s TargetsSort) Len() int      { return len(s) }
func (s TargetsSort) Swap(i, j int) { s[i], s[j] = s[j], s[i] }
func (s TargetsSort) Less(i, j int) bool {
	if s[i].Lang < s[j].Lang {
		return true
	} else if s[i].Lang > s[j].Lang {
		return false
	} else if s[i].Text < s[j].Text {
		return true
	}
	return false
}

// PrintTranslation は，翻訳結果をファイルに書き出します。
func PrintTranslation(category ison.Category, keys []string, writesCategoryID bool, writer io.Writer) {
	translator := NewTranslator(category)
	for _, key := range keys {
		key = strings.TrimSpace(key)
		if key == "" {
			continue
		}
		if !translator.hasKey(key) {
			continue
		}
		if writesCategoryID {
			writer.Write([]byte("category:" + category.ID + "\n"))
		}
		writer.Write([]byte("[key]" + key + "\n"))
		for _, lang := range ison.VideoLocalizationLangs {
			s := translator.translateKey(key, lang)
			writer.Write([]byte("[lang:" + lang.String() + "]" + s + "\n"))
		}
		writer.Write([]byte("---\n"))
	}
}
