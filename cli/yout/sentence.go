package yout

import (
	"bytes"
	"fmt"
	"isonschool/cli/ison"
	"log"
	"sort"
	"strconv"
	"strings"
)

// Sentence は，問題です。
type Sentence struct {
	SerieName          string
	No                 int
	VideoTitle         string
	ThumbnailTitle     string // Use Thumbnail to retrieve thumbnail string.
	YoutubeDescription string
	YoutubeTitle       string
	Boxes              []Box
	Pars               []Par
	Exercises          Exercises
	CannotChange       bool
}

// AddExercise appends new exercise if there is no such exercise.
func (s *Sentence) AddExercise(exerciseName string) {
	for _, a := range s.Exercises {
		if a.Name == exerciseName {
			return
		}
	}
	s.Exercises = append(s.Exercises, Exercise{Name: exerciseName})
}

// Thumbnail returns a thumbnail string
func (s *Sentence) Thumbnail() string {
	if s.ThumbnailTitle != "" {
		return s.ThumbnailTitle
	}
	return s.VideoTitle
}

// Header は，Par の前に何を描画するかを示します。
type Header string

// Header の種類です。
const (
	HeaderNone   Header = ""
	HeaderPerson Header = "P"
)

// ToHeader は，Header に変換します。
func ToHeader(s string) Header {
	if s == string(HeaderNone) || s == string(HeaderPerson) {
		return Header(s)
	}
	log.Panicf("不正なHeaderです。%s", s)
	return ""
}

func (h Header) String() string {
	if h == HeaderNone || h == HeaderPerson {
		return string(h)
	}
	log.Panicf("不正なHeaderです。%s", string(h))
	return ""
}

// Par は，段落です。
type Par struct {
	Header Header
	Poses  []int
}

// Sort は，ソートします。
func (s *Sentence) Sort() {
	sort.Sort(s.Exercises)
}

// Exercise は，Sentenceから生成された問題集です。
type Exercise struct {
	Name             string
	Problems         Problems
	VideoFile        ison.Filename // ビデオを生成した後，そのファイル名をここにいれます。空文字は，ビデオがまだ作られていないことを意味します。
	AnimeticCaptions []AnimeticCaption
}

// CaptionKeyName は，キャプションです。
type CaptionKeyName struct {
	CaptionKey  string
	CaptionName string
}

// CaptionKey の種類です。
const (
	CaptionKeyRuby        = "ruby"
	CaptionKeyTranslation = "translation"
)

// Exercises は，Name でソートします。
type Exercises []Exercise

func (e Exercises) Len() int           { return len(e) }
func (e Exercises) Swap(i, j int)      { e[i], e[j] = e[j], e[i] }
func (e Exercises) Less(i, j int) bool { return e[i].Name < e[j].Name }

// Voice は，text-to-speech で用いる音声名を保持します。
type Voice struct {
	PersonName string
	VoiceKeys  [3]VoiceKey
}

// VoiceKey は，音声キーです。
type VoiceKey string

// Voices は，PersonName でソートします。
type Voices []Voice

func (v Voices) Len() int           { return len(v) }
func (v Voices) Swap(i, j int)      { v[i], v[j] = v[j], v[i] }
func (v Voices) Less(i, j int) bool { return v[i].PersonName < v[j].PersonName }

// Equals は，等しいかどうかを表します。
func (v Voices) Equals(w Voices) bool {
	return fmt.Sprintf("%+v", v) == fmt.Sprintf("%+v", w)
}

// Gender は性別を表します。
type Gender int

func (g Gender) String() string {
	if g == GenderNeutral {
		return "Neutral"
	} else if g == GenderMale {
		return "Male"
	} else if g == GenderFemale {
		return "Female"
	}
	log.Panicf("不正なGenderです。%d", int(g))
	return ""
}

// ToGender は，文字列を Gender に変更します。
func ToGender(s string) Gender {
	if s == "" || s == "N" {
		return GenderNeutral
	} else if s == "M" {
		return GenderMale
	} else if s == "F" {
		return GenderFemale
	}
	log.Panicf("Genderが不正です。%s", s)
	return GenderNeutral
}

// Gender には3種類あります。
const (
	GenderNeutral = Gender(0)
	GenderMale    = Gender(1)
	GenderFemale  = Gender(2)
)

// Problems は Problem のスライスです。
type Problems []Problem

func (p Problems) String() string {
	var b bytes.Buffer
	for _, problem := range p {
		b.WriteString(problem.String() + "\n")
	}
	return b.String()
}

// Box は，Sentencを構成する部分です。
type Box struct {
	Pos        int
	PersonName string
	Candidates []Candidate
}

// Candidate は，単語に入る候補です。
type Candidate struct {
	Word             string
	Root             string
	FeatureSet       FeatureSet
	Conditions       []Condition
	PersonConditions []PersonCondition
}

// FeatureSet は，Feature の集まりです。
type FeatureSet []string

// Merge は，2つのFeatureSetを重複がないように合わせます。
func (f FeatureSet) Merge(g FeatureSet) FeatureSet {
	var ret FeatureSet
	ret = append(ret, f...)
	for _, s := range g {
		if !ison.Contains(ret, s) {
			ret = append(ret, s)
		}
	}
	return ret
}

// Count は，feature の数を返します。
func (f FeatureSet) Count() int {
	return len([]string(f))
}

// Matches は，ほかのFeatureSet に自分自身が含まれているかチェックします。
func (f FeatureSet) Matches(g FeatureSet) bool {
	for _, s := range f {
		if !ison.Contains(g, s) {
			return false
		}
	}
	return true
}

// MatchesExcept は，ほかのFeatureSet に自分自身が含まれているかチェックします。
func (f FeatureSet) MatchesExcept(g FeatureSet, h FeatureSet) bool {
	for _, s := range f {
		if !ison.Contains(g, s) && !ison.Contains(h, s) {
			return false
		}
	}
	return true
}

// DiffCount は，ほかのFeatureSet との違いを返します。
// feature数がすくない方から考えて，いくつ違いがあるかカウントします。
func (f FeatureSet) DiffCount(g FeatureSet) int {
	a, b := f, g
	if f.Count() > g.Count() {
		a, b = g, f
	}
	var diff int
	for _, s := range a {
		if !ison.Contains(b, s) {
			diff++
		}
	}
	return diff
}

// ToFeatureSet は文字列から FeatureSet を作成します。
func ToFeatureSet(s string) FeatureSet {
	ss := strings.Split(s, ",")
	var ws []string
	for _, t := range ss {
		t = strings.TrimSpace(t)
		if t == "" {
			continue
		}
		ws = append(ws, t)
	}
	return FeatureSet(ws)
}

// CandidateWord は，Problem の中で使います。
type CandidateWord struct {
	Word string
	Ruby string
}

// Condition は，満たすべき条件です。
type Condition struct {
	Pos       int
	Morphemes []Morpheme
}

// PersonCondition は，人物の条件です。
type PersonCondition struct {
	PersonName string
	Gender     Gender
}

// Morpheme は形態素を表します。
type Morpheme struct {
	Word       string
	Root       string
	FeatureSet FeatureSet
}

// Update は，テキストデータからアップデートします。
func (s *Sentence) Update(category ison.Category, t TxtSentence) {
	if s.SerieName != "" && s.SerieName != t.SerieName {
		log.Fatalf("SerieNameがちがいます。 %s %s", s.SerieName, t.SerieName)
	}
	if s.No != 0 && s.No != t.No {
		log.Fatalf("No がちがいます。 %d %d", s.No, t.No)
	}
	s.SerieName = t.SerieName
	s.No = t.No
	boxes := t.Boxes()
	if boxChanged(s.Boxes, boxes) {
		if s.CannotChange {
			if ison.CheckYes("Boxに変更が加えられました。が変更不可です。無理に変えますか？y/n %s %d\n", s.SerieName, s.No) {
				s.Boxes = boxes
			}
		} else {
			s.Boxes = boxes
			for i := range s.Exercises {
				s.Exercises[i].Problems = nil
				s.Exercises[i].VideoFile = ""
			}
		}
	}
	if fmt.Sprintf("%+v", s.Pars) != fmt.Sprintf("%+v", t.Pars()) {
		if s.CannotChange {
			if len(s.Pars) == 0 || ison.CheckYes("Parsに変更が加えられました。が変更不可です。無理に変えますか？y/n %s %d\n", s.SerieName, s.No) {
				s.Pars = t.Pars()
			}
		} else {
			s.Pars = t.Pars()
			for i := range s.Exercises {
				s.Exercises[i].Problems = nil
				s.Exercises[i].VideoFile = ""
			}
		}
	}
	if s.VideoTitle != t.VideoTitle {
		if s.CannotChange {
			if ison.CheckYes("VideoTitleに変更が加えられました。が，もうすでに変更不可になっています。無理に変えますか？y/n %s %d %s %s \n", s.SerieName, s.No, s.VideoTitle, t.VideoTitle) {
				s.VideoTitle = t.VideoTitle
			}
		} else {
			s.VideoTitle = t.VideoTitle
			for i := range s.Exercises {
				s.Exercises[i].VideoFile = ""
			}
		}
	}
	if s.Thumbnail() != t.ThumbnailTitle {
		s.ThumbnailTitle = t.ThumbnailTitle
	}
	s.YoutubeDescription = t.YoutubeDescription
	s.YoutubeTitle = t.YoutubeTitle
}

func boxChanged(oldBoxes, newBoxes []Box) bool {
	oldStr := fmt.Sprintf("%+v", oldBoxes)
	newStr := fmt.Sprintf("%+v", newBoxes)
	return oldStr != newStr
}

// Problem は，問題です。
type Problem struct {
	ProblemNo          int
	OriginalCandidates []CandidateWord
	ChangeCandidatePos int
	ChangeCandidates   []CandidateWord
	ChangeMorphemes    []Morpheme
	AnswerCandidates   []CandidateWord
}

// GenderMap は，性別を返します。
func (s *Sentence) GenderMap(candidates []CandidateWord) map[string]Gender {
	genderMap := make(map[string]Gender)
	for i, b := range s.Boxes {
		for _, c := range b.Candidates {
			if c.Word == candidates[i].Word {
				for _, pc := range c.PersonConditions {
					if genderMap[pc.PersonName] != GenderNeutral && genderMap[pc.PersonName] != pc.Gender {
						log.Panicf("Genderがいっちしません。%+v personName:%s", s, pc.PersonName)
					}
					genderMap[pc.PersonName] = pc.Gender
				}
			}
		}
	}
	return genderMap
}

// OriginalRuby は，問題のもとの文です。
func (p *Problem) OriginalRuby() string {
	var sb bytes.Buffer
	for _, c := range p.OriginalCandidates {
		if sb.Len() > 0 {
			sb.WriteString(" ")
		}
		sb.WriteString(c.Ruby)
	}
	return sb.String()
}

// Line は，セリフです。
type Line struct {
	PersonName string
	Gender     Gender
	Text       string
	Ruby       string
}

func lines(candidates []CandidateWord, category ison.Category, serie Serie, sentence Sentence) []Line {
	hasSpace := serie.AudioLang.HasSpace()
	var ret []Line
	var sb bytes.Buffer
	var sbr bytes.Buffer
	var pName string
	genderMap := sentence.GenderMap(candidates)
	for i, c := range candidates {
		if sentence.Boxes[i].PersonName != pName {
			if sb.Len() > 0 {
				ret = append(ret, Line{
					PersonName: pName,
					Gender:     genderMap[pName],
					Text:       sb.String(),
					Ruby:       sbr.String(),
				})
			}
			pName = sentence.Boxes[i].PersonName
			sb.Reset()
			sbr.Reset()
		}
		if hasSpace && sb.Len() > 0 {
			sb.WriteString(" ")
		}
		if sbr.Len() > 0 {
			sbr.WriteString(" ")
		}
		sb.WriteString(c.Word)
		sbr.WriteString(c.Ruby)
	}
	if sb.Len() > 0 {
		ret = append(ret, Line{
			PersonName: pName,
			Gender:     genderMap[pName],
			Text:       sb.String(),
			Ruby:       sbr.String(),
		})
	}
	return ret
}

// OriginalLines は，問題のもとの文です。
func (p *Problem) OriginalLines(category ison.Category, serie Serie, sentence Sentence) []Line {
	return lines(p.OriginalCandidates, category, serie, sentence)
}

// AnswerLines は，解答文です。
func (p *Problem) AnswerLines(category ison.Category, serie Serie, sentence Sentence) []Line {
	return lines(p.AnswerCandidates, category, serie, sentence)
}

// ChangeLines は，変更部分です。
func (p *Problem) ChangeLines(category ison.Category, sentence Sentence) []Line {
	var ret []Line
	genderMap := sentence.GenderMap(p.AnswerCandidates)
	for i, c := range p.ChangeCandidates {
		if c.Word != "" {
			ret = append(ret, Line{
				PersonName: sentence.Boxes[i].PersonName,
				Gender:     genderMap[sentence.Boxes[i].PersonName],
				Text:       c.Word,
				Ruby:       c.Ruby,
			})
		}
	}
	return ret
}

// ChangeLine は，変更部分です。
func (p *Problem) ChangeLine(category ison.Category, sentence Sentence) Line {
	pn := sentence.Boxes[p.ChangeCandidatePos].PersonName
	c := p.ChangeCandidates[p.ChangeCandidatePos]
	genderMap := sentence.GenderMap(p.AnswerCandidates)
	return Line{
		PersonName: pn,
		Gender:     genderMap[pn],
		Text:       c.Word,
		Ruby:       c.Ruby,
	}
}

func (p Problem) String() string {
	var b bytes.Buffer
	for _, o := range p.OriginalCandidates {
		b.WriteString(o.Word + " ")
	}
	b.WriteString("\n")
	for _, o := range p.AnswerCandidates {
		b.WriteString(o.Word + " ")
	}
	b.WriteString("\n")
	return b.String()
}

// TxtSentence は，Sentence のテキストデータです。
type TxtSentence struct {
	SerieName          string
	No                 int
	VideoTitle         string
	TxtBoxes           []TxtBox
	pars               []string
	YoutubeDescription string
	YoutubeTitle       string
	ThumbnailTitle     string
}

// TxtBox は，Box 行のデータです。
type TxtBox struct {
	PosStr     string
	PersonName string
	Value      string
}

// Pars は，pars を解析して，[]Parを返します。
func (t *TxtSentence) Pars() []Par {
	var allPoses []int
	var posMap = make(map[string]int)
	for i, b := range t.TxtBoxes {
		posMap[b.PosStr] = i
		allPoses = append(allPoses, i)
	}
	if len(t.pars) == 0 {
		return []Par{Par{
			Header: HeaderNone,
			Poses:  allPoses,
		}}
	}
	var ret = make([]Par, len(t.pars))
	for i, p := range t.pars {
		var par Par
		if strings.Contains(p, ":") {
			par.Header = ToHeader(p[:strings.Index(p, ":")])
			p = p[strings.Index(p, ":")+1:]
		}
		for _, s := range strings.Split(p, ",") {
			par.Poses = append(par.Poses, posMap[strings.TrimSpace(s)])
		}
		ret[i] = par
	}
	return ret
}

// Boxes は，[]TxtBoxから[]Boxを作ります。
func (t *TxtSentence) Boxes() []Box {
	var posMap = make(map[string]int)
	var personNameMap = make(map[string]bool)
	for i, b := range t.TxtBoxes {
		posMap[b.PosStr] = i
		personNameMap[b.PersonName] = true
	}
	var boxes = make([]Box, len(t.TxtBoxes))
	for bi, b := range t.TxtBoxes {
		var candidates = []Candidate{}
		candidateStrs := strings.Split(b.Value, ";")
		for _, s := range candidateStrs {
			s = strings.TrimSpace(s)
			if s == "" {
				continue
			}
			var candidate Candidate
			if strings.IndexAny(s, "({[") < 0 {
				candidate.Word = s
			} else {
				candidate.Word = strings.TrimSpace(s[:strings.IndexAny(s, "({[")])
			}
			ind := strings.Index(s, "{")
			if strings.Index(s, "(") >= 0 && (ind < 0 || strings.Index(s, "(") < ind) {
				candidate.Root = strings.TrimSpace(s[strings.Index(s, "(")+1 : strings.Index(s, ")")])
			}
			if strings.Index(s, "[") >= 0 && (ind < 0 || strings.Index(s, "[") < ind) {
				candidate.FeatureSet = ToFeatureSet(s[strings.Index(s, "[")+1 : strings.Index(s, "]")])
			}
			for ind >= 0 {
				closingInd := strings.Index(s, "}")
				personName := ""
				genderStr := s[ind+1 : closingInd]
				if strings.Contains(s[:closingInd], ":") {
					personName = s[ind+1 : strings.Index(s, ":")]
					genderStr = s[strings.Index(s, ":")+1 : closingInd]
				}
				if personNameMap[personName] {
					candidate.PersonConditions = append(candidate.PersonConditions, PersonCondition{PersonName: personName, Gender: ToGender(genderStr)})
				} else {
					var condition Condition
					posStr := s[ind+1 : strings.Index(s, ":")]
					pos, ok := posMap[posStr]
					if !ok {
						log.Panicf("pos str が間違っています。%s, %s", posStr, b.Value)
					}
					condition.Pos = pos
					parts := strings.Split(s[strings.Index(s, ":")+1:closingInd], "|")
					for _, s := range parts {
						word := strings.TrimSpace(s)
						root := ""
						feat := ""
						if strings.IndexAny(s, "[(") >= 0 {
							word = strings.TrimSpace(s[:strings.IndexAny(s, "[(")])
						}
						if strings.Contains(s, "[") {
							feat = strings.TrimSpace(s[strings.Index(s, "[")+1 : strings.Index(s, "]")])
						}
						if strings.Contains(s, "(") {
							root = strings.TrimSpace(s[strings.Index(s, "(")+1 : strings.Index(s, ")")])
						}
						condition.Morphemes = append(condition.Morphemes, Morpheme{
							Word:       word,
							Root:       root,
							FeatureSet: ToFeatureSet(feat),
						})
					}
					candidate.Conditions = append(candidate.Conditions, condition)
				}
				s = s[closingInd+1:]
				ind = strings.Index(s, "{")
			}
			candidates = append(candidates, candidate)
		}
		boxes[bi] = Box{
			Pos:        bi,
			PersonName: b.PersonName,
			Candidates: candidates,
		}
	}
	return boxes
}

// ParseTxtSentence は，パースします。
func ParseTxtSentence(m *ison.TxtMarshaller) []TxtSentence {
	m.Add("seriename", func(d interface{}, k, v string) {
		d.(*TxtSentence).SerieName = v
	})
	m.Add("no", func(d interface{}, k, v string) {
		no, err := strconv.Atoi(v)
		if err != nil {
			log.Fatal(err)
		}
		d.(*TxtSentence).No = no
	})
	m.Add("videotitle", func(d interface{}, k, v string) {
		d.(*TxtSentence).VideoTitle = v
	})
	m.Add("thumbnailtitle", func(d interface{}, k, v string) {
		d.(*TxtSentence).ThumbnailTitle = v
	})
	m.Add("description", func(d interface{}, k, v string) {
		d.(*TxtSentence).YoutubeDescription = v
	})
	m.Add("title", func(d interface{}, k, v string) {
		d.(*TxtSentence).YoutubeTitle = v
	})
	m.Add("box", func(d interface{}, k, v string) {
		posstr := k
		personname := ""
		ind := strings.Index(k, ":")
		if ind >= 0 {
			posstr = k[:ind]
			personname = k[ind+1:]
		}
		d.(*TxtSentence).TxtBoxes = append(d.(*TxtSentence).TxtBoxes, TxtBox{
			PosStr:     posstr,
			PersonName: personname,
			Value:      v,
		})
	})
	m.Add("par", func(d interface{}, k, v string) {
		d.(*TxtSentence).pars = append(d.(*TxtSentence).pars, v)
	})
	var sentences []TxtSentence
	var success, isEOF bool
	for !isEOF {
		var d = TxtSentence{}
		success, isEOF = m.Unmarshal(&d)
		if success {
			sentences = append(sentences, d)
		}
	}
	return sentences
}

// MergeTxtGrammarFiles は，新しいビデオのデータをマージします。
func MergeTxtGrammarFiles(category ison.Category, condSerieName string, condNo int) {
	var txtSentences []TxtSentence
	for _, file := range category.TxtGrammarFiles() {
		m := ison.NewTxtMarshallerFromFile(file)
		txtSentences = append(txtSentences, ParseTxtSentence(m)...)
	}
	m := make(map[string]bool)
	for _, txtSentence := range txtSentences {
		if condSerieName != "" && condSerieName != txtSentence.SerieName {
			continue
		}
		if condNo > 0 && condNo != txtSentence.No {
			continue
		}
		if m[txtSentence.SerieName+"_"+strconv.Itoa(txtSentence.No)] {
			log.Panicf("%s_%d が重複して定義されています。", txtSentence.SerieName, txtSentence.No)
		}
		m[txtSentence.SerieName+"_"+strconv.Itoa(txtSentence.No)] = true
		f := category.JSONGrammarFile(txtSentence.SerieName, txtSentence.No)
		var v Sentence
		ison.ReadJSON(&v, f)
		v.Update(category, txtSentence)
		ison.WriteJSON(&v, f)
	}
}
