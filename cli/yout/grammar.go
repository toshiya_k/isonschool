package yout

import (
	"bytes"
	"isonschool/cli/ison"
	"isonschool/cli/translation"
	"strings"
)

// MakeJSONVideoFilesFromGrammar は，JSON Video File を作ります。
func MakeJSONVideoFilesFromGrammar(category ison.Category, consSerieName string, condNo int, condExName string) {
	translator := translation.NewTranslator(category)
	for _, file := range category.JSONGrammarFiles() {
		var sentence Sentence
		ison.ReadJSON(&sentence, file)
		if consSerieName != "" && consSerieName != sentence.SerieName {
			continue
		}
		if condNo > 0 && condNo != sentence.No {
			continue
		}
		for _, exercise := range sentence.Exercises {
			if condExName != "" && exercise.Name != condExName {
				continue
			}
			var serie Serie
			ison.ReadJSON(&serie, category.JSONSerieFile(sentence.SerieName, exercise.Name))
			videoFile := grammarVideoFilename(sentence, exercise)
			videoFilepath := category.JSONVideoFile(videoFile)
			var video Video
			ison.ReadJSON(&video, videoFilepath)
			var txtVideo TxtVideo
			txtVideo.AudioLang = serie.AudioLang
			txtVideo.TxtFiles = append(txtVideo.TxtFiles, TxtFile{File: videoFile, Lang: serie.YoutubeLang(sentence.No)})
			txtVideo.TxtLocalizationMap = make(map[ison.Lang]TxtLocalization)
			for _, lang := range ison.VideoLocalizationLangs {
				txtVideo.TxtLocalizationMap[lang] = TxtLocalization{
					Lang:        lang,
					Title:       replaceGrammar(serie.YoutubeTitle, serie, sentence, exercise, category, translator, lang),
					Description: replaceGrammar(serie.YoutubeDescription, serie, sentence, exercise, category, translator, lang),
					Tags:        replaceGrammar(serie.YoutubeTags, serie, sentence, exercise, category, translator, lang),
				}
			}
			video.Update(category, txtVideo, txtVideo.TxtFiles[0], translator)
			if !translator.HasError() {
				ison.WriteJSON(&video, videoFilepath)
			}
		}
		ison.WriteJSON(&sentence, file)
	}
	translator.CheckNeedsTranslation()
}

func replaceGrammar(s string, serie Serie, sentence Sentence, exercise Exercise, category ison.Category, translator *translation.Translator, lang ison.Lang) string {
	s = strings.ReplaceAll(s, "[:VideoTitle:]", sentence.VideoTitle)
	s = strings.ReplaceAll(s, "[:SentenceTitle:]", sentence.YoutubeTitle)
	s = strings.ReplaceAll(s, "[:SentenceDescription:]", sentence.YoutubeDescription)
	var b bytes.Buffer
	var bruby bytes.Buffer
	var bt bytes.Buffer
	var brubyt bytes.Buffer
	b.WriteString("\n\n")
	bruby.WriteString("\n\n")
	bt.WriteString("\n\n")
	brubyt.WriteString("\n\n")
	for _, p := range exercise.Problems {
		for _, c := range exercise.AnimeticCaptions {
			if c.Kind == CaptionKindOriginal && c.ProblemNo == p.ProblemNo {
				b.WriteString(toSimpleTimeString(c.Start) + " ")
				bruby.WriteString(toSimpleTimeString(c.Start) + " ")
				bt.WriteString(toSimpleTimeString(c.Start) + " ")
				brubyt.WriteString(toSimpleTimeString(c.Start) + " ")
			}
		}
		for _, l := range p.OriginalLines(category, serie, sentence) {
			b.WriteString("\n" + l.Text + "\n")
			bruby.WriteString("\n" + l.Text + "\n")
			bt.WriteString("\n" + l.Text + "\n")
			brubyt.WriteString("\n" + l.Text + "\n")

			bruby.WriteString(l.Ruby + "\n")
			brubyt.WriteString(l.Ruby + "\n")

			bt.WriteString(translator.Translate("{"+l.Text+"}", lang) + "\n")
			brubyt.WriteString(translator.Translate("{"+l.Text+"}", lang) + "\n")
		}
		b.WriteString("\n")
		bruby.WriteString("\n")
		bt.WriteString("\n")
		brubyt.WriteString("\n")
	}
	s = strings.ReplaceAll(s, "[:GrammarDescription:]", b.String())
	s = strings.ReplaceAll(s, "[:GrammarDescriptionWithRuby:]", bruby.String())
	s = strings.ReplaceAll(s, "[:GrammarDescriptionTranslation:]", bt.String())
	s = strings.ReplaceAll(s, "[:GrammarDescriptionTranslationWithRuby:]", brubyt.String())
	return s
}
