package yout

import (
	"fmt"
	"isonschool/cli/ison"
	"isonschool/cli/mp3"
	"log"
	"math"
	"os"
	"os/exec"
	"path/filepath"
	"strconv"
	"strings"
)

const tmpFolder = "/Users/juny/go/src/yossie/tmp/"

func clearTmpFolder() {
	// tmpフォルダを空にする。
	err := os.RemoveAll(tmpFolder)
	if err != nil {
		log.Panicf("os.RemoveAll %s : %v", tmpFolder, err)
	}
	err = os.Mkdir(tmpFolder, 0755)
	if err != nil {
		log.Panicf("os.Mkdir %s : %v", tmpFolder, err)
	}
}

// SoundKind は，音の種類
type SoundKind int

// SoundKind の種類です。
const (
	SoundKindNoSound SoundKind = iota
	SoundKindText
	SoundKindTitle
	SoundKindAnswer
)

const (
	noSoundFile     = "/Users/juny/go/src/isonschool/assets/sounds/nosound.mp3"
	titleSoundFile  = "/Users/juny/go/src/isonschool/assets/sounds/teardrop.mp3"
	answerSoundFile = "/Users/juny/go/src/isonschool/assets/sounds/decision.mp3"
)

var (
	noSoundLength     = mp3.GetData(noSoundFile).Seconds()
	answerSoundLength = mp3.GetData(answerSoundFile).Seconds()
	titleSoundLength  = mp3.GetData(titleSoundFile).Seconds()
)

// CaptionKind は，キャプションの種類です。
type CaptionKind int

// CaptionKindの集まりです。
const (
	CaptionKindTitle CaptionKind = iota
	CaptionKindOriginal
	CaptionKindChange
	CaptionKindThinking
	CaptionKindAnswer
	CaptionKindBlank
	CaptionKindIsonSchool
	CaptionKindSubscribe
	CaptionKindFormulaExplanation
	CaptionKindFormulaEquation
)

// MakeVideo は，ビデオを作成します。
func MakeVideo(category ison.Category, condSerieName string, condNo int, condExName string) {
	for _, sentenceFile := range category.JSONGrammarFiles() {
		var sentence Sentence
		ison.ReadJSON(&sentence, sentenceFile)
		if condSerieName != "" && condSerieName != sentence.SerieName {
			continue
		}
		if condNo > 0 && condNo != sentence.No {
			continue
		}
		for ei, exercise := range sentence.Exercises {
			if condExName != "" && exercise.Name != condExName {
				continue
			}
			if exercise.VideoFile == "" || len(exercise.AnimeticCaptions) == 0 {
				fmt.Printf("ビデオ作成開始 %s-%d\n", sentence.SerieName, sentence.No)
				clearTmpFolder()
				var serie Serie
				ison.ReadJSON(&serie, category.JSONSerieFile(sentence.SerieName, exercise.Name))
				animetic := NewAnimetic(category, sentence, exercise, serie)
				animetic.MakeAnimetic()
				tmpaudio := ison.Join(tmpFolder, "tmp.mp3")
				fmt.Printf("音声ファイル作成 %s-%d\n", sentence.SerieName, sentence.No)
				animetic.MakeAudio(tmpaudio)
				videoFilename := grammarVideoFilename(sentence, exercise)
				animetic.MakeSymbolicLinksAndMakeVideo(tmpFolder, tmpaudio, category.BinaryVideoFile(videoFilename))
				fmt.Printf("ビデオ作成終了 %s-%d %s\n", sentence.SerieName, sentence.No, videoFilename)
				sentence.Exercises[ei].VideoFile = videoFilename
				sentence.Exercises[ei].AnimeticCaptions = animetic.Captions
				ison.WriteJSON(&sentence, sentenceFile)
			}
		}
	}
}

func grammarVideoFilename(sentence Sentence, exercise Exercise) ison.Filename {
	return ison.ToFilename(sentence.SerieName + "_" + strconv.Itoa(sentence.No) + "_" + exercise.Name + ".mp4")
}

// Animetic は，動画の1秒ごとのフレームです。
type Animetic struct {
	Images   []AnimeticImage
	Sounds   []AnimeticSound
	Captions []AnimeticCaption
	category ison.Category
	sentence Sentence
	exercise Exercise
	serie    Serie
}

// NewAnimetic は，
func NewAnimetic(category ison.Category, sentence Sentence, exercise Exercise, serie Serie) *Animetic {
	return &Animetic{category: category, sentence: sentence, exercise: exercise, serie: serie}
}

// ImageFilepath は，ファイルパスを返します。
func (a *Animetic) ImageFilepath(ai AnimeticImage) ison.Filepath {
	/*
		if ai.ProblemNo == blankImageNo {
			return blankImageFile
		} else if ai.ProblemNo == isonSchoolImageNo {
			return isonSchoolImageFile
		} else if ai.ProblemNo == logo2ImageNo {
			return logo2ImageFile
		} else if ai.ProblemNo == logo3ImageNo {
			return logo3ImageFile
		} else if ai.ProblemNo == subscribeImageNo {
			return subscribeImageFile
		}*/
	return a.category.BinaryImageFile(a.sentence.SerieName, a.sentence.No, a.exercise.Name, ai.ProblemNo, ai.Status)
}

// AnimeticImage は，画像です。
type AnimeticImage struct {
	Start     int
	End       int
	ProblemNo int
	Status    string
}

// AnimeticSound は，音を表します。
type AnimeticSound struct {
	Filepath      ison.Filepath
	SoundLength   float64
	NoSoundLength float64
}

// AnimeticCaption は，動画のキャプションです。
type AnimeticCaption struct {
	Start     int
	End       int
	Text      string
	Ruby      string
	Lang      ison.Lang
	ProblemNo int
	Kind      CaptionKind
}

// MakeSymbolicLinksAndMakeVideo は，シンボリックリンクを作成します。
func (a *Animetic) MakeSymbolicLinksAndMakeVideo(folder string, audiooutput, videooutput ison.Filepath) {
	var i int
	for _, image := range a.Images {
		f := a.ImageFilepath(image)
		if _, err := os.Stat(string(f)); os.IsNotExist(err) {
			log.Panicf("MakeSymbolicLinks %s が存在しません。", f)
		}
		for m := image.Start; m < image.End; m++ {
			err := os.Symlink(string(f), filepath.Join(folder, fmt.Sprintf("img_%03d.png", i)))
			if err != nil {
				log.Panicf("MakeSymbolicLinks %s : %v", folder, err)
			}
			i++
		}
	}

	// ffmpeg -r 30 -i xxx_%3d.jpeg test.mp4
	var command = "ffmpeg -loglevel error -y -i " + string(audiooutput) + " -r 1 -i " + filepath.Join(folder, "img_%03d.png") + " -pix_fmt yuv420p " + string(videooutput)
	c := strings.Split(command, " ")
	cmd := exec.Command(c[0], c[1:]...)
	//cmd.Dir = cm.cmddir
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stdout
	err := cmd.Run()
	if err != nil {
		log.Panicf("%s : %v", videooutput, err)
	}
}

// MakeAudio は，複数の音声ファイルと無音ファイルをつないだファイルを作成します。
func (a *Animetic) MakeAudio(output ison.Filepath) {
	var inputfiles []string
	var nosoundlengths []float64
	for _, s := range a.Sounds {
		inputfiles = append(inputfiles, s.Filepath.String())
		nosoundlengths = append(nosoundlengths, s.NoSoundLength)
	}
	mp3.Merge(output.String(), inputfiles, noSoundFile, nosoundlengths)
}

func (a *Animetic) makeAnimetic(time int, displayMoreThanOne bool, opening bool, endImageStatus string, statuses ...string) int {
	const blankSeconds = 3
	thinkingTime := 1.5
	if opening {
		problemNo := 3
		p := a.exercise.Problems[problemNo]
		// 変化部分の提示
		if displayMoreThanOne {
			for _, l := range p.ChangeLines(a.category, a.sentence) {
				time = a.appendFrames(l, problemNo, statuses[1], 1.0, 0, SoundKindText, CaptionKindChange, time)
			}
		} else {
			time = a.appendFrames(p.ChangeLine(a.category, a.sentence), problemNo, statuses[1], 1.0, 0, SoundKindText, CaptionKindChange, time)
		}
		return time
	}
	for problemNo, p := range a.exercise.Problems {
		// 最初の文の提示
		for i, l := range p.OriginalLines(a.category, a.serie, a.sentence) {
			plus := 1
			if i < len(p.OriginalLines(a.category, a.serie, a.sentence))-1 {
				plus = 0
			}
			time = a.appendFrames(l, problemNo, statuses[0], 1.0, plus, SoundKindText, CaptionKindOriginal, time)
		}

		// 変化部分の提示
		if displayMoreThanOne {
			for _, l := range p.ChangeLines(a.category, a.sentence) {
				time = a.appendFrames(l, problemNo, statuses[1], 1.0, 0, SoundKindText, CaptionKindChange, time)
			}
		} else {
			time = a.appendFrames(p.ChangeLine(a.category, a.sentence), problemNo, statuses[1], 1.0, 0, SoundKindText, CaptionKindChange, time)
		}

		// シンキングタイム
		for i, l := range p.AnswerLines(a.category, a.serie, a.sentence) {
			sk := SoundKindAnswer
			if i > 0 {
				sk = SoundKindNoSound
			}
			plus := 1
			if i < len(p.AnswerLines(a.category, a.serie, a.sentence))-1 {
				plus = 0
			}
			time = a.appendFrames(l, problemNo, statuses[2], thinkingTime, plus, sk, CaptionKindThinking, time)
		}

		// 解答の提示
		for i, l := range p.AnswerLines(a.category, a.serie, a.sentence) {
			plus := 3
			if i < len(p.AnswerLines(a.category, a.serie, a.sentence))-1 {
				plus = 0
			}
			time = a.appendFrames(l, problemNo, statuses[3], 1.0, plus, SoundKindText, CaptionKindAnswer, time)
		}
	}
	a.Images = append(a.Images, AnimeticImage{
		Start:  time,
		End:    time + blankSeconds,
		Status: endImageStatus,
	})
	a.Sounds = append(a.Sounds, AnimeticSound{
		Filepath:      titleSoundFile,
		SoundLength:   titleSoundLength,
		NoSoundLength: blankSeconds - titleSoundLength,
	})
	a.Captions = append(a.Captions, AnimeticCaption{Lang: "",
		Text:  "",
		Ruby:  "",
		Start: time,
		End:   time + blankSeconds,
		Kind:  CaptionKindBlank,
	})
	time += blankSeconds
	return time
}

/*
// CheckTranslation checks if needs translation.
func (a *Animetic) CheckTranslation() {
	var needTranslate bool
	for _, c := range a.serie.CaptionKeyNames {
		if c.CaptionKey == CaptionKeyTranslation {
			needTranslate = true
		}
	}
	if !needTranslate {
		return
	}
	translator := translation.NewTranslator(a.category)
	for _, c := range a.Captions {
		for _, lang := range ison.VideoLocalizationLangs {
			translator.Translate(c.Text, lang)
		}
	}
	translator.CheckNeedsTranslation()
}
*/

/*
MakeAnimetic は，アニメティックを作ります。

1. タイトル。タイトルは，titleFrameNum秒つづきます。
2. 3周する。
0a(Original.mp3の秒数+1),1a(Change.mp3の秒数+1),1b(Answer.mp3の秒数*2),3a(Answer.mp3の秒数+3)
0a,2a,2b,3a
0a,0a,0b,3a
*/
func (a *Animetic) MakeAnimetic() int {
	const titleSeconds = 3
	var time int
	time = a.makeAnimetic(time, false, true, "", "10a", "1pa", "1pb", "13a")

	a.Images = append(a.Images, AnimeticImage{
		Start:  0,
		End:    titleSeconds,
		Status: "title",
	})
	a.Sounds = append(a.Sounds, AnimeticSound{
		Filepath:      titleSoundFile,
		SoundLength:   titleSoundLength,
		NoSoundLength: titleSeconds - titleSoundLength,
	})
	a.Captions = append(a.Captions, AnimeticCaption{Lang: "",
		Text:  a.serie.SerieTitle + " #" + strconv.Itoa(a.sentence.No) + " " + a.sentence.VideoTitle,
		Ruby:  "",
		Start: time,
		End:   time + titleSeconds,
		Kind:  CaptionKindTitle,
	})
	time += titleSeconds

	time = a.makeAnimetic(time, true, false, "logo2", "10a", "12a", "12b", "13a")
	time = a.makeAnimetic(time, false, false, "logo3", "10a", "1pa", "1pb", "13a")
	time = a.makeAnimetic(time, false, false, "00a", "p0a", "p1a", "p1b", "13a")

	subscribeSeconds := 20
	a.Images = append(a.Images, AnimeticImage{
		Start:  time,
		End:    time + subscribeSeconds,
		Status: "logo1",
	})
	a.Sounds = append(a.Sounds, AnimeticSound{
		Filepath:      titleSoundFile,
		SoundLength:   titleSoundLength,
		NoSoundLength: float64(subscribeSeconds) - titleSoundLength,
	})
	a.Captions = append(a.Captions, AnimeticCaption{
		Lang:  "en",
		Text:  "{Please subscribe!}",
		Start: time,
		End:   time + subscribeSeconds,
		Kind:  CaptionKindSubscribe,
	})
	return time
}

func (a *Animetic) appendFrames(line Line, problemNo int, status string, long float64, plus int, soundKind SoundKind, captionKind CaptionKind, time int) int {
	audioFile := a.category.BinaryAudioFile(line.Text, a.serie.VoiceName(line.PersonName, line.Gender))
	audioLength := mp3.GetData(audioFile).Seconds()
	seconds := int(math.Ceil(audioLength)*long) + plus
	a.Images = append(a.Images, AnimeticImage{
		Start:     time,
		End:       time + seconds,
		ProblemNo: problemNo,
		Status:    status,
	})
	if soundKind == SoundKindText {
		a.Sounds = append(a.Sounds, AnimeticSound{
			Filepath:      a.category.BinaryAudioFile(line.Text, a.serie.VoiceName(line.PersonName, line.Gender)),
			SoundLength:   audioLength,
			NoSoundLength: float64(seconds) - audioLength,
		})
	} else if soundKind == SoundKindAnswer {
		a.Sounds = append(a.Sounds, AnimeticSound{
			Filepath:      answerSoundFile,
			SoundLength:   answerSoundLength,
			NoSoundLength: float64(seconds) - answerSoundLength,
		})
	} else if soundKind == SoundKindNoSound {
		a.Sounds = append(a.Sounds, AnimeticSound{
			Filepath:      noSoundFile,
			SoundLength:   noSoundLength,
			NoSoundLength: float64(seconds) - noSoundLength,
		})
	}
	a.Captions = append(a.Captions, AnimeticCaption{
		Lang:      a.serie.AudioLang,
		Text:      "{" + line.Text + "}",
		Ruby:      line.Ruby,
		Start:     time,
		End:       time + seconds,
		ProblemNo: problemNo,
		Kind:      captionKind,
	})
	return time + seconds
}
