package yout

import (
	"isonschool/cli/colour"
	"isonschool/cli/ison"
	"isonschool/cli/ruby"
	"log"
	"strings"
)

// Serie は，シリーズです。
type Serie struct {
	SerieName          string
	ExerciseName       string
	NextPublishAt      string
	PublishHours       int
	ProblemNum         int
	AudioLang          ison.Lang
	VideoLang          ison.Lang
	Voices             Voices
	SerieTitle         string
	YoutubeTitle       string
	YoutubeDescription string
	YoutubeTags        string
	RubyFunc           ruby.RubyFunc
	HidesRuby          bool
	CaptionKeyNames    []CaptionKeyName
	VoiceKeyNames      []VoiceKeyName
	Palettes           []colour.Palette
	TitleFont          ison.Filename
	SubtitleFont       ison.Filename
	ProblemFont        ison.Filename
	RubyFont           ison.Filename
	Langs              []ison.Lang
}

// HasCaptionKeyTranslation returns if the serie has CaptionKeyTranslation.
func (s *Serie) HasCaptionKeyTranslation() bool {
	for _, a := range s.CaptionKeyNames {
		if a.CaptionKey == CaptionKeyTranslation {
			return true
		}
	}
	return false
}

// YoutubeLang returns youtube language.
func (s *Serie) YoutubeLang(no int) ison.Lang {
	return s.Langs[(no-1+len(s.Langs))%len(s.Langs)]
}

// TxtSerie は，serie テキストファイルデータです。
type TxtSerie struct {
	SerieName          string
	ExerciseName       string
	NextPublishAt      string
	PublishHours       int
	ProblemNum         int
	AudioLang          ison.Lang
	VideoLang          ison.Lang
	Voices             Voices
	SerieTitle         string
	YoutubeTitle       string
	YoutubeDescription string
	YoutubeTags        string
	RubyFunc           ruby.RubyFunc
	HidesRuby          bool
	CaptionKeyNames    []CaptionKeyName
	VoiceKeyNames      []VoiceKeyName
	Palettes           []colour.Palette
	FontMap            map[string]ison.Filename
	Langs              string
}

// VoiceName は，音声名を返します。
func (s *Serie) VoiceName(personName string, gender Gender) string {
	var voiceKey VoiceKey
	for _, v := range s.Voices {
		if v.PersonName == personName {
			voiceKey = v.VoiceKeys[gender]
		}
	}
	if voiceKey == "" {
		log.Panicf("%s, %s に対応する VoiceKey がありません。 %+v", personName, gender, s)
		return ""
	}
	for _, kn := range s.VoiceKeyNames {
		if kn.VoiceKey == voiceKey {
			return kn.VoiceName
		}
	}
	log.Panicf("%s に対応する VoiceName がありません。 %+v", voiceKey, s)
	return ""
}

// VoiceKeyName は，VoiceKeyとVoiceNameのペアです。
type VoiceKeyName struct {
	VoiceKey  VoiceKey
	VoiceName string
}

// Update は，アップデートします。
func (s *Serie) Update(category ison.Category, t TxtSerie) {
	if (s.SerieName != "" && s.SerieName != t.SerieName) || (s.ExerciseName != "" && s.ExerciseName != t.ExerciseName) {
		log.Panicf("SerieName あるいは ExerciseName が異なります。s:%s %s, t:%s %s", s.SerieName, s.ExerciseName, t.SerieName, t.ExerciseName)
	}
	s.SerieName = t.SerieName
	s.ExerciseName = t.ExerciseName
	if t.NextPublishAt != "" {
		s.NextPublishAt = t.NextPublishAt
	}
	s.PublishHours = t.PublishHours
	s.ProblemNum = t.ProblemNum
	s.AudioLang = t.AudioLang
	s.VideoLang = t.VideoLang
	s.Voices = t.Voices
	s.SerieTitle = t.SerieTitle
	s.YoutubeTitle = t.YoutubeTitle
	s.YoutubeDescription = t.YoutubeDescription
	s.YoutubeTags = t.YoutubeTags
	s.RubyFunc = t.RubyFunc
	s.HidesRuby = t.HidesRuby
	s.CaptionKeyNames = t.CaptionKeyNames
	s.VoiceKeyNames = t.VoiceKeyNames
	s.Palettes = t.Palettes
	s.TitleFont = t.FontMap["title"]
	s.SubtitleFont = t.FontMap["subtitle"]
	s.ProblemFont = t.FontMap["problem"]
	s.RubyFont = t.FontMap["ruby"]
	var langs = []ison.Lang{}
	for _, lang := range strings.Split(t.Langs, ",") {
		if lang == "" {
			continue
		}
		if lang == "all" {
			langs = append(langs, ison.VideoLocalizationLangs...)
		} else if strings.HasPrefix(lang, "all+") {
			start := ison.Atoi(lang[len("all+"):])
			langs = append(langs, ison.VideoLocalizationLangs[start:]...)
			langs = append(langs, ison.VideoLocalizationLangs[:start]...)
		} else {
			langs = append(langs, ison.ToLang(lang))
		}
	}
	s.Langs = langs
}

// ParseTxtSerie は，serieテキストファイルをパースします。
func ParseTxtSerie(m *ison.TxtMarshaller) []TxtSerie {
	m.Add("seriename", func(d interface{}, k, v string) {
		d.(*TxtSerie).SerieName = v
	})
	m.Add("exercisename", func(d interface{}, k, v string) {
		d.(*TxtSerie).ExerciseName = v
	})
	m.Add("nextpublishat", func(d interface{}, k, v string) {
		d.(*TxtSerie).NextPublishAt = v
	})
	m.Add("publishhours", func(d interface{}, k, v string) {
		d.(*TxtSerie).PublishHours = ison.Atoi(v)
	})
	m.Add("problemnum", func(d interface{}, k, v string) {
		d.(*TxtSerie).ProblemNum = ison.Atoi(v)
	})
	m.Add("videolang", func(d interface{}, k, v string) {
		d.(*TxtSerie).VideoLang = ison.Lang(v)
	})
	m.Add("voice", func(d interface{}, k, v string) {
		var voiceKeys [3]VoiceKey
		for i, s := range strings.Split(v, ",") {
			voiceKeys[i] = VoiceKey(s)
		}
		d.(*TxtSerie).Voices = append(d.(*TxtSerie).Voices, Voice{PersonName: k, VoiceKeys: voiceKeys})
	})
	m.Add("serietitle", func(d interface{}, k, v string) {
		d.(*TxtSerie).SerieTitle = v
	})
	m.Add("title", func(d interface{}, k, v string) {
		d.(*TxtSerie).YoutubeTitle = v
	})
	m.Add("description", func(d interface{}, k, v string) {
		d.(*TxtSerie).YoutubeDescription = v
	})
	m.Add("tags", func(d interface{}, k, v string) {
		d.(*TxtSerie).YoutubeTags = v
	})
	m.Add("rubyfunc", func(d interface{}, k, v string) {
		d.(*TxtSerie).RubyFunc = ruby.RubyFunc(v)
	})
	m.Add("hidesruby", func(d interface{}, k, v string) {
		d.(*TxtSerie).HidesRuby = ison.ParseBool(v)
	})
	m.Add("caption", func(d interface{}, k, v string) {
		d.(*TxtSerie).CaptionKeyNames = append(d.(*TxtSerie).CaptionKeyNames, CaptionKeyName{
			CaptionKey:  k,
			CaptionName: v,
		})
	})
	m.Add("audiolang", func(d interface{}, k, v string) {
		d.(*TxtSerie).AudioLang = ison.Lang(v)
	})
	m.Add("voicekeyname", func(d interface{}, k, v string) {
		d.(*TxtSerie).VoiceKeyNames = append(d.(*TxtSerie).VoiceKeyNames, VoiceKeyName{VoiceKey: VoiceKey(k), VoiceName: v})
	})
	m.Add("palette", func(d interface{}, k, v string) {
		p := colour.ToPalette(v)
		p.PaletteName = k
		d.(*TxtSerie).Palettes = append(d.(*TxtSerie).Palettes, p)
	})
	m.Add("font", func(d interface{}, k, v string) {
		d.(*TxtSerie).FontMap[k] = ison.Filename(v)
	})
	m.Add("langs", func(d interface{}, k, v string) {
		d.(*TxtSerie).Langs = v
	})
	var series []TxtSerie
	var success, isEOF bool
	for !isEOF {
		var d = TxtSerie{FontMap: make(map[string]ison.Filename)}
		success, isEOF = m.Unmarshal(&d)
		if success {
			series = append(series, d)
		}
	}
	return series
}

// MergeTxtSerieFiles は，テキストファイルを読み込んで，JSONデータにマージします。
func MergeTxtSerieFiles(category ison.Category, condSerieName string) {
	var txtSeries []TxtSerie
	for _, file := range category.TxtSerieFiles() {
		m := ison.NewTxtMarshallerFromFile(file)
		txtSeries = append(txtSeries, ParseTxtSerie(m)...)
	}
	m := make(map[string]bool)
	for _, txtSerie := range txtSeries {
		if condSerieName != "" && condSerieName != txtSerie.SerieName {
			continue
		}
		if m[txtSerie.SerieName+"_"+txtSerie.ExerciseName] {
			log.Panicf("%s_%s が重複して定義されています。", txtSerie.SerieName, txtSerie.ExerciseName)
		}
		m[txtSerie.SerieName+"_"+txtSerie.ExerciseName] = true
		f := category.JSONSerieFile(txtSerie.SerieName, txtSerie.ExerciseName)
		var v Serie
		ison.ReadJSON(&v, f)
		v.Update(category, txtSerie)
		ison.WriteJSON(&v, f)
	}
}
