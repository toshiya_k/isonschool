package yout

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"isonschool/cli/ison"
	"isonschool/cli/ruby"
	"isonschool/cli/translation"
	"log"
	"math/rand"
	"strconv"
)

// HTMLRecorderData は，jsFilepath にエクスポートするデータの構造体です。
type HTMLRecorderData struct {
	Exercise Exercise
	Sentence Sentence
	Serie    Serie
	Category ison.Category
	Title    string
	Subtitle string
	//FeatureTranslationMap map[string]string
}

// MakeHTMLRecorderJSON は，問題の画像生成用HTMLで使われるJSON javascriptファイルを生成します。
func MakeHTMLRecorderJSON(category ison.Category, condSerieName, jsFilepath string) bool {
	var data []HTMLRecorderData
	translator := translation.NewTranslator(category)
	for _, file := range category.JSONGrammarFiles() {
		var sentence Sentence
		ison.ReadJSON(&sentence, file)
		if condSerieName != "" && condSerieName != sentence.SerieName {
			continue
		}
		for _, e := range sentence.Exercises {
			if e.VideoFile == "" {
				var serie Serie
				ison.ReadJSON(&serie, category.JSONSerieFile(sentence.SerieName, e.Name))
				data = append(data, HTMLRecorderData{
					Exercise: e,
					Sentence: sentence,
					Serie:    serie,
					Category: category,
					Title:    translator.Translate(serie.SerieTitle, serie.VideoLang),
					Subtitle: translator.Translate(sentence.VideoTitle, serie.VideoLang),
				})
			}
		}
	}
	translator.CheckNeedsTranslation()

	if len(data) == 0 {
		return false
	}
	b, err := json.MarshalIndent(&data, "", "  ")
	if err != nil {
		log.Panicf("%v", err)
	}
	var o bytes.Buffer
	o.WriteString("var data = ")
	o.Write(b)
	o.WriteString(";\n")
	err = ioutil.WriteFile(jsFilepath, o.Bytes(), 0644)
	if err != nil {
		log.Panicf("%v", err)
	}
	return true
}

func checkRubyChanged(category ison.Category, serie Serie, exercise Exercise) bool {
	rubier := ruby.NewRubier(category, serie.RubyFunc)
	for _, p := range exercise.Problems {
		for i, oc := range p.OriginalCandidates {
			var pre string
			var post string
			if i > 0 {
				pre = p.OriginalCandidates[i-1].Word
			}
			if i < len(p.OriginalCandidates)-1 {
				post = p.OriginalCandidates[i+1].Word
			}
			if oc.Ruby != rubier.GetRuby(oc.Word, pre, post) {
				return true
			}
		}
		for i, oc := range p.AnswerCandidates {
			var pre string
			var post string
			if i > 0 {
				pre = p.AnswerCandidates[i-1].Word
			}
			if i < len(p.AnswerCandidates)-1 {
				post = p.AnswerCandidates[i+1].Word
			}
			if oc.Ruby != rubier.GetRuby(oc.Word, pre, post) {
				return true
			}
		}
	}
	return false
}

// MakeProblems は，問題を生成します。
func MakeProblems(category ison.Category, condSerieName string, condNo int, condExName string) {
	translator := translation.NewTranslator(category)
	for _, file := range category.JSONGrammarFiles() {
		var sentence Sentence
		ison.ReadJSON(&sentence, file)
		if condSerieName != "" && condSerieName != sentence.SerieName {
			continue
		}
		if condNo > 0 && condNo != sentence.No {
			continue
		}
		for _, ename := range category.SerieExerciseNames(sentence.SerieName) {
			sentence.AddExercise(ename)
		}
		for i, e := range sentence.Exercises {
			if condExName != "" && e.Name != condExName {
				continue
			}
			if e.VideoFile != "" {
				var video Video
				ison.ReadJSON(&video, category.JSONVideoFile(e.VideoFile))
				if video.ID != "" {
					// すでにビデオがアップロードされている場合は，スキップする。
					continue
				}
			}
			var serie Serie
			ison.ReadJSON(&serie, category.JSONSerieFile(sentence.SerieName, e.Name))
			rubier := ruby.NewRubier(category, serie.RubyFunc)
			fmt.Printf("問題を作成します。 %s %d\n", sentence.SerieName, sentence.No)
			ison.SetRandSeed(category.ID + sentence.SerieName + strconv.Itoa(sentence.No) + e.Name)
			problems, err := generateProblem(category, sentence, serie.ProblemNum, rubier)
			if err != nil {
				log.Panicf("%v", err)
			}
			fmt.Printf("問題作成が終了しました。%s %d\n", sentence.SerieName, sentence.No)
			if serie.HasCaptionKeyTranslation() {
				fmt.Printf("翻訳を確認します。%s %s %d\n", sentence.SerieName, e.Name, sentence.No)
				for _, p := range problems {
					text := p.OriginalLines(category, serie, sentence)[0].Text
					for _, lang := range ison.VideoLocalizationLangs {
						translator.Translate(text, lang)
					}
					for _, l := range p.AnswerLines(category, serie, sentence) {
						for _, lang := range ison.VideoLocalizationLangs {
							translator.Translate(l.Text, lang)
						}
					}
				}
			}
			if !rubier.HasError() && fmt.Sprintf("%+v", e.Problems) != fmt.Sprintf("%+v", problems) {
				sentence.Exercises[i].Problems = problems
				sentence.Exercises[i].VideoFile = ""
				//fmt.Printf("%s\n", sentence.Exercises[i].Problems)
			}
			if !rubier.HasError() {
				ison.WriteJSON(&sentence, file)
			}
		}
	}
	ruby.CheckNeedsRuby()
	translator.CheckNeedsTranslation()
}

type weightWord struct {
	pos       int
	candidate Candidate
}

// generateProblem は，問題を作成します。
func generateProblem(category ison.Category, sentence Sentence, num int, rubier *ruby.Rubier) ([]Problem, error) {
	var boxes = sentence.Boxes
	var printLog = false

	var ret []Problem
	for i := 0; i < 10000; i++ {
		originalCands := make([]Candidate, len(boxes))
		for c := 0; c < len(boxes); c++ {
			originalCands[c] = boxes[c].Candidates[rand.Intn(len(boxes[c].Candidates))]
		}
		if !passConditions(originalCands) {
			if printLog {
				log.Printf("さいしょのoriginal %+v", originalCands)
			}
			continue
		}
		ret = []Problem{}
		var weights = []weightWord{}
		for len(weights) <= 10+i { // 少し条件を変えて，問題を作りやすくする。
			for i, b := range boxes {
				if len(b.Candidates) == 1 {
					// 1つだけしか候補がない場合は，変化できないから。
					continue
				}
				for _, c := range b.Candidates {
					weights = append(weights, weightWord{i, c})
					for _, cond := range c.Conditions {
						for range cond.Morphemes {
							weights = append(weights, weightWord{i, c})
						}
					}
				}
			}
		}
		for m := 0; m < 1000; m++ {
			var changeCands = make([]Candidate, len(boxes))
			var changeMophemes = make([]Morpheme, len(boxes))
			var changeCandW = weightWord{pos: -1}
			var randPos int
			var nn int
			var ok bool
			for !ok {
				randPos = rand.Intn(len(weights))
				changeCandW = weights[randPos]
				if nn > 1000 {
					break
				}
				ok = true
				if changeCandW.pos < 0 || changeCandW.candidate.Word == originalCands[changeCandW.pos].Word {
					ok = false
				}
				// Root が変わらない場合，FeatureSet の違いは1以下でなければならない。
				if changeCandW.candidate.Root == originalCands[changeCandW.pos].Root && originalCands[changeCandW.pos].FeatureSet.DiffCount(changeCandW.candidate.FeatureSet) > 1 {
					ok = false
				}
				// Root が変わる場合，自分から見て，FeatureSet の変化はあってはいけない。
				if changeCandW.candidate.Root != originalCands[changeCandW.pos].Root && originalCands[changeCandW.pos].FeatureSet.DiffCount(changeCandW.candidate.FeatureSet) > 0 {
					ok = false
				}
				nn++
			}
			if nn > 1000 {
				log.Printf("nn > 1000 %s %d", sentence.SerieName, sentence.No)
				break
			}
			changeCands[changeCandW.pos] = changeCandW.candidate
			changeMophemes[changeCandW.pos] = Morpheme{
				Word:       changeCandW.candidate.Word,
				Root:       changeCandW.candidate.Root,
				FeatureSet: changeCandW.candidate.FeatureSet,
			}
			// 条件があるならそれに合うように変化させる。
			var hasMoreThanOneCandidate bool
			var notFound bool
			for _, cond := range changeCandW.candidate.Conditions {
				var found bool
				for _, m := range cond.Morphemes {
					for _, c := range boxes[cond.Pos].Candidates {
						o := originalCands[cond.Pos]
						// うん => ううん featureSet=[n]
						// [n].Matches([n,current]) && [p,current].MatchesExcept([n,current],[n])
						if (m.Word == "" || (m.Word == c.Word && c.Root == o.Root && c.FeatureSet.Matches(o.FeatureSet))) && (m.Root == "" || (m.Root == c.Root && c.FeatureSet.Matches(o.FeatureSet))) && (m.FeatureSet.Count() == 0 || (c.Root == o.Root && m.FeatureSet.Matches(c.FeatureSet) && c.FeatureSet.MatchesExcept(o.FeatureSet, m.FeatureSet))) {
							changeCands[cond.Pos] = c
							changeMophemes[cond.Pos] = m
							if found {
								hasMoreThanOneCandidate = true
							}
							found = true
						}
					}
				}
				if !found {
					notFound = true
				}
			}
			if notFound {
				if printLog {
					log.Printf("かえる先が見つかりませんでした。!found: %+v", changeCandW)
					log.Printf("%+v", originalCands)
				}
				continue
			}
			if hasMoreThanOneCandidate {
				if printLog {
					log.Printf("hasMoreThanOneCandidate %+v", changeCandW)
					log.Printf("%+v", originalCands)
				}
				continue
			}
			var answerCands = make([]Candidate, len(boxes))
			for ci := range boxes {
				if changeCands[ci].Word != "" {
					answerCands[ci] = changeCands[ci]
				} else {
					answerCands[ci] = originalCands[ci]
				}
			}
			if !passConditions(answerCands) {
				if printLog {
					log.Printf("notPassConditions %+v", changeCandW)
					log.Printf("%+v", originalCands)
					log.Printf("%+v", changeCands)
					log.Printf("%+v", answerCands)
				}
				continue
			}
			if isDuplicate(ret, answerCands) {
				if printLog {
					log.Printf("isDuplicate %+v", changeCandW)
					log.Printf("%+v", originalCands)
					log.Printf("%+v", changeCands)
					log.Printf("%+v", answerCands)
				}
				continue
			}
			weights = append(weights[:randPos], weights[randPos+1:]...)
			ret = append(ret, Problem{
				ProblemNo:          len(ret),
				OriginalCandidates: toCandidateWords(originalCands, rubier),
				ChangeCandidatePos: changeCandW.pos,
				ChangeCandidates:   toCandidateWords(changeCands, rubier),
				ChangeMorphemes:    changeMophemes,
				AnswerCandidates:   toCandidateWords(answerCands, rubier),
			})
			originalCands = answerCands
			if len(ret) >= num {
				// すべての単語が使われているかチェック。
				var allUsed = true
				if num >= 10 {
					for bi, box := range boxes {
						for _, c := range box.Candidates {
							var used bool
							for _, r := range ret {
								if r.OriginalCandidates[bi].Word == c.Word {
									used = true
									break
								} else if r.AnswerCandidates[bi].Word == c.Word {
									used = true
									break
								}
							}
							if !used {
								allUsed = false
								break
							}
						}
					}
				}
				if !allUsed {
					// すべての単語がつかわれていたわけではないので，もう一度問題作成する。
					if printLog {
						fmt.Printf("すべての単語がつかわれていたわけではないので，もう一度問題作成する。 %s %d\n", sentence.SerieName, sentence.No)
					}
					break
				}
				return ret, nil
			}
		}
	}
	fmt.Printf("十分な問題が作成できませんでした。 %s %d, 作成できた数:%d %+v\n", sentence.SerieName, sentence.No, len(ret), sentence)
	return nil, ErrCannotMakeEnoughProblems
}

func toCandidateWords(cands []Candidate, rubier *ruby.Rubier) []CandidateWord {
	var ret = make([]CandidateWord, len(cands))
	for i, c := range cands {
		var pre string
		var post string
		if i > 0 {
			pre = cands[i-1].Word
		}
		if i < len(cands)-1 {
			post = cands[i+1].Word
		}
		ret[i] = CandidateWord{Word: c.Word, Ruby: rubier.GetRuby(c.Word, pre, post)}
	}
	return ret
}

// ErrCannotMakeEnoughProblems は，指定された数の問題を作成できなかったことを示します。
var ErrCannotMakeEnoughProblems = errors.New("Cannot make enough problems")

func isDuplicate(problems []Problem, cands []Candidate) bool {
	for _, p := range problems {
		same := true
		for i := range p.OriginalCandidates {
			if p.OriginalCandidates[i].Word != cands[i].Word {
				same = false
				break
			}
		}
		if same {
			return true
		}
		same = true
		for i := range p.AnswerCandidates {
			if p.AnswerCandidates[i].Word != cands[i].Word {
				same = false
				break
			}
		}
		if same {
			return true
		}
	}
	return false
}

func passConditions(cs []Candidate) bool {
	for _, c := range cs {
		for _, cond := range c.Conditions {
			var ok = false
			// どれかを満たせば良い。
			for _, m := range cond.Morphemes {
				if (m.Word == "" || m.Word == cs[cond.Pos].Word) && (m.Root == "" || m.Root == cs[cond.Pos].Root) && (m.FeatureSet.Count() == 0 || m.FeatureSet.Matches(cs[cond.Pos].FeatureSet)) {
					ok = true
				}
			}
			if !ok {
				return false
			}
		}
	}
	genderCond := make(map[string]Gender)
	for _, c := range cs {
		for _, cond := range c.PersonConditions {
			g := genderCond[cond.PersonName]
			if g != GenderNeutral && g != cond.Gender {
				return false
			}
			genderCond[cond.PersonName] = cond.Gender
		}
	}
	return true
}
