package yout

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"isonschool/cli/ison"
	"log"
	"math/rand"
	"net/http"
	"os"
	"strings"
	"time"

	"golang.org/x/net/context"
	"golang.org/x/oauth2"
	"golang.org/x/oauth2/google"
	"google.golang.org/api/youtube/v3"
)

// UploadThumbnail は，サムネールをアップロードします。
func UploadThumbnail(category ison.Category, condSerieName string, condNo int, condExName string) {
	for _, file := range category.JSONVideoFiles() {
		var video Video
		ison.ReadJSON(&video, file)
		if video.ID == "" {
			continue
		}
		if category.IsGrammar() {
			serieName, sentenceNo, exName := video.File.SerieNameAndNoAndExerciseName()
			if condSerieName != "" && serieName != condSerieName {
				continue
			}
			if condNo != 0 && sentenceNo != condNo {
				continue
			}
			if condExName != "" && condExName != exName {
				continue
			}
		}
		if !video.ThumbnailUploaded && category.BinaryThumbnailFile(video.File).Exist() {
			uploadThumbnail(category, video)
			video.ThumbnailUploaded = true
			ison.WriteJSON(&video, file)
		}
	}
}

// UploadVideo は，ビデオをYouTubeにアップロードします。
func UploadVideo(category ison.Category, disableUpload bool, condSerieName string, condNo int, condExName string) {
	rand.Seed(time.Now().Unix())
	for _, file := range category.JSONVideoFiles() {
		var video Video
		ison.ReadJSON(&video, file)
		if category.IsGrammar() {
			serieName, sentenceNo, exName := video.File.SerieNameAndNoAndExerciseName()
			if condSerieName != "" && serieName != condSerieName {
				continue
			}
			if condNo != 0 && sentenceNo != condNo {
				continue
			}
			if condExName != "" && exName != condExName {
				continue
			}
		}
		if video.ID == "" {
			if disableUpload {
				continue
			}
			serieFile := category.JSONSerieFileFromVideoFile(video.File)
			var serie Serie
			ison.ReadJSON(&serie, serieFile)
			if serie.SerieName == "" {
				serie.SerieName, serie.ExerciseName = video.File.SerieNameAndExerciseName()
				t := time.Now()
				t = time.Date(t.Year(), t.Month(), t.Day()+1, 0, 0, 0, 0, time.Local)
				serie.NextPublishAt = t.Format("2006-01-02T15:04:05.0Z07:00")
				serie.PublishHours = 12
			}
			if serie.NextPublishAt < time.Now().Format("2006-01-02T15:04:05.0Z07:00") {
				t := time.Now()
				t = time.Date(t.Year(), t.Month(), t.Day()+1, 0, 0, 0, 0, time.Local)
				serie.NextPublishAt = t.Format("2006-01-02T15:04:05.0Z07:00")
			}
			id := uploadVideo(category, video, serie.NextPublishAt)
			fmt.Printf("[Youtube] Upload video successful! %s Video ID: %v, PublishAt: %s\n", video.File, id, serie.NextPublishAt)
			t, err := time.Parse("2006-01-02T15:04:05.0Z07:00", serie.NextPublishAt)
			if err != nil {
				log.Panicf("%s がパースできませんでした。", serie.NextPublishAt)
			}
			t = t.Add(time.Duration(serie.PublishHours) * time.Hour)
			serie.NextPublishAt = t.Format("2006-01-02T15:04:05.0Z07:00")
			ison.WriteJSON(serie, serieFile)
			fmt.Printf("エンドスクリーン編集 %s https://www.youtube.com/endscreen?v=%s&nrts=1&nv=1 \n", video.File, id)
			video.ID = id
			video.YoutubeSynced = true
			if category.IsGrammar() {
				var sentence Sentence
				serieName, sentenceNo := video.File.SerieNameAndSentenceNo()
				sentenceFilepath := category.JSONGrammarFile(serieName, sentenceNo)
				ison.ReadJSON(&sentence, sentenceFilepath)
				sentence.CannotChange = true
				ison.WriteJSON(&sentence, sentenceFilepath)
			}
		} else if !video.YoutubeSynced && disableUpload {
			updateVideo(video)
			fmt.Printf("[Youtube] Update video successful! %s Video ID: %v\n", video.File, video.ID)
			video.YoutubeSynced = true
		} else {
			continue
		}
		ison.WriteJSON(&video, file)
	}
}

// UploadPlaylist は，プレイリストをYouTubeにアップロードします。
func UploadPlaylist(category ison.Category, enableUpload bool) {
	for _, file := range category.JSONPlaylistFiles() {
		var playlist Playlist
		ison.ReadJSON(&playlist, file)
		if playlist.ID == "" {
			if !enableUpload {
				continue
			}
			id := uploadPlaylist(category, playlist)
			fmt.Printf("[Youtube] Upload playlist successful! %s Playlist ID: %v\n", playlist.Name, id)
			playlist.ID = id
			playlist.Updated = true
			ison.WriteJSON(&playlist, file)
		} else if !playlist.Updated {
			updatePlaylist(playlist)
			fmt.Printf("[Youtube] Update playlist successful! %s Playlist ID: %v\n", playlist.Name, playlist.ID)
			playlist.Updated = true
			ison.WriteJSON(&playlist, file)
		}
	}
}

// UploadPlaylistItem は，プレイリストのアイテムをYouTubeにアップロードします。
func UploadPlaylistItem(category ison.Category) {
	for _, file := range category.JSONPlaylistFiles() {
		var playlist Playlist
		ison.ReadJSON(&playlist, file)
		if playlist.ID == "" {
			continue
		}
		for i, item := range playlist.PlaylistItems {
			if item.ID == "" {
				var video Video
				ison.ReadJSON(&video, category.JSONVideoFile(item.VideoFile))
				if video.ID == "" {
					continue
				}
				id := insertPlaylistItem(playlist, item, video)
				playlist.PlaylistItems[i].ID = id
				playlist.PlaylistItems[i].VideoID = video.ID
				ison.WriteJSON(&playlist, file)
			}
		}
	}
}

type clientFile struct {
	clientSecret string
	credential   string
}

var defaultClientFile = clientFile{clientSecret: "/Users/juny/go/src/yossie/secret/client_secret_1.json", credential: "/Users/juny/go/src/yossie/secret/force_ssl_credential_1.json"}

func init() {
	rand.Seed(time.Now().Unix())
}

const missingClientSecretsMessage = `
Please configure OAuth 2.0
`

// getClient uses a Context and Config to retrieve a Token
// then generate a Client. It returns the generated Client.
func getClient(exceeded bool) *http.Client {
	if exceeded {
		log.Panicf("Exceeded limit. There is no client.")
	}
	cf := defaultClientFile
	ctx := context.Background()
	b, err := ioutil.ReadFile(cf.clientSecret)
	if err != nil {
		log.Panicf("Unable to read client secret file: %v", err)
	}
	fmt.Printf("%s\n", cf.clientSecret)

	config, err := google.ConfigFromJSON(b, youtube.YoutubeUploadScope, youtube.YoutubeForceSslScope, youtube.YoutubeScope, youtube.YoutubeReadonlyScope)
	if err != nil {
		log.Panicf("Unable to parse client secret file to config: %v", err)
	}

	tok, err := tokenFromFile(cf.credential)
	if err != nil {
		tok = getTokenFromWeb(config)
		saveToken(cf.credential, tok)
	}
	return config.Client(ctx, tok)
}

// getTokenFromWeb uses Config to request a Token.
// It returns the retrieved Token.
func getTokenFromWeb(config *oauth2.Config) *oauth2.Token {
	authURL := config.AuthCodeURL("state-token", oauth2.AccessTypeOffline)
	fmt.Printf("Go to the following link in your browser then type the "+
		"authorization code: \n%v\n", authURL)

	var code string
	if _, err := fmt.Scan(&code); err != nil {
		log.Panicf("Unable to read authorization code %v", err)
	}

	tok, err := config.Exchange(oauth2.NoContext, code)
	if err != nil {
		log.Panicf("Unable to retrieve token from web %v", err)
	}
	return tok
}

// tokenFromFile retrieves a Token from a given file path.
// It returns the retrieved Token and any read error encountered.
func tokenFromFile(file string) (*oauth2.Token, error) {
	f, err := os.Open(file)
	if err != nil {
		return nil, err
	}
	t := &oauth2.Token{}
	err = json.NewDecoder(f).Decode(t)
	defer f.Close()
	return t, err
}

// saveToken uses a file path to create a file and store the
// token in it.
func saveToken(file string, token *oauth2.Token) {
	fmt.Printf("Saving credential file to: %s\n", file)
	f, err := os.OpenFile(file, os.O_RDWR|os.O_CREATE|os.O_TRUNC, 0600)
	if err != nil {
		log.Panicf("Unable to cache oauth token: %v", err)
	}
	defer f.Close()
	json.NewEncoder(f).Encode(token)
}

func handleError(err error, message string) {
	if message == "" {
		message = "Error making API call"
	}
	if err != nil {
		log.Panicf(message+": %v", err.Error())
	}
}

func uploadThumbnail(category ison.Category, video Video) {
	service := getService(false)
	for {
		call := service.Thumbnails.Set(video.ID)
		file, err := os.Open(category.BinaryThumbnailFile(video.File).String())
		defer file.Close()
		if err != nil {
			log.Panicf("Error opening %v: %v", "", err)
		}
		_, err = call.Media(file).Do()
		if err != nil && (strings.Contains(err.Error(), "quotaExceeded") || strings.Contains(err.Error(), "dailyLimitExceeded")) {
			fmt.Printf("%v\n", err)
			service = getService(true)
			continue
		} else if err != nil {
			log.Panicf("uploadThumbnail list call error: %s, %v", video.File, err)
		}
		fmt.Printf("uploadThumbnail succeeded. %s\n", video.File)
		return
	}
}

func updateVideo(video Video) {
	service := getService(false)
	for {
		call := service.Videos.List("snippet,localizations")
		call = call.Id(video.ID)
		response, err := call.Do()
		if err != nil && (strings.Contains(err.Error(), "quotaExceeded") || strings.Contains(err.Error(), "dailyLimitExceeded")) {
			fmt.Printf("%v\n", err)
			service = getService(true)
			continue
		} else if err != nil {
			log.Panicf("updateVideo list call error: %s, %v", video.File, err)
		}
		if len(response.Items) == 0 {
			log.Panicf("updateVideo no result item %s %s", video.File, video.ID)
		}
		v := response.Items[0]
		v.Snippet.Title = video.Title
		v.Snippet.Description = video.Description
		v.Snippet.Tags = ison.SplitTags(video.VideoTags)
		v.Snippet.DefaultLanguage = video.DefaultLang.String()
		localizations := make(map[string]youtube.VideoLocalization)
		for _, l := range video.Localizations {
			localizations[l.Lang.String()] = youtube.VideoLocalization{Title: l.Title, Description: l.Description}
		}
		v.Localizations = localizations
		updateCall := service.Videos.Update("snippet,localizations", v)
		_, err = updateCall.Do()
		if err != nil && (strings.Contains(err.Error(), "quotaExceeded") || strings.Contains(err.Error(), "dailyLimitExceeded")) {
			fmt.Printf("%v\n", err)
			service = getService(true)
			continue
		} else if err != nil {
			log.Panicf("updateVideo update call error: %s, %v", video.File, err)
		}
		return
	}
}

var clientFileN int

func getService(useNext bool) *youtube.Service {
	client := getClient(useNext)
	service, err := youtube.New(client)
	handleError(err, "Error creating YouTube client")
	return service
}

// UploadCaption は，キャプションをアップロードします。
func UploadCaption(category ison.Category, condSerieName string, condNo int, condExName string) {
	for _, videoFilepath := range category.JSONVideoFiles() {
		videoFilename := videoFilepath.Base()
		var captionSet CaptionSet
		captionSetFile := category.JSONCaptionSetFile(videoFilename)
		read := ison.ReadJSON(&captionSet, captionSetFile)
		if !read {
			continue
		}
		var video Video
		read = ison.ReadJSON(&video, category.JSONVideoFile(captionSet.VideoFile))
		if !read {
			log.Panicf("ファイルがありません %s", category.JSONVideoFile(captionSet.VideoFile))
		}
		// ビデオがまだアップロードされていないと字幕はアップロードできない
		if video.ID == "" {
			continue
		}
		if category.IsGrammar() {
			serieName, sentenceNo, exName := video.File.SerieNameAndNoAndExerciseName()
			if condSerieName != "" && serieName != condSerieName {
				continue
			}
			if condNo != 0 && sentenceNo != condNo {
				continue
			}
			if condExName != "" && condExName != exName {
				continue
			}
		}
		for i, caption := range captionSet.Captions {
			if caption.ID == "" {
				id := insertCaption(category, caption, video)
				fmt.Printf("[Youtube] insert caption succeed! videoFile: %s, caption ID: %s\n", videoFilename, id)
				captionSet.Captions[i].ID = id
				captionSet.Captions[i].YoutubeSynced = true
				ison.WriteJSON(&captionSet, captionSetFile)
			} else if !caption.YoutubeSynced {
				updateCaption(category, caption, video)
				fmt.Printf("[Youtube] update caption succeed! videoFile: %s, caption ID: %s\n", videoFilename, caption.ID)
				captionSet.Captions[i].YoutubeSynced = true
				ison.WriteJSON(&captionSet, captionSetFile)
			}
		}
	}
}

func insertCaption(category ison.Category, caption Caption, video Video) string {
	service := getService(false)
	for {
		upload := &youtube.Caption{
			Snippet: &youtube.CaptionSnippet{
				VideoId:  video.ID,
				Language: caption.Lang.String(),
				Name:     caption.CaptionName,
			},
		}
		call := service.Captions.Insert("snippet", upload)

		file, err := os.Open(category.BinaryCaptionSBVFile(video.File, caption.CaptionKey, caption.Lang).String())
		defer file.Close()
		if err != nil {
			log.Panicf("Error opening %v: %v", "", err)
		}
		response, err := call.Media(file).Do()
		if err != nil && (strings.Contains(err.Error(), "quotaExceeded") || strings.Contains(err.Error(), "dailyLimitExceeded")) {
			fmt.Printf("%v\n", err)
			service = getService(true)
			continue
		} else if err != nil && strings.Contains(err.Error(), "captionExists") {
			fmt.Printf("captionExists %v", err)
			return "-"
		} else if err != nil {
			log.Panicf("insertCaption error: %s, %v", video.File, err)
		}
		return response.Id
	}
}

func updateCaption(category ison.Category, caption Caption, video Video) {
	service := getService(false)
	for {
		call := service.Captions.List("snippet", video.ID)
		call.Id(caption.ID)
		response, err := call.Do()
		if err != nil && (strings.Contains(err.Error(), "quotaExceeded") || strings.Contains(err.Error(), "dailyLimitExceeded")) {
			fmt.Printf("%v\n", err)
			service = getService(true)
			continue
		} else if err != nil {
			log.Panicf("updateCaption error: %v, %v", video, err)
		}
		youtubeCaption := response.Items[0]
		youtubeCaption.Snippet.Name = caption.CaptionName
		/*
			if caption.CaptionKey == "ruby" {
				youtubeCaption.Snippet.TrackKind = "commentary"
			} else if caption.CaptionKey == "translation" {
				youtubeCaption.Snippet.TrackKind = "primary"
			}
		*/

		file, err := os.Open(category.BinaryCaptionSBVFile(video.File, caption.CaptionKey, caption.Lang).String())
		defer file.Close()
		if err != nil {
			log.Panicf("Error opening %v: %v", "", err)
		}
		updateCall := service.Captions.Update("snippet", youtubeCaption)
		_, err = updateCall.Media(file).Do()
		if err != nil && (strings.Contains(err.Error(), "quotaExceeded") || strings.Contains(err.Error(), "dailyLimitExceeded")) {
			fmt.Printf("%v\n", err)
			service = getService(true)
			continue
		} else if err != nil {
			log.Panicf("uploadVideo error: %s, %v", video.File, err)
		}
		return
	}
}

func uploadVideo(category ison.Category, video Video, nextPublishAt string) string {
	service := getService(false)
	for {
		localizations := make(map[string]youtube.VideoLocalization)
		for _, l := range video.Localizations {
			localizations[l.Lang.String()] = youtube.VideoLocalization{Title: l.Title, Description: l.Description}
		}
		upload := &youtube.Video{
			Localizations: localizations,
			Snippet: &youtube.VideoSnippet{
				Title:                video.Title,
				Description:          video.Description,
				CategoryId:           "27", // Education
				DefaultLanguage:      video.DefaultLang.String(),
				DefaultAudioLanguage: video.AudioLang.String(),
				Tags:                 ison.SplitTags(video.VideoTags),
			},
			Status: &youtube.VideoStatus{
				PrivacyStatus: "private",
				License:       "creativeCommon",
				PublishAt:     nextPublishAt,
			},
		}
		call := service.Videos.Insert("snippet,status,localizations", upload)
		call.AutoLevels(false)
		call.NotifySubscribers(false)
		call.Stabilize(false)

		file, err := os.Open(category.BinaryVideoFile(video.File).String())
		defer file.Close()
		if err != nil {
			log.Panicf("Error opening %v: %v", "", err)
		}
		response, err := call.Media(file).Do()
		if err != nil && (strings.Contains(err.Error(), "quotaExceeded") || strings.Contains(err.Error(), "dailyLimitExceeded")) {
			fmt.Printf("%v\n", err)
			service = getService(true)
			continue
		} else if err != nil {
			log.Panicf("uploadVideo error: %s, %v", video.File, err)
		}
		return response.Id
	}
}

func uploadPlaylist(category ison.Category, playlist Playlist) string {
	service := getService(false)
	for {
		localizations := make(map[string]youtube.PlaylistLocalization)
		for _, l := range playlist.Localizations {
			if !l.Lang.IsDisplayLang() || l.Lang == playlist.DefaultLang {
				continue
			}
			localizations[l.Lang.String()] = youtube.PlaylistLocalization{Title: l.Title, Description: l.Description}
		}
		upload := &youtube.Playlist{
			Localizations: localizations,
			Snippet: &youtube.PlaylistSnippet{
				Title:           playlist.Title,
				Description:     playlist.Description,
				DefaultLanguage: playlist.DefaultLang.String(),
				Tags:            ison.SplitTags(playlist.PlaylistTags),
			},
			Status: &youtube.PlaylistStatus{
				PrivacyStatus: "public",
			},
		}
		call := service.Playlists.Insert("snippet,localizations,status", upload)

		response, err := call.Do()
		if err != nil && (strings.Contains(err.Error(), "quotaExceeded") || strings.Contains(err.Error(), "dailyLimitExceeded")) {
			fmt.Printf("%v\n", err)
			service = getService(true)
			continue
		} else if err != nil {
			log.Panicf("uploadPlaylist error: categoryID:%s, DefaultLang:%s, Title:%s, %s, %s, %s, LocalizationNum:%d, %+v", category.ID, playlist.DefaultLang, playlist.Title, playlist.Description, playlist.ID, playlist.Name, len(playlist.Localizations), err)
		}
		return response.Id
	}
}

func updatePlaylist(playlist Playlist) {
	service := getService(false)
	for {
		call := service.Playlists.List("snippet,localizations")
		call = call.Id(playlist.ID)
		response, err := call.Do()
		if err != nil && (strings.Contains(err.Error(), "quotaExceeded") || strings.Contains(err.Error(), "dailyLimitExceeded")) {
			fmt.Printf("%v\n", err)
			service = getService(true)
			continue
		} else if err != nil {
			log.Panicf("updatePlaylist list error: %v, %v", playlist, err)
		}
		v := response.Items[0]
		v.Snippet.Title = playlist.Title
		v.Snippet.Description = playlist.Description
		v.Snippet.Tags = ison.SplitTags(playlist.PlaylistTags)
		v.Snippet.DefaultLanguage = playlist.DefaultLang.String()
		localizations := make(map[string]youtube.PlaylistLocalization)
		for _, l := range playlist.Localizations {
			if !l.Lang.IsDisplayLang() {
				continue
			}
			localizations[l.Lang.String()] = youtube.PlaylistLocalization{Title: l.Title, Description: l.Description}
		}
		v.Localizations = localizations
		updateCall := service.Playlists.Update("snippet,localizations", v)
		_, err = updateCall.Do()
		if err != nil && (strings.Contains(err.Error(), "quotaExceeded") || strings.Contains(err.Error(), "dailyLimitExceeded")) {
			fmt.Printf("%v\n", err)
			service = getService(true)
			continue
		} else if err != nil {
			log.Panicf("updatePlaylist update error: %v, %v", playlist, err)
		}
		return
	}
}

func insertPlaylistItem(playlist Playlist, item PlaylistItem, video Video) string {
	service := getService(false)
	for {
		playlistItem := youtube.PlaylistItem{
			Snippet: &youtube.PlaylistItemSnippet{PlaylistId: playlist.ID, ResourceId: &youtube.ResourceId{Kind: "youtube#video", VideoId: video.ID}},
		}
		insertPlalistItemCall := service.PlaylistItems.Insert("snippet", &playlistItem)
		response, err := insertPlalistItemCall.Do()
		if err != nil && (strings.Contains(err.Error(), "quotaExceeded") || strings.Contains(err.Error(), "dailyLimitExceeded")) {
			fmt.Printf("%v\n", err)
			service = getService(true)
			continue
		} else if err != nil {
			log.Panicf("insertPlaylistItem error: %v, %v, %v", playlist, item, err)
		}
		return response.Id
	}
}

// UpdateChannel は，チャンネルをアップデートします。
func UpdateChannel(category ison.Category) {
	file := category.JSONChannelFile()
	var channel Channel
	ison.ReadJSON(&channel, file)
	if !channel.Updated {
		updateChannel(channel)
		fmt.Printf("[Youtube] Update channel successful!\n")
		channel.Updated = true
		ison.WriteJSON(&channel, file)
	}
}

func updateChannel(channel Channel) {
	service := getService(false)
	for {
		call := service.Channels.List("brandingSettings")
		call = call.Id(channel.ID)
		response, err := call.Do()
		if err != nil && (strings.Contains(err.Error(), "quotaExceeded") || strings.Contains(err.Error(), "dailyLimitExceeded")) {
			fmt.Printf("%v\n", err)
			service = getService(true)
			continue
		} else if err != nil {
			log.Panicf("updateChannel error: %v, %v", channel, err)
		}
		v := response.Items[0]
		v.BrandingSettings.Channel.Description = channel.Description
		v.BrandingSettings.Channel.DefaultLanguage = channel.DefaultLang.String()
		v.BrandingSettings.Channel.Keywords = channel.Keywords

		updateCall := service.Channels.Update("brandingSettings", v)
		_, err = updateCall.Do()
		if err != nil && (strings.Contains(err.Error(), "quotaExceeded") || strings.Contains(err.Error(), "dailyLimitExceeded")) {
			fmt.Printf("%v\n", err)
			service = getService(true)
			continue
		} else if err != nil {
			log.Panicf("updateChannel brandingSettings error: %v, %v", channel, err)
		}

		call = service.Channels.List("localizations")
		call = call.Id(channel.ID)
		response, err = call.Do()
		if err != nil && (strings.Contains(err.Error(), "quotaExceeded") || strings.Contains(err.Error(), "dailyLimitExceeded")) {
			fmt.Printf("%v\n", err)
			service = getService(true)
			continue
		} else if err != nil {
			log.Panicf("updateChannel localizations list call error: %v, %v", channel, err)
		}
		v = response.Items[0]
		localizations := make(map[string]youtube.ChannelLocalization)
		for _, l := range channel.Localizations {
			if l.Lang == channel.DefaultLang {
				continue
			}
			if !l.Lang.IsDisplayLang() {
				continue
			}
			localizations[l.Lang.String()] = youtube.ChannelLocalization{Title: l.Title, Description: l.Description}
		}
		v.Localizations = localizations

		updateCall2 := service.Channels.Update("localizations", v)
		_, err = updateCall2.Do()
		if err != nil && (strings.Contains(err.Error(), "quotaExceeded") || strings.Contains(err.Error(), "dailyLimitExceeded")) {
			fmt.Printf("%v\n", err)
			service = getService(true)
			continue
		} else if err != nil {
			log.Panicf("updateChannel localizations update call error: %v, %v", channel, err)
		}
		return
	}
}
