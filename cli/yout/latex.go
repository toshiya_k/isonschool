package yout

import (
	"bytes"
	"fmt"
	"image"
	"image/draw"
	"image/png"
	"isonschool/cli/colour"
	"isonschool/cli/ison"
	"log"
	"strconv"
	"strings"

	"github.com/mattn/go-pipeline"
)

const (
	questionMask = "/Users/juny/go/src/isonschool/assets/images/question-mask500.png"
	checkMask    = "/Users/juny/go/src/isonschool/assets/images/check-mask500.png"
	crossMask    = "/Users/juny/go/src/isonschool/assets/images/cross-mask500.png"

	mathMaskWidth  = 500
	mathMaskHeight = 500
)

func pngDecode(b []byte) image.Image {
	p, err := png.Decode(bytes.NewBuffer(b))
	if err != nil {
		log.Panicf("%v", err)
	}
	return p
}

func expTexFilepath(no int) ison.Filepath {
	return ison.Join(tmpFolder, fmt.Sprintf("exp%d.tex", no))
}

func expTexPngFilepath(no int) ison.Filepath {
	return ison.Join(tmpFolder, fmt.Sprintf("exp%d.png", no))
}

func texFilepath(eqno int, imageno int) ison.Filepath {
	return ison.Join(tmpFolder, fmt.Sprintf("eq%d-status%d.tex", eqno, imageno))
}

func texPngFilepath(eqno int, imageno int) ison.Filepath {
	return ison.Join(tmpFolder, fmt.Sprintf("eq%d-status%d.png", eqno, imageno))
}

const formulaPaddingLeft = 50
const formulaPaddingTop = 50

// MakePngImages makes png images.
func MakePngImages(exps, eqs []string, palette colour.Palette) ([][]byte, [][5][]byte) {
	var ret [][5][]byte
	var c []string = []string{
		"node",
		"/Users/juny/go/src/isonschool/assets/math2png/math2png.js",
	}
	var isChecks []bool
	clearTmpFolder()
	for no, e := range exps {
		ison.WriteFile(expTexFilepath(no), []byte(e))
		c = append(c, expTexFilepath(no).String(), expTexPngFilepath(no).String())
	}
	for eqno, e := range eqs {
		mathImageTexs, isCheck := makeMathImageTexs(e)
		for imageno, mi := range mathImageTexs {
			ison.WriteFile(texFilepath(eqno, imageno), mi)
			c = append(c, texFilepath(eqno, imageno).String(), texPngFilepath(eqno, imageno).String())
		}
		isChecks = append(isChecks, isCheck)
	}
	c = append(c, "--width", strconv.Itoa(1280-formulaPaddingLeft*2), "--height", strconv.Itoa(620-formulaPaddingTop))

	_, err := pipeline.Output(c)
	if err != nil {
		log.Panicf("%v", err)
	}
	var expImages [][]byte = make([][]byte, len(exps))
	for no := range exps {
		expImages[no] = imageMath(pngDecode(ison.ReadFile(expTexPngFilepath(no))), palette)
	}
	for eqno := range eqs {
		var mathImages [4][]byte
		for imageno := 0; imageno < 3; imageno++ {
			mathImages[imageno] = ison.ReadFile(texPngFilepath(eqno, imageno))
		}
		var r [5][]byte
		r[0] = imageMath(pngDecode(mathImages[0]), palette)                                                                         // 1.
		r[1] = imageMathWithMask(pngDecode(mathImages[1]), palette.Background(), palette.Fore(), questionMask, palette.Secondary()) // 2-1
		if isChecks[eqno] {
			r[2] = imageMathWithMask(pngDecode(mathImages[1]), palette.Background(), palette.Fore(), checkMask, palette.Secondary()) // 2-2
		} else {
			r[2] = imageMathWithMask(pngDecode(mathImages[1]), palette.Background(), palette.Thirdary(), crossMask, palette.Secondary()) // 2-2
		}
		r[3], r[4] = imageMath2(pngDecode(mathImages[2]), pngDecode(mathImages[0]), palette) // 3
		ret = append(ret, r)
	}
	return expImages, ret
}

// MakePngImage makes png images.
func MakePngImage(exp string) image.Image {
	var c []string = []string{
		"node",
		"/Users/juny/go/src/isonschool/assets/math2png/math2png.js",
	}
	clearTmpFolder()
	no := 0
	ison.WriteFile(expTexFilepath(no), []byte(exp))
	c = append(c, expTexFilepath(no).String(), expTexPngFilepath(no).String())
	c = append(c, "--width", strconv.Itoa(1280-formulaPaddingLeft*2), "--height", strconv.Itoa(620-formulaPaddingTop))

	_, err := pipeline.Output(c)
	if err != nil {
		log.Panicf("%v", err)
	}
	return pngDecode(ison.ReadFile(expTexPngFilepath(no)))
}

// makeMathImageTexs makes png images.
func makeMathImageTexs(e string) ([4][]byte, bool) {
	i := strings.Index(e, "[")
	if i < 0 {
		log.Fatalf("[ がありません。%s", e)
	}
	j := strings.Index(e[i:], ":")
	if j < 0 {
		log.Fatalf(": がありません。%s", e)
	}
	k := strings.Index(e[i:], "]")
	if k < 0 || k < j {
		log.Fatalf("] がありません。%s", e)
	}
	j += i
	k += i
	answer := e[i+1 : j]
	fault := e[j+1 : k]
	if fault == "" {
		fault = answer
	}
	var b0 bytes.Buffer
	var b1 bytes.Buffer
	var b2 bytes.Buffer
	var b3 bytes.Buffer
	b0.WriteString(fmt.Sprintf("\\require{color}\n"))
	b0.WriteString("\\begin{eqnarray}\n")
	b0.WriteString(e[:i])
	b0.WriteString(answer)
	b0.WriteString(e[k+1:])
	b0.WriteString("\n")
	b0.WriteString("\\end{eqnarray}\n")

	b1.WriteString(fmt.Sprintf("\\require{color}\n"))
	b1.WriteString("\\begin{eqnarray}\n")
	b1.WriteString(e[:i])
	b1.WriteString(fault)
	b1.WriteString(e[k+1:])
	b1.WriteString("\n")
	b1.WriteString("\\end{eqnarray}\n")

	b2.WriteString(fmt.Sprintf("\\require{color}\n"))
	b2.WriteString("\\begin{eqnarray}\n")
	b2.WriteString(e[:i])
	b2.WriteString(fmt.Sprintf("\\textcolor[rgb]{1,1,1}{%s}", answer))
	b2.WriteString(e[k+1:])
	b2.WriteString("\n")
	b2.WriteString("\\end{eqnarray}\n")

	b3.WriteString(fmt.Sprintf("\\require{color}\n"))
	b3.WriteString("\\begin{eqnarray}\n")
	b3.WriteString(fmt.Sprintf("\\textcolor[rgb]{1,1,1}{%s}", e[:i]))
	b3.WriteString(answer)
	b3.WriteString(fmt.Sprintf("\\textcolor[rgb]{1,1,1}{%s}", e[k+1:]))
	b3.WriteString("\n")
	b3.WriteString("\\end{eqnarray}\n")

	return [4][]byte{b0.Bytes(), b1.Bytes(), b2.Bytes(), b3.Bytes()}, fault == answer
}

/*
func tex2png(t string) []byte {
	// 1280*720
	svg0, _ := pipeline.Output([]string{"echo", t}, []string{"/Users/juny/.nvm/versions/node/v10.16.0/bin/math2image"})
	out0, _ := pipeline.Output([]string{"echo", strings.ReplaceAll(string(svg0), "#ffffff", "rgba(0,0,0,0)")}, []string{"/Users/juny/.nvm/versions/node/v10.16.0/bin/svg2png", "--width", "1280", "--height", "720"})
	return out0
}
*/

func imageMath(m image.Image, palette colour.Palette) []byte {
	img := image.NewRGBA(image.Rect(0, 0, imageWidth, imageHeight))
	draw.Draw(img, img.Bounds(), &image.Uniform{palette.Background().Color()}, image.ZP, draw.Src)
	draw.DrawMask(img, m.Bounds().Add(image.Pt(formulaPaddingLeft, formulaPaddingTop)), image.NewUniform(palette.Fore().Color()), image.ZP, m, image.ZP, draw.Over)
	return pngEncode(img)
}

func imageMath2(m image.Image, m2 image.Image, palette colour.Palette) ([]byte, []byte) {
	img := image.NewRGBA(image.Rect(0, 0, imageWidth, imageHeight))
	draw.Draw(img, img.Bounds(), &image.Uniform{palette.Background().Color()}, image.ZP, draw.Src)
	draw.DrawMask(img, m.Bounds().Add(image.Pt(formulaPaddingLeft, formulaPaddingTop)), image.NewUniform(palette.Fore().Color()), image.ZP, m, image.ZP, draw.Over)
	r0 := pngEncode(img)
	draw.DrawMask(img, m2.Bounds().Add(image.Pt(formulaPaddingLeft, formulaPaddingTop)), image.NewUniform(palette.Primary().Color()), image.ZP, m2, image.ZP, draw.Over)
	draw.DrawMask(img, m.Bounds().Add(image.Pt(formulaPaddingLeft, formulaPaddingTop)), image.NewUniform(palette.Fore().Color()), image.ZP, m, image.ZP, draw.Over)
	r1 := pngEncode(img)
	return r0, r1
}

func imageMathWithMask(m image.Image, background, fore colour.Colour, maskFilepath ison.Filepath, c colour.Colour) []byte {
	img := image.NewRGBA(image.Rect(0, 0, imageWidth, imageHeight))
	draw.Draw(img, img.Bounds(), &image.Uniform{background.Color()}, image.ZP, draw.Src)

	mask := readMask(maskFilepath)
	draw.DrawMask(img, mask.Bounds().Add(image.Pt((imageWidth-mathMaskWidth)/2, (imageHeight-mathMaskHeight)/2)), image.NewUniform(c.Color()), image.ZP, mask, image.ZP, draw.Over)
	draw.DrawMask(img, m.Bounds().Add(image.Pt(formulaPaddingLeft, formulaPaddingTop)), image.NewUniform(fore.Color()), image.ZP, m, image.ZP, draw.Over)
	return pngEncode(img)
}
