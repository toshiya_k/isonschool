package yout

import (
	"isonschool/cli/ison"
	"isonschool/cli/translation"
	"log"
)

// Channel はチャンネルデータです。
type Channel struct {
	ID            string
	Description   string
	DefaultLang   ison.Lang
	Keywords      string
	Localizations Localizations
	Updated       bool
}

// TxtChannel はチャンネルデータです。
type TxtChannel struct {
	ID          string
	Title       string
	Description string
	Lang        ison.Lang
	Keywords    string
}

// MergeTxtChannel は，テキストからJSONデータに変換します。
func MergeTxtChannel(category ison.Category) {
	var txt TxtChannel
	for _, file := range category.TxtChannelFiles() {
		m := ison.NewTxtMarshallerFromFile(file)
		txt = ParseTxtChannel(m)
	}
	var translator = translation.NewTranslator(category)
	var channel Channel
	ison.ReadJSON(&channel, category.JSONChannelFile())
	channel.ID = txt.ID
	description := translator.Translate(txt.Description, txt.Lang)
	if channel.Description != description {
		channel.Description = description
		channel.Updated = false
	}
	if channel.DefaultLang != txt.Lang {
		channel.DefaultLang = txt.Lang
		channel.Updated = false
	}
	if channel.Keywords != txt.Keywords {
		channel.Keywords = txt.Keywords
		channel.Updated = false
	}
	for _, lang := range ison.DisplayLocalizationLangs {
		title := translator.Translate(txt.Title, lang)
		description := translator.Translate(txt.Description, lang)
		if channel.Localizations.Update(lang, title, description) {
			channel.Updated = false
		}
	}
	translator.CheckNeedsTranslation()
	ison.WriteJSON(&channel, category.JSONChannelFile())
}

// ParseTxtChannel は，チャンネルを更新します。
func ParseTxtChannel(m *ison.TxtMarshaller) TxtChannel {
	m.Add("id", func(d interface{}, k, v string) {
		d.(*TxtChannel).ID = v
	})
	m.Add("title", func(d interface{}, k, v string) {
		d.(*TxtChannel).Title = v
	})
	m.Add("description", func(d interface{}, k, v string) {
		d.(*TxtChannel).Description = v
	})
	m.Add("lang", func(d interface{}, k, v string) {
		d.(*TxtChannel).Lang = ison.Lang(v)
	})
	m.Add("keywords", func(d interface{}, k, v string) {
		d.(*TxtChannel).Keywords = v
	})
	var channel TxtChannel
	var success, isEOF bool
	for !isEOF {
		success, isEOF = m.Unmarshal(&channel)
		if success {
			break
		}
	}
	if !success {
		log.Fatalf("ParseTxtChannel パースできませんでした。 %s", m.File())
	}
	return channel
}
