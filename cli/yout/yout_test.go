package yout_test

import (
	"isonschool/cli/ison"
	"isonschool/cli/yout"
	"testing"
)

type parseTextVideoData struct {
	text     string
	expected []yout.TxtVideo
}

func TestParseTxtVideo(t *testing.T) {
	data := []parseTextVideoData{
		parseTextVideoData{
			text:     ``,
			expected: []yout.TxtVideo{},
		},
		parseTextVideoData{
			text: `
[file:en]math-addition_1.webm
[file:zh-Hans]math-addition_2.webm
[title]タイトル
[description]説明
[tags]タグ
`,
			expected: []yout.TxtVideo{
				yout.TxtVideo{
					TxtFiles: []yout.TxtFile{
						yout.TxtFile{
							File: "math-addition_1.webm",
							Lang: "en",
						},
						yout.TxtFile{
							File: "math-addition_2.webm",
							Lang: "zh-Hans",
						},
					},
					TxtLocalizationMap: map[ison.Lang]yout.TxtLocalization{
						"": yout.TxtLocalization{
							Lang:        "",
							Title:       "タイトル",
							Description: "説明",
							Tags:        "タグ",
						},
					},
				},
			},
		},
	}
	for _, d := range data {
		var m = ison.NewTxtMarshallerFromString(d.text)
		rs := yout.ParseTxtVideo(m)
		if len(rs) != len(d.expected) {
			t.Fatalf("%d %d", len(rs), len(d.expected))
		}
		for i, r := range rs {
			e := d.expected[i]
			if len(r.TxtFiles) != len(e.TxtFiles) {
				t.Fatalf("%+v %+v", r.TxtFiles, e.TxtFiles)
			}
			for j, rf := range r.TxtFiles {
				ef := e.TxtFiles[j]
				if rf.File != ef.File {
					t.Fatalf("%s %s", rf.File, ef.File)
				}
				if rf.Lang != ef.Lang {
					t.Fatalf("%s %s", rf.Lang, ef.Lang)
				}
			}
			if len(r.TxtLocalizationMap) != len(e.TxtLocalizationMap) {
				t.Fatalf("TxtLocalizationMap %+v %+v", r.TxtLocalizationMap, e.TxtLocalizationMap)
			}
			for key, rv := range r.TxtLocalizationMap {
				ev := e.TxtLocalizationMap[key]
				if rv.Lang != ev.Lang {
					t.Fatalf("Lang %s %s", rv.Lang, ev.Lang)
				}
				if rv.Title != ev.Title {
					t.Fatalf("Title %s %s", rv.Title, ev.Title)
				}
				if rv.Description != ev.Description {
					t.Fatalf("Description %s %s", rv.Description, ev.Description)
				}
				if rv.Tags != ev.Tags {
					t.Fatalf("Tags %s %s", rv.Tags, ev.Tags)
				}
			}
		}
	}
}

type txtSentenceData struct {
	text     string
	expected yout.TxtSentence
}

func TestParseTxtSentence(t *testing.T) {
	data := []txtSentenceData{
		txtSentenceData{
			text: `
[seriename]grammar-ja-a
[no]1
[videotitle]Time
[box:0]学校は;会議は
[box:1]午前9時に;午前10時半に;午後1時20分に;午後4時15分に
[box:2]始まります;終わります
//
			`,
			expected: yout.TxtSentence{
				SerieName:  "grammar-ja-a",
				No:         1,
				VideoTitle: "Time",
				TxtBoxes: []yout.TxtBox{
					yout.TxtBox{
						PosStr: "0",
						Value:  "学校は;会議は",
					},
					yout.TxtBox{
						PosStr: "1",
						Value:  "午前9時に;午前10時半に;午後1時20分に;午後4時15分に",
					},
					yout.TxtBox{
						PosStr: "2",
						Value:  "始まります;終わります",
					},
				},
			},
		},
	}
	for _, d := range data {
		m := ison.NewTxtMarshallerFromString(d.text)
		r := yout.ParseTxtSentence(m)[0]
		if r.SerieName != d.expected.SerieName {
			t.Errorf("SerieName %s %s", r.SerieName, d.expected.SerieName)
		}
		if r.No != d.expected.No {
			t.Errorf("No %d %d", r.No, d.expected.No)
		}
		if r.VideoTitle != d.expected.VideoTitle {
			t.Errorf("VideoTitle %s %s", r.VideoTitle, d.expected.VideoTitle)
		}
		if len(r.TxtBoxes) != len(d.expected.TxtBoxes) {
			t.Errorf("len(TxtBoxes) %d %d", len(r.TxtBoxes), len(d.expected.TxtBoxes))
		}
		for i, rt := range r.TxtBoxes {
			et := d.expected.TxtBoxes[i]
			if rt.PosStr != et.PosStr {
				t.Errorf("PosStr %s %s", rt.PosStr, et.PosStr)
			}
			if rt.Value != et.Value {
				t.Errorf("Value %s %s", rt.Value, et.Value)
			}
		}
	}
}
