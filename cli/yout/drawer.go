package yout

import (
	"bytes"
	"image"
	"image/color"
	"image/png"
	"isonschool/cli/colour"
	"isonschool/cli/ison"
	"isonschool/cli/translation"
	"log"
	"math"
	"strconv"

	"golang.org/x/image/draw"

	"golang.org/x/image/font"
	"golang.org/x/image/math/fixed"

	"github.com/golang/freetype/truetype"
)

const (
	imageWidth  = 1280
	imageHeight = 720
)

func minI(a, b fixed.Int26_6) fixed.Int26_6 {
	if a < b {
		return a
	}
	return b
}

// IntR は，レファレンス用の大きさです。
type IntR struct {
	r float64
}

// Mul は，掛け算です。
func (i IntR) Mul(j IntR) IntR {
	return ToIntR(i.r * j.r)
}

// Add は，足し算です。
func (i IntR) Add(j IntR) IntR {
	return ToIntR(i.r + j.r)
}

// IntToIntR は， int を IntR に変換します。
func IntToIntR(i int) IntR {
	return IntR{r: float64(i)}
}

// ToIntR は， float64 を IntR に変換します。
func ToIntR(i float64) IntR {
	return IntR{r: i}
}

/*
// Float64 は float64 に変換します。
func (i IntR) Float64() float64 {
	return float64(i)
}

// Floor は int に変換します。
func (i IntR) Floor() int {
	return int(i)
}
*/

// Px は，ピクセルサイズ
func (i IntR) Px(f FtSize) float64 {
	return i.r * f.f / float64(100)
}

// Max は大きい方。
func (i IntR) Max(j IntR) IntR {
	if i.r > j.r {
		return i
	}
	return j
}

// Min は，小さい方。
func (i IntR) Min(j IntR) IntR {
	if i.r < j.r {
		return i
	}
	return j
}

// FtSize は，幅あるいは高さから，フォントサイズを計算します。
func (i IntR) FtSize(px float64) FtSize {
	return ToFtSize(px * 100 / i.r)
}

// ToFtSize は，float64 を FtSize に変更します。
func ToFtSize(f float64) FtSize {
	return FtSize{f: f}
}

// FtSize は，フォントサイズです。
type FtSize struct {
	f float64
}

// Float は，float64にキャストします。
func (f FtSize) Float() float64 {
	return f.f
}

// MulFloat は，掛け算です。
func (f FtSize) MulFloat(a float64) FtSize {
	return ToFtSize(f.f * a)
}

// Mul は，掛け算です。
func (f FtSize) Mul(bunshi, bunbo IntR) FtSize {
	return ToFtSize(f.f * bunshi.r / bunbo.r)
}

// Min は，小さい方。
func (f FtSize) Min(g FtSize) FtSize {
	if f.Float() < g.Float() {
		return f
	}
	return g
}

// OneCharR は，一文字分です。
var OneCharR = ToIntR(100)

// Drawer は，画像を作成します。
type Drawer struct {
	palette colour.Palette
	fonts   [4]Font
}

// DrawFuncs は，描画用関数の集まりです。
type DrawFuncs struct {
	DrawBackground     func(img draw.Image)
	DrawSayImage       func(img draw.Image)
	DrawOriHeaderImage func(img draw.Image, pi int, personName string, g Gender)
	DrawAnsHeaderImage func(img draw.Image, pi int, personName string, g Gender)
	DrawOri            func(img draw.Image, pi int, bi int, cand CandidateWord, hasAlpha bool)
	DrawAns            func(img draw.Image, pi int, bi int, cand CandidateWord, isChange bool)
}

// DrawStatus は，それぞれどのように表示するかを表します。
type DrawStatus struct {
	DrawsSayImage      bool
	DrawsOriHeader     bool
	DrawOriginalStatus DrawOriginalStatus
	DrawsAnsHeader     bool
	DrawAnswerStatus   DrawAnswerStatus
}

/*
func (d DrawStatus) String() string {
	var b bytes.Buffer
	if d.DrawsSayImage {
		b.WriteString("S")
	} else {
		b.WriteString("-")
	}
	if d.DrawsOriHeader {
		b.WriteString("O")
	} else {
		b.WriteString("-")
	}
	if d.DrawsOriginal {
		b.WriteString("o")
	} else {
		b.WriteString("-")
	}
	if d.DrawsAnsHeader {
		b.WriteString("A")
	} else {
		b.WriteString("-")
	}
	b.WriteString(d.DrawAnswerStatus.String())
	return b.String()
}
*/

// DrawAnswerStatus は，解答の表示状態です
type DrawAnswerStatus int

// DrawAnswer
const (
	DrawAnswerNone DrawAnswerStatus = iota
	DrawAllChanges
	DrawMainChange
	DrawAllAnswer
)

/*
func (d DrawAnswerStatus) String() string {
	switch d {
	case DrawAnswerNone:
		return "-"
	case DrawMainChange:
		return "m"
	case DrawAllChanges:
		return "c"
	case DrawAllAnswer:
		return "a"
	}
	log.Panicf("不正なDrawAnswerStatus です。%d", int(d))
	return ""
}
*/

// DrawOriginalStatus は，もとの文の表示状態です
type DrawOriginalStatus int

// DrawOriginal
const (
	DrawOriginalNone DrawOriginalStatus = iota
	DrawOriginalNormal
	DrawOriginalAChanges
	DrawOriginalAMainChange
	DrawOriginalAAll
)

// Font は，フォント情報を保持します。
type Font struct {
	file       ison.Filepath
	font       *truetype.Font
	dr         *font.Drawer
	oneBoundRW IntR
	oneBoundRH IntR
}

// FT は，描画で用いるフォント情報の種類です。
type FT int

// FT は，描画で用いるフォント情報の種類です。
const (
	FTTitle = iota
	FTSubtitle
	FTProblem
	FTRuby
)

// NewDrawer は，新しい Drawer を作成します。
func NewDrawer(palette colour.Palette, titlefont, subtitlefont, problemfont, rubyfont ison.Filepath) *Drawer {
	var ff = func(f ison.Filepath) Font {
		if f.Empty() {
			return Font{}
		}
		ft := readFont(f)
		dr := &font.Drawer{
			Face: truetype.NewFace(ft, &truetype.Options{
				Size: 100,
			}),
		}
		ob, _ := dr.BoundString("W")
		return Font{
			file:       f,
			font:       ft,
			dr:         dr,
			oneBoundRW: IntToIntR(ob.Max.Sub(ob.Min).X.Floor()),
			oneBoundRH: IntToIntR(ob.Max.Sub(ob.Min).Y.Floor()),
		}
	}
	var ret Drawer
	ret.palette = palette
	ret.fonts[FTTitle] = ff(titlefont)
	ret.fonts[FTSubtitle] = ff(subtitlefont)
	ret.fonts[FTProblem] = ff(problemfont)
	ret.fonts[FTRuby] = ff(rubyfont)
	return &ret
}

/*
// Font は，Font を返します。
func (d *Drawer) Font(t FT) *truetype.Font {
	return d.fonts[t].font
}
*/

var faceCache = make(map[ison.Filepath]map[float64]font.Face)

func (d *Drawer) face(ft FT, ftsize FtSize) font.Face {
	if faceCache[d.fonts[ft].file] == nil {
		faceCache[d.fonts[ft].file] = make(map[float64]font.Face)
	}
	if f, ok := faceCache[d.fonts[ft].file][ftsize.Float()]; ok {
		return f
	}
	f := truetype.NewFace(d.fonts[ft].font, &truetype.Options{Size: ftsize.Float()})
	faceCache[d.fonts[ft].file][ftsize.Float()] = f
	return f
}

// DrawString は，文字列を書き込みます。
func (d *Drawer) DrawString(img draw.Image, c color.Color, ft FT, ftsize FtSize, dotX, dotY float64, s string) (int, int) {
	dr := &font.Drawer{
		Dst:  img,
		Src:  image.NewUniform(c),
		Face: d.face(ft, ftsize),
		Dot:  fixed.Point26_6{X: fixed.I(int(dotX)), Y: fixed.I(int(dotY))},
	}
	dr.DrawString(s)
	return dr.Dot.X.Floor(), dr.Dot.Y.Floor()
}

// OneBoundRH は，100ptの 文字 W のBoundを返します。
func (d *Drawer) OneBoundRH(t FT) IntR {
	return d.fonts[t].oneBoundRH
}

// OneBoundRW は，100ptの 文字 W のBoundを返します。
func (d *Drawer) OneBoundRW(t FT) IntR {
	return d.fonts[t].oneBoundRW
}

// BoundRW は， 100ptのBoundの幅を返します。
func (d *Drawer) BoundRW(s string, t FT) IntR {
	r, _, _ := d.BoundR(s, t)
	return r
}

/*
// BoundRH は， 100ptのBoundの高さを返します。
func (d *Drawer) BoundRH(s string, t FT) IntR {
	_, r, _ := d.BoundR(s, t)
	return r
}
*/

// BoundR は，100ptのBoundを返します。
func (d *Drawer) BoundR(s string, t FT) (IntR, IntR, IntR) {
	if d.fonts[t].dr == nil {
		return ToIntR(0), ToIntR(0), ToIntR(0)
	}
	r, advance := d.fonts[t].dr.BoundString(s)
	return IntToIntR(advance.Floor()), IntToIntR(-r.Max.Y.Floor()).Max(IntToIntR(-r.Min.Y.Floor())), IntToIntR(r.Max.Y.Floor()).Max(IntToIntR(r.Min.Y.Floor()))
}

// BoundStringR は，100ptのBoundを返します。
func (d *Drawer) BoundStringR(s string, t FT) (fixed.Rectangle26_6, fixed.Int26_6) {
	if d.fonts[t].dr == nil {
		return fixed.Rectangle26_6{}, fixed.I(0)
	}
	return d.fonts[t].dr.BoundString(s)
}

// MeasureR は，100ptのAdvanceを返します。
func (d *Drawer) MeasureR(s string, t FT) IntR {
	if d.fonts[t].dr == nil {
		return ToIntR(0)
	}
	r := d.fonts[t].dr.MeasureString(s)
	return IntToIntR(r.Floor())
}

var (
	maskCache map[ison.Filepath]image.Image    = make(map[ison.Filepath]image.Image)
	fontCache map[ison.Filepath]*truetype.Font = make(map[ison.Filepath]*truetype.Font)
)

func readFont(file ison.Filepath) *truetype.Font {
	c, ok := fontCache[file]
	if ok {
		return c
	}
	ft, err := truetype.Parse(ison.ReadFile(file))
	if err != nil {
		log.Panicf("%v %s", err, file)
	}
	fontCache[file] = ft
	return ft
}

func readMask(file ison.Filepath) image.Image {
	c, ok := maskCache[file]
	if ok {
		return c
	}
	b := ison.ReadFile(file)
	mask, err := png.Decode(bytes.NewBuffer(b))
	if err != nil {
		log.Panicf("%s %v", file, err)
	}
	maskCache[file] = mask
	return mask
}

func pngEncode(img image.Image) []byte {
	var buf bytes.Buffer
	err := png.Encode(&buf, img)
	if err != nil {
		log.Panicf("%v", err)
	}
	return buf.Bytes()
}

const (
	titleIsonSchoolTextMask = "/Users/juny/go/src/isonschool/assets/images/isonSchool140BoldMask.png"
	titleLogoMask           = "/Users/juny/go/src/isonschool/assets/images/logo150Mask.png"
	sayMask                 = "/Users/juny/go/src/isonschool/assets/images/say512Mask.png"
	personMask              = "/Users/juny/go/src/isonschool/assets/images/person-neutral-mask.png"
	maleMask                = "/Users/juny/go/src/isonschool/assets/images/person-male-mask.png"
	femaleMask              = "/Users/juny/go/src/isonschool/assets/images/person-female-mask.png"
)

/*
var statuses = []string{
	"00a", // まっくろ
	"p0a", // 問題文の顔のみ
	"13a", // すべて
	"10a", // 問題文のみ
	"12a", // 変化するところすべて含めて
	"12b", // 変化するところをすべてみせて，シンキングタイム
	"1pa", // 変化するところは全く見せない
	"1pb", // 変化するところは全く見せない，シンキングタイム
	"p1a", // もとの文なし。主に変化するところだけ。
	"p1b", // もとの文なし。主に変化するところだけ。シンキングタイム
}
*/

// DrawStatuses は，すべての必要なステータス。
var DrawStatuses = map[string]DrawStatus{
	"00a": DrawStatus{},                                                                                                                                            // まっくろ
	"p0A": DrawStatus{DrawsOriHeader: true},                                                                                                                        // 問題文の顔のみ
	"13a": DrawStatus{DrawsOriHeader: true, DrawsAnsHeader: true, DrawsSayImage: true, DrawOriginalStatus: DrawOriginalAAll, DrawAnswerStatus: DrawAllAnswer},      // すべて
	"10a": DrawStatus{DrawsOriHeader: true, DrawOriginalStatus: DrawOriginalNormal},                                                                                // 問題文のみ
	"12a": DrawStatus{DrawsOriHeader: true, DrawOriginalStatus: DrawOriginalAChanges, DrawsAnsHeader: true, DrawAnswerStatus: DrawAllChanges},                      // 変化するところすべて含めて
	"12b": DrawStatus{DrawsOriHeader: true, DrawOriginalStatus: DrawOriginalAChanges, DrawsAnsHeader: true, DrawAnswerStatus: DrawAllChanges, DrawsSayImage: true}, // 変化するところをすべてみせて，シンキングタイム
	"1pa": DrawStatus{DrawsOriHeader: true, DrawOriginalStatus: DrawOriginalAMainChange, DrawsAnsHeader: true},                                                     // 変化するところは全く見せない
	"1pb": DrawStatus{DrawsOriHeader: true, DrawOriginalStatus: DrawOriginalAMainChange, DrawsAnsHeader: true, DrawsSayImage: true},                                // 変化するところは全く見せない，シンキングタイム
	"p1a": DrawStatus{DrawsOriHeader: true, DrawsAnsHeader: true, DrawAnswerStatus: DrawMainChange},                                                                // もとの文なし。主に変化するところだけ。
	"p1b": DrawStatus{DrawsOriHeader: true, DrawsAnsHeader: true, DrawAnswerStatus: DrawMainChange, DrawsSayImage: true},                                           // もとの文なし。主に変化するところだけ。シンキングタイム
}

// SetupData は，セットアップします。
func (d *Drawer) SetupData(serie Serie, sentence Sentence, exercise Exercise) DrawFuncs {
	var maxSumRW, maxTextRA, maxTextRD, maxRubyRA, maxRubyRD IntR
	boxMaxRWs := make([]IntR, len(sentence.Boxes))
	parSumRWs := make([]IntR, len(sentence.Pars))
	var hasHeader bool
	for pi, par := range sentence.Pars {
		if par.Header != HeaderNone {
			hasHeader = true
		}
		for _, bi := range par.Poses {
			var max IntR
			for _, pr := range exercise.Problems {
				for _, cand := range []CandidateWord{pr.OriginalCandidates[bi], pr.AnswerCandidates[bi]} {
					bw, ba, bd := d.BoundR(cand.Word, FTProblem)
					_, bra, brd := d.BoundR(cand.Ruby, FTRuby)
					max = max.Max(bw)
					maxTextRA = maxTextRA.Max(ba)
					maxTextRD = maxTextRA.Max(bd)
					maxRubyRA = maxRubyRA.Max(bra)
					maxRubyRD = maxRubyRA.Max(brd)
				}
			}
			boxMaxRWs[bi] = max.Add(d.OneBoundRW(FTProblem))
			parSumRWs[pi] = parSumRWs[pi].Add(boxMaxRWs[bi])
		}
		maxSumRW = maxSumRW.Max(parSumRWs[pi])
	}
	headerRW := OneCharR
	if !hasHeader {
		headerRW = ToIntR(0)
	}
	var textFtSize FtSize = maxSumRW.Add(headerRW.Mul(ToIntR(2))).FtSize(float64(imageWidth - 100))
	//log.Println("textFtSize", textFtSize, "maxSumRW", maxSumRW, "headerRW", headerRW)
	var rubyFtSize FtSize = ToFtSize(100000)
	for bi := range sentence.Boxes {
		for _, p := range exercise.Problems {
			for _, cand := range []CandidateWord{p.OriginalCandidates[bi], p.AnswerCandidates[bi]} {
				w := textFtSize.Mul(boxMaxRWs[bi], d.BoundRW(cand.Ruby, FTRuby).Add(d.OneBoundRW(FTRuby)))
				rubyFtSize = rubyFtSize.Min(w)
			}
		}
	}
	if serie.HidesRuby {
		rubyFtSize = ToFtSize(0)
	}
	textH := maxTextRA.Add(maxTextRD).Px(textFtSize)
	rubyH := maxRubyRA.Add(maxRubyRD).Px(rubyFtSize)
	const paddingTop = 50
	if float64(imageHeight-paddingTop*(len(sentence.Pars)+1))-float64(2*len(sentence.Pars))*(textH+rubyH) < 0 {
		hasToDecrease := textH + rubyH - float64(imageHeight-paddingTop*(len(sentence.Pars)+1))/float64(2*len(sentence.Pars))
		log.Printf("hastodecrease %f", hasToDecrease)
		textH -= hasToDecrease * textFtSize.Float() / (textFtSize.Float() + rubyFtSize.Float())
		rubyH -= hasToDecrease * rubyFtSize.Float() / (textFtSize.Float() + rubyFtSize.Float())
		textFtSize = maxTextRA.Add(maxTextRD).FtSize(textH)
		rubyFtSize = maxRubyRA.Add(maxRubyRD).FtSize(rubyH)
	}
	textH = maxTextRA.Add(maxTextRD).Px(textFtSize)
	rubyH = maxRubyRA.Add(maxRubyRD).Px(rubyFtSize)
	var extraH = float64(imageHeight-paddingTop*(len(sentence.Pars)+1)) - 2*(textH+rubyH)*float64(len(sentence.Pars))
	var extraHPortion = extraH / 10 / float64(len(sentence.Pars))
	headerW := headerRW.Px(textFtSize)
	topMargin := float64(paddingTop) + extraHPortion*3
	l := math.Min(6*topMargin/10, 80)

	var drawBackground = func(img draw.Image) {
		draw.Draw(img, img.Bounds(), image.NewUniform(d.palette.Background().Color()), image.ZP, draw.Src)
	}

	sayImageMask := readMask(sayMask)
	var drawSayImage = func(img draw.Image) {
		draw.CatmullRom.Scale(
			img,
			image.Rectangle{
				Min: image.Point{
					X: int((imageWidth - l) / 2),
					Y: int(topMargin / 5),
				},
				Max: image.Point{
					X: int((imageWidth + l) / 2),
					Y: int(topMargin/5 + l),
				},
			},
			image.NewUniform(d.palette.Primary().Color()),
			image.Rect(0, 0, 512, 512),
			draw.Over,
			&draw.Options{SrcMask: sayImageMask, SrcMaskP: image.ZP},
		)
	}

	headerMask := []image.Image{readMask(personMask), readMask(maleMask), readMask(femaleMask)}

	log.Printf("extraHPortion: %f\n", extraHPortion)
	var calY = func(pi int) (float64, float64, float64, float64) {
		oriRubyY := float64(paddingTop*(1+pi)) + (rubyH+textH)*2*float64(pi) + extraHPortion*(10*float64(pi)+3)
		oriTextY := oriRubyY + rubyH + extraHPortion
		ansTextY := oriTextY + textH + extraHPortion*2
		ansRubyY := ansTextY + textH + extraHPortion
		return oriRubyY, oriTextY, ansTextY, ansRubyY
	}

	var personColorMap = make(map[string]colour.Colour)
	var colourUsedMap = make(map[colour.Colour]int)
	var personColor = func(personName string) colour.Colour {
		if c, ok := personColorMap[personName]; ok {
			return c
		}
		for n := 0; n < 2; n++ {
			for _, c := range []colour.Colour{d.palette.Secondary(), d.palette.Thirdary(), d.palette.Primary()} {
				if colourUsedMap[c] == n {
					colourUsedMap[c]++
					personColorMap[personName] = c
					return c
				}
			}
		}
		log.Panicf("personColor 決められません。 %s %+v %+v", personName, personColorMap, colourUsedMap)
		return d.palette.Secondary()
	}
	var drawOriHeaderImage = func(img draw.Image, pi int, personName string, g Gender) {
		_, oriTextY, _, _ := calY(pi)
		draw.CatmullRom.Scale(
			img,
			image.Rectangle{
				Min: image.Point{
					X: 50,
					Y: int(oriTextY - maxTextRD.Px(textFtSize)/10),
				},
				Max: image.Point{
					X: 50 + int(headerW),
					Y: int(oriTextY + headerW - maxTextRD.Px(textFtSize)/10),
				},
			},
			image.NewUniform(personColor(personName).Color()),
			image.Rect(0, 0, 512, 512),
			draw.Over,
			&draw.Options{SrcMask: headerMask[g], SrcMaskP: image.ZP},
		)
	}
	var drawAnsHeaderImage = func(img draw.Image, pi int, personName string, g Gender) {
		_, _, ansTextY, _ := calY(pi)
		draw.CatmullRom.Scale(
			img,
			image.Rectangle{
				Min: image.Point{
					X: 50,
					Y: int(ansTextY),
				},
				Max: image.Point{
					X: 50 + int(headerW),
					Y: int(ansTextY + headerW),
				},
			},
			image.NewUniform(personColor(personName).Color()),
			image.Rect(0, 0, 512, 512),
			draw.Over,
			&draw.Options{SrcMask: headerMask[g], SrcMaskP: image.ZP},
		)
	}

	var xx = headerW/2 + (imageWidth-maxSumRW.Px(textFtSize))/2
	var drawOri = func(img draw.Image, pi, bi int, cand CandidateWord, hasAlpha bool) {
		oriRubyY, oriTextY, _, _ := calY(pi)
		x := xx
		//log.Println("extraHPortion", extraHPortion, "oriRubyY", oriRubyY, "oriTextY", oriTextY, "x", x, "textFtSize", textFtSize)
		for _, pos := range sentence.Pars[pi].Poses {
			if pos == bi {
				break
			}
			x += boxMaxRWs[pos].Px(textFtSize)
		}
		c := d.palette.Fore().Color()
		if hasAlpha {
			c = d.palette.Fore().ColorA()
		}
		//log.Println(pi, bi, cand.Word, x, sentence.Pars[pi].Poses, boxMaxRWs[bi], boxMaxRWs)
		d.DrawString(img, c, FTProblem, textFtSize, x+(boxMaxRWs[bi].Px(textFtSize)-d.BoundRW(cand.Word, FTProblem).Px(textFtSize))/2, oriTextY+maxTextRA.Px(textFtSize), cand.Word)
		if !serie.HidesRuby {
			d.DrawString(img, c, FTRuby, rubyFtSize, x+(boxMaxRWs[bi].Px(textFtSize)-d.BoundRW(cand.Ruby, FTRuby).Px(rubyFtSize))/2, oriRubyY+maxRubyRA.Px(rubyFtSize), cand.Ruby)
		}
	}

	var drawAns = func(img draw.Image, pi, bi int, cand CandidateWord, isChange bool) {
		_, _, ansTextY, ansRubyY := calY(pi)
		//log.Println("残り", imageHeight-ansRubyY-rubyH, "oriRubyY", oriRubyY, "oriTextY", oriTextY, "ansRubyY", ansRubyY, "ansTextY", ansTextY, "extraHPortion", extraHPortion, "rubyH", rubyH, "textH", textH)
		x := xx
		for _, pos := range sentence.Pars[pi].Poses {
			if pos == bi {
				break
			}
			x += boxMaxRWs[pos].Px(textFtSize)
		}
		c := d.palette.Fore().Color()
		if isChange {
			c = d.palette.Primary().Color()
		}
		d.DrawString(img, c, FTProblem, textFtSize, x+(boxMaxRWs[bi].Px(textFtSize)-d.BoundRW(cand.Word, FTProblem).Px(textFtSize))/2, ansTextY+maxTextRA.Px(textFtSize), cand.Word)
		if !serie.HidesRuby {
			d.DrawString(img, c, FTRuby, rubyFtSize, x+(boxMaxRWs[bi].Px(textFtSize)-d.BoundRW(cand.Ruby, FTRuby).Px(rubyFtSize))/2, ansRubyY+maxRubyRA.Px(rubyFtSize), cand.Ruby)
		}
	}

	return DrawFuncs{
		DrawBackground:     drawBackground,
		DrawSayImage:       drawSayImage,
		DrawOriHeaderImage: drawOriHeaderImage,
		DrawAnsHeaderImage: drawAnsHeaderImage,
		DrawOri:            drawOri,
		DrawAns:            drawAns,
	}
}

// DrawProblem は，問題を画像化します。
func (d *Drawer) DrawProblem(serie Serie, sentence Sentence, exercise Exercise, problem Problem, status DrawStatus, funcs DrawFuncs) []byte {
	img := image.NewRGBA(image.Rect(0, 0, imageWidth, imageHeight))
	funcs.DrawBackground(img)
	if status.DrawsSayImage {
		funcs.DrawSayImage(img)
	}
	for pi, par := range sentence.Pars {
		oriGender := sentence.GenderMap(problem.OriginalCandidates)[sentence.Boxes[par.Poses[0]].PersonName]
		ansGender := sentence.GenderMap(problem.AnswerCandidates)[sentence.Boxes[par.Poses[0]].PersonName]
		if status.DrawsOriHeader {
			funcs.DrawOriHeaderImage(img, pi, sentence.Boxes[par.Poses[0]].PersonName, oriGender)
		}
		if status.DrawsAnsHeader && oriGender != ansGender {
			funcs.DrawAnsHeaderImage(img, pi, sentence.Boxes[par.Poses[0]].PersonName, ansGender)
		}
		for _, i := range par.Poses {
			if status.DrawOriginalStatus != DrawOriginalNone {
				funcs.DrawOri(img, pi, i, problem.OriginalCandidates[i], status.DrawOriginalStatus == DrawOriginalAAll || (status.DrawOriginalStatus == DrawOriginalAMainChange && problem.ChangeCandidatePos == i) || (status.DrawOriginalStatus == DrawOriginalAChanges && problem.ChangeCandidates[i].Word != ""))
			}
			switch status.DrawAnswerStatus {
			case DrawMainChange:
				if problem.ChangeCandidatePos == i {
					funcs.DrawAns(img, pi, i, problem.ChangeCandidates[i], true)
				}
			case DrawAllChanges:
				funcs.DrawAns(img, pi, i, problem.ChangeCandidates[i], true)
			case DrawAllAnswer:
				funcs.DrawAns(img, pi, i, problem.AnswerCandidates[i], problem.ChangeCandidates[i].Word != "")
			}
		}
	}
	return pngEncode(img)
}

// DrawTitle は，タイトル画面を作成します。
func (d *Drawer) DrawTitle(title, subtitle string) []byte {
	img := image.NewRGBA(image.Rect(0, 0, imageWidth, imageHeight))
	draw.Draw(img, img.Bounds(), &image.Uniform{d.palette.Background().Color()}, image.ZP, draw.Src)
	titleTextMask := readMask(titleIsonSchoolTextMask)
	draw.DrawMask(img, titleTextMask.Bounds().Add(image.Pt(320, 147)), image.NewUniform(d.palette.Fore().Color()), image.ZP, titleTextMask, image.ZP, draw.Over)
	titleIconMask := readMask(titleLogoMask)
	draw.DrawMask(img, titleIconMask.Bounds().Add(image.Pt(115, 140)), image.NewUniform(d.palette.Primary().Color()), image.ZP, titleIconMask, image.ZP, draw.Over)

	boundRW, aR, dR := d.BoundR(title, FTTitle)
	ftsize := boundRW.FtSize(imageWidth - 200).Min(aR.Add(dR).FtSize(imageHeight / 5))
	_, doty := d.DrawString(img, d.palette.Fore().Color(), FTTitle, ftsize, (imageWidth-boundRW.Px(ftsize))/2, 400+aR.Px(ftsize), title)
	//_, doty := d.DrawString(img, d.palette.Fore(), FTTitle, ftsize, (imageWidth-boundRW.Px(ftsize))/2, 430, title)

	subboundRW, saR, _ := d.BoundR(subtitle, FTSubtitle)
	subftsize := subboundRW.FtSize(imageWidth - 200).Min(aR.Add(dR).FtSize(float64(imageHeight - doty - 100))).Min(ftsize.MulFloat(1.5))
	//log.Printf("%v, %v", ftsize, subftsize)
	//log.Printf("%v, %v", ftsize, subftsize)
	d.DrawString(img, d.palette.Secondary().Color(), FTSubtitle, subftsize, (imageWidth-subboundRW.Px(subftsize))/2, float64(doty+40)+1.3*saR.Px(subftsize), subtitle)
	//d.DrawString(img, d.palette.Secondary(), FTSubtitle, subftsize, (imageWidth-subboundRW.Px(subftsize))/2, float64(doty+40)+1.3*subboundRH.Px(subftsize), subtitle)
	return pngEncode(img)
}

// DrawIsonSchoolTitle は，タイトル画面を作成します。
func (d *Drawer) DrawIsonSchoolTitle() []byte {
	img := image.NewRGBA(image.Rect(0, 0, imageWidth, imageHeight))
	draw.Draw(img, img.Bounds(), &image.Uniform{d.palette.Background().Color()}, image.ZP, draw.Src)
	titleTextMask := readMask(titleIsonSchoolTextMask)
	titleIconMask := readMask(titleLogoMask)
	y := (imageHeight - titleIconMask.Bounds().Dy()) / 2
	draw.DrawMask(img, titleTextMask.Bounds().Add(image.Pt(320, y+7)), image.NewUniform(d.palette.Fore().Color()), image.ZP, titleTextMask, image.ZP, draw.Over)
	draw.DrawMask(img, titleIconMask.Bounds().Add(image.Pt(115, y)), image.NewUniform(d.palette.Primary().Color()), image.ZP, titleIconMask, image.ZP, draw.Over)
	return pngEncode(img)
}

// DrawLogo は，いちょうのロゴを n 個出力します。
func (d *Drawer) DrawLogo(n int) []byte {
	img := image.NewRGBA(image.Rect(0, 0, imageWidth, imageHeight))
	draw.Draw(img, img.Bounds(), &image.Uniform{d.palette.Background().Color()}, image.ZP, draw.Src)
	titleIconMask := readMask(titleLogoMask)
	nf := float64(n)
	totalW := float64(titleIconMask.Bounds().Dx()) * (nf + (nf-1)/2)
	x := (imageWidth - totalW) / 2
	y := (imageHeight - titleIconMask.Bounds().Dy()) / 2
	for i := 0; i < n; i++ {
		draw.DrawMask(img, titleIconMask.Bounds().Add(image.Pt(int(x), int(y))), image.NewUniform(d.palette.Primary().Color()), image.ZP, titleIconMask, image.ZP, draw.Over)
		x += float64(titleIconMask.Bounds().Dx()) * 1.5
	}
	return pngEncode(img)
}

// DrawThumbnail は，サムネール画像を作成します。
func (d *Drawer) DrawThumbnail(subtitle string) []byte {
	var c = d.palette.Background().Color()
	if colour.CheckContrastRatio(d.palette.Primary(), d.palette.Thirdary()) {
		c = d.palette.Thirdary().Color()
	} else if colour.CheckContrastRatio(d.palette.Primary(), d.palette.Secondary()) {
		c = d.palette.Secondary().Color()
	} else if d.palette.IsDarkMode() && colour.CheckContrastRatio(d.palette.Primary(), d.palette.Fore()) {
		c = d.palette.Fore().Color()
	}
	img := image.NewRGBA(image.Rect(0, 0, imageWidth, imageHeight))
	draw.Draw(img, img.Bounds(), &image.Uniform{d.palette.Primary().Color()}, image.Point{}, draw.Src)
	bRW, bRA, bRD := d.BoundR(subtitle, FTSubtitle)
	subftsize := bRW.FtSize(imageWidth - 100).Min(bRA.Add(bRD).FtSize(float64(imageHeight - 100)))
	d.DrawString(img, c, FTSubtitle, subftsize, (imageWidth-bRW.Px(subftsize))/2, (imageHeight-bRA.Add(bRD).Px(subftsize))/2+bRA.Px(subftsize), subtitle)
	return pngEncode(img)
}

// DrawFormulaThumbnail は，サムネール画像を作成します。
func (d *Drawer) DrawFormulaThumbnail(exp string) []byte {
	var c = d.palette.Background().Color()
	if colour.CheckContrastRatio(d.palette.Primary(), d.palette.Thirdary()) {
		c = d.palette.Thirdary().Color()
	} else if colour.CheckContrastRatio(d.palette.Primary(), d.palette.Secondary()) {
		c = d.palette.Secondary().Color()
	} else if d.palette.IsDarkMode() && colour.CheckContrastRatio(d.palette.Primary(), d.palette.Fore()) {
		c = d.palette.Fore().Color()
	}
	m := MakePngImage(exp)
	img := image.NewRGBA(image.Rect(0, 0, imageWidth, imageHeight))
	draw.Draw(img, img.Bounds(), &image.Uniform{d.palette.Primary().Color()}, image.ZP, draw.Src)
	draw.DrawMask(img, m.Bounds().Add(image.Pt(formulaPaddingLeft, formulaPaddingTop)), image.NewUniform(c), image.ZP, m, image.ZP, draw.Over)
	return pngEncode(img)
}

// MakeImages は，画像ファイルを作成します。
func MakeImages(category ison.Category, condSerieName string, condNo int, condExName string) {
	category.RemoveAllBinaryImageFiles()
	translator := translation.NewTranslator(category)
	for _, file := range category.JSONGrammarFiles() {
		var sentence Sentence
		ison.ReadJSON(&sentence, file)
		if condSerieName != "" && condSerieName != sentence.SerieName {
			continue
		}
		if condNo > 0 && condNo != sentence.No {
			continue
		}
		for _, e := range sentence.Exercises {
			if condExName != "" && e.Name != condExName {
				continue
			}
			if e.VideoFile != "" {
				continue
			}
			var serie Serie
			ison.ReadJSON(&serie, category.JSONSerieFile(sentence.SerieName, e.Name))

			drawer := NewDrawer(serie.Palettes[(sentence.No-1+len(serie.Palettes))%len(serie.Palettes)], category.FontFile(serie.TitleFont), category.FontFile(serie.SubtitleFont), category.FontFile(serie.ProblemFont), category.FontFile(serie.RubyFont))

			ison.WriteFile(
				category.BinaryImageFile(sentence.SerieName, sentence.No, e.Name, 0, "title"),
				drawer.DrawTitle(translator.Translate(serie.SerieTitle, serie.VideoLang)+" #"+strconv.Itoa(sentence.No), translator.Translate(sentence.VideoTitle, serie.VideoLang)),
			)
			for i := 1; i <= 3; i++ {
				ison.WriteFile(
					category.BinaryImageFile(sentence.SerieName, sentence.No, e.Name, 0, "logo"+strconv.Itoa(i)),
					drawer.DrawLogo(i),
				)
			}
			funcs := drawer.SetupData(serie, sentence, e)
			for _, p := range e.Problems {
				for status, st := range DrawStatuses {
					ison.WriteFile(
						category.BinaryImageFile(sentence.SerieName, sentence.No, e.Name, p.ProblemNo, status),
						drawer.DrawProblem(serie, sentence, e, p, st, funcs),
					)
				}
			}
		}
	}
	translator.CheckNeedsTranslation()
}

// MakeThumbnailImage は，サムネール画像ファイルを作成します。
func MakeThumbnailImage(category ison.Category, condSerieName string, condNo int, condExName string, remake bool) {
	translator := translation.NewTranslator(category)
	for _, file := range category.JSONGrammarFiles() {
		var sentence Sentence
		ison.ReadJSON(&sentence, file)
		if condSerieName != "" && condSerieName != sentence.SerieName {
			continue
		}
		if condNo > 0 && condNo != sentence.No {
			continue
		}
		for _, e := range sentence.Exercises {
			if condExName != "" && e.Name != condExName {
				continue
			}
			if e.VideoFile == "" {
				continue
			}
			var video Video
			ison.ReadJSON(&video, category.JSONVideoFile(e.VideoFile))
			if !remake && video.ThumbnailUploaded {
				continue
			}
			var serie Serie
			ison.ReadJSON(&serie, category.JSONSerieFile(sentence.SerieName, e.Name))

			drawer := NewDrawer(serie.Palettes[(sentence.No-1+len(serie.Palettes))%len(serie.Palettes)], category.FontFile(serie.TitleFont), category.FontFile(serie.SubtitleFont), category.FontFile(serie.ProblemFont), category.FontFile(serie.RubyFont))

			ison.WriteFile(
				category.BinaryThumbnailFile(e.VideoFile),
				drawer.DrawThumbnail(sentence.Thumbnail()),
			)
		}
	}
	translator.CheckNeedsTranslation()
}
