package yout

import (
	"fmt"
	"isonschool/cli/ison"
	"isonschool/cli/mp3"
	"log"
	"os"
	"os/exec"
	"path/filepath"
	"strconv"
	"strings"
)

// MakeFormulaVideo は，ビデオを作成します。
func MakeFormulaVideo(category ison.Category, condSerieName string, condNo int, condExName string) {
	for _, formulaFile := range category.JSONFormulaFiles() {
		var formula Formula
		ison.ReadJSON(&formula, formulaFile)
		if condSerieName != "" && condSerieName != formula.SerieName {
			continue
		}
		if condNo > 0 && condNo != formula.No {
			continue
		}
		for ei, exercise := range formula.FormulaExercises {
			if condExName != "" && exercise.Name != condExName {
				continue
			}
			if !VideoUploaded(&category, exercise.VideoFile) {
				fmt.Printf("ビデオ作成開始 %s-%d\n", formula.SerieName, formula.No)
				clearTmpFolder()
				var serie Serie
				ison.ReadJSON(&serie, category.JSONSerieFile(formula.SerieName, exercise.Name))
				animetic := NewFormulaAnimetic(category, formula, exercise, serie)
				animetic.MakeAnimetic()
				tmpaudio := ison.Join(tmpFolder, "tmp.mp3")
				fmt.Printf("音声ファイル作成 %s-%d\n", formula.SerieName, formula.No)
				animetic.MakeAudio(tmpaudio)
				videoFilename := formulaVideoFilename(formula, exercise)
				animetic.MakeSymbolicLinksAndMakeVideo(tmpFolder, tmpaudio, category.BinaryVideoFile(videoFilename))
				fmt.Printf("ビデオ作成終了 %s-%d %s\n", formula.SerieName, formula.No, videoFilename)
				formula.FormulaExercises[ei].VideoFile = videoFilename
				formula.FormulaExercises[ei].AnimeticCaptions = animetic.Captions
				ison.WriteJSON(&formula, formulaFile)
			}
		}
	}
}

func formulaVideoFilename(formula Formula, exercise FormulaExercise) ison.Filename {
	return ison.ToFilename(formula.SerieName + "_" + strconv.Itoa(formula.No) + "_" + exercise.Name + ".mp4")
}

// FormulaAnimetic は，動画の1秒ごとのフレームです。
type FormulaAnimetic struct {
	Images   []AnimeticImage
	Sounds   []AnimeticSound
	Captions []AnimeticCaption
	category ison.Category
	formula  Formula
	exercise FormulaExercise
	serie    Serie
}

// NewFormulaAnimetic は，
func NewFormulaAnimetic(category ison.Category, formula Formula, exercise FormulaExercise, serie Serie) *FormulaAnimetic {
	return &FormulaAnimetic{category: category, formula: formula, exercise: exercise, serie: serie}
}

// ImageFilepath は，ファイルパスを返します。
func (a *FormulaAnimetic) ImageFilepath(ai AnimeticImage) ison.Filepath {
	return a.category.BinaryImageFile(a.formula.SerieName, a.formula.No, a.exercise.Name, ai.ProblemNo, ai.Status)
}

// MakeSymbolicLinksAndMakeVideo は，シンボリックリンクを作成します。
func (a *FormulaAnimetic) MakeSymbolicLinksAndMakeVideo(folder string, audiooutput, videooutput ison.Filepath) {
	var i int
	for _, image := range a.Images {
		f := a.ImageFilepath(image)
		if _, err := os.Stat(string(f)); os.IsNotExist(err) {
			log.Panicf("MakeSymbolicLinks %s が存在しません。", f)
		}
		for m := image.Start; m < image.End; m++ {
			err := os.Symlink(string(f), filepath.Join(folder, fmt.Sprintf("img_%03d.png", i)))
			if err != nil {
				log.Panicf("MakeSymbolicLinks %s : %v", folder, err)
			}
			i++
		}
	}

	os.MkdirAll(videooutput.Dir(), 0755)

	// ffmpeg -r 30 -i xxx_%3d.jpeg test.mp4
	var command = "ffmpeg -loglevel error -y -i " + string(audiooutput) + " -r 1 -i " + filepath.Join(folder, "img_%03d.png") + " -pix_fmt yuv420p " + string(videooutput)
	c := strings.Split(command, " ")
	cmd := exec.Command(c[0], c[1:]...)
	//cmd.Dir = cm.cmddir
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stdout
	err := cmd.Run()
	if err != nil {
		log.Panicf("%s : %v", videooutput, err)
	}
}

// MakeAudio は，複数の音声ファイルと無音ファイルをつないだファイルを作成します。
func (a *FormulaAnimetic) MakeAudio(output ison.Filepath) {
	var inputfiles []string
	var nosoundlengths []float64
	for _, s := range a.Sounds {
		inputfiles = append(inputfiles, s.Filepath.String())
		nosoundlengths = append(nosoundlengths, s.NoSoundLength)
	}
	mp3.Merge(output.String(), inputfiles, noSoundFile, nosoundlengths)
}

func (a *FormulaAnimetic) makeAnimetic(time int, endImageStatus string, statuses ...string) int {
	const blankSeconds = 3
	for problemNo := range a.formula.Equations {
		for i, status := range statuses {
			var plus = 3
			if i == 0 && a.formula.Time > 0 {
				plus = a.formula.Time
			}
			time = a.appendFrames(problemNo, status, plus, SoundKindNoSound, CaptionKindFormulaEquation, time)
		}
	}
	a.Images = append(a.Images, AnimeticImage{
		Start:  time,
		End:    time + blankSeconds,
		Status: endImageStatus,
	})
	a.Sounds = append(a.Sounds, AnimeticSound{
		Filepath:      titleSoundFile,
		SoundLength:   titleSoundLength,
		NoSoundLength: blankSeconds - titleSoundLength,
	})
	a.Captions = append(a.Captions, AnimeticCaption{
		Lang:  "",
		Text:  "",
		Ruby:  "",
		Start: time,
		End:   time + blankSeconds,
		Kind:  CaptionKindBlank,
	})
	time += blankSeconds
	return time
}

func (a *FormulaAnimetic) makeExpAnimetic(time int, endImageStatus string) int {
	const blankSeconds = 3
	const plus = 6
	for no := range a.formula.Explanations {
		time = a.appendFrames(0, "exp"+strconv.Itoa(no), plus, SoundKindNoSound, CaptionKindFormulaExplanation, time)
	}
	a.Images = append(a.Images, AnimeticImage{
		Start:  time,
		End:    time + blankSeconds,
		Status: endImageStatus,
	})
	a.Sounds = append(a.Sounds, AnimeticSound{
		Filepath:      titleSoundFile,
		SoundLength:   titleSoundLength,
		NoSoundLength: blankSeconds - titleSoundLength,
	})
	a.Captions = append(a.Captions, AnimeticCaption{
		Lang:  "",
		Text:  "",
		Ruby:  "",
		Start: time,
		End:   time + blankSeconds,
		Kind:  CaptionKindBlank,
	})
	time += blankSeconds
	return time
}

/*
MakeAnimetic は，アニメティックを作ります。
*/
func (a *FormulaAnimetic) MakeAnimetic() int {
	const titleSeconds = 3
	var time int
	const openingProblemNo = 3
	time = a.appendFrames(openingProblemNo, "tex1", 2, SoundKindNoSound, CaptionKindFormulaEquation, time)
	a.Images = append(a.Images, AnimeticImage{
		Start:  time,
		End:    time + titleSeconds,
		Status: "title",
	})
	a.Sounds = append(a.Sounds, AnimeticSound{
		Filepath:      titleSoundFile,
		SoundLength:   titleSoundLength,
		NoSoundLength: titleSeconds - titleSoundLength,
	})
	a.Captions = append(a.Captions, AnimeticCaption{Lang: "",
		Text:  a.serie.SerieTitle + " #" + strconv.Itoa(a.formula.No) + " " + a.formula.VideoTitle,
		Ruby:  "",
		Start: time,
		End:   time + titleSeconds,
		Kind:  CaptionKindTitle,
	})
	time += titleSeconds

	time = a.makeExpAnimetic(time, "logo2")
	//time = a.makeAnimetic(time, "logo2", "tex0")
	time = a.makeAnimetic(time, "logo3", "tex1", "tex2")
	time = a.makeAnimetic(time, "logo0", "tex3", "tex4")

	subscribeSeconds := 20
	a.Images = append(a.Images, AnimeticImage{
		Start:  time,
		End:    time + subscribeSeconds,
		Status: "logo1",
	})
	a.Sounds = append(a.Sounds, AnimeticSound{
		Filepath:      titleSoundFile,
		SoundLength:   titleSoundLength,
		NoSoundLength: float64(subscribeSeconds) - titleSoundLength,
	})
	a.Captions = append(a.Captions, AnimeticCaption{
		Lang:  "en",
		Text:  "{Please subscribe!}",
		Start: time,
		End:   time + subscribeSeconds,
		Kind:  CaptionKindSubscribe,
	})
	return time
}

func (a *FormulaAnimetic) appendFrames(problemNo int, status string, plus int, soundKind SoundKind, captionKind CaptionKind, time int) int {
	seconds := plus
	a.Images = append(a.Images, AnimeticImage{
		Start:     time,
		End:       time + seconds,
		ProblemNo: problemNo,
		Status:    status,
	})
	if soundKind == SoundKindAnswer {
		a.Sounds = append(a.Sounds, AnimeticSound{
			Filepath:      answerSoundFile,
			SoundLength:   answerSoundLength,
			NoSoundLength: float64(seconds) - answerSoundLength,
		})
	} else if soundKind == SoundKindNoSound {
		a.Sounds = append(a.Sounds, AnimeticSound{
			Filepath:      noSoundFile,
			SoundLength:   noSoundLength,
			NoSoundLength: float64(seconds) - noSoundLength,
		})
	}
	a.Captions = append(a.Captions, AnimeticCaption{
		Lang:      "",
		Text:      "",
		Ruby:      "",
		Start:     time,
		End:       time + seconds,
		ProblemNo: problemNo,
		Kind:      captionKind,
	})
	return time + seconds
}
