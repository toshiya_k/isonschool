package yout

import (
	"bytes"
	"fmt"
	"isonschool/cli/ison"
	"isonschool/cli/translation"
	"math"
	"sort"
)

// Caption は，キャプションを表します。
type Caption struct {
	ID            string    // Youtube 上でのID
	Lang          ison.Lang // 言語
	CaptionKey    string
	CaptionName   string // Youtubeで表示されるキャプション名
	YoutubeSynced bool   // アップデートが必要か
}

// CaptionSet は，ビデオのすべてのキャプションです。
type CaptionSet struct {
	VideoFile ison.Filename // ビデオファイル名
	Captions  Captions
}

// UpdateCaption は，Captionを更新します。
func (c *CaptionSet) UpdateCaption(caption Caption, setYoutubeSyncedFalse bool) {
	for i, cp := range c.Captions {
		if cp.Lang == caption.Lang && cp.CaptionKey == caption.CaptionKey {
			if cp.CaptionName != caption.CaptionName {
				c.Captions[i].CaptionName = caption.CaptionName
				c.Captions[i].YoutubeSynced = false
			}
			if setYoutubeSyncedFalse {
				c.Captions[i].YoutubeSynced = false
			}
			return
		}
	}
	c.Captions = append(c.Captions, caption)
}

// Sort は，ソートします。。
func (c *CaptionSet) Sort() {
	sort.Sort(c.Captions)
}

// Captions は，Captionのスライスです。
type Captions []Caption

func (c Captions) Len() int           { return len(c) }
func (c Captions) Swap(i, j int)      { c[i], c[j] = c[j], c[i] }
func (c Captions) Less(i, j int) bool { return c[i].Lang < c[j].Lang }

// MakeCaption は，キャプションを作ります。
func MakeCaption(category ison.Category, condSerieName string, condNo int, condExName string) {
	var translator = translation.NewTranslator(category)
	for _, sentenceFile := range category.JSONGrammarFiles() {
		var sentence Sentence
		ison.ReadJSON(&sentence, sentenceFile)
		if condSerieName != "" && condSerieName != sentence.SerieName {
			continue
		}
		if condNo > 0 && condNo != sentence.No {
			continue
		}
		for _, exercise := range sentence.Exercises {
			if condExName != "" && exercise.Name != condExName {
				continue
			}
			var serie Serie
			ison.ReadJSON(&serie, category.JSONSerieFile(sentence.SerieName, exercise.Name))
			makeCaptionFiles(category, serie, exercise, translator)
		}
	}
	translator.CheckNeedsTranslation()
}

// MakeCaptionFiles は，キャプションファイルを作成します。
// SubViewer 形式。(.sbv)
//
// 0:00:00.599,0:00:04.160
// こんにちは
//
// 0:00:10.800,0:00:16.300
// さようなら
func makeCaptionFiles(category ison.Category, serie Serie, exercise Exercise, translator *translation.Translator) {
	jsonfile := category.JSONCaptionSetFile(exercise.VideoFile)
	var captionSet CaptionSet
	ison.ReadJSON(&captionSet, jsonfile)
	captionSet.VideoFile = exercise.VideoFile
	for _, ck := range serie.CaptionKeyNames {
		if ck.CaptionKey == CaptionKeyRuby {
			var b bytes.Buffer
			for _, c := range exercise.AnimeticCaptions {
				if c.Kind == CaptionKindOriginal || c.Kind == CaptionKindChange || c.Kind == CaptionKindAnswer {
					b.WriteString(toTimeString(c.Start) + "," + toTimeString(c.End) + "\n")
					b.WriteString(c.Ruby + "\n\n")
				}
			}
			var caption = Caption{
				Lang:        serie.AudioLang,
				CaptionKey:  ck.CaptionKey,
				CaptionName: translator.Translate(ck.CaptionName, serie.AudioLang),
			}
			var setYoutubeSyncedFalse bool
			sbvfile := category.BinaryCaptionSBVFile(exercise.VideoFile, ck.CaptionKey, serie.AudioLang)
			sbv := ison.ReadFile(sbvfile)
			if bytes.Compare(sbv, b.Bytes()) != 0 {
				setYoutubeSyncedFalse = true
			}
			ison.WriteFile(sbvfile, b.Bytes())
			captionSet.UpdateCaption(caption, setYoutubeSyncedFalse)
		} else if ck.CaptionKey == CaptionKeyTranslation {
			for _, lang := range ison.VideoLocalizationLangs {
				var b bytes.Buffer
				for _, c := range exercise.AnimeticCaptions {
					if c.Kind == CaptionKindTitle || c.Kind == CaptionKindSubscribe || c.Kind == CaptionKindAnswer {
						b.WriteString(toTimeString(c.Start) + "," + toTimeString(c.End) + "\n")
						b.WriteString(translator.Translate(c.Text, lang) + "\n\n")
					}
					if c.Kind == CaptionKindOriginal {
						translator.Translate(c.Text, lang) // 必要な翻訳をあらかじめ生成するため.
					}
				}
				var caption = Caption{
					Lang:        lang,
					CaptionKey:  ck.CaptionKey,
					CaptionName: translator.Translate(ck.CaptionName, lang),
				}
				var setYoutubeSyncedFalse bool
				sbvfile := category.BinaryCaptionSBVFile(exercise.VideoFile, ck.CaptionKey, lang)
				sbv := ison.ReadFile(sbvfile)
				if bytes.Compare(sbv, b.Bytes()) != 0 {
					setYoutubeSyncedFalse = true
				}
				if translator.HasError() {
					continue
				}
				ison.WriteFile(sbvfile, b.Bytes())
				captionSet.UpdateCaption(caption, setYoutubeSyncedFalse)
			}
		}
	}
	ison.WriteJSON(&captionSet, jsonfile)
}

func toSimpleTimeString(seconds int) string {
	minutes := int(math.Floor(float64(seconds) / float64(60)))
	seconds = seconds - minutes*60
	return fmt.Sprintf("%d:%02d", minutes, seconds)
}

func toTimeString(seconds int) string {
	minutes := int(math.Floor(float64(seconds) / float64(60)))
	seconds = seconds - minutes*60
	return fmt.Sprintf("0:%02d:%02d.000", minutes, seconds)
}
