package yout

import (
	"isonschool/cli/ison"
	"isonschool/cli/translation"
	"log"
	"sort"
	"strconv"
	"strings"
	"unicode/utf8"
)

// VideoUploaded returns if the video is already uploaded.
func VideoUploaded(category *ison.Category, videoFile ison.Filename) bool {
	if videoFile.Empty() {
		return false
	}
	var video Video
	ison.ReadJSON(&video, category.JSONVideoFile(videoFile))
	return video.ID != ""
}

// Video は，ビデオです。
type Video struct {
	File              ison.Filename // ビデオのファイル(mp4, webmなど)
	ID                string        // Youtube上でのID
	AudioLang         ison.Lang     // 音声言語
	DefaultLang       ison.Lang     // タイトルの言語
	Title             string        // タイトル
	Description       string        // 説明
	VideoTags         string        // タグ 15個まで
	YoutubeSynced     bool          // これが false のときは，アップデートする
	ThumbnailUploaded bool          // これが false のときは，アップロードする。
	Localizations     Localizations // 多言語
	HatenaPermalink   string        `json:"HatenaPermalink,omitempty"`
}

// Update は，テキストビデオのデータにアップデートします。
func (v *Video) Update(category ison.Category, nv TxtVideo, txtFile TxtFile, translator *translation.Translator) {
	if n, ok := nv.TxtLocalizationMap[""]; ok {
		for _, lang := range ison.VideoLocalizationLangs {
			_, aok := nv.TxtLocalizationMap[lang]
			if !aok {
				nv.TxtLocalizationMap[lang] = n
			}
		}
	}
	delete(nv.TxtLocalizationMap, "")
	_, serienos := txtFile.File.SerieNameAndNos()
	v.File = txtFile.File
	if v.DefaultLang != txtFile.Lang {
		v.DefaultLang = txtFile.Lang
		v.YoutubeSynced = false
	}
	v.AudioLang = nv.AudioLang
	title := replace(nv.TxtLocalizationMap[txtFile.Lang].Title, serienos, translator, txtFile.Lang)
	if utf8.RuneCountInString(title) > 100 {
		log.Panicf("titleが長すぎます。%s %s %s", title, txtFile.File, txtFile.Lang)
	}
	if v.Title != title {
		v.Title = title
		v.YoutubeSynced = false
	}
	description := replace(nv.TxtLocalizationMap[txtFile.Lang].Description, serienos, translator, txtFile.Lang)
	if v.Description != description {
		v.Description = description
		v.YoutubeSynced = false
	}
	tags := replace(nv.TxtLocalizationMap[txtFile.Lang].Tags, serienos, translator, txtFile.Lang)
	if v.VideoTags != tags {
		v.VideoTags = tags
		v.YoutubeSynced = false
	}
	for lang, loc := range nv.TxtLocalizationMap {
		title := replace(loc.Title, serienos, translator, lang)
		if utf8.RuneCountInString(title) > 100 {
			log.Panicf("titleが長すぎます。%s %s %s", title, txtFile.File, lang)
		}
		description := replace(loc.Description, serienos, translator, lang)
		updated := v.Localizations.Update(lang, title, description)
		if updated {
			v.YoutubeSynced = false
		}
	}
}

func replace(s string, serienos []string, translator *translation.Translator, lang ison.Lang) string {
	for i, no := range serienos {
		s = strings.ReplaceAll(s, "["+strconv.Itoa(i+1)+"]", no)
	}
	return translator.Translate(s, lang)
}

// Sort は，ソートします。
func (v *Video) Sort() {
	sort.Sort(v.Localizations)
}

// Localization は，それぞれの言語用のビデオです。
type Localization struct {
	Lang            ison.Lang // 言語
	Title           string    // タイトル
	Description     string    // 説明
	TwitterStatusID int64     `json:"TwitterStatusID,omitempty"`
}

// Localizations は，Lang でソートします。
type Localizations []Localization

func (l Localizations) Len() int           { return len(l) }
func (l Localizations) Swap(i, j int)      { l[i], l[j] = l[j], l[i] }
func (l Localizations) Less(i, j int) bool { return l[i].Lang < l[j].Lang }

// Update は，lang をもとにアップデートし，必要ならインサートします。
func (l *Localizations) Update(lang ison.Lang, title, description string) bool {
	var updated bool
	for i, a := range *l {
		if a.Lang == lang {
			if (*l)[i].Title != title {
				(*l)[i].Title = title
				updated = true
			}
			if (*l)[i].Description != description {
				(*l)[i].Description = description
				updated = true
			}
			return updated
		}
	}
	*l = append(*l, Localization{Lang: lang, Title: title, Description: description})
	return true
}

// MergeTxtVideoFiles は，新しいビデオのデータをマージします。
func MergeTxtVideoFiles(category ison.Category) {
	translator := translation.NewTranslator(category)
	var txtVideos []TxtVideo
	for _, file := range category.TxtVideoFiles() {
		m := ison.NewTxtMarshallerFromFile(file)
		txtVideos = append(txtVideos, ParseTxtVideo(m)...)
	}
	//fmt.Printf("%+v", txtVideos)
	m := make(map[string]bool)
	for _, txtVideo := range txtVideos {
		for _, txtFile := range txtVideo.TxtFiles {
			if m[txtFile.File.String()] {
				log.Panicf("%s が重複して定義されています。", txtFile.File)
			}
			m[txtFile.File.String()] = true
			f := category.JSONVideoFile(txtFile.File)
			var v Video
			ison.ReadJSON(&v, f)
			v.Update(category, txtVideo, txtFile, translator)
			if !translator.HasError() {
				ison.WriteJSON(&v, f)
			}
		}
	}
}

// ParseTxtVideo は，テキストビデオファイルをパースします。
func ParseTxtVideo(m *ison.TxtMarshaller) []TxtVideo {
	m.Add("audiolang", func(d interface{}, k, v string) {
		d.(*TxtVideo).AudioLang = ison.Lang(v)
	})
	m.Add("file", func(d interface{}, k, v string) {
		d.(*TxtVideo).TxtFiles = append(d.(*TxtVideo).TxtFiles, TxtFile{Lang: ison.Lang(k), File: ison.ToFilename(v)})
	})
	m.Add("title", func(d interface{}, k, v string) {
		l := d.(*TxtVideo).TxtLocalizationMap[ison.Lang(k)]
		l.Title = v
		d.(*TxtVideo).TxtLocalizationMap[ison.Lang(k)] = l
	})
	m.Add("description", func(d interface{}, k, v string) {
		l := d.(*TxtVideo).TxtLocalizationMap[ison.Lang(k)]
		l.Description = v
		d.(*TxtVideo).TxtLocalizationMap[ison.Lang(k)] = l
	})
	m.Add("tags", func(d interface{}, k, v string) {
		l := d.(*TxtVideo).TxtLocalizationMap[ison.Lang(k)]
		l.Tags = v
		d.(*TxtVideo).TxtLocalizationMap[ison.Lang(k)] = l
	})
	var videos []TxtVideo
	var success, isEOF bool
	for !isEOF {
		var d = TxtVideo{TxtLocalizationMap: make(map[ison.Lang]TxtLocalization)}
		success, isEOF = m.Unmarshal(&d)
		if success {
			videos = append(videos, d)
		}
	}
	return videos
}

// TxtVideo は，テキストビデオファイルの内容です。
type TxtVideo struct {
	TxtFiles           []TxtFile
	TxtLocalizationMap map[ison.Lang]TxtLocalization
	AudioLang          ison.Lang
}

// TxtFile は，ファイルと言語の組み合わせです。
type TxtFile struct {
	File ison.Filename
	Lang ison.Lang
}

// TxtLocalization は，テキストローカライゼーションです。
type TxtLocalization struct {
	Lang        ison.Lang
	Title       string
	Description string
	Tags        string
}
