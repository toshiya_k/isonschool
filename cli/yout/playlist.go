package yout

import (
	"isonschool/cli/ison"
	"isonschool/cli/translation"
	"log"
	"sort"
	"strings"
)

// Playlist は，プレイリストです。
type Playlist struct {
	Name          string        // ファイル名は，Name + ".json"
	ID            string        // Youtube 上でのプレイリストID
	DefaultLang   ison.Lang     // 言語
	Title         string        // タイトル
	Description   string        // 説明
	Localizations Localizations // 多言語
	PlaylistTags  string        // タグ
	Updated       bool
	PlaylistItems PlaylistItems // プレイリストの中のアイテム
}

// Sort は，Localizations をソートします。
func (p *Playlist) Sort() {
	sort.Sort(p.Localizations)
}

// Update は，更新します。
func (p *Playlist) Update(category ison.Category, t TxtPlaylist, lang ison.Lang) {
	translator := translation.NewTranslator(category)
	p.Name = t.Name
	p.DefaultLang = lang
	title := translator.Translate(t.Title, p.DefaultLang)
	if p.Title != title {
		p.Title = title
		p.Updated = false
	}
	description := translator.Translate(t.Description, p.DefaultLang)
	if p.Description != description {
		p.Description = description
		p.Updated = false
	}
	tags := translator.Translate(t.Tags, p.DefaultLang)
	if p.PlaylistTags != tags {
		p.PlaylistTags = tags
		p.Updated = false
	}
	if lang.IsEnglish() || t.MakeLocalizations {
		for _, lang := range ison.DisplayLocalizationLangs {
			title := translator.Translate(t.Title, lang)
			description := translator.Translate(t.Description, lang)
			p.Localizations.Update(lang, title, description)
		}
	}
	for ts, te := range t.SerieExs {
		for _, tf := range category.JSONVideoSerieFiles(ts) {
			tff := tf.Base()
			if te != "" {
				if _, e := tff.SerieNameAndExerciseName(); te != e {
					continue
				}
			}
			// Files にあって， PlaylistItems にないものを追加する。
			if _, ok := p.PlaylistItems.Get(tff.TrimExtJSON()); !ok {
				var video Video
				ison.ReadJSON(&video, category.JSONVideoFile(tff))
				if video.File.Empty() {
					continue
				}
				p.PlaylistItems = append(p.PlaylistItems, PlaylistItem{ID: "", VideoID: video.ID, VideoFile: video.File})
			}
		}
	}
	for _, tf := range t.Files {
		// Files にあって， PlaylistItems にないものを追加する。
		if _, ok := p.PlaylistItems.Get(tf); !ok {
			var video Video
			ison.ReadJSON(&video, category.JSONVideoFile(tf))
			if video.File.Empty() {
				continue
			}
			p.PlaylistItems = append(p.PlaylistItems, PlaylistItem{ID: "", VideoID: video.ID, VideoFile: video.File})
		}
	}
	translator.CheckNeedsTranslation()
}

// PlaylistItem は，プレイリストアイテムです。
type PlaylistItem struct {
	ID        string        // Youtube上でのプレイリストアイテムID。空文字の場合は， insert が必要。
	VideoID   string        // Youtube上でのビデオID
	VideoFile ison.Filename // アップロードしたビデオファイル名
	//Remove    bool   // 削除が必要
}

// PlaylistItems は，PlaylistItem のスライスです。
type PlaylistItems []PlaylistItem

// Get は， VideoFile が videoFile である PlaylistItem を返します。
func (p PlaylistItems) Get(videoFile ison.Filename) (PlaylistItem, bool) {
	for _, item := range p {
		if item.VideoFile == videoFile {
			return item, true
		}
	}
	return PlaylistItem{}, false
}

// MergeTxtPlaylists は，テキストプレイリストをJSONにマージします。
func MergeTxtPlaylists(category ison.Category) {
	var txtPls []TxtPlaylist
	for _, file := range category.TxtPlaylistFiles() {
		m := ison.NewTxtMarshallerFromFile(file)
		txtPls = append(txtPls, ParseTxtPlaylist(m)...)
	}
	var m = make(map[string]bool)
	for _, txtPl := range txtPls {
		if m[txtPl.Name] {
			log.Panicf("%s が重複して定義されています。", txtPl.Name)
		}
		m[txtPl.Name] = true
		for _, l := range txtPl.Langs {
			f := category.JSONPlaylistFile(txtPl.Name, l)
			var pl Playlist
			ison.ReadJSON(&pl, f)
			pl.Update(category, txtPl, l)
			ison.WriteJSON(&pl, f)
		}
	}
}

// ParseTxtPlaylist は，パースします。
func ParseTxtPlaylist(m *ison.TxtMarshaller) []TxtPlaylist {
	m.Add("playlist", func(d interface{}, k, v string) { d.(*TxtPlaylist).Name = v })
	m.Add("langs", func(d interface{}, k, v string) {
		var langs []ison.Lang
		for _, l := range strings.Split(v, ",") {
			if l == "" {
				continue
			} else if l == "all" {
				langs = append(langs, ison.DisplayLocalizationLangs...)
			} else {
				langs = append(langs, ison.ToLang(l))
			}
		}
		d.(*TxtPlaylist).Langs = langs
	})
	m.Add("title", func(d interface{}, k, v string) { d.(*TxtPlaylist).Title = v })
	m.Add("description", func(d interface{}, k, v string) { d.(*TxtPlaylist).Description = v })
	m.Add("tags", func(d interface{}, k, v string) { d.(*TxtPlaylist).Tags = v })
	m.Add("makelocalizations", func(d interface{}, k, v string) { d.(*TxtPlaylist).MakeLocalizations = true })
	m.Add("serie", func(d interface{}, k, v string) { d.(*TxtPlaylist).SerieExs[k] = v })
	m.Add("file", func(d interface{}, k, v string) {
		d.(*TxtPlaylist).Files = append(d.(*TxtPlaylist).Files, ison.Filename(v))
	})
	var playlists []TxtPlaylist
	var success, isEOF bool
	for !isEOF {
		var d = TxtPlaylist{SerieExs: make(map[string]string)}
		success, isEOF = m.Unmarshal(&d)
		if success {
			playlists = append(playlists, d)
		}
	}
	return playlists
}

// TxtPlaylist は，プレイリストテキストの内容です。
type TxtPlaylist struct {
	Name              string
	Langs             []ison.Lang
	Title             string
	Description       string
	Tags              string
	SerieExs          map[string]string
	Files             []ison.Filename
	MakeLocalizations bool
}
