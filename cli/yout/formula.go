package yout

import (
	"isonschool/cli/ison"
	"isonschool/cli/translation"
	"log"
	"sort"
	"strconv"
	"strings"
)

// Formula is a math problem unit
type Formula struct {
	SerieName          string
	No                 int
	VideoTitle         string
	YoutubeDescription string
	YoutubeTitle       string
	Time               int
	Explanations       []string
	Equations          []string
	FormulaExercises   []FormulaExercise
}

// Thumbnail returns a thumbnail string
func (f *Formula) Thumbnail() string {
	return f.VideoTitle
}

type stringPair struct {
	key, value string
}

type stringPairs []stringPair

func (s stringPairs) Sort() {
	sort.Sort(s)
}

func (s stringPairs) Len() int           { return len(s) }
func (s stringPairs) Less(i, j int) bool { return s[i].key < s[j].key }
func (s stringPairs) Swap(i, j int)      { s[i], s[j] = s[j], s[i] }
func (s *stringPairs) Add(key, value string) {
	for i, p := range *s {
		if p.key == key {
			(*s)[i].value = (*s)[i].value + "\n" + value
			return
		}
	}
	*s = append(*s, stringPair{key: key, value: value})
}

// FormulaExercise is a formula exercise.
type FormulaExercise struct {
	Name             string
	VideoFile        ison.Filename
	AnimeticCaptions []AnimeticCaption
}

// Update is to update formula
func (f *Formula) Update(category ison.Category, t TxtFormula) {
	if f.SerieName == "" {
		f.SerieName = t.SerieName
	} else if f.SerieName != t.SerieName {
		log.Fatalf("SerieName is not the same. %s %s", f.SerieName, t.SerieName)
	}
	if f.No == 0 {
		f.No = t.No
	} else if f.No != t.No {
		log.Fatalf("No is not the same. %d %d", f.No, t.No)
	}
	f.VideoTitle = t.VideoTitle
	f.YoutubeDescription = t.YoutubeDescription
	f.YoutubeTitle = t.YoutubeTitle
	f.Time = t.Time
	t.Explanations.Sort()
	f.Explanations = make([]string, len(t.Explanations))
	for i, e := range t.Explanations {
		f.Explanations[i] = e.value
	}
	f.Equations = make([]string, len(t.Equations))
	for i, e := range t.Equations {
		f.Equations[i] = e.value
	}
}

// AddFormulaExercise returns true if adds this exercise name.
func (f *Formula) AddFormulaExercise(exName string) (FormulaExercise, bool) {
	for _, e := range f.FormulaExercises {
		if e.Name == exName {
			return e, false
		}
	}
	r := FormulaExercise{Name: exName}
	f.FormulaExercises = append(f.FormulaExercises, r)
	return r, true
}

// TxtFormula is a formula
type TxtFormula struct {
	SerieName          string
	No                 int
	VideoTitle         string
	YoutubeDescription string
	YoutubeTitle       string
	Time               int
	Explanations       stringPairs
	Equations          stringPairs
}

// ParseTxtFormula parses formula text file
func ParseTxtFormula(m *ison.TxtMarshaller) []TxtFormula {
	m.Add("seriename", func(d interface{}, k, v string) {
		d.(*TxtFormula).SerieName = v
	})
	m.Add("no", func(d interface{}, k, v string) {
		d.(*TxtFormula).No = ison.Atoi(v)
	})
	m.Add("videotitle", func(d interface{}, k, v string) {
		d.(*TxtFormula).VideoTitle = v
	})
	m.Add("description", func(d interface{}, k, v string) {
		d.(*TxtFormula).YoutubeDescription = v
	})
	m.Add("title", func(d interface{}, k, v string) {
		d.(*TxtFormula).YoutubeTitle = v
	})
	m.Add("time", func(d interface{}, k, v string) {
		d.(*TxtFormula).Time = ison.Atoi(v)
	})
	m.Add("explanation", func(d interface{}, k, v string) {
		d.(*TxtFormula).Explanations.Add(k, v)
	})
	m.Add("equation", func(d interface{}, k, v string) {
		d.(*TxtFormula).Equations.Add(k, v)
	})
	var formulas []TxtFormula
	var success, isEOF bool
	for !isEOF {
		var d = TxtFormula{}
		success, isEOF = m.Unmarshal(&d)
		if success {
			formulas = append(formulas, d)
		}
	}
	return formulas
}

// MergeTxtFormulaFiles は，テキストファイルを読み込んで，JSONデータにマージします。
func MergeTxtFormulaFiles(category ison.Category, condSerieName string) {
	var txtFormulas []TxtFormula
	for _, file := range category.TxtFormulaFiles() {
		m := ison.NewTxtMarshallerFromFile(file)
		txtFormulas = append(txtFormulas, ParseTxtFormula(m)...)
	}
	m := make(map[string]bool)
	for _, txtFormula := range txtFormulas {
		if condSerieName != "" && condSerieName != txtFormula.SerieName {
			continue
		}
		if m[txtFormula.SerieName+strconv.Itoa(txtFormula.No)] {
			log.Panicf("%s が重複して定義されています。", txtFormula.SerieName)
		}
		m[txtFormula.SerieName+strconv.Itoa(txtFormula.No)] = true
		f := category.JSONFormulaFile(txtFormula.SerieName, txtFormula.No)
		var v Formula
		ison.ReadJSON(&v, f)
		v.Update(category, txtFormula)
		ison.WriteJSON(&v, f)
	}
}

// MakeFormulaImages makes images
func MakeFormulaImages(category ison.Category, condSerieName string, condNo int) {
	category.RemoveAllBinaryImageFiles()
	//translator := translation.NewTranslator(category)
	for _, file := range category.JSONFormulaFiles() {
		var formula Formula
		ison.ReadJSON(&formula, file)
		if condSerieName != "" && condSerieName != formula.SerieName {
			continue
		}
		if condNo > 0 && condNo != formula.No {
			continue
		}
		for _, serieFile := range category.JSONSerieFiles(formula.SerieName) {
			var serie Serie
			ison.ReadJSON(&serie, serieFile)
			e, b := formula.AddFormulaExercise(serie.ExerciseName)
			if b {
				ison.WriteJSON(&formula, file)
			}

			if VideoUploaded(&category, e.VideoFile) {
				continue
			}
			palette := serie.Palettes[(formula.No-1+len(serie.Palettes))%len(serie.Palettes)]

			drawer := NewDrawer(palette, category.FontFile(serie.TitleFont), category.FontFile(serie.SubtitleFont), category.FontFile(serie.ProblemFont), category.FontFile(serie.RubyFont))

			ison.WriteFile(
				category.BinaryImageFile(formula.SerieName, formula.No, serie.ExerciseName, 0, "title"),
				drawer.DrawIsonSchoolTitle(),
			)
			for i := 0; i <= 3; i++ {
				ison.WriteFile(
					category.BinaryImageFile(formula.SerieName, formula.No, serie.ExerciseName, 0, "logo"+strconv.Itoa(i)),
					drawer.DrawLogo(i),
				)
			}
			expPngs, pngs := MakePngImages(formula.Explanations, formula.Equations, palette)
			for i, p := range expPngs {
				ison.WriteFile(category.BinaryImageFile(formula.SerieName, formula.No, serie.ExerciseName, 0, "exp"+strconv.Itoa(i)), p)
			}
			for i, ps := range pngs {
				for j, p := range ps {
					ison.WriteFile(category.BinaryImageFile(formula.SerieName, formula.No, serie.ExerciseName, i, "tex"+strconv.Itoa(j)), p)
				}
			}
		}
	}
}

// MakeJSONVideoFilesFromFormula は，JSON Video File を作ります。
func MakeJSONVideoFilesFromFormula(category ison.Category, consSerieName string, condNo int, condExName string) {
	translator := translation.NewTranslator(category)
	for _, file := range category.JSONFormulaFiles() {
		var formula Formula
		ison.ReadJSON(&formula, file)
		if consSerieName != "" && consSerieName != formula.SerieName {
			continue
		}
		if condNo > 0 && condNo != formula.No {
			continue
		}
		for _, exercise := range formula.FormulaExercises {
			if condExName != "" && exercise.Name != condExName {
				continue
			}
			var serie Serie
			ison.ReadJSON(&serie, category.JSONSerieFile(formula.SerieName, exercise.Name))
			videoFile := formulaVideoFilename(formula, exercise)
			videoFilepath := category.JSONVideoFile(videoFile)
			var video Video
			ison.ReadJSON(&video, videoFilepath)
			var txtVideo TxtVideo
			txtVideo.AudioLang = serie.AudioLang
			txtVideo.TxtFiles = append(txtVideo.TxtFiles, TxtFile{File: videoFile, Lang: serie.YoutubeLang(formula.No)})
			txtVideo.TxtLocalizationMap = make(map[ison.Lang]TxtLocalization)
			for _, lang := range ison.VideoLocalizationLangs {
				txtVideo.TxtLocalizationMap[lang] = TxtLocalization{
					Lang:        lang,
					Title:       replaceFormula(serie.YoutubeTitle, serie, formula, exercise, category, translator, lang),
					Description: replaceFormula(serie.YoutubeDescription, serie, formula, exercise, category, translator, lang),
					Tags:        replaceFormula(serie.YoutubeTags, serie, formula, exercise, category, translator, lang),
				}
			}
			video.Update(category, txtVideo, txtVideo.TxtFiles[0], translator)
			if !translator.HasError() {
				ison.WriteJSON(&video, videoFilepath)
			}
		}
		ison.WriteJSON(&formula, file)
	}
	translator.CheckNeedsTranslation()
}

func replaceFormula(s string, serie Serie, formula Formula, exercise FormulaExercise, category ison.Category, translator *translation.Translator, lang ison.Lang) string {
	s = strings.ReplaceAll(s, "[:VideoTitle:]", formula.VideoTitle)
	s = strings.ReplaceAll(s, "[:Title:]", formula.YoutubeTitle)
	s = strings.ReplaceAll(s, "[:Description:]", formula.YoutubeDescription)
	return s
}

// MakeFormulaThumbnailImage は，サムネール画像ファイルを作成します。
func MakeFormulaThumbnailImage(category ison.Category, condSerieName string, condNo int, condExName string, remake bool) {
	translator := translation.NewTranslator(category)
	for _, file := range category.JSONFormulaFiles() {
		var formula Formula
		ison.ReadJSON(&formula, file)
		if condSerieName != "" && condSerieName != formula.SerieName {
			continue
		}
		if condNo > 0 && condNo != formula.No {
			continue
		}
		for _, e := range formula.FormulaExercises {
			if condExName != "" && e.Name != condExName {
				continue
			}
			if e.VideoFile == "" {
				continue
			}
			var video Video
			ison.ReadJSON(&video, category.JSONVideoFile(e.VideoFile))
			if !remake && video.ThumbnailUploaded {
				continue
			}
			var serie Serie
			ison.ReadJSON(&serie, category.JSONSerieFile(formula.SerieName, e.Name))

			drawer := NewDrawer(serie.Palettes[(formula.No-1+len(serie.Palettes))%len(serie.Palettes)], category.FontFile(serie.TitleFont), category.FontFile(serie.SubtitleFont), category.FontFile(serie.ProblemFont), category.FontFile(serie.RubyFont))

			ison.WriteFile(
				category.BinaryThumbnailFile(e.VideoFile),
				drawer.DrawFormulaThumbnail(formula.Thumbnail()),
			)
		}
	}
	translator.CheckNeedsTranslation()
}
