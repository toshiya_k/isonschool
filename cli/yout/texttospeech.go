package yout

import (
	"context"
	"fmt"
	"io"
	"io/ioutil"
	"isonschool/cli/ison"
	"log"
	"os"
	"os/exec"
	"strings"

	texttospeech "cloud.google.com/go/texttospeech/apiv1"
	texttospeechpb "google.golang.org/genproto/googleapis/cloud/texttospeech/v1"
)

// DownloadTextToSpeechMP3 は，Google Cloud Text To Speech API を使って MP3 をダウンロードします。
func DownloadTextToSpeechMP3(category ison.Category, condSerieName string, condNo int, condExName string) {
	for _, file := range category.JSONGrammarFiles() {
		var sentence Sentence
		ison.ReadJSON(&sentence, file)
		if condSerieName != "" && condSerieName != sentence.SerieName {
			continue
		}
		if condNo > 0 && condNo != sentence.No {
			continue
		}
		for _, e := range sentence.Exercises {
			if condExName != "" && e.Name != condExName {
				continue
			}
			var serie Serie
			ison.ReadJSON(&serie, category.JSONSerieFile(sentence.SerieName, e.Name))
			for _, p := range e.Problems {
				for _, l := range p.OriginalLines(category, serie, sentence) {
					create(l.Text, category, serie.VoiceName(l.PersonName, l.Gender))
				}
				for _, l := range p.AnswerLines(category, serie, sentence) {
					create(l.Text, category, serie.VoiceName(l.PersonName, l.Gender))
				}
				for _, l := range p.ChangeLines(category, sentence) {
					create(l.Text, category, serie.VoiceName(l.PersonName, l.Gender))
				}
			}
		}
	}
}

func create(text string, category ison.Category, voiceName string) {
	if text == "" {
		return
	}
	filename := category.BinaryAudioFile(text, voiceName)
	_, err := os.Stat(string(filename))
	if !os.IsNotExist(err) {
		return // ファイルがすでに存在する場合には再作成しない。
	}
	ctx := context.Background()

	client, err := texttospeech.NewClient(ctx)
	if err != nil {
		log.Fatal(err)
	}

	req := texttospeechpb.SynthesizeSpeechRequest{
		Input: &texttospeechpb.SynthesisInput{
			InputSource: &texttospeechpb.SynthesisInput_Text{Text: text},
		},
		Voice: &texttospeechpb.VoiceSelectionParams{
			LanguageCode: langCodeFromVoiceName(voiceName),
			Name:         voiceName,
		},
		AudioConfig: &texttospeechpb.AudioConfig{
			AudioEncoding: texttospeechpb.AudioEncoding_MP3,
		},
	}

	resp, err := client.SynthesizeSpeech(ctx, &req)
	if err != nil {
		log.Fatalf("%v text:%s, req:%+v, resp:%+v", err, text, req, resp)
	}

	err = ioutil.WriteFile(string(filename), resp.AudioContent, 0644)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Printf("afplay %v\n", filename)
	cmd := exec.Command("afplay", filename.String())
	cmd.Run()
}

func langCodeFromVoiceName(voiceName string) string {
	a := strings.Split(voiceName, "-")
	return a[0] + "-" + a[1]
}

// ListVoices lists the available text to speech voices.
func ListVoices(w io.Writer) error {
	ctx := context.Background()

	client, err := texttospeech.NewClient(ctx)
	if err != nil {
		return err
	}

	// Performs the list voices request.
	resp, err := client.ListVoices(ctx, &texttospeechpb.ListVoicesRequest{})
	if err != nil {
		return err
	}

	for _, voice := range resp.Voices {
		// Display the voice's name. Example: tpc-vocoded
		fmt.Fprintf(w, "Name: %v\n", voice.Name)

		// Display the supported language codes for this voice. Example: "en-US"
		for _, languageCode := range voice.LanguageCodes {
			fmt.Fprintf(w, "  Supported language: %v\n", languageCode)
		}

		// Display the SSML Voice Gender.
		fmt.Fprintf(w, "  SSML Voice Gender: %v\n", voice.SsmlGender.String())

		// Display the natural sample rate hertz for this voice. Example: 24000
		fmt.Fprintf(w, "  Natural Sample Rate Hertz: %v\n",
			voice.NaturalSampleRateHertz)
	}

	return nil
}
